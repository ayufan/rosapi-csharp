using System;
using System.Net;
using RouterOS.API;
using RouterOS.DataTypes;
using RouterOS.Serialization;

namespace RouterOS
{
	[RosObject("certificate", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Certificate : RosItemObject<Certificate>
	{
		// Whether the key is used for building or verifying certificate chains
		[RosValue("ca")]
		public Boolean? Ca { get; set; }
		
		// E-mail address
		[RosValue("email")]
		public String Email { get; set; }
		
		// Issuer of the certificate
		[RosValue("issuer")]
		public String Issuer { get; set; }
		
		// Reference name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Holder (subject) of the certificate
		[RosValue("subject")]
		public String Subject { get; set; }
		
		[RosValue("alias", IsDynamic=true)]
		public String Alias { get; set; }
		
		[RosValue("decrypted-private-key", IsDynamic=true)]
		public String DecryptedPrivateKey { get; set; }
		
		[RosValue("dsa", IsDynamic=true)]
		public String Dsa { get; set; }
		
		[RosValue("invalid-after", IsDynamic=true)]
		public DateTime? InvalidAfter { get; set; }
		
		[RosValue("invalid-before", IsDynamic=true)]
		public DateTime? InvalidBefore { get; set; }
		
		[RosValue("private-key", IsDynamic=true)]
		public String PrivateKey { get; set; }
		
		[RosValue("rsa", IsDynamic=true)]
		public String Rsa { get; set; }
		
		[RosValue("serial-number", IsDynamic=true)]
		public String SerialNumber { get; set; }
		
		public static Certificate Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			Certificate obj = new Certificate();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("driver", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Driver : RosItemObject<Driver>
	{
		// IO port base address
		[RosValue("io")]
		public Int32? Io { get; set; }
		
		// IRQ number
		[RosValue("irq")]
		public Int32? Irq { get; set; }
		
		public enum IsdnProtocolEnum
		{
			[RosValue("euro")]
			Euro,
			[RosValue("german")]
			German,
		}
		
		// ISDN line protocol
		[RosValue("isdn-protocol")]
		public IsdnProtocolEnum? IsdnProtocol { get; set; }
		
		// Shared Memory base address
		[RosValue("memory")]
		public Int32? Memory { get; set; }
		
		public enum NameEnum
		{
			[RosValue("3c509")]
			E3c509,
			[RosValue("lance")]
			Lance,
			[RosValue("ne2k-isa")]
			Ne2kIsa,
		}
		
		// Driver name
		[RosValue("name")]
		public NameEnum? Name { get; set; }
		
		[RosValue("driver", IsDynamic=true)]
		public String DriverName { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		public static Driver Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			Driver obj = new Driver();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("file", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class File : RosItemObject<File>
	{
		// Content of the file
		[RosValue("contents")]
		public String Contents { get; set; }
		
		[RosValue("creation-time", IsDynamic=true)]
		public DateTime? CreationTime { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("package-architecture", IsDynamic=true)]
		public String PackageArchitecture { get; set; }
		
		[RosValue("package-build-time", IsDynamic=true)]
		public DateTime? PackageBuildTime { get; set; }
		
		[RosValue("package-name", IsDynamic=true)]
		public String PackageName { get; set; }
		
		[RosValue("package-version", IsDynamic=true)]
		public String PackageVersion { get; set; }
		
		[RosValue("size", IsDynamic=true)]
		public Int64? Size { get; set; }
		
		[RosValue("type", IsDynamic=true)]
		public String Type { get; set; }
		
		public static File Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			File obj = new File();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface", CanSet=true, CanGet=true)]
	public class Interface : RosItemObject<Interface>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("drops", IsDynamic=true)]
		public Int32? Drops { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("errors", IsDynamic=true)]
		public Int32? Errors { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("slave", IsDynamic=true)]
		public String Slave { get; set; }
		
		[RosValue("type", IsDynamic=true)]
		public String Type { get; set; }
		
		public static Interface Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			Interface obj = new Interface();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface bonding", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceBonding : RosItemObject<InterfaceBonding>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Time in milliseconds for monitoring ARP requests
		[RosValue("arp-interval")]
		public TimeSpan? ArpInterval { get; set; }
		
		// IP addresses for monitoring
		[RosValue("arp-ip-targets")]
		public IPAddress[] ArpIpTargets { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Time period the interface is disabled  if a link failure has been detected
		[RosValue("down-delay")]
		public TimeSpan? DownDelay { get; set; }
		
		public enum LacpRateEnum
		{
			[RosValue("1sec")]
			E1sec,
			[RosValue("30secs")]
			E30secs,
		}
		
		// Link Aggregation Control Protocol rate specifies how often to exchange with LACPDUs between bonding peer
		[RosValue("lacp-rate")]
		public LacpRateEnum? LacpRate { get; set; }
		
		public enum LinkMonitoringEnum
		{
			[RosValue("arp")]
			Arp,
			[RosValue("mii-type1")]
			MiiType1,
			[RosValue("mii-type2")]
			MiiType2,
			[RosValue("none")]
			None,
		}
		
		// Method for monitoring the link
		[RosValue("link-monitoring")]
		public LinkMonitoringEnum? LinkMonitoring { get; set; }
		
		// Time in milliseconds for monitoring mii-type link
		[RosValue("mii-interval")]
		public TimeSpan? MiiInterval { get; set; }
		
		// Interface bonding mode
		[RosValue("mode")]
		public String Mode { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum MasterPortEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct PrimaryType 
                { 
                    private object value; 
                    public MasterPortEnum? MasterPort 
                { 
                    get { return value as MasterPortEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrimaryType(MasterPortEnum? value) 
                { 
                    return new PrimaryType() { value = value }; 
                }public RouterOS.Interface Slave 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrimaryType(RouterOS.Interface value) 
                { 
                    return new PrimaryType() { value = value }; 
                } 
                    public static PrimaryType Parse(string str, Connection conn)
                    {
                        
                try { return new PrimaryType() { value = TypeFormatter.ConvertFromString<MasterPortEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new PrimaryType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Slave that will be used in active-backup mode as active link
		[RosValue("primary")]
		public PrimaryType? Primary { get; set; }
		
		// Interfaces that are used in bonding
		[RosValue("slaves")]
		public RouterOS.Interface[] Slaves { get; set; }
		
		public enum TransmitHashPolicyEnum
		{
			[RosValue("layer-2")]
			Layer2,
			[RosValue("layer-2-and-3")]
			Layer2And3,
			[RosValue("layer-3-and-4")]
			Layer3And4,
		}
		
		[RosValue("transmit-hash-policy")]
		public TransmitHashPolicyEnum? TransmitHashPolicy { get; set; }
		
		// Time period the interface is disabled if a link has been brought up
		[RosValue("up-delay")]
		public TimeSpan? UpDelay { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceBonding Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceBonding obj = new InterfaceBonding();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface bridge", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceBridge : RosItemObject<InterfaceBridge>
	{
		[RosValue("admin-mac")]
		public MACAddress? AdminMac { get; set; }
		
		public enum AgeingTimeEnum
		{
			[RosValue("AgeingTime")]
			AgeingTime,
		}
		
		// Time the information about host will be kept in the the data base
		[RosValue("ageing-time")]
		public AgeingTimeEnum? AgeingTime { get; set; }
		
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		[RosValue("auto-mac")]
		public Boolean? AutoMac { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum ForwardDelayEnum
		{
			[RosValue("ForwardDelay")]
			ForwardDelay,
		}
		
		// Time which is spent in listening/learning state
		[RosValue("forward-delay")]
		public ForwardDelayEnum? ForwardDelay { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		public enum MaxMessageAgeEnum
		{
			[RosValue("MaxMessageAge")]
			MaxMessageAge,
		}
		
		// Time to remember Hello messages received from other bridges
		[RosValue("max-message-age")]
		public MaxMessageAgeEnum? MaxMessageAge { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Bridge name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Bridge interface priority
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		public enum ProtocolModeEnum
		{
			[RosValue("none")]
			None,
			[RosValue("rstp")]
			Rstp,
			[RosValue("stp")]
			Stp,
		}
		
		[RosValue("protocol-mode")]
		public ProtocolModeEnum? ProtocolMode { get; set; }
		
		[RosValue("transmit-hold-count")]
		public Int32? TransmitHoldCount { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceBridge Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceBridge obj = new InterfaceBridge();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface bridge calea", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceBridgeCalea : RosItemObject<InterfaceBridgeCalea>
	{
		public enum ActionEnum
		{
			[RosValue("sniff")]
			Sniff,
			[RosValue("sniff-pc")]
			SniffPc,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// ARP destination address
		[RosValue("arp-dst-address", CanUnset=true)]
		public String ArpDstAddress { get; set; }
		
		// ARP destination MAC address
		[RosValue("arp-dst-mac-address", CanUnset=true)]
		public String ArpDstMacAddress { get; set; }
		
		[RosValue("arp-gratuitous", CanUnset=true)]
		public Boolean? ArpGratuitous { get; set; }
		
		// The hardware type
		[RosValue("arp-hardware-type", CanUnset=true)]
		public String ArpHardwareType { get; set; }
		
		// The ARP opcode
		[RosValue("arp-opcode", CanUnset=true)]
		public String ArpOpcode { get; set; }
		
		// ARP packet type
		[RosValue("arp-packet-type", CanUnset=true)]
		public String ArpPacketType { get; set; }
		
		// ARP source address
		[RosValue("arp-src-address", CanUnset=true)]
		public String ArpSrcAddress { get; set; }
		
		// ARP source MAC address
		[RosValue("arp-src-mac-address", CanUnset=true)]
		public String ArpSrcMacAddress { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		// Chain name
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Destination address
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination MAC address
		[RosValue("dst-mac-address", CanUnset=true)]
		public String DstMacAddress { get; set; }
		
		// Destination port
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		// Bridge interface the packet is coming in
		[RosValue("in-bridge", CanUnset=true)]
		public String InBridge { get; set; }
		
		// Interface name packets are coming into the bridge
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		// IP protocol name
		[RosValue("ip-protocol", CanUnset=true)]
		public String IpProtocol { get; set; }
		
		// Restricts packet match rate to a given limit
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Defines the prefix to be printed before the logging information
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Protocol name  or number
		[RosValue("mac-protocol", CanUnset=true)]
		public String MacProtocol { get; set; }
		
		// Bridge interface the packet is going out
		[RosValue("out-bridge", CanUnset=true)]
		public String OutBridge { get; set; }
		
		// Interface name packets are leaving the bridge
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet type
		[RosValue("packet-type", CanUnset=true)]
		public String PacketType { get; set; }
		
		[RosValue("sniff-id")]
		public Int32? SniffId { get; set; }
		
		[RosValue("sniff-target")]
		public IPAddress SniffTarget { get; set; }
		
		[RosValue("sniff-target-port")]
		public Int32? SniffTargetPort { get; set; }
		
		// Source address
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source port
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// BPDU flags
		[RosValue("stp-flags", CanUnset=true)]
		public String StpFlags { get; set; }
		
		// Forward delay timer
		[RosValue("stp-forward-delay", CanUnset=true)]
		public String StpForwardDelay { get; set; }
		
		// STP hello packets time
		[RosValue("stp-hello-time", CanUnset=true)]
		public String StpHelloTime { get; set; }
		
		// STP maximum age
		[RosValue("stp-max-age", CanUnset=true)]
		public String StpMaxAge { get; set; }
		
		// STP message age
		[RosValue("stp-msg-age", CanUnset=true)]
		public String StpMsgAge { get; set; }
		
		// STP port identifier
		[RosValue("stp-port", CanUnset=true)]
		public String StpPort { get; set; }
		
		// Root bridge MAC address
		[RosValue("stp-root-address", CanUnset=true)]
		public String StpRootAddress { get; set; }
		
		// Root bridge cost
		[RosValue("stp-root-cost", CanUnset=true)]
		public String StpRootCost { get; set; }
		
		// Root bridge priority
		[RosValue("stp-root-priority", CanUnset=true)]
		public String StpRootPriority { get; set; }
		
		// STP message sender MAC address
		[RosValue("stp-sender-address", CanUnset=true)]
		public String StpSenderAddress { get; set; }
		
		// Sender priority
		[RosValue("stp-sender-priority", CanUnset=true)]
		public String StpSenderPriority { get; set; }
		
		// Bridge Protocol Data Unit (BPDU) type
		[RosValue("stp-type", CanUnset=true)]
		public String StpType { get; set; }
		
		// The encapsulated Ethernet type
		[RosValue("vlan-encap", CanUnset=true)]
		public String VlanEncap { get; set; }
		
		// VLAN identifier field
		[RosValue("vlan-id", CanUnset=true)]
		public String VlanId { get; set; }
		
		// The user priority field
		[RosValue("vlan-priority", CanUnset=true)]
		public String VlanPriority { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
	}
	[RosObject("interface bridge filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceBridgeFilter : RosItemObject<InterfaceBridgeFilter>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("drop")]
			Drop,
			[RosValue("jump")]
			Jump,
			[RosValue("log")]
			Log,
			[RosValue("mark-packet")]
			MarkPacket,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// ARP destination address
		[RosValue("arp-dst-address", CanUnset=true)]
		public String ArpDstAddress { get; set; }
		
		// ARP destination MAC address
		[RosValue("arp-dst-mac-address", CanUnset=true)]
		public String ArpDstMacAddress { get; set; }
		
		[RosValue("arp-gratuitous", CanUnset=true)]
		public Boolean? ArpGratuitous { get; set; }
		
		// The hardware type
		[RosValue("arp-hardware-type", CanUnset=true)]
		public String ArpHardwareType { get; set; }
		
		// The ARP opcode
		[RosValue("arp-opcode", CanUnset=true)]
		public String ArpOpcode { get; set; }
		
		// ARP packet type
		[RosValue("arp-packet-type", CanUnset=true)]
		public String ArpPacketType { get; set; }
		
		// ARP source address
		[RosValue("arp-src-address", CanUnset=true)]
		public String ArpSrcAddress { get; set; }
		
		// ARP source MAC address
		[RosValue("arp-src-mac-address", CanUnset=true)]
		public String ArpSrcMacAddress { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		// Chain name
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Destination address
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination MAC address
		[RosValue("dst-mac-address", CanUnset=true)]
		public String DstMacAddress { get; set; }
		
		// Destination port
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		// Bridge interface the packet is coming in
		[RosValue("in-bridge", CanUnset=true)]
		public String InBridge { get; set; }
		
		// Interface name packets are coming into the bridge
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		// IP protocol name
		[RosValue("ip-protocol", CanUnset=true)]
		public String IpProtocol { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		// if action=jump, then jump destination should be set here
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		// Restricts packet match rate to a given limit
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Defines the prefix to be printed before the logging information
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Protocol name  or number
		[RosValue("mac-protocol", CanUnset=true)]
		public String MacProtocol { get; set; }
		
		[RosValue("new-packet-mark")]
		public String NewPacketMark { get; set; }
		
		public enum NewPriorityEnum
		{
			[RosValue("from-ingress")]
			FromIngress,
		}
		
		public struct NewPriorityType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewPriorityType(Int32? value) 
                { 
                    return new NewPriorityType() { value = value }; 
                }public NewPriorityEnum? State 
                { 
                    get { return value as NewPriorityEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewPriorityType(NewPriorityEnum? value) 
                { 
                    return new NewPriorityType() { value = value }; 
                } 
                    public static NewPriorityType Parse(string str, Connection conn)
                    {
                        
                try { return new NewPriorityType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new NewPriorityType() { value = TypeFormatter.ConvertFromString<NewPriorityEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("new-priority")]
		public NewPriorityType? NewPriority { get; set; }
		
		// Bridge interface the packet is going out
		[RosValue("out-bridge", CanUnset=true)]
		public String OutBridge { get; set; }
		
		// Interface name packets are leaving the bridge
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet type
		[RosValue("packet-type", CanUnset=true)]
		public String PacketType { get; set; }
		
		[RosValue("passthrough")]
		public Boolean? Passthrough { get; set; }
		
		// Source address
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source port
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// BPDU flags
		[RosValue("stp-flags", CanUnset=true)]
		public String StpFlags { get; set; }
		
		// Forward delay timer
		[RosValue("stp-forward-delay", CanUnset=true)]
		public String StpForwardDelay { get; set; }
		
		// STP hello packets time
		[RosValue("stp-hello-time", CanUnset=true)]
		public String StpHelloTime { get; set; }
		
		// STP maximum age
		[RosValue("stp-max-age", CanUnset=true)]
		public String StpMaxAge { get; set; }
		
		// STP message age
		[RosValue("stp-msg-age", CanUnset=true)]
		public String StpMsgAge { get; set; }
		
		// STP port identifier
		[RosValue("stp-port", CanUnset=true)]
		public String StpPort { get; set; }
		
		// Root bridge MAC address
		[RosValue("stp-root-address", CanUnset=true)]
		public String StpRootAddress { get; set; }
		
		// Root bridge cost
		[RosValue("stp-root-cost", CanUnset=true)]
		public String StpRootCost { get; set; }
		
		// Root bridge priority
		[RosValue("stp-root-priority", CanUnset=true)]
		public String StpRootPriority { get; set; }
		
		// STP message sender MAC address
		[RosValue("stp-sender-address", CanUnset=true)]
		public String StpSenderAddress { get; set; }
		
		// Sender priority
		[RosValue("stp-sender-priority", CanUnset=true)]
		public String StpSenderPriority { get; set; }
		
		// Bridge Protocol Data Unit (BPDU) type
		[RosValue("stp-type", CanUnset=true)]
		public String StpType { get; set; }
		
		// The encapsulated Ethernet type
		[RosValue("vlan-encap", CanUnset=true)]
		public String VlanEncap { get; set; }
		
		// VLAN identifier field
		[RosValue("vlan-id", CanUnset=true)]
		public String VlanId { get; set; }
		
		// The user priority field
		[RosValue("vlan-priority", CanUnset=true)]
		public String VlanPriority { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
	}
	[RosObject("interface bridge host", CanGet=true)]
	public class InterfaceBridgeHost : RosItemObject<InterfaceBridgeHost>
	{
		[RosValue("age", IsDynamic=true)]
		public TimeSpan? Age { get; set; }
		
		[RosValue("bridge", IsDynamic=true)]
		public RouterOS.Interface Bridge { get; set; }
		
		[RosValue("external-fdb", IsDynamic=true)]
		public String ExternalFdb { get; set; }
		
		[RosValue("local", IsDynamic=true)]
		public String Local { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("on-interface", IsDynamic=true)]
		public RouterOS.Interface OnInterface { get; set; }
		
	}
	[RosObject("interface bridge nat", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceBridgeNat : RosItemObject<InterfaceBridgeNat>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("arp-reply")]
			ArpReply,
			[RosValue("drop")]
			Drop,
			[RosValue("dst-nat")]
			DstNat,
			[RosValue("jump")]
			Jump,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// ARP destination address
		[RosValue("arp-dst-address", CanUnset=true)]
		public String ArpDstAddress { get; set; }
		
		// ARP destination MAC address
		[RosValue("arp-dst-mac-address", CanUnset=true)]
		public String ArpDstMacAddress { get; set; }
		
		[RosValue("arp-gratuitous", CanUnset=true)]
		public Boolean? ArpGratuitous { get; set; }
		
		// The hardware type
		[RosValue("arp-hardware-type", CanUnset=true)]
		public String ArpHardwareType { get; set; }
		
		// The ARP opcode
		[RosValue("arp-opcode", CanUnset=true)]
		public String ArpOpcode { get; set; }
		
		// ARP packet type
		[RosValue("arp-packet-type", CanUnset=true)]
		public String ArpPacketType { get; set; }
		
		// ARP source address
		[RosValue("arp-src-address", CanUnset=true)]
		public String ArpSrcAddress { get; set; }
		
		// ARP source MAC address
		[RosValue("arp-src-mac-address", CanUnset=true)]
		public String ArpSrcMacAddress { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("dstnat")]
			Dstnat,
			[RosValue("srcnat")]
			Srcnat,
		}
		
		// Chain name
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Destination address
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination MAC address
		[RosValue("dst-mac-address", CanUnset=true)]
		public String DstMacAddress { get; set; }
		
		// Destination port
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		// Bridge interface the packet is coming in
		[RosValue("in-bridge", CanUnset=true)]
		public String InBridge { get; set; }
		
		// Interface name packets are coming into the bridge
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		// IP protocol name
		[RosValue("ip-protocol", CanUnset=true)]
		public String IpProtocol { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("dstnat")]
			Dstnat,
			[RosValue("srcnat")]
			Srcnat,
		}
		
		// if action=jump, then jump destination should be set here
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		// Restricts packet match rate to a given limit
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Defines the prefix to be printed before the logging information
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Protocol name  or number
		[RosValue("mac-protocol", CanUnset=true)]
		public String MacProtocol { get; set; }
		
		[RosValue("new-packet-mark")]
		public String NewPacketMark { get; set; }
		
		public enum NewPriorityEnum
		{
			[RosValue("from-ingress")]
			FromIngress,
		}
		
		[RosValue("new-priority")]
		public NewPriorityEnum? NewPriority { get; set; }
		
		// Bridge interface the packet is going out
		[RosValue("out-bridge", CanUnset=true)]
		public String OutBridge { get; set; }
		
		// Interface name packets are leaving the bridge
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet type
		[RosValue("packet-type", CanUnset=true)]
		public String PacketType { get; set; }
		
		[RosValue("passthrough")]
		public Boolean? Passthrough { get; set; }
		
		// Source address
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source port
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// BPDU flags
		[RosValue("stp-flags", CanUnset=true)]
		public String StpFlags { get; set; }
		
		// Forward delay timer
		[RosValue("stp-forward-delay", CanUnset=true)]
		public String StpForwardDelay { get; set; }
		
		// STP hello packets time
		[RosValue("stp-hello-time", CanUnset=true)]
		public String StpHelloTime { get; set; }
		
		// STP maximum age
		[RosValue("stp-max-age", CanUnset=true)]
		public String StpMaxAge { get; set; }
		
		// STP message age
		[RosValue("stp-msg-age", CanUnset=true)]
		public String StpMsgAge { get; set; }
		
		// STP port identifier
		[RosValue("stp-port", CanUnset=true)]
		public String StpPort { get; set; }
		
		// Root bridge MAC address
		[RosValue("stp-root-address", CanUnset=true)]
		public String StpRootAddress { get; set; }
		
		// Root bridge cost
		[RosValue("stp-root-cost", CanUnset=true)]
		public String StpRootCost { get; set; }
		
		// Root bridge priority
		[RosValue("stp-root-priority", CanUnset=true)]
		public String StpRootPriority { get; set; }
		
		// STP message sender MAC address
		[RosValue("stp-sender-address", CanUnset=true)]
		public String StpSenderAddress { get; set; }
		
		// Sender priority
		[RosValue("stp-sender-priority", CanUnset=true)]
		public String StpSenderPriority { get; set; }
		
		// Bridge Protocol Data Unit (BPDU) type
		[RosValue("stp-type", CanUnset=true)]
		public String StpType { get; set; }
		
		// Source MAC address to put in Ethernet frame and ARP payload, when action=arp-reply is selected
		[RosValue("to-arp-reply-mac-address")]
		public MACAddress? ToArpReplyMacAddress { get; set; }
		
		// Destination MAC address to put in Ethernet frames, when action=dst-nat is selected
		[RosValue("to-dst-mac-address")]
		public MACAddress? ToDstMacAddress { get; set; }
		
		// Source MAC address to put in Ethernet frames, when action=src-nat is selected
		[RosValue("to-src-mac-address")]
		public MACAddress? ToSrcMacAddress { get; set; }
		
		// The encapsulated Ethernet type
		[RosValue("vlan-encap", CanUnset=true)]
		public String VlanEncap { get; set; }
		
		// VLAN identifier field
		[RosValue("vlan-id", CanUnset=true)]
		public String VlanId { get; set; }
		
		// The user priority field
		[RosValue("vlan-priority", CanUnset=true)]
		public String VlanPriority { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
	}
	[RosObject("interface bridge port", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceBridgePort : RosItemObject<InterfaceBridgePort>
	{
		// The bridge interface the respective interface is grouped in
		[RosValue("bridge")]
		public RouterOS.Interface Bridge { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum EdgeEnum
		{
			[RosValue("auto")]
			Auto,
			[RosValue("no")]
			No,
			[RosValue("no-discover")]
			NoDiscover,
			[RosValue("yes")]
			Yes,
			[RosValue("yes-discover")]
			YesDiscover,
		}
		
		[RosValue("edge")]
		public EdgeEnum? Edge { get; set; }
		
		public enum ExternalFdbEnum
		{
			[RosValue("auto")]
			Auto,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		[RosValue("external-fdb")]
		public ExternalFdbEnum? ExternalFdb { get; set; }
		
		public enum HorizonEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct HorizonType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HorizonType(Int32? value) 
                { 
                    return new HorizonType() { value = value }; 
                }public HorizonEnum? State 
                { 
                    get { return value as HorizonEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HorizonType(HorizonEnum? value) 
                { 
                    return new HorizonType() { value = value }; 
                } 
                    public static HorizonType Parse(string str, Connection conn)
                    {
                        
                try { return new HorizonType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new HorizonType() { value = TypeFormatter.ConvertFromString<HorizonEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("horizon")]
		public HorizonType? Horizon { get; set; }
		
		// Name of the interface
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Path cost to the interface, used by STP to determine the 'best' path
		[RosValue("path-cost")]
		public Int32? PathCost { get; set; }
		
		public enum PointToPointEnum
		{
			[RosValue("auto")]
			Auto,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		[RosValue("point-to-point")]
		public PointToPointEnum? PointToPoint { get; set; }
		
		// The priority of the interface in comparison with other going to the same subnet
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		[RosValue("debug-info", IsDynamic=true)]
		public String DebugInfo { get; set; }
		
		[RosValue("designated-bridge", IsDynamic=true)]
		public Int32? DesignatedBridge { get; set; }
		
		[RosValue("designated-cost", IsDynamic=true)]
		public Int32? DesignatedCost { get; set; }
		
		[RosValue("designated-port-number", IsDynamic=true)]
		public Int32? DesignatedPortNumber { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("edge-port", IsDynamic=true)]
		public String EdgePort { get; set; }
		
		[RosValue("edge-port-discovery", IsDynamic=true)]
		public String EdgePortDiscovery { get; set; }
		
		[RosValue("external-fdb-status", IsDynamic=true)]
		public String ExternalFdbStatus { get; set; }
		
		[RosValue("forwarding", IsDynamic=true)]
		public String Forwarding { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		[RosValue("learning", IsDynamic=true)]
		public String Learning { get; set; }
		
		[RosValue("point-to-point-port", IsDynamic=true)]
		public String PointToPointPort { get; set; }
		
		[RosValue("port-number", IsDynamic=true)]
		public Int32? PortNumber { get; set; }
		
		public enum RoleType
		{
			[RosValue("alternate-port")]
			AlternatePort,
			[RosValue("backup-port")]
			BackupPort,
			[RosValue("designated-port")]
			DesignatedPort,
			[RosValue("disabled-port")]
			DisabledPort,
			[RosValue("root-port")]
			RootPort,
		}
		
		[RosValue("role", IsDynamic=true)]
		public RoleType? Role { get; set; }
		
		[RosValue("root-path-cost", IsDynamic=true)]
		public Int32? RootPathCost { get; set; }
		
		[RosValue("sending-rstp", IsDynamic=true)]
		public String SendingRstp { get; set; }
		
		public enum StatusType
		{
			[RosValue("in-bridge")]
			InBridge,
			[RosValue("inactive")]
			Inactive,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
	}
	[RosObject("interface bridge settings", CanSet=true, CanGet=true)]
	public class InterfaceBridgeSettings : RosValueObject
	{
		[RosValue("use-ip-firewall")]
		public Boolean? UseIpFirewall { get; set; }
		
		[RosValue("use-ip-firewall-for-pppoe")]
		public Boolean? UseIpFirewallForPppoe { get; set; }
		
		[RosValue("use-ip-firewall-for-vlan")]
		public Boolean? UseIpFirewallForVlan { get; set; }
		
	}
	[RosObject("interface eoip", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceEoip : RosItemObject<InterfaceEoip>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("keepalive", CanUnset=true)]
		public Int32? Keepalive { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// MAC address of the EoIP interface
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Tunnel name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Remote address of tunnel
		[RosValue("remote-address")]
		public IPAddress RemoteAddress { get; set; }
		
		// Tunnel identity
		[RosValue("tunnel-id")]
		public Int32? TunnelId { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceEoip Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceEoip obj = new InterfaceEoip();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface eoipv6", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceEoipv6 : RosItemObject<InterfaceEoipv6>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("keepalive", CanUnset=true)]
		public Int32? Keepalive { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		[RosValue("local-address")]
		public String LocalAddress { get; set; }
		
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("remote-address")]
		public String RemoteAddress { get; set; }
		
		[RosValue("tunnel-id")]
		public Int32? TunnelId { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceEoipv6 Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceEoipv6 obj = new InterfaceEoipv6();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ethernet", CanSet=true, CanGet=true)]
	public class InterfaceEthernet : RosItemObject<InterfaceEthernet>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// When enabled the interface "advertises" the maximum capabilities to achieve the best connection possible
		[RosValue("auto-negotiation")]
		public Boolean? AutoNegotiation { get; set; }
		
		public enum CableSettingsEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("short")]
			Short,
			[RosValue("standard")]
			Standard,
		}
		
		// Ethernet cable type
		[RosValue("cable-settings")]
		public CableSettingsEnum? CableSettings { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Disable running check
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Defines whether the transmissionof data appearsin two directions simultaneously
		[RosValue("full-duplex")]
		public Boolean? FullDuplex { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// MAC address
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("mdix-enable")]
		public Boolean? MdixEnable { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum PoeOutEnum
		{
			[RosValue("auto")]
			Auto,
			[RosValue("off")]
			Off,
		}
		
		[RosValue("poe-out")]
		public PoeOutEnum? PoeOut { get; set; }
		
		public enum SpeedEnum
		{
			[RosValue("100Mbps")]
			E100Mbps,
			[RosValue("10Mbps")]
			E10Mbps,
			[RosValue("1Gbps")]
			E1Gbps,
		}
		
		// sets the data transmission speed of the interface
		[RosValue("speed")]
		public SpeedEnum? Speed { get; set; }
		
		[RosValue("driver-rx-byte", IsDynamic=true)]
		public Int64? DriverRxByte { get; set; }
		
		[RosValue("driver-rx-packet", IsDynamic=true)]
		public Int64? DriverRxPacket { get; set; }
		
		[RosValue("driver-tx-byte", IsDynamic=true)]
		public Int64? DriverTxByte { get; set; }
		
		[RosValue("driver-tx-packet", IsDynamic=true)]
		public Int64? DriverTxPacket { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("rx-1024-1518", IsDynamic=true)]
		public Int64? Rx10241518 { get; set; }
		
		[RosValue("rx-128-255", IsDynamic=true)]
		public Int64? Rx128255 { get; set; }
		
		[RosValue("rx-1519-max", IsDynamic=true)]
		public Int64? Rx1519Max { get; set; }
		
		[RosValue("rx-256-511", IsDynamic=true)]
		public Int64? Rx256511 { get; set; }
		
		[RosValue("rx-512-1023", IsDynamic=true)]
		public Int64? Rx5121023 { get; set; }
		
		[RosValue("rx-64", IsDynamic=true)]
		public Int64? Rx64 { get; set; }
		
		[RosValue("rx-65-127", IsDynamic=true)]
		public Int64? Rx65127 { get; set; }
		
		[RosValue("rx-align-error", IsDynamic=true)]
		public Int64? RxAlignError { get; set; }
		
		[RosValue("rx-broadcast", IsDynamic=true)]
		public Int64? RxBroadcast { get; set; }
		
		[RosValue("rx-bytes", IsDynamic=true)]
		public Int64? RxBytes { get; set; }
		
		[RosValue("rx-carrier-error", IsDynamic=true)]
		public Int64? RxCarrierError { get; set; }
		
		[RosValue("rx-code-error", IsDynamic=true)]
		public Int64? RxCodeError { get; set; }
		
		[RosValue("rx-control", IsDynamic=true)]
		public Int64? RxControl { get; set; }
		
		[RosValue("rx-drop", IsDynamic=true)]
		public Int64? RxDrop { get; set; }
		
		[RosValue("rx-fcs-error", IsDynamic=true)]
		public Int64? RxFcsError { get; set; }
		
		[RosValue("rx-fragment", IsDynamic=true)]
		public Int64? RxFragment { get; set; }
		
		[RosValue("rx-jabber", IsDynamic=true)]
		public Int64? RxJabber { get; set; }
		
		[RosValue("rx-length-error", IsDynamic=true)]
		public Int64? RxLengthError { get; set; }
		
		[RosValue("rx-multicast", IsDynamic=true)]
		public Int64? RxMulticast { get; set; }
		
		[RosValue("rx-overflow", IsDynamic=true)]
		public Int64? RxOverflow { get; set; }
		
		[RosValue("rx-packet", IsDynamic=true)]
		public Int64? RxPacket { get; set; }
		
		[RosValue("rx-pause", IsDynamic=true)]
		public Int64? RxPause { get; set; }
		
		[RosValue("rx-too-long", IsDynamic=true)]
		public Int64? RxTooLong { get; set; }
		
		[RosValue("rx-too-short", IsDynamic=true)]
		public Int64? RxTooShort { get; set; }
		
		[RosValue("rx-unknown-op", IsDynamic=true)]
		public Int64? RxUnknownOp { get; set; }
		
		[RosValue("slave", IsDynamic=true)]
		public String Slave { get; set; }
		
		[RosValue("tx-1024-1518", IsDynamic=true)]
		public Int64? Tx10241518 { get; set; }
		
		[RosValue("tx-1519-max", IsDynamic=true)]
		public Int64? Tx1519Max { get; set; }
		
		[RosValue("tx-512-1023", IsDynamic=true)]
		public Int64? Tx5121023 { get; set; }
		
		[RosValue("tx-65-127", IsDynamic=true)]
		public Int64? Tx65127 { get; set; }
		
		[RosValue("tx-bytes", IsDynamic=true)]
		public Int64? TxBytes { get; set; }
		
		[RosValue("tx-control", IsDynamic=true)]
		public Int64? TxControl { get; set; }
		
		[RosValue("tx-drop", IsDynamic=true)]
		public Int64? TxDrop { get; set; }
		
		[RosValue("tx-excessive-deferred", IsDynamic=true)]
		public Int64? TxExcessiveDeferred { get; set; }
		
		[RosValue("tx-fragment", IsDynamic=true)]
		public Int64? TxFragment { get; set; }
		
		[RosValue("tx-late-collision", IsDynamic=true)]
		public Int64? TxLateCollision { get; set; }
		
		[RosValue("tx-multiple-collision", IsDynamic=true)]
		public Int64? TxMultipleCollision { get; set; }
		
		[RosValue("tx-pause", IsDynamic=true)]
		public Int64? TxPause { get; set; }
		
		[RosValue("tx-rx-1024-1518", IsDynamic=true)]
		public Int64? TxRx10241518 { get; set; }
		
		[RosValue("tx-rx-1519-max", IsDynamic=true)]
		public Int64? TxRx1519Max { get; set; }
		
		[RosValue("tx-rx-512-1023", IsDynamic=true)]
		public Int64? TxRx5121023 { get; set; }
		
		[RosValue("tx-rx-65-127", IsDynamic=true)]
		public Int64? TxRx65127 { get; set; }
		
		[RosValue("tx-too-long", IsDynamic=true)]
		public Int64? TxTooLong { get; set; }
		
		[RosValue("tx-total-collision", IsDynamic=true)]
		public Int64? TxTotalCollision { get; set; }
		
		[RosValue("tx-128-255", IsDynamic=true)]
		public Int64? Tx128255 { get; set; }
		
		[RosValue("tx-256-511", IsDynamic=true)]
		public Int64? Tx256511 { get; set; }
		
		[RosValue("tx-64", IsDynamic=true)]
		public Int64? Tx64 { get; set; }
		
		[RosValue("tx-broadcast", IsDynamic=true)]
		public Int64? TxBroadcast { get; set; }
		
		[RosValue("tx-collision", IsDynamic=true)]
		public Int64? TxCollision { get; set; }
		
		[RosValue("tx-deferred", IsDynamic=true)]
		public Int64? TxDeferred { get; set; }
		
		[RosValue("tx-excessive-collision", IsDynamic=true)]
		public Int64? TxExcessiveCollision { get; set; }
		
		[RosValue("tx-fcs-error", IsDynamic=true)]
		public Int64? TxFcsError { get; set; }
		
		[RosValue("tx-jabber", IsDynamic=true)]
		public Int64? TxJabber { get; set; }
		
		[RosValue("tx-multicast", IsDynamic=true)]
		public Int64? TxMulticast { get; set; }
		
		[RosValue("tx-packet", IsDynamic=true)]
		public Int64? TxPacket { get; set; }
		
		[RosValue("tx-pause-honored", IsDynamic=true)]
		public Int64? TxPauseHonored { get; set; }
		
		[RosValue("tx-rx-128-255", IsDynamic=true)]
		public Int64? TxRx128255 { get; set; }
		
		[RosValue("tx-rx-256-511", IsDynamic=true)]
		public Int64? TxRx256511 { get; set; }
		
		[RosValue("tx-rx-64", IsDynamic=true)]
		public Int64? TxRx64 { get; set; }
		
		[RosValue("tx-single-collision", IsDynamic=true)]
		public Int64? TxSingleCollision { get; set; }
		
		[RosValue("tx-too-short", IsDynamic=true)]
		public Int64? TxTooShort { get; set; }
		
		[RosValue("tx-underrun", IsDynamic=true)]
		public Int64? TxUnderrun { get; set; }
		
		public static InterfaceEthernet Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceEthernet obj = new InterfaceEthernet();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface farsync", CanSet=true, CanGet=true)]
	public class InterfaceFarsync : RosItemObject<InterfaceFarsync>
	{
		// CHDLC keepalive period in seconds
		[RosValue("chdlc-keepalive")]
		public TimeSpan? ChdlcKeepalive { get; set; }
		
		// Speed of internal clock
		[RosValue("clock-rate")]
		public Int32? ClockRate { get; set; }
		
		public enum ClockSourceEnum
		{
			[RosValue("external")]
			External,
			[RosValue("internal")]
			Internal,
		}
		
		// Clock source
		[RosValue("clock-source")]
		public ClockSourceEnum? ClockSource { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum DslAnnexEnum
		{
			[RosValue("a")]
			A,
			[RosValue("b")]
			B,
		}
		
		[RosValue("dsl-annex")]
		public DslAnnexEnum? DslAnnex { get; set; }
		
		[RosValue("dsl-backoff")]
		public Int32? DslBackoff { get; set; }
		
		public enum DslEncapsulationEnum
		{
			[RosValue("mpoa")]
			Mpoa,
			[RosValue("ppp")]
			Ppp,
		}
		
		[RosValue("dsl-encapsulation")]
		public DslEncapsulationEnum? DslEncapsulation { get; set; }
		
		[RosValue("dsl-lpath")]
		public Int32? DslLpath { get; set; }
		
		[RosValue("dsl-snrth")]
		public Int32? DslSnrth { get; set; }
		
		public enum DslTerminalEnum
		{
			[RosValue("central")]
			Central,
			[RosValue("remote")]
			Remote,
		}
		
		[RosValue("dsl-terminal")]
		public DslTerminalEnum? DslTerminal { get; set; }
		
		[RosValue("dsl-test-mode")]
		public Int32? DslTestMode { get; set; }
		
		[RosValue("dsl-vci")]
		public Int32? DslVci { get; set; }
		
		[RosValue("dsl-vpi")]
		public Int32? DslVpi { get; set; }
		
		// Operate or not in DCE mode
		[RosValue("frame-relay-dce")]
		public Boolean? FrameRelayDce { get; set; }
		
		public enum FrameRelayLmiTypeEnum
		{
			[RosValue("ansi")]
			Ansi,
			[RosValue("ccitt")]
			Ccitt,
		}
		
		// Frame-Relay Local Management Interface type
		[RosValue("frame-relay-lmi-type")]
		public FrameRelayLmiTypeEnum? FrameRelayLmiType { get; set; }
		
		[RosValue("invert-phase")]
		public Boolean? InvertPhase { get; set; }
		
		public enum LineProtocolEnum
		{
			[RosValue("cisco-hdlc")]
			CiscoHdlc,
			[RosValue("frame-relay")]
			FrameRelay,
			[RosValue("sync-ppp")]
			SyncPpp,
		}
		
		// Line protocol
		[RosValue("line-protocol")]
		public LineProtocolEnum? LineProtocol { get; set; }
		
		public enum MediaTypeEnum
		{
			[RosValue("E1")]
			E1,
			[RosValue("SHDSL")]
			SHDSL,
			[RosValue("T1")]
			T1,
			[RosValue("V24")]
			V24,
			[RosValue("V35")]
			V35,
		}
		
		// Type of the media
		[RosValue("media-type")]
		public MediaTypeEnum? MediaType { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum Te1CodingEnum
		{
			[RosValue("ami")]
			Ami,
			[RosValue("ami-zcs")]
			AmiZcs,
			[RosValue("b8zs")]
			B8zs,
			[RosValue("hdb3")]
			Hdb3,
		}
		
		[RosValue("te1-coding")]
		public Te1CodingEnum? Te1Coding { get; set; }
		
		public enum Te1EqualizerEnum
		{
			[RosValue("long")]
			Long,
			[RosValue("short")]
			Short,
		}
		
		[RosValue("te1-equalizer")]
		public Te1EqualizerEnum? Te1Equalizer { get; set; }
		
		[RosValue("te1-idle-code")]
		public Int32? Te1IdleCode { get; set; }
		
		public enum Te1InterfaceEnum
		{
			[RosValue("bnc")]
			Bnc,
			[RosValue("rj48c")]
			Rj48c,
		}
		
		[RosValue("te1-interface")]
		public Te1InterfaceEnum? Te1Interface { get; set; }
		
		public enum Te1LineBuildOutEnum
		{
			[RosValue("0db")]
			E0db,
			[RosValue("15db")]
			E15db,
			[RosValue("22db")]
			E22db,
			[RosValue("7db")]
			E7db,
		}
		
		[RosValue("te1-line-build-out")]
		public Te1LineBuildOutEnum? Te1LineBuildOut { get; set; }
		
		public enum Te1LoopEnum
		{
			[RosValue("local")]
			Local,
			[RosValue("none")]
			None,
			[RosValue("payload-exc-ts0")]
			PayloadExcTs0,
			[RosValue("payload-inc-ts0")]
			PayloadIncTs0,
			[RosValue("remote")]
			Remote,
		}
		
		[RosValue("te1-loop")]
		public Te1LoopEnum? Te1Loop { get; set; }
		
		public enum Te1ModeEnum
		{
			[RosValue("master")]
			Master,
			[RosValue("slave")]
			Slave,
		}
		
		[RosValue("te1-mode")]
		public Te1ModeEnum? Te1Mode { get; set; }
		
		public enum Te1RangeEnum
		{
			[RosValue("0m-40m")]
			E0m40m,
			[RosValue("122m-162m")]
			E122m162m,
			[RosValue("162m-200m")]
			E162m200m,
			[RosValue("40m-81m")]
			E40m81m,
			[RosValue("81m-122m")]
			E81m122m,
		}
		
		[RosValue("te1-range")]
		public Te1RangeEnum? Te1Range { get; set; }
		
		public enum Te1RxBufferEnum
		{
			[RosValue("1-frame")]
			E1Frame,
			[RosValue("2-frame")]
			E2Frame,
			[RosValue("96-bit")]
			E96Bit,
			[RosValue("none")]
			None,
		}
		
		[RosValue("te1-rx-buffer")]
		public Te1RxBufferEnum? Te1RxBuffer { get; set; }
		
		[RosValue("te1-start-slot")]
		public Int32? Te1StartSlot { get; set; }
		
		public enum Te1StructureEnum
		{
			[RosValue("e1-crc4")]
			E1Crc4,
			[RosValue("e1-crc4m")]
			E1Crc4m,
			[RosValue("e1-double")]
			E1Double,
			[RosValue("t1-f12")]
			T1F12,
			[RosValue("t1-f24")]
			T1F24,
		}
		
		[RosValue("te1-structure")]
		public Te1StructureEnum? Te1Structure { get; set; }
		
		public enum Te1TxBufferEnum
		{
			[RosValue("1-frame")]
			E1Frame,
			[RosValue("2-frame")]
			E2Frame,
			[RosValue("96-bit")]
			E96Bit,
			[RosValue("none")]
			None,
		}
		
		[RosValue("te1-tx-buffer")]
		public Te1TxBufferEnum? Te1TxBuffer { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceFarsync Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceFarsync obj = new InterfaceFarsync();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface gre", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceGre : RosItemObject<InterfaceGre>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("keepalive", CanUnset=true)]
		public Int32? Keepalive { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		[RosValue("local-address")]
		public IPAddress LocalAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("remote-address")]
		public IPAddress RemoteAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceGre Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceGre obj = new InterfaceGre();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ipip", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceIpip : RosItemObject<InterfaceIpip>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Local address of tunnel
		[RosValue("local-address")]
		public IPAddress LocalAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Tunnel name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Remote address of tunnel
		[RosValue("remote-address")]
		public IPAddress RemoteAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceIpip Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceIpip obj = new InterfaceIpip();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ipipv6", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceIpipv6 : RosItemObject<InterfaceIpipv6>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum DscpEnum
		{
			[RosValue("inherit")]
			Inherit,
		}
		
		public struct DscpType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DscpType(Int32? value) 
                { 
                    return new DscpType() { value = value }; 
                }public DscpEnum? State 
                { 
                    get { return value as DscpEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DscpType(DscpEnum? value) 
                { 
                    return new DscpType() { value = value }; 
                } 
                    public static DscpType Parse(string str, Connection conn)
                    {
                        
                try { return new DscpType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new DscpType() { value = TypeFormatter.ConvertFromString<DscpEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("dscp")]
		public DscpType? Dscp { get; set; }
		
		[RosValue("local-address")]
		public String LocalAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("remote-address")]
		public String RemoteAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceIpipv6 Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceIpipv6 obj = new InterfaceIpipv6();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface isdn-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceIsdnClient : RosItemObject<InterfaceIsdnClient>
	{
		// Add PPP remote address as a default route
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// The authentication method to allow for the client
		[RosValue("allow")]
		public String Allow { get; set; }
		
		[RosValue("bundle-128k")]
		public Boolean? Bundle128k { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Enable/Disable dial on demand
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum L2ProtocolEnum
		{
			[RosValue("hdlc")]
			Hdlc,
			[RosValue("x75bui")]
			X75bui,
			[RosValue("x75i")]
			X75i,
			[RosValue("x75ui")]
			X75ui,
		}
		
		// Level 2 protocol to use
		[RosValue("l2-protocol")]
		public L2ProtocolEnum? L2Protocol { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		// MSN/EAZ of ISDN line
		[RosValue("msn")]
		public String Msn { get; set; }
		
		// ISDN client name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Password that will be provided to the remote server
		[RosValue("password")]
		public String Password { get; set; }
		
		// Phone number for dialout
		[RosValue("phone")]
		public String Phone { get; set; }
		
		// Profile to use when connecting to the remote server
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// Use or not peer DNS
		[RosValue("use-peer-dns")]
		public Boolean? UsePeerDns { get; set; }
		
		// User name to use for dialout
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceIsdnClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceIsdnClient obj = new InterfaceIsdnClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface isdn-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceIsdnServer : RosItemObject<InterfaceIsdnServer>
	{
		// The kind of authentication
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		[RosValue("bundle-128k")]
		public Boolean? Bundle128k { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum L2ProtocolEnum
		{
			[RosValue("hdlc")]
			Hdlc,
			[RosValue("x75bui")]
			X75bui,
			[RosValue("x75i")]
			X75i,
			[RosValue("x75ui")]
			X75ui,
		}
		
		// Level 2 protocol to use
		[RosValue("l2-protocol")]
		public L2ProtocolEnum? L2Protocol { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		// MSN/EAZ of ISDN line
		[RosValue("msn")]
		public String Msn { get; set; }
		
		// ISDN server name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Profile to use when connecting to the server
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceIsdnServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceIsdnServer obj = new InterfaceIsdnServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface l2tp-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceL2tpClient : RosItemObject<InterfaceL2tpClient>
	{
		// Whether to use server which this client is connected to as its default gateway
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// The authentication method to allow for the client
		[RosValue("allow")]
		public String Allow { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// The IP address of the L2TP server to connect to
		[RosValue("connect-to")]
		public IPAddress ConnectTo { get; set; }
		
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// User's password to use when logging to the remote server
		[RosValue("password")]
		public String Password { get; set; }
		
		// Profile to use when connecting to the remote server
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// The name of the user
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceL2tpClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceL2tpClient obj = new InterfaceL2tpClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface l2tp-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceL2tpServer : RosItemObject<InterfaceL2tpServer>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// The name of the user
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("client-address", IsDynamic=true)]
		public String ClientAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("encoding", IsDynamic=true)]
		public String Encoding { get; set; }
		
		[RosValue("mru", IsDynamic=true)]
		public Int32? Mru { get; set; }
		
		[RosValue("mtu", IsDynamic=true)]
		public Int32? Mtu { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static InterfaceL2tpServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceL2tpServer obj = new InterfaceL2tpServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface l2tp-server server", CanSet=true, CanGet=true)]
	public class InterfaceL2tpServerServer : RosValueObject
	{
		// Authentication algorithm
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		// Default profile to use
		[RosValue("default-profile")]
		public RouterOS.PppProfile DefaultProfile { get; set; }
		
		// Defines whether L2TP server is enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
	}
	[RosObject("interface mesh", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceMesh : RosItemObject<InterfaceMesh>
	{
		[RosValue("admin-mac")]
		public MACAddress? AdminMac { get; set; }
		
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		[RosValue("auto-mac")]
		public Boolean? AutoMac { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("hwmp-default-hoplimit")]
		public Int32? HwmpDefaultHoplimit { get; set; }
		
		public enum HwmpPrepLifetimeEnum
		{
			[RosValue("HwmpPrepLifetime")]
			HwmpPrepLifetime,
		}
		
		[RosValue("hwmp-prep-lifetime")]
		public HwmpPrepLifetimeEnum? HwmpPrepLifetime { get; set; }
		
		[RosValue("hwmp-preq-destination-only")]
		public Boolean? HwmpPreqDestinationOnly { get; set; }
		
		[RosValue("hwmp-preq-reply-and-forward")]
		public Boolean? HwmpPreqReplyAndForward { get; set; }
		
		[RosValue("hwmp-preq-retries")]
		public Int32? HwmpPreqRetries { get; set; }
		
		public enum HwmpPreqWaitingTimeEnum
		{
			[RosValue("HwmpPreqWaitingTime")]
			HwmpPreqWaitingTime,
		}
		
		[RosValue("hwmp-preq-waiting-time")]
		public HwmpPreqWaitingTimeEnum? HwmpPreqWaitingTime { get; set; }
		
		public enum HwmpRannIntervalEnum
		{
			[RosValue("HwmpRannInterval")]
			HwmpRannInterval,
		}
		
		[RosValue("hwmp-rann-interval")]
		public HwmpRannIntervalEnum? HwmpRannInterval { get; set; }
		
		public enum HwmpRannLifetimeEnum
		{
			[RosValue("HwmpRannLifetime")]
			HwmpRannLifetime,
		}
		
		[RosValue("hwmp-rann-lifetime")]
		public HwmpRannLifetimeEnum? HwmpRannLifetime { get; set; }
		
		[RosValue("hwmp-rann-propagation-delay")]
		public Single? HwmpRannPropagationDelay { get; set; }
		
		[RosValue("mesh-portal")]
		public Boolean? MeshPortal { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public UInt16? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("reoptimize-paths")]
		public Boolean? ReoptimizePaths { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceMesh Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceMesh obj = new InterfaceMesh();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface mesh fdb", CanGet=true)]
	public class InterfaceMeshFdb : RosItemObject<InterfaceMeshFdb>
	{
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("age", IsDynamic=true)]
		public TimeSpan? Age { get; set; }
		
		[RosValue("lifetime", IsDynamic=true)]
		public TimeSpan? Lifetime { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("mesh", IsDynamic=true)]
		public RouterOS.Interface Mesh { get; set; }
		
		[RosValue("metric", IsDynamic=true)]
		public Int32? Metric { get; set; }
		
		[RosValue("on-interface", IsDynamic=true)]
		public RouterOS.Interface OnInterface { get; set; }
		
		[RosValue("root", IsDynamic=true)]
		public String Root { get; set; }
		
		[RosValue("seq-number", IsDynamic=true)]
		public Int32? SeqNumber { get; set; }
		
		public enum TypeType
		{
			[RosValue("direct")]
			Direct,
			[RosValue("larval")]
			Larval,
			[RosValue("local")]
			Local,
			[RosValue("mesh")]
			Mesh,
			[RosValue("neighbor")]
			Neighbor,
			[RosValue("outsider")]
			Outsider,
			[RosValue("unknown")]
			Unknown,
		}
		
		[RosValue("type", IsDynamic=true)]
		public TypeType? Type { get; set; }
		
	}
	[RosObject("interface mesh port", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceMeshPort : RosItemObject<InterfaceMeshPort>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum HelloIntervalEnum
		{
			[RosValue("HelloInterval")]
			HelloInterval,
		}
		
		[RosValue("hello-interval")]
		public HelloIntervalEnum? HelloInterval { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("mesh")]
		public RouterOS.Interface Mesh { get; set; }
		
		[RosValue("path-cost")]
		public Int32? PathCost { get; set; }
		
		public enum PortTypeEnum
		{
			[RosValue("WDS")]
			WDS,
			[RosValue("auto")]
			Auto,
			[RosValue("ethernet")]
			Ethernet,
			[RosValue("wireless")]
			Wireless,
		}
		
		[RosValue("port-type")]
		public PortTypeEnum? PortType { get; set; }
		
		public enum ActivePortTypeType
		{
			[RosValue("WDS")]
			WDS,
			[RosValue("ethernet-bridge")]
			EthernetBridge,
			[RosValue("ethernet-mesh")]
			EthernetMesh,
			[RosValue("ethernet-mixed")]
			EthernetMixed,
			[RosValue("wireless")]
			Wireless,
		}
		
		[RosValue("active-port-type", IsDynamic=true)]
		public ActivePortTypeType? ActivePortType { get; set; }
		
		[RosValue("dr-address", IsDynamic=true)]
		public MACAddress? DrAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
	}
	[RosObject("interface ovpn-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceOvpnClient : RosItemObject<InterfaceOvpnClient>
	{
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		public enum AuthEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("none")]
			None,
			[RosValue("sha1")]
			Sha1,
		}
		
		[RosValue("auth")]
		public AuthEnum? Auth { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		public enum CipherEnum
		{
			[RosValue("aes128")]
			Aes128,
			[RosValue("aes192")]
			Aes192,
			[RosValue("aes256")]
			Aes256,
			[RosValue("blowfish128")]
			Blowfish128,
			[RosValue("none")]
			None,
		}
		
		[RosValue("cipher")]
		public CipherEnum? Cipher { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("connect-to")]
		public IPAddress ConnectTo { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum ModeEnum
		{
			[RosValue("ethernet")]
			Ethernet,
			[RosValue("ip")]
			Ip,
		}
		
		[RosValue("mode")]
		public ModeEnum? Mode { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("password")]
		public String Password { get; set; }
		
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceOvpnClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceOvpnClient obj = new InterfaceOvpnClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ovpn-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceOvpnServer : RosItemObject<InterfaceOvpnServer>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("client-address", IsDynamic=true)]
		public String ClientAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("encoding", IsDynamic=true)]
		public String Encoding { get; set; }
		
		[RosValue("mtu", IsDynamic=true)]
		public Int32? Mtu { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static InterfaceOvpnServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceOvpnServer obj = new InterfaceOvpnServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ovpn-server server", CanSet=true, CanGet=true)]
	public class InterfaceOvpnServerServer : RosValueObject
	{
		[RosValue("auth")]
		public String Auth { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		[RosValue("cipher")]
		public String Cipher { get; set; }
		
		[RosValue("default-profile")]
		public RouterOS.PppProfile DefaultProfile { get; set; }
		
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		public enum KeepaliveTimeoutEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutEnum? KeepaliveTimeout { get; set; }
		
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum ModeEnum
		{
			[RosValue("ethernet")]
			Ethernet,
			[RosValue("ip")]
			Ip,
		}
		
		[RosValue("mode")]
		public ModeEnum? Mode { get; set; }
		
		[RosValue("netmask")]
		public String Netmask { get; set; }
		
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		[RosValue("require-client-certificate")]
		public Boolean? RequireClientCertificate { get; set; }
		
	}
	[RosObject("interface ppp-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePppClient : RosItemObject<InterfacePppClient>
	{
		// Add PPP remote address as a default route
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// The authentication method to allow for the client
		[RosValue("allow")]
		public String Allow { get; set; }
		
		[RosValue("apn")]
		public String Apn { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("data-channel")]
		public Int32? DataChannel { get; set; }
		
		// The dial command
		[RosValue("dial-command")]
		public String DialCommand { get; set; }
		
		// Enable/Disable dial on demand
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("info-channel")]
		public Int32? InfoChannel { get; set; }
		
		[RosValue("keepalive-timeout")]
		public Int32? KeepaliveTimeout { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		// Modem init string
		[RosValue("modem-init")]
		public String ModemInit { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Enable/Disable nullmodem mode
		[RosValue("null-modem")]
		public Boolean? NullModem { get; set; }
		
		// P2P user password on the remote server to use for dialout
		[RosValue("password")]
		public String Password { get; set; }
		
		// Phone number for dialout
		[RosValue("phone")]
		public String Phone { get; set; }
		
		[RosValue("pin")]
		public String Pin { get; set; }
		
		// Serial port
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		// Local profile to use for dialout
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		[RosValue("remote-address")]
		public IPAddress RemoteAddress { get; set; }
		
		// Enable/Disable using of peer DNS
		[RosValue("use-peer-dns")]
		public Boolean? UsePeerDns { get; set; }
		
		// User name to use for dialout
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfacePppClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePppClient obj = new InterfacePppClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface ppp-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePppServer : RosItemObject<InterfacePppServer>
	{
		// Authentication protocol
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		// Modem init string
		[RosValue("modem-init")]
		public String ModemInit { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Enable/Disable nullmodem mode
		[RosValue("null-modem")]
		public Boolean? NullModem { get; set; }
		
		// Serial port
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		// Profile name for the link
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// Number of rings to wait before answering
		[RosValue("ring-count")]
		public Int32? RingCount { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfacePppServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePppServer obj = new InterfacePppServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface pppoe-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePppoeClient : RosItemObject<InterfacePppoeClient>
	{
		// Access Concentrator name
		[RosValue("ac-name")]
		public String AcName { get; set; }
		
		// Enable/Disable adding default route through this interface
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// The authentication method to allow for the client
		[RosValue("allow")]
		public String Allow { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Enable/Disable dial on demand
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Ethernet interface name
		[RosValue("interface")]
		public RouterOS.Interface[] Interface { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// A user password used to connect the PPPoE server
		[RosValue("password")]
		public String Password { get; set; }
		
		// Default profile for the connection
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// Service name
		[RosValue("service-name")]
		public String ServiceName { get; set; }
		
		// Enable/Disable using of peer DNS
		[RosValue("use-peer-dns")]
		public Boolean? UsePeerDns { get; set; }
		
		// User name to use for dialout
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfacePppoeClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePppoeClient obj = new InterfacePppoeClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface pppoe-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePppoeServer : RosItemObject<InterfacePppoeServer>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Name of the service
		[RosValue("service")]
		public String Service { get; set; }
		
		// A user name
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("encoding", IsDynamic=true)]
		public String Encoding { get; set; }
		
		[RosValue("mru", IsDynamic=true)]
		public Int32? Mru { get; set; }
		
		[RosValue("mtu", IsDynamic=true)]
		public Int32? Mtu { get; set; }
		
		[RosValue("remote-address", IsDynamic=true)]
		public String RemoteAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static InterfacePppoeServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePppoeServer obj = new InterfacePppoeServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface pppoe-server server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePppoeServerServer : RosItemObject<InterfacePppoeServerServer>
	{
		// Authentication algorithm
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		// Default profile to use for the client
		[RosValue("default-profile")]
		public RouterOS.PppProfile DefaultProfile { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Ethernet interface name
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		public enum KeepaliveTimeoutEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		// Time period after which not responding client is proclaimed
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutEnum? KeepaliveTimeout { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		// Maximal count of sessions
		[RosValue("max-sessions")]
		public Int32? MaxSessions { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Allows only one session for each client
		[RosValue("one-session-per-host")]
		public Boolean? OneSessionPerHost { get; set; }
		
		// Service name
		[RosValue("service-name")]
		public String ServiceName { get; set; }
		
	}
	[RosObject("interface pptp-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePptpClient : RosItemObject<InterfacePptpClient>
	{
		// Add PPTP remote address as a default route
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// The authentication method to allow for the client
		[RosValue("allow")]
		public String Allow { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// PPTP server address
		[RosValue("connect-to")]
		public IPAddress ConnectTo { get; set; }
		
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// User password to use when logging to the remote server
		[RosValue("password")]
		public String Password { get; set; }
		
		// Profile to use when connecting to the remote server
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// User name to use for dialout
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfacePptpClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePptpClient obj = new InterfacePptpClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface pptp-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePptpServer : RosItemObject<InterfacePptpServer>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// User name
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("client-address", IsDynamic=true)]
		public String ClientAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("encoding", IsDynamic=true)]
		public String Encoding { get; set; }
		
		[RosValue("mru", IsDynamic=true)]
		public Int32? Mru { get; set; }
		
		[RosValue("mtu", IsDynamic=true)]
		public Int32? Mtu { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static InterfacePptpServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePptpServer obj = new InterfacePptpServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface pptp-server server", CanSet=true, CanGet=true)]
	public class InterfacePptpServerServer : RosValueObject
	{
		// Authentication algorithm
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		// Activate the default profile
		[RosValue("default-profile")]
		public RouterOS.PppProfile DefaultProfile { get; set; }
		
		// Defines whether PPTP server is enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		public enum KeepaliveTimeoutEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		// Time period after which not responding client is proclaimed
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutEnum? KeepaliveTimeout { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
	}
	[RosObject("interface pvc", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfacePvc : RosItemObject<InterfacePvc>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Data Link Connection Identifier assigned to the PVC interface
		[RosValue("dlci")]
		public Int32? Dlci { get; set; }
		
		// Frame Relay interface
		[RosValue("interface")]
		public String Interface { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfacePvc Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfacePvc obj = new InterfacePvc();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface sstp-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceSstpClient : RosItemObject<InterfaceSstpClient>
	{
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("connect-to")]
		public String ConnectTo { get; set; }
		
		[RosValue("dial-on-demand")]
		public Boolean? DialOnDemand { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("http-proxy")]
		public String HttpProxy { get; set; }
		
		public enum KeepaliveTimeoutEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutEnum? KeepaliveTimeout { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("password")]
		public String Password { get; set; }
		
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("verify-server-certificate")]
		public Boolean? VerifyServerCertificate { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceSstpClient Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceSstpClient obj = new InterfaceSstpClient();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface sstp-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceSstpServer : RosItemObject<InterfaceSstpServer>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("user")]
		public String User { get; set; }
		
		[RosValue("client-address", IsDynamic=true)]
		public String ClientAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("encoding", IsDynamic=true)]
		public String Encoding { get; set; }
		
		[RosValue("mtu", IsDynamic=true)]
		public Int32? Mtu { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static InterfaceSstpServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceSstpServer obj = new InterfaceSstpServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface sstp-server server", CanSet=true, CanGet=true)]
	public class InterfaceSstpServerServer : RosValueObject
	{
		[RosValue("authentication")]
		public String Authentication { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		[RosValue("default-profile")]
		public RouterOS.PppProfile DefaultProfile { get; set; }
		
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		public enum KeepaliveTimeoutEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutEnum? KeepaliveTimeout { get; set; }
		
		// Maximum Receive Unit
		[RosValue("max-mru")]
		public Int32? MaxMru { get; set; }
		
		// Maximum Transmission Unit
		[RosValue("max-mtu")]
		public Int32? MaxMtu { get; set; }
		
		public enum MrruEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct MrruType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(Int32? value) 
                { 
                    return new MrruType() { value = value }; 
                }public MrruEnum? State 
                { 
                    get { return value as MrruEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MrruType(MrruEnum? value) 
                { 
                    return new MrruType() { value = value }; 
                } 
                    public static MrruType Parse(string str, Connection conn)
                    {
                        
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MrruType() { value = TypeFormatter.ConvertFromString<MrruEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mrru")]
		public MrruType? Mrru { get; set; }
		
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		[RosValue("verify-client-certificate")]
		public Boolean? VerifyClientCertificate { get; set; }
		
	}
	[RosObject("interface traffic-eng", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceTrafficEng : RosItemObject<InterfaceTrafficEng>
	{
		[RosValue("affinity-exclude", CanUnset=true)]
		public Int32? AffinityExclude { get; set; }
		
		[RosValue("affinity-include-all", CanUnset=true)]
		public Int32? AffinityIncludeAll { get; set; }
		
		[RosValue("affinity-include-any", CanUnset=true)]
		public Int32? AffinityIncludeAny { get; set; }
		
		public enum AutoBandwidthAvgIntervalEnum
		{
			[RosValue("AutoBandwidthAvgInterval")]
			AutoBandwidthAvgInterval,
		}
		
		[RosValue("auto-bandwidth-avg-interval")]
		public AutoBandwidthAvgIntervalEnum? AutoBandwidthAvgInterval { get; set; }
		
		[RosValue("auto-bandwidth-range")]
		public String AutoBandwidthRange { get; set; }
		
		public struct AutoBandwidthReserveType
                {
                    public Int32 Value { get; set; }

                    public static AutoBandwidthReserveType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator AutoBandwidthReserveType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(AutoBandwidthReserveType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("auto-bandwidth-reserve")]
		public AutoBandwidthReserveType? AutoBandwidthReserve { get; set; }
		
		public enum AutoBandwidthUpdateIntervalEnum
		{
			[RosValue("AutoBandwidthUpdateInterval")]
			AutoBandwidthUpdateInterval,
		}
		
		[RosValue("auto-bandwidth-update-interval")]
		public AutoBandwidthUpdateIntervalEnum? AutoBandwidthUpdateInterval { get; set; }
		
		[RosValue("bandwidth")]
		public Bits Bandwidth { get; set; }
		
		[RosValue("bandwidth-limit")]
		public String BandwidthLimit { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum AutoEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct FromAddressType 
                { 
                    private object value; 
                    public AutoEnum? Auto 
                { 
                    get { return value as AutoEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator FromAddressType(AutoEnum? value) 
                { 
                    return new FromAddressType() { value = value }; 
                }public IPAddress Address 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator FromAddressType(IPAddress value) 
                { 
                    return new FromAddressType() { value = value }; 
                } 
                    public static FromAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new FromAddressType() { value = TypeFormatter.ConvertFromString<AutoEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new FromAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("from-address")]
		public FromAddressType? FromAddress { get; set; }
		
		[RosValue("holding-priority", CanUnset=true)]
		public Int32? HoldingPriority { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("primary-path")]
		public String PrimaryPath { get; set; }
		
		[RosValue("primary-retry-interval")]
		public TimeSpan? PrimaryRetryInterval { get; set; }
		
		[RosValue("record-route", CanUnset=true)]
		public Boolean? RecordRoute { get; set; }
		
		[RosValue("reoptimize-interval", CanUnset=true)]
		public TimeSpan? ReoptimizeInterval { get; set; }
		
		[RosValue("secondary-paths")]
		public String SecondaryPaths { get; set; }
		
		[RosValue("setup-priority", CanUnset=true)]
		public Int32? SetupPriority { get; set; }
		
		[RosValue("to-address")]
		public IPAddress ToAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceTrafficEng Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceTrafficEng obj = new InterfaceTrafficEng();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface vlan", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceVlan : RosItemObject<InterfaceVlan>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Physical interface to the network where are VLANs
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Name of the VLAN interface
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("use-service-tag")]
		public Boolean? UseServiceTag { get; set; }
		
		// Virtual LAN identificator or tag that is used to distinguish VLANs
		[RosValue("vlan-id")]
		public Int32? VlanId { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("slave", IsDynamic=true)]
		public String Slave { get; set; }
		
		public static InterfaceVlan Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceVlan obj = new InterfaceVlan();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface vpls", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceVpls : RosItemObject<InterfaceVpls>
	{
		[RosValue("advertised-l2mtu")]
		public Int32? AdvertisedL2mtu { get; set; }
		
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		[RosValue("cisco-style")]
		public Boolean? CiscoStyle { get; set; }
		
		[RosValue("cisco-style-id")]
		public Int32? CiscoStyleId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum PwTypeEnum
		{
			[RosValue("raw-ethernet")]
			RawEthernet,
			[RosValue("tagged-ethernet")]
			TaggedEthernet,
		}
		
		[RosValue("pw-type")]
		public PwTypeEnum? PwType { get; set; }
		
		[RosValue("remote-peer")]
		public IPAddress RemotePeer { get; set; }
		
		[RosValue("vpls-id", CanUnset=true)]
		public String VplsId { get; set; }
		
		[RosValue("bgp-signaled", IsDynamic=true)]
		public String BgpSignaled { get; set; }
		
		[RosValue("cisco-bgp-signaled", IsDynamic=true)]
		public String CiscoBgpSignaled { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		[RosValue("vpls", IsDynamic=true)]
		public String Vpls { get; set; }
		
		public static InterfaceVpls Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceVpls obj = new InterfaceVpls();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface vpls bgp-vpls", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceVplsBgpVpls : RosItemObject<InterfaceVplsBgpVpls>
	{
		public struct BridgeType 
                { 
                    private object value; 
                    public RouterOS.Interface Bridge 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeType(RouterOS.Interface value) 
                { 
                    return new BridgeType() { value = value }; 
                }public RouterOS.Interface Bridge2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeType(RouterOS.Interface value) 
                { 
                    return new BridgeType() { value = value }; 
                } 
                    public static BridgeType Parse(string str, Connection conn)
                    {
                        
                try { return new BridgeType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new BridgeType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bridge")]
		public BridgeType? Bridge { get; set; }
		
		[RosValue("bridge-cost")]
		public Int32? BridgeCost { get; set; }
		
		public enum BridgeHorizonEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct BridgeHorizonType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeHorizonType(Int32? value) 
                { 
                    return new BridgeHorizonType() { value = value }; 
                }public BridgeHorizonEnum? State 
                { 
                    get { return value as BridgeHorizonEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeHorizonType(BridgeHorizonEnum? value) 
                { 
                    return new BridgeHorizonType() { value = value }; 
                } 
                    public static BridgeHorizonType Parse(string str, Connection conn)
                    {
                        
                try { return new BridgeHorizonType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new BridgeHorizonType() { value = TypeFormatter.ConvertFromString<BridgeHorizonEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bridge-horizon")]
		public BridgeHorizonType? BridgeHorizon { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("export-route-targets", CanUnset=true)]
		public String ExportRouteTargets { get; set; }
		
		[RosValue("import-route-targets", CanUnset=true)]
		public String ImportRouteTargets { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("route-distinguisher", CanUnset=true)]
		public String RouteDistinguisher { get; set; }
		
		[RosValue("site-id")]
		public Int32? SiteId { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		public static InterfaceVplsBgpVpls Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceVplsBgpVpls obj = new InterfaceVplsBgpVpls();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface vpls cisco-bgp-vpls", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceVplsCiscoBgpVpls : RosItemObject<InterfaceVplsCiscoBgpVpls>
	{
		public struct BridgeType 
                { 
                    private object value; 
                    public RouterOS.Interface Bridge 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeType(RouterOS.Interface value) 
                { 
                    return new BridgeType() { value = value }; 
                }public RouterOS.Interface Bridge2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeType(RouterOS.Interface value) 
                { 
                    return new BridgeType() { value = value }; 
                } 
                    public static BridgeType Parse(string str, Connection conn)
                    {
                        
                try { return new BridgeType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new BridgeType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bridge")]
		public BridgeType? Bridge { get; set; }
		
		[RosValue("bridge-cost")]
		public Int32? BridgeCost { get; set; }
		
		public enum BridgeHorizonEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct BridgeHorizonType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeHorizonType(Int32? value) 
                { 
                    return new BridgeHorizonType() { value = value }; 
                }public BridgeHorizonEnum? State 
                { 
                    get { return value as BridgeHorizonEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BridgeHorizonType(BridgeHorizonEnum? value) 
                { 
                    return new BridgeHorizonType() { value = value }; 
                } 
                    public static BridgeHorizonType Parse(string str, Connection conn)
                    {
                        
                try { return new BridgeHorizonType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new BridgeHorizonType() { value = TypeFormatter.ConvertFromString<BridgeHorizonEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bridge-horizon")]
		public BridgeHorizonType? BridgeHorizon { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("export-route-targets", CanUnset=true)]
		public String ExportRouteTargets { get; set; }
		
		[RosValue("import-route-targets", CanUnset=true)]
		public String ImportRouteTargets { get; set; }
		
		[RosValue("l2router-id")]
		public IPAddress L2routerId { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("route-distinguisher", CanUnset=true)]
		public String RouteDistinguisher { get; set; }
		
		[RosValue("vpls-id", CanUnset=true)]
		public String VplsId { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		public static InterfaceVplsCiscoBgpVpls Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceVplsCiscoBgpVpls obj = new InterfaceVplsCiscoBgpVpls();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface vrrp", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceVrrp : RosItemObject<InterfaceVrrp>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		public enum AuthenticationEnum
		{
			[RosValue("ah")]
			Ah,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		[RosValue("authentication")]
		public AuthenticationEnum? Authentication { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		public enum IntervalEnum
		{
			[RosValue("Interval")]
			Interval,
		}
		
		[RosValue("interval")]
		public IntervalEnum? Interval { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("on-backup")]
		public String OnBackup { get; set; }
		
		[RosValue("on-master")]
		public String OnMaster { get; set; }
		
		[RosValue("password")]
		public String Password { get; set; }
		
		[RosValue("preemption-mode")]
		public Boolean? PreemptionMode { get; set; }
		
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		public enum V3ProtocolEnum
		{
			[RosValue("ipv4")]
			Ipv4,
			[RosValue("ipv6")]
			Ipv6,
		}
		
		[RosValue("v3-protocol")]
		public V3ProtocolEnum? V3Protocol { get; set; }
		
		public enum VersionEnum
		{
			[RosValue("2")]
			E2,
			[RosValue("3")]
			E3,
		}
		
		[RosValue("version")]
		public VersionEnum? Version { get; set; }
		
		[RosValue("vrid")]
		public Int32? Vrid { get; set; }
		
		[RosValue("backup", IsDynamic=true)]
		public String Backup { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("master", IsDynamic=true)]
		public String Master { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceVrrp Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceVrrp obj = new InterfaceVrrp();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceWireless : RosItemObject<InterfaceWireless>
	{
		public enum AdaptiveNoiseImmunityEnum
		{
			[RosValue("ap-and-client-mode")]
			ApAndClientMode,
			[RosValue("client-mode")]
			ClientMode,
			[RosValue("none")]
			None,
		}
		
		[RosValue("adaptive-noise-immunity")]
		public AdaptiveNoiseImmunityEnum? AdaptiveNoiseImmunity { get; set; }
		
		[RosValue("allow-sharedkey")]
		public Boolean? AllowSharedkey { get; set; }
		
		// Antenna gain in dBi
		[RosValue("antenna-gain")]
		public Int32? AntennaGain { get; set; }
		
		public enum AntennaModeEnum
		{
			[RosValue("ant-a")]
			AntA,
			[RosValue("ant-b")]
			AntB,
			[RosValue("rxa-txb")]
			RxaTxb,
			[RosValue("txa-rxb")]
			TxaRxb,
		}
		
		// Select antenna to use for transmitting and for receiving.
		[RosValue("antenna-mode")]
		public AntennaModeEnum? AntennaMode { get; set; }
		
		[RosValue("area")]
		public String Area { get; set; }
		
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Defines set of used data rates, channel frequencies and widths.
		[RosValue("band")]
		public String Band { get; set; }
		
		// Basic rates in 802.11a or 802.11g standard
		[RosValue("basic-rates-a/g")]
		public String BasicRatesAG { get; set; }
		
		// Basic rates in 802.11b mode
		[RosValue("basic-rates-b")]
		public String BasicRatesB { get; set; }
		
		public enum BridgeModeEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
		}
		
		[RosValue("bridge-mode")]
		public BridgeModeEnum? BridgeMode { get; set; }
		
		public enum BurstTimeEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		// Time in ms which will be used to send data without stopping
		[RosValue("burst-time")]
		public BurstTimeEnum? BurstTime { get; set; }
		
		public enum ChannelWidthEnum
		{
			[RosValue("10mhz")]
			E10mhz,
			[RosValue("20mhz")]
			E20mhz,
			[RosValue("40mhz")]
			E40mhz,
			[RosValue("5mhz")]
			E5mhz,
		}
		
		[RosValue("channel-width")]
		public ChannelWidthEnum? ChannelWidth { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("compression")]
		public Boolean? Compression { get; set; }
		
		public enum CountryEnum
		{
			[RosValue("albania")]
			Albania,
			[RosValue("algeria")]
			Algeria,
			[RosValue("argentina")]
			Argentina,
			[RosValue("armenia")]
			Armenia,
			[RosValue("australia")]
			Australia,
		}
		
		// Limits a frequency and a transmit power to those which are allowed in the respective country
		[RosValue("country")]
		public CountryEnum? Country { get; set; }
		
		[RosValue("default-ap-tx-limit")]
		public Int32? DefaultApTxLimit { get; set; }
		
		// Accept or reject a client that wants to associate, but it's not in the access-list
		[RosValue("default-authentication")]
		public Boolean? DefaultAuthentication { get; set; }
		
		[RosValue("default-client-tx-limit")]
		public Int32? DefaultClientTxLimit { get; set; }
		
		// Defines whether to use default forwarding or not
		[RosValue("default-forwarding")]
		public Boolean? DefaultForwarding { get; set; }
		
		public enum DfsModeEnum
		{
			[RosValue("no-radar-detect")]
			NoRadarDetect,
			[RosValue("none")]
			None,
			[RosValue("radar-detect")]
			RadarDetect,
		}
		
		// Used for APs to dynamically select frequency at which this AP will operate
		[RosValue("dfs-mode")]
		public DfsModeEnum? DfsMode { get; set; }
		
		// Disable or not running check
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum DisconnectTimeoutEnum
		{
			[RosValue("DisconnectTimeout")]
			DisconnectTimeout,
		}
		
		// Only above this value the client device is considered as disconnected
		[RosValue("disconnect-timeout")]
		public DisconnectTimeoutEnum? DisconnectTimeout { get; set; }
		
		public enum DistanceEnum
		{
			[RosValue("dynamic")]
			Dynamic,
			[RosValue("indoors")]
			Indoors,
		}
		
		[RosValue("distance")]
		public DistanceEnum? Distance { get; set; }
		
		[RosValue("frame-lifetime")]
		public Int32? FrameLifetime { get; set; }
		
		// Channel frequency on which AP will operate.
		[RosValue("frequency")]
		public Int32? Frequency { get; set; }
		
		public enum FrequencyModeEnum
		{
			[RosValue("manual-txpower")]
			ManualTxpower,
			[RosValue("regulatory-domain")]
			RegulatoryDomain,
			[RosValue("superchannel")]
			Superchannel,
		}
		
		// Defines which frequency channels to allow
		[RosValue("frequency-mode")]
		public FrequencyModeEnum? FrequencyMode { get; set; }
		
		[RosValue("frequency-offset")]
		public String FrequencyOffset { get; set; }
		
		// Hide SSID string
		[RosValue("hide-ssid")]
		public Boolean? HideSsid { get; set; }
		
		[RosValue("ht-ampdu-priorities")]
		public String HtAmpduPriorities { get; set; }
		
		[RosValue("ht-amsdu-limit")]
		public Int32? HtAmsduLimit { get; set; }
		
		[RosValue("ht-amsdu-threshold")]
		public Int32? HtAmsduThreshold { get; set; }
		
		[RosValue("ht-basic-mcs")]
		public String HtBasicMcs { get; set; }
		
		public enum HtExtensionChannelEnum
		{
			[RosValue("above-control")]
			AboveControl,
			[RosValue("below-control")]
			BelowControl,
			[RosValue("disabled")]
			Disabled,
		}
		
		[RosValue("ht-extension-channel")]
		public HtExtensionChannelEnum? HtExtensionChannel { get; set; }
		
		public enum HtGuardIntervalEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("long")]
			Long,
		}
		
		[RosValue("ht-guard-interval")]
		public HtGuardIntervalEnum? HtGuardInterval { get; set; }
		
		[RosValue("ht-rxchains")]
		public String HtRxchains { get; set; }
		
		[RosValue("ht-supported-mcs")]
		public String HtSupportedMcs { get; set; }
		
		[RosValue("ht-txchains")]
		public String HtTxchains { get; set; }
		
		public enum HwFragmentationThresholdEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct HwFragmentationThresholdType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HwFragmentationThresholdType(Int32? value) 
                { 
                    return new HwFragmentationThresholdType() { value = value }; 
                }public HwFragmentationThresholdEnum? State 
                { 
                    get { return value as HwFragmentationThresholdEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HwFragmentationThresholdType(HwFragmentationThresholdEnum? value) 
                { 
                    return new HwFragmentationThresholdType() { value = value }; 
                } 
                    public static HwFragmentationThresholdType Parse(string str, Connection conn)
                    {
                        
                try { return new HwFragmentationThresholdType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new HwFragmentationThresholdType() { value = TypeFormatter.ConvertFromString<HwFragmentationThresholdEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("hw-fragmentation-threshold")]
		public HwFragmentationThresholdType? HwFragmentationThreshold { get; set; }
		
		public enum HwProtectionModeEnum
		{
			[RosValue("cts-to-self")]
			CtsToSelf,
			[RosValue("none")]
			None,
			[RosValue("rts-cts")]
			RtsCts,
		}
		
		[RosValue("hw-protection-mode")]
		public HwProtectionModeEnum? HwProtectionMode { get; set; }
		
		[RosValue("hw-protection-threshold")]
		public Int32? HwProtectionThreshold { get; set; }
		
		[RosValue("hw-retries")]
		public Int32? HwRetries { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// MAC address of the interface
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Physical wireless interface used by virtual AP.
		[RosValue("master-interface")]
		public String MasterInterface { get; set; }
		
		// The maximum count of the stations
		[RosValue("max-station-count")]
		public Int32? MaxStationCount { get; set; }
		
		public enum ModeEnum
		{
			[RosValue("alignment-only")]
			AlignmentOnly,
			[RosValue("ap-bridge")]
			ApBridge,
			[RosValue("bridge")]
			Bridge,
			[RosValue("nstreme-dual-slave")]
			NstremeDualSlave,
			[RosValue("station")]
			Station,
			[RosValue("wds-slave")]
			WdsSlave,
			[RosValue("station-pseudobridge")]
			StationPseudobridge,
			[RosValue("station-wds")]
			StationWds,
			[RosValue("station-pseudobridge-clone")]
			StationPseudobridgeClone,
		}
		
		// Operate as AP or station 
		[RosValue("mode")]
		public ModeEnum? Mode { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// The name of the interface
		[RosValue("name")]
		public String Name { get; set; }
		
		// Value in dBm below whcih it's rather noise than a normal signal
		[RosValue("noise-floor-threshold")]
		public String NoiseFloorThreshold { get; set; }
		
		[RosValue("nv2-cell-radius")]
		public Int32? Nv2CellRadius { get; set; }
		
		[RosValue("nv2-noise-floor-offset")]
		public String Nv2NoiseFloorOffset { get; set; }
		
		[RosValue("nv2-preshared-key")]
		public String Nv2PresharedKey { get; set; }
		
		public enum Nv2QosEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("frame-priority")]
			FramePriority,
		}
		
		[RosValue("nv2-qos")]
		public Nv2QosEnum? Nv2Qos { get; set; }
		
		[RosValue("nv2-queue-count")]
		public Int32? Nv2QueueCount { get; set; }
		
		public enum Nv2SecurityEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
		}
		
		[RosValue("nv2-security")]
		public Nv2SecurityEnum? Nv2Security { get; set; }
		
		public enum OnFailRetryTimeEnum
		{
			[RosValue("OnFailRetryTime")]
			OnFailRetryTime,
		}
		
		[RosValue("on-fail-retry-time")]
		public OnFailRetryTimeEnum? OnFailRetryTime { get; set; }
		
		public enum PeriodicCalibrationEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
		}
		
		// To ensure performance of chip set over temperature and environmental changes, the software performs periodic calibration
		[RosValue("periodic-calibration")]
		public PeriodicCalibrationEnum? PeriodicCalibration { get; set; }
		
		[RosValue("periodic-calibration-interval")]
		public Int32? PeriodicCalibrationInterval { get; set; }
		
		public enum PreambleModeEnum
		{
			[RosValue("both")]
			Both,
			[RosValue("long")]
			Long,
			[RosValue("short")]
			Short,
		}
		
		// The synchronization field in a wireless packet
		[RosValue("preamble-mode")]
		public PreambleModeEnum? PreambleMode { get; set; }
		
		public enum PrismCardtypeEnum
		{
			[RosValue("100mW")]
			E100mW,
			[RosValue("200mW")]
			E200mW,
			[RosValue("30mW")]
			E30mW,
		}
		
		// The output of the Prism chipset based card
		[RosValue("prism-cardtype")]
		public PrismCardtypeEnum? PrismCardtype { get; set; }
		
		[RosValue("proprietary-extensions")]
		public String ProprietaryExtensions { get; set; }
		
		// A Descriptive name of the card
		[RosValue("radio-name")]
		public String RadioName { get; set; }
		
		public enum RateSetEnum
		{
			[RosValue("configured")]
			Configured,
			[RosValue("default")]
			Default,
		}
		
		// Rate set to use
		[RosValue("rate-set")]
		public RateSetEnum? RateSet { get; set; }
		
		// Set of channel frequencies that will be used by device.
		[RosValue("scan-list")]
		public String ScanList { get; set; }
		
		[RosValue("security-profile")]
		public RouterOS.InterfaceWirelessSecurityProfiles SecurityProfile { get; set; }
		
		// Name that identifies wireless network.
		[RosValue("ssid")]
		public String Ssid { get; set; }
		
		[RosValue("station-bridge-clone-mac")]
		public MACAddress? StationBridgeCloneMac { get; set; }
		
		// Rates to be supported in 802.11a or 802.11g standard
		[RosValue("supported-rates-a/g")]
		public String SupportedRatesAG { get; set; }
		
		// Rates to be supported in 802.11b standard
		[RosValue("supported-rates-b")]
		public String SupportedRatesB { get; set; }
		
		[RosValue("tdma-debug")]
		public Int32? TdmaDebug { get; set; }
		
		public enum TdmaOverrideRateEnum
		{
			[RosValue("12mbps")]
			E12mbps,
			[RosValue("18mbps")]
			E18mbps,
			[RosValue("24mbps")]
			E24mbps,
			[RosValue("36mbps")]
			E36mbps,
			[RosValue("48mbps")]
			E48mbps,
		}
		
		[RosValue("tdma-override-rate")]
		public TdmaOverrideRateEnum? TdmaOverrideRate { get; set; }
		
		[RosValue("tdma-override-size")]
		public Int32? TdmaOverrideSize { get; set; }
		
		[RosValue("tdma-period-size")]
		public Int32? TdmaPeriodSize { get; set; }
		
		// Transmit power in dBm
		[RosValue("tx-power")]
		public String TxPower { get; set; }
		
		public enum TxPowerModeEnum
		{
			[RosValue("all-rates-fixed")]
			AllRatesFixed,
			[RosValue("card-rates")]
			CardRates,
			[RosValue("default")]
			Default,
			[RosValue("manual-table")]
			ManualTable,
		}
		
		// Transmit power mode for the card
		[RosValue("tx-power-mode")]
		public TxPowerModeEnum? TxPowerMode { get; set; }
		
		public enum DisabledEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public enum UpdateStatsIntervalEnum
		{
			[RosValue("Interval")]
			Interval,
		}
		
		public struct UpdateStatsIntervalType 
                { 
                    private object value; 
                    public DisabledEnum? Disabled 
                { 
                    get { return value as DisabledEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator UpdateStatsIntervalType(DisabledEnum? value) 
                { 
                    return new UpdateStatsIntervalType() { value = value }; 
                }public UpdateStatsIntervalEnum? State 
                { 
                    get { return value as UpdateStatsIntervalEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator UpdateStatsIntervalType(UpdateStatsIntervalEnum? value) 
                { 
                    return new UpdateStatsIntervalType() { value = value }; 
                } 
                    public static UpdateStatsIntervalType Parse(string str, Connection conn)
                    {
                        
                try { return new UpdateStatsIntervalType() { value = TypeFormatter.ConvertFromString<DisabledEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new UpdateStatsIntervalType() { value = TypeFormatter.ConvertFromString<UpdateStatsIntervalEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// How often to update statistics in '/interface wireless registration-table'
		[RosValue("update-stats-interval")]
		public UpdateStatsIntervalType? UpdateStatsInterval { get; set; }
		
		[RosValue("wds-cost-range")]
		public String WdsCostRange { get; set; }
		
		public enum TlsCertificateEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct WdsDefaultBridgeType 
                { 
                    private object value; 
                    public TlsCertificateEnum? TlsCertificate 
                { 
                    get { return value as TlsCertificateEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator WdsDefaultBridgeType(TlsCertificateEnum? value) 
                { 
                    return new WdsDefaultBridgeType() { value = value }; 
                }public RouterOS.Interface WdsDefaultBridge 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator WdsDefaultBridgeType(RouterOS.Interface value) 
                { 
                    return new WdsDefaultBridgeType() { value = value }; 
                } 
                    public static WdsDefaultBridgeType Parse(string str, Connection conn)
                    {
                        
                try { return new WdsDefaultBridgeType() { value = TypeFormatter.ConvertFromString<TlsCertificateEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new WdsDefaultBridgeType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Default bridge for WDS interface
		[RosValue("wds-default-bridge")]
		public WdsDefaultBridgeType? WdsDefaultBridge { get; set; }
		
		[RosValue("wds-default-cost")]
		public Int32? WdsDefaultCost { get; set; }
		
		// Whether the AP will create WDS links with any other AP in this frequency
		[RosValue("wds-ignore-ssid")]
		public Boolean? WdsIgnoreSsid { get; set; }
		
		public enum WdsModeEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("dynamic")]
			Dynamic,
			[RosValue("dynamic-mesh")]
			DynamicMesh,
			[RosValue("static")]
			Static,
			[RosValue("static-mesh")]
			StaticMesh,
		}
		
		// WDS mode
		[RosValue("wds-mode")]
		public WdsModeEnum? WdsMode { get; set; }
		
		[RosValue("wireless-protocol")]
		public String WirelessProtocol { get; set; }
		
		public enum WmmSupportEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("required")]
			Required,
		}
		
		[RosValue("wmm-support")]
		public WmmSupportEnum? WmmSupport { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public enum InterfaceTypeType
		{
			[RosValue("Atheros 11N")]
			Atheros11N,
			[RosValue("Atheros AR521...  ")]
			AtherosAR521,
			[RosValue("Atheros")]
			Atheros,
			[RosValue("AR5413")]
			AR5413,
			[RosValue("Atheros AR9271")]
			AtherosAR9271,
			[RosValue("Atheros AR9300")]
			AtherosAR9300,
			[RosValue("Prism")]
			Prism,
			[RosValue("virtual-AP")]
			VirtualAP,
		}
		
		[RosValue("interface-type", IsDynamic=true)]
		public InterfaceTypeType? InterfaceType { get; set; }
		
		public static InterfaceWireless Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWireless obj = new InterfaceWireless();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless access-list", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class InterfaceWirelessAccessList : RosItemObject<InterfaceWirelessAccessList>
	{
		[RosValue("ap-tx-limit")]
		public Int32? ApTxLimit { get; set; }
		
		// Accept or not this client when ir tries to connect
		[RosValue("authentication")]
		public Boolean? Authentication { get; set; }
		
		[RosValue("client-tx-limit")]
		public Int32? ClientTxLimit { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Forward or not the client's frames to other wireless client
		[RosValue("forwarding")]
		public Boolean? Forwarding { get; set; }
		
		public enum InterfaceEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public InterfaceEnum? State 
                { 
                    get { return value as InterfaceEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(InterfaceEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<InterfaceEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// AP interface
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		// MAC address of the client
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("management-protection-key")]
		public String ManagementProtectionKey { get; set; }
		
		public enum PrivateAlgoEnum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// Encryption algorithm to use
		[RosValue("private-algo")]
		public PrivateAlgoEnum? PrivateAlgo { get; set; }
		
		// Private key  of the client to validate during authentication
		[RosValue("private-key")]
		public String PrivateKey { get; set; }
		
		[RosValue("private-pre-shared-key")]
		public String PrivatePreSharedKey { get; set; }
		
		[RosValue("signal-range")]
		public String SignalRange { get; set; }
		
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
	}
	[RosObject("interface wireless align", CanSet=true, CanGet=true)]
	public class InterfaceWirelessAlign : RosValueObject
	{
		// Whether the interface will receive and transmit alignment packets or it will only receive them
		[RosValue("active-mode")]
		public Boolean? ActiveMode { get; set; }
		
		// Signal-strength at which audion frequency will be the highest
		[RosValue("audio-max")]
		public String AudioMax { get; set; }
		
		// Signal-strength at which audion frequency will be the lowest
		[RosValue("audio-min")]
		public String AudioMin { get; set; }
		
		// MAC address of the remote host which will be listened
		[RosValue("audio-monitor")]
		public MACAddress? AudioMonitor { get; set; }
		
		// MAC address of the remote host from which it's  necessary to receive packets
		[RosValue("filter-mac")]
		public MACAddress? FilterMac { get; set; }
		
		// Size of alignment packets that will be transmitted
		[RosValue("frame-size")]
		public Int32? FrameSize { get; set; }
		
		// Number of frames that will be sent per second (in active-mode)
		[RosValue("frames-per-second")]
		public Int32? FramesPerSecond { get; set; }
		
		// Whether the interface gathers the packets about other 802.11 standard packets or only alignment packets
		[RosValue("receive-all")]
		public Boolean? ReceiveAll { get; set; }
		
		// Whether you want to accept packets from hosts with other ssid that yours
		[RosValue("ssid-all")]
		public Boolean? SsidAll { get; set; }
		
	}
	[RosObject("interface wireless connect-list", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class InterfaceWirelessConnectList : RosItemObject<InterfaceWirelessConnectList>
	{
		[RosValue("area-prefix")]
		public String AreaPrefix { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("connect")]
		public Boolean? Connect { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		public enum TlsCertificateEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct SecurityProfileType 
                { 
                    private object value; 
                    public TlsCertificateEnum? TlsCertificate 
                { 
                    get { return value as TlsCertificateEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SecurityProfileType(TlsCertificateEnum? value) 
                { 
                    return new SecurityProfileType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles E5 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SecurityProfileType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new SecurityProfileType() { value = value }; 
                } 
                    public static SecurityProfileType Parse(string str, Connection conn)
                    {
                        
                try { return new SecurityProfileType() { value = TypeFormatter.ConvertFromString<TlsCertificateEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new SecurityProfileType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("security-profile")]
		public SecurityProfileType? SecurityProfile { get; set; }
		
		[RosValue("signal-range")]
		public String SignalRange { get; set; }
		
		[RosValue("ssid")]
		public String Ssid { get; set; }
		
		[RosValue("wireless-protocol")]
		public String WirelessProtocol { get; set; }
		
	}
	[RosObject("interface wireless info", CanGet=true)]
	public class InterfaceWirelessInfo : RosItemObject<InterfaceWirelessInfo>
	{
		public enum CapabilitiesType
		{
			[RosValue("ack-timeout-control")]
			AckTimeoutControl,
			[RosValue("alignment-mode")]
			AlignmentMode,
			[RosValue("burst-support")]
			BurstSupport,
			[RosValue("compression")]
			Compression,
			[RosValue("ht40-a")]
			Ht40A,
			[RosValue("ht40-g")]
			Ht40G,
			[RosValue("no-ant-mode")]
			NoAntMode,
			[RosValue("noise-floor-control")]
			NoiseFloorControl,
			[RosValue("nstreme")]
			Nstreme,
			[RosValue("nv2")]
			Nv2,
			[RosValue("power-channel")]
			PowerChannel,
			[RosValue("scanning")]
			Scanning,
			[RosValue("sniffing")]
			Sniffing,
			[RosValue("spectral-scan")]
			SpectralScan,
			[RosValue("tx-power-control")]
			TxPowerControl,
			[RosValue("virtual-ap")]
			VirtualAp,
			[RosValue("wmm")]
			Wmm,
		}
		
		[RosValue("capabilities", IsDynamic=true)]
		public CapabilitiesType? Capabilities { get; set; }
		
		[RosValue("chip-info", IsDynamic=true)]
		public String ChipInfo { get; set; }
		
		public enum DefaultPeriodicCalibrationType
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
		}
		
		[RosValue("default-periodic-calibration", IsDynamic=true)]
		public DefaultPeriodicCalibrationType? DefaultPeriodicCalibration { get; set; }
		
		[RosValue("firmware", IsDynamic=true)]
		public String Firmware { get; set; }
		
		[RosValue("ht-chains", IsDynamic=true)]
		public Int32? HtChains { get; set; }
		
		public enum InterfaceTypeType
		{
			[RosValue("Atheros 11N")]
			Atheros11N,
			[RosValue("Atheros AR521...  ")]
			AtherosAR521,
			[RosValue("Atheros")]
			Atheros,
			[RosValue("AR5413")]
			AR5413,
			[RosValue("Prism")]
			Prism,
			[RosValue("virtual-AP")]
			VirtualAP,
		}
		
		[RosValue("interface-type", IsDynamic=true)]
		public InterfaceTypeType? InterfaceType { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("pci-info", IsDynamic=true)]
		public String PciInfo { get; set; }
		
		[RosValue("supported-bands", IsDynamic=true)]
		public String SupportedBands { get; set; }
		
		public static InterfaceWirelessInfo Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessInfo obj = new InterfaceWirelessInfo();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless manual-tx-power-table", CanSet=true, CanGet=true)]
	public class InterfaceWirelessManualTxPowerTable : RosItemObject<InterfaceWirelessManualTxPowerTable>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Define tx-power in dBm for each rate, separate by commas
		[RosValue("manual-tx-powers")]
		public String ManualTxPowers { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public static InterfaceWirelessManualTxPowerTable Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessManualTxPowerTable obj = new InterfaceWirelessManualTxPowerTable();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless nstreme", CanSet=true, CanGet=true)]
	public class InterfaceWirelessNstreme : RosItemObject<InterfaceWirelessNstreme>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disable-csma")]
		public Boolean? DisableCsma { get; set; }
		
		// Whether to switch the card into the nstreme mode
		[RosValue("enable-nstreme")]
		public Boolean? EnableNstreme { get; set; }
		
		// Whether to use polling for clients
		[RosValue("enable-polling")]
		public Boolean? EnablePolling { get; set; }
		
		// Maximal frame size
		[RosValue("framer-limit")]
		public Int32? FramerLimit { get; set; }
		
		public enum FramerPolicyEnum
		{
			[RosValue("best-fit")]
			BestFit,
			[RosValue("dynamic-size")]
			DynamicSize,
			[RosValue("exact-size")]
			ExactSize,
			[RosValue("none")]
			None,
		}
		
		// The method how to combine frames
		[RosValue("framer-policy")]
		public FramerPolicyEnum? FramerPolicy { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public static InterfaceWirelessNstreme Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessNstreme obj = new InterfaceWirelessNstreme();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless nstreme-dual", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceWirelessNstremeDual : RosItemObject<InterfaceWirelessNstremeDual>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disable-csma")]
		public Boolean? DisableCsma { get; set; }
		
		// Whether the interface should always be treated as running even if there is no connection to a remote peer
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Maximal frame size
		[RosValue("framer-limit")]
		public Int32? FramerLimit { get; set; }
		
		public enum FramerPolicyEnum
		{
			[RosValue("best-fit")]
			BestFit,
			[RosValue("exact-size")]
			ExactSize,
			[RosValue("none")]
			None,
		}
		
		// The method how to combine frames
		[RosValue("framer-policy")]
		public FramerPolicyEnum? FramerPolicy { get; set; }
		
		public enum HtChannelWidthEnum
		{
			[RosValue("2040mhz")]
			E2040mhz,
			[RosValue("20mhz")]
			E20mhz,
			[RosValue("40mhz")]
			E40mhz,
		}
		
		[RosValue("ht-channel-width")]
		public HtChannelWidthEnum? HtChannelWidth { get; set; }
		
		public enum HtGuardIntervalEnum
		{
			[RosValue("both")]
			Both,
			[RosValue("long")]
			Long,
			[RosValue("short")]
			Short,
		}
		
		[RosValue("ht-guard-interval")]
		public HtGuardIntervalEnum? HtGuardInterval { get; set; }
		
		[RosValue("ht-rates")]
		public String HtRates { get; set; }
		
		public enum HtStreamsEnum
		{
			[RosValue("both")]
			Both,
			[RosValue("double")]
			Double,
			[RosValue("single")]
			Single,
		}
		
		[RosValue("ht-streams")]
		public HtStreamsEnum? HtStreams { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Reference name of the interface
		[RosValue("name")]
		public String Name { get; set; }
		
		// Rates to be supported in 802.11a or 802.11g standard
		[RosValue("rates-a/g")]
		public String RatesAG { get; set; }
		
		// Rates to be supported in 802.11b standard
		[RosValue("rates-b")]
		public String RatesB { get; set; }
		
		// Which MAC address to connect to (this would be the remote receiver card's MAC address)
		[RosValue("remote-mac")]
		public MACAddress? RemoteMac { get; set; }
		
		public enum RxBandEnum
		{
			[RosValue("2ghz-b")]
			E2ghzB,
			[RosValue("2ghz-g")]
			E2ghzG,
			[RosValue("2ghz-n")]
			E2ghzN,
			[RosValue("5ghz-a")]
			E5ghzA,
			[RosValue("5ghz-n")]
			E5ghzN,
		}
		
		// Operating band of the receiving radio
		[RosValue("rx-band")]
		public RxBandEnum? RxBand { get; set; }
		
		public enum RxChannelWidthEnum
		{
			[RosValue("10mhz")]
			E10mhz,
			[RosValue("20mhz")]
			E20mhz,
			[RosValue("40mhz")]
			E40mhz,
			[RosValue("5mhz")]
			E5mhz,
		}
		
		[RosValue("rx-channel-width")]
		public RxChannelWidthEnum? RxChannelWidth { get; set; }
		
		// Frequency to use for receiving frames
		[RosValue("rx-frequency")]
		public Int32? RxFrequency { get; set; }
		
		// Which radio should be used for receiving frames
		[RosValue("rx-radio")]
		public String RxRadio { get; set; }
		
		public enum TxBandEnum
		{
			[RosValue("2ghz-b")]
			E2ghzB,
			[RosValue("2ghz-g")]
			E2ghzG,
			[RosValue("2ghz-n")]
			E2ghzN,
			[RosValue("5ghz-a")]
			E5ghzA,
			[RosValue("5ghz-n")]
			E5ghzN,
		}
		
		// Operating band of the transmitting radio
		[RosValue("tx-band")]
		public TxBandEnum? TxBand { get; set; }
		
		public enum TxChannelWidthEnum
		{
			[RosValue("10mhz")]
			E10mhz,
			[RosValue("20mhz")]
			E20mhz,
			[RosValue("40mhz")]
			E40mhz,
			[RosValue("5mhz")]
			E5mhz,
		}
		
		[RosValue("tx-channel-width")]
		public TxChannelWidthEnum? TxChannelWidth { get; set; }
		
		// Frequency to use for transmitting frames
		[RosValue("tx-frequency")]
		public Int32? TxFrequency { get; set; }
		
		// Which radio should be used for transmitting frames
		[RosValue("tx-radio")]
		public String TxRadio { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceWirelessNstremeDual Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessNstremeDual obj = new InterfaceWirelessNstremeDual();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless registration-table", CanGet=true, CanRemove=true)]
	public class InterfaceWirelessRegistrationTable : RosItemObject<InterfaceWirelessRegistrationTable>
	{
		[RosValue("ap", IsDynamic=true)]
		public String Ap { get; set; }
		
		public enum AuthenticationTypeType
		{
			[RosValue("wpa-eap")]
			WpaEap,
			[RosValue("wpa-psk")]
			WpaPsk,
			[RosValue("wpa2-eap")]
			Wpa2Eap,
			[RosValue("wpa2-psk")]
			Wpa2Psk,
		}
		
		[RosValue("authentication-type", IsDynamic=true)]
		public AuthenticationTypeType? AuthenticationType { get; set; }
		
		[RosValue("client-tx-limit", IsDynamic=true)]
		public Int32? ClientTxLimit { get; set; }
		
		[RosValue("compression", IsDynamic=true)]
		public String Compression { get; set; }
		
		public enum EncryptionType
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		[RosValue("encryption", IsDynamic=true)]
		public EncryptionType? Encryption { get; set; }
		
		[RosValue("frames", IsDynamic=true)]
		public Int32? Frames { get; set; }
		
		[RosValue("framing-limit", IsDynamic=true)]
		public Int32? FramingLimit { get; set; }
		
		public enum GroupEncryptionType
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		[RosValue("group-encryption", IsDynamic=true)]
		public GroupEncryptionType? GroupEncryption { get; set; }
		
		[RosValue("hw-frames", IsDynamic=true)]
		public Int32? HwFrames { get; set; }
		
		[RosValue("last-activity", IsDynamic=true)]
		public TimeSpan? LastActivity { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("nstreme", IsDynamic=true)]
		public String Nstreme { get; set; }
		
		[RosValue("packed-bytes", IsDynamic=true)]
		public Int32? PackedBytes { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("routeros-version", IsDynamic=true)]
		public String RouterosVersion { get; set; }
		
		[RosValue("rx-rate", IsDynamic=true)]
		public String RxRate { get; set; }
		
		[RosValue("signal-to-noise", IsDynamic=true)]
		public String SignalToNoise { get; set; }
		
		[RosValue("tdma-retx", IsDynamic=true)]
		public Int32? TdmaRetx { get; set; }
		
		[RosValue("tdma-timing-offset", IsDynamic=true)]
		public String TdmaTimingOffset { get; set; }
		
		[RosValue("tdma-winfull", IsDynamic=true)]
		public Int32? TdmaWinfull { get; set; }
		
		[RosValue("tx-frames-timed-out", IsDynamic=true)]
		public Int32? TxFramesTimedOut { get; set; }
		
		[RosValue("tx-signal-strength", IsDynamic=true)]
		public String TxSignalStrength { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		[RosValue("wmm-enabled", IsDynamic=true)]
		public String WmmEnabled { get; set; }
		
		[RosValue("ack-timeout", IsDynamic=true)]
		public Int32? AckTimeout { get; set; }
		
		[RosValue("ap-tx-limit", IsDynamic=true)]
		public Int32? ApTxLimit { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int32? Bytes { get; set; }
		
		[RosValue("comment", IsDynamic=true)]
		public String Comment { get; set; }
		
		[RosValue("distance", IsDynamic=true)]
		public Int32? Distance { get; set; }
		
		[RosValue("frame-bytes", IsDynamic=true)]
		public Int32? FrameBytes { get; set; }
		
		[RosValue("framing-current-size", IsDynamic=true)]
		public Int32? FramingCurrentSize { get; set; }
		
		public enum FramingModeType
		{
			[RosValue("best-fit")]
			BestFit,
			[RosValue("exact-size")]
			ExactSize,
			[RosValue("none")]
			None,
		}
		
		[RosValue("framing-mode", IsDynamic=true)]
		public FramingModeType? FramingMode { get; set; }
		
		[RosValue("hw-frame-bytes", IsDynamic=true)]
		public Int32? HwFrameBytes { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("last-ip", IsDynamic=true)]
		public IPAddress LastIp { get; set; }
		
		[RosValue("management-protection", IsDynamic=true)]
		public String ManagementProtection { get; set; }
		
		[RosValue("p-throughput", IsDynamic=true)]
		public Int32? PThroughput { get; set; }
		
		[RosValue("packed-frames", IsDynamic=true)]
		public Int32? PackedFrames { get; set; }
		
		[RosValue("radio-name", IsDynamic=true)]
		public String RadioName { get; set; }
		
		public struct RxCcqType
                {
                    public Int32 Value { get; set; }

                    public static RxCcqType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator RxCcqType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(RxCcqType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("rx-ccq", IsDynamic=true)]
		public RxCcqType? RxCcq { get; set; }
		
		[RosValue("signal-strength-ch0", IsDynamic=true)]
		public String SignalStrengthCh0 { get; set; }
		
		[RosValue("signal-strength-ch1", IsDynamic=true)]
		public String SignalStrengthCh1 { get; set; }
		
		[RosValue("signal-strength-ch2", IsDynamic=true)]
		public String SignalStrengthCh2 { get; set; }
		
		[RosValue("strength-at-rates", IsDynamic=true)]
		public String StrengthAtRates { get; set; }
		
		[RosValue("tdma-rx-size", IsDynamic=true)]
		public Int32? TdmaRxSize { get; set; }
		
		[RosValue("tdma-tx-size", IsDynamic=true)]
		public Int32? TdmaTxSize { get; set; }
		
		public struct TxCcqType
                {
                    public Int32 Value { get; set; }

                    public static TxCcqType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator TxCcqType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(TxCcqType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("tx-ccq", IsDynamic=true)]
		public TxCcqType? TxCcq { get; set; }
		
		[RosValue("tx-rate", IsDynamic=true)]
		public String TxRate { get; set; }
		
		[RosValue("tx-signal-strength-ch0", IsDynamic=true)]
		public String TxSignalStrengthCh0 { get; set; }
		
		[RosValue("tx-signal-strength-ch1", IsDynamic=true)]
		public String TxSignalStrengthCh1 { get; set; }
		
		[RosValue("tx-signal-strength-ch2", IsDynamic=true)]
		public String TxSignalStrengthCh2 { get; set; }
		
		[RosValue("wds", IsDynamic=true)]
		public String Wds { get; set; }
		
	}
	[RosObject("interface wireless security-profiles", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceWirelessSecurityProfiles : RosItemObject<InterfaceWirelessSecurityProfiles>
	{
		[RosValue("authentication-types")]
		public String AuthenticationTypes { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum MethodEnum
		{
			[RosValue("eap-tls")]
			EapTls,
			[RosValue("passthrough")]
			Passthrough,
		}
		
		[RosValue("eap-methods")]
		public MethodEnum[] EapMethods { get; set; }
		
		[RosValue("group-ciphers")]
		public String GroupCiphers { get; set; }
		
		public enum GroupKeyUpdateEnum
		{
			[RosValue("GroupKeyUpdate")]
			GroupKeyUpdate,
		}
		
		// How often to update group key
		[RosValue("group-key-update")]
		public GroupKeyUpdateEnum? GroupKeyUpdate { get; set; }
		
		[RosValue("interim-update")]
		public TimeSpan? InterimUpdate { get; set; }
		
		public enum ManagementProtectionEnum
		{
			[RosValue("allowed")]
			Allowed,
			[RosValue("disabled")]
			Disabled,
			[RosValue("required")]
			Required,
		}
		
		[RosValue("management-protection")]
		public ManagementProtectionEnum? ManagementProtection { get; set; }
		
		[RosValue("management-protection-key")]
		public String ManagementProtectionKey { get; set; }
		
		public enum ModeEnum
		{
			[RosValue("dynamic-keys")]
			DynamicKeys,
			[RosValue("none")]
			None,
			[RosValue("static-keys-optional")]
			StaticKeysOptional,
			[RosValue("static-keys-required")]
			StaticKeysRequired,
		}
		
		// Security mode
		[RosValue("mode")]
		public ModeEnum? Mode { get; set; }
		
		// Descriptive name for the security profile
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("radius-eap-accounting")]
		public Boolean? RadiusEapAccounting { get; set; }
		
		[RosValue("radius-mac-accounting")]
		public Boolean? RadiusMacAccounting { get; set; }
		
		// Whether to use Radius server MAC authentication or not
		[RosValue("radius-mac-authentication")]
		public Boolean? RadiusMacAuthentication { get; set; }
		
		public enum RadiusMacCachingDisableEnum
		{
			[RosValue("disabled")]
			Disabled,
		}
		
		public struct RadiusMacCachingType 
                { 
                    private object value; 
                    public RadiusMacCachingDisableEnum? RadiusMacCachingDisable 
                { 
                    get { return value as RadiusMacCachingDisableEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RadiusMacCachingType(RadiusMacCachingDisableEnum? value) 
                { 
                    return new RadiusMacCachingType() { value = value }; 
                }public TimeSpan? RadiusMacCachingTime 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RadiusMacCachingType(TimeSpan? value) 
                { 
                    return new RadiusMacCachingType() { value = value }; 
                } 
                    public static RadiusMacCachingType Parse(string str, Connection conn)
                    {
                        
                try { return new RadiusMacCachingType() { value = TypeFormatter.ConvertFromString<RadiusMacCachingDisableEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new RadiusMacCachingType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("radius-mac-caching")]
		public RadiusMacCachingType? RadiusMacCaching { get; set; }
		
		[RosValue("radius-mac-format")]
		public String RadiusMacFormat { get; set; }
		
		public enum RadiusMacModeEnum
		{
			[RosValue("as-username")]
			AsUsername,
			[RosValue("as-username-and-password")]
			AsUsernameAndPassword,
		}
		
		[RosValue("radius-mac-mode")]
		public RadiusMacModeEnum? RadiusMacMode { get; set; }
		
		public enum StaticAlgo0Enum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// 1st encryption algorithm to use
		[RosValue("static-algo-0")]
		public StaticAlgo0Enum? StaticAlgo0 { get; set; }
		
		public enum StaticAlgo1Enum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// 2nd encryption algorithm to use
		[RosValue("static-algo-1")]
		public StaticAlgo1Enum? StaticAlgo1 { get; set; }
		
		public enum StaticAlgo2Enum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// 3rd encryption algorithm to use
		[RosValue("static-algo-2")]
		public StaticAlgo2Enum? StaticAlgo2 { get; set; }
		
		public enum StaticAlgo3Enum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// 4th encryption algorithm to use
		[RosValue("static-algo-3")]
		public StaticAlgo3Enum? StaticAlgo3 { get; set; }
		
		// Hexadecimal key which will be used to encrypt packets with algo-0
		[RosValue("static-key-0")]
		public String StaticKey0 { get; set; }
		
		// Hexadecimal key which will be used to encrypt packets with algo-1
		[RosValue("static-key-1")]
		public String StaticKey1 { get; set; }
		
		// Hexadecimal key which will be used to encrypt packets with algo-2
		[RosValue("static-key-2")]
		public String StaticKey2 { get; set; }
		
		// Hexadecimal key which will be used to encrypt packets with algo-3
		[RosValue("static-key-3")]
		public String StaticKey3 { get; set; }
		
		public enum StaticStaPrivateAlgoEnum
		{
			[RosValue("104bit-wep")]
			E104bitWep,
			[RosValue("40bit-wep")]
			E40bitWep,
			[RosValue("aes-ccm")]
			AesCcm,
			[RosValue("none")]
			None,
			[RosValue("tkip")]
			Tkip,
		}
		
		// Algorithm to use if the sta-private-key is set
		[RosValue("static-sta-private-algo")]
		public StaticStaPrivateAlgoEnum? StaticStaPrivateAlgo { get; set; }
		
		// Private encryption key for the station
		[RosValue("static-sta-private-key")]
		public String StaticStaPrivateKey { get; set; }
		
		public enum StaticTransmitKeyEnum
		{
			[RosValue("key-0")]
			Key0,
			[RosValue("key-1")]
			Key1,
			[RosValue("key-2")]
			Key2,
			[RosValue("key-3")]
			Key3,
		}
		
		// Key to use for broadcast packets
		[RosValue("static-transmit-key")]
		public StaticTransmitKeyEnum? StaticTransmitKey { get; set; }
		
		[RosValue("supplicant-identity")]
		public String SupplicantIdentity { get; set; }
		
		[RosValue("tls-certificate")]
		public String TlsCertificate { get; set; }
		
		public enum TlsModeEnum
		{
			[RosValue("dont-verify-certificate")]
			DontVerifyCertificate,
			[RosValue("no-certificates")]
			NoCertificates,
			[RosValue("verify-certificate")]
			VerifyCertificate,
		}
		
		[RosValue("tls-mode")]
		public TlsModeEnum? TlsMode { get; set; }
		
		[RosValue("unicast-ciphers")]
		public String UnicastCiphers { get; set; }
		
		[RosValue("wpa-pre-shared-key")]
		public String WpaPreSharedKey { get; set; }
		
		[RosValue("wpa2-pre-shared-key")]
		public String Wpa2PreSharedKey { get; set; }
		
		public static InterfaceWirelessSecurityProfiles DefaultSecurityProfiles
		{
			get { return new InterfaceWirelessSecurityProfiles() { Id = 0 }; }
		}
		
		public static InterfaceWirelessSecurityProfiles Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessSecurityProfiles obj = new InterfaceWirelessSecurityProfiles();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("interface wireless sniffer", CanSet=true, CanGet=true)]
	public class InterfaceWirelessSniffer : RosValueObject
	{
		// How long to sniff each channel, if multiple-channels is set to yes
		[RosValue("channel-time")]
		public TimeSpan? ChannelTime { get; set; }
		
		// Limits file-name's file size in KB
		[RosValue("file-limit")]
		public Int32? FileLimit { get; set; }
		
		// Name of the file where to save packets in PCAP format
		[RosValue("file-name")]
		public String FileName { get; set; }
		
		// How much memory to use for sniffed packets in KB
		[RosValue("memory-limit")]
		public Int32? MemoryLimit { get; set; }
		
		// Whether to sniff multiple channels or a single channel
		[RosValue("multiple-channels")]
		public Boolean? MultipleChannels { get; set; }
		
		// Sniff only wireless packet heders
		[RosValue("only-headers")]
		public Boolean? OnlyHeaders { get; set; }
		
		// Whether to receive packets with CRC errors
		[RosValue("receive-errors")]
		public Boolean? ReceiveErrors { get; set; }
		
		// Whether to send packets to server in TZSP format
		[RosValue("streaming-enabled")]
		public Boolean? StreamingEnabled { get; set; }
		
		// How many packets per second the router will accept
		[RosValue("streaming-max-rate")]
		public Int32? StreamingMaxRate { get; set; }
		
		// Streaming server's IP address
		[RosValue("streaming-server")]
		public IPAddress StreamingServer { get; set; }
		
	}
	[RosObject("interface wireless sniffer packet", CanGet=true)]
	public class InterfaceWirelessSnifferPacket : RosItemObject<InterfaceWirelessSnifferPacket>
	{
		public enum BandType
		{
			[RosValue("2ghz-10mhz")]
			E2ghz10mhz,
			[RosValue("2ghz-40mhz")]
			E2ghz40mhz,
			[RosValue("2ghz-5mhz")]
			E2ghz5mhz,
			[RosValue("2ghz-b")]
			E2ghzB,
			[RosValue("2ghz-g")]
			E2ghzG,
			[RosValue("2ghz-n")]
			E2ghzN,
			[RosValue("2ghz-n-10mhz")]
			E2ghzN10mhz,
			[RosValue("2ghz-n-5mhz")]
			E2ghzN5mhz,
			[RosValue("5ghz-10mhz")]
			E5ghz10mhz,
			[RosValue("5ghz-40mhz")]
			E5ghz40mhz,
			[RosValue("5ghz-5mhz")]
			E5ghz5mhz,
			[RosValue("5ghz-a")]
			E5ghzA,
			[RosValue("5ghz-n")]
			E5ghzN,
			[RosValue("5ghz-n-10mhz")]
			E5ghzN10mhz,
			[RosValue("5ghz-n-5mhz")]
			E5ghzN5mhz,
		}
		
		[RosValue("band", IsDynamic=true)]
		public BandType? Band { get; set; }
		
		[RosValue("crc-error", IsDynamic=true)]
		public String CrcError { get; set; }
		
		[RosValue("dst", IsDynamic=true)]
		public MACAddress? Dst { get; set; }
		
		[RosValue("freq", IsDynamic=true)]
		public Int32? Freq { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("signal-at-rate", IsDynamic=true)]
		public String SignalAtRate { get; set; }
		
		[RosValue("src", IsDynamic=true)]
		public MACAddress? Src { get; set; }
		
		[RosValue("time", IsDynamic=true)]
		public Single? Time { get; set; }
		
		public enum TypeType
		{
			[RosValue("ack")]
			Ack,
			[RosValue("assoc-req")]
			AssocReq,
			[RosValue("assoc-resp")]
			AssocResp,
			[RosValue("atim")]
			Atim,
			[RosValue("auth")]
			Auth,
			[RosValue("beacon")]
			Beacon,
			[RosValue("cf-end")]
			CfEnd,
			[RosValue("cf-endack")]
			CfEndack,
			[RosValue("cts")]
			Cts,
			[RosValue("d-cfack")]
			DCfack,
			[RosValue("d-cfackpoll")]
			DCfackpoll,
			[RosValue("d-cfpoll")]
			DCfpoll,
			[RosValue("data")]
			Data,
			[RosValue("data-null")]
			DataNull,
			[RosValue("deauth")]
			Deauth,
			[RosValue("disassoc")]
			Disassoc,
			[RosValue("nd-cfack")]
			NdCfack,
			[RosValue("nd-cfackpoll")]
			NdCfackpoll,
			[RosValue("nd-cfpoll")]
			NdCfpoll,
			[RosValue("probe-req")]
			ProbeReq,
			[RosValue("probe-resp")]
			ProbeResp,
			[RosValue("ps-poll")]
			PsPoll,
			[RosValue("reassoc-req")]
			ReassocReq,
			[RosValue("reassoc-resp")]
			ReassocResp,
			[RosValue("rts")]
			Rts,
		}
		
		[RosValue("type", IsDynamic=true)]
		public TypeType? Type { get; set; }
		
	}
	[RosObject("interface wireless snooper", CanSet=true, CanGet=true)]
	public class InterfaceWirelessSnooper : RosValueObject
	{
		// How long to snoop each channel, if multiple-channels is set to 'yes'
		[RosValue("channel-time")]
		public TimeSpan? ChannelTime { get; set; }
		
		// Whether to snoop multiple channels or a single channel
		[RosValue("multiple-channels")]
		public Boolean? MultipleChannels { get; set; }
		
		// Whether to receive packets with CRC errors
		[RosValue("receive-errors")]
		public Boolean? ReceiveErrors { get; set; }
		
	}
	[RosObject("interface wireless wds", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class InterfaceWirelessWds : RosItemObject<InterfaceWirelessWds>
	{
		public enum ArpEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
			[RosValue("proxy-arp")]
			ProxyArp,
			[RosValue("reply-only")]
			ReplyOnly,
		}
		
		// Address Resolution Protocol
		[RosValue("arp")]
		public ArpEnum? Arp { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disable-running-check")]
		public Boolean? DisableRunningCheck { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("l2mtu")]
		public UInt16? L2mtu { get; set; }
		
		// Wireless interface which will be used by WDS
		[RosValue("master-interface")]
		public RouterOS.Interface MasterInterface { get; set; }
		
		// Maximum Transmit Unit
		[RosValue("mtu")]
		public Int32? Mtu { get; set; }
		
		// Interface name
		[RosValue("name")]
		public String Name { get; set; }
		
		// MAC address of the remote WDS host
		[RosValue("wds-address")]
		public MACAddress? WdsAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("running", IsDynamic=true)]
		public String Running { get; set; }
		
		public static InterfaceWirelessWds Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			InterfaceWirelessWds obj = new InterfaceWirelessWds();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip accounting", CanSet=true, CanGet=true)]
	public class IpAccounting : RosValueObject
	{
		[RosValue("account-local-traffic")]
		public Boolean? AccountLocalTraffic { get; set; }
		
		// Defines whether the accounting is enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// maximum number of IP pairs for the traffic accounting table
		[RosValue("threshold")]
		public Int32? Threshold { get; set; }
		
	}
	[RosObject("ip accounting snapshot", CanGet=true)]
	public class IpAccountingSnapshot : RosItemObject<IpAccountingSnapshot>
	{
		[RosValue("bytes", IsDynamic=true)]
		public Int32? Bytes { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPAddress DstAddress { get; set; }
		
		[RosValue("dst-user", IsDynamic=true)]
		public String DstUser { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("src-address", IsDynamic=true)]
		public IPAddress SrcAddress { get; set; }
		
		[RosValue("src-user", IsDynamic=true)]
		public String SrcUser { get; set; }
		
	}
	[RosObject("ip accounting web-access", CanSet=true, CanGet=true)]
	public class IpAccountingWebAccess : RosValueObject
	{
		// If yes, the router can be accessed from web
		[RosValue("accessible-via-web")]
		public Boolean? AccessibleViaWeb { get; set; }
		
		// Address from which the router can be accessed
		[RosValue("address")]
		public IPPrefix? Address { get; set; }
		
	}
	[RosObject("ip address", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpAddress : RosItemObject<IpAddress>
	{
		// Local IP address
		[RosValue("address")]
		public String Address { get; set; }
		
		// Broadcast address
		[RosValue("broadcast")]
		public IPAddress Broadcast { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Network mask
		[RosValue("netmask")]
		public IPAddress Netmask { get; set; }
		
		// Network prefix
		[RosValue("network")]
		public IPAddress Network { get; set; }
		
		[RosValue("actual-interface", IsDynamic=true)]
		public RouterOS.Interface ActualInterface { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ip arp", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpArp : RosItemObject<IpArp>
	{
		// IP address
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// MAC address
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ip dhcp-client", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpDhcpClient : RosItemObject<IpDhcpClient>
	{
		// Defines whether to add the default route to the gateway specified by DHCP server
		[RosValue("add-default-route")]
		public Boolean? AddDefaultRoute { get; set; }
		
		// Client identifier (optional)
		[RosValue("client-id")]
		public String ClientId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("default-route-distance", CanUnset=true)]
		public Int32? DefaultRouteDistance { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Client host name (optional)
		[RosValue("host-name")]
		public String HostName { get; set; }
		
		// Interface name
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Whether to accept the DNS settings advertized by DHCP server
		[RosValue("use-peer-dns")]
		public Boolean? UsePeerDns { get; set; }
		
		// Whether to accept the NTP settings advertized by DHCP server
		[RosValue("use-peer-ntp")]
		public Boolean? UsePeerNtp { get; set; }
		
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("dhcp-server", IsDynamic=true)]
		public IPAddress DhcpServer { get; set; }
		
		[RosValue("expires-after", IsDynamic=true)]
		public TimeSpan? ExpiresAfter { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("netmask", IsDynamic=true)]
		public IPAddress Netmask { get; set; }
		
		[RosValue("primary-dns", IsDynamic=true)]
		public IPAddress PrimaryDns { get; set; }
		
		[RosValue("primary-ntp", IsDynamic=true)]
		public IPAddress PrimaryNtp { get; set; }
		
		[RosValue("secondary-dns", IsDynamic=true)]
		public IPAddress SecondaryDns { get; set; }
		
		[RosValue("secondary-ntp", IsDynamic=true)]
		public IPAddress SecondaryNtp { get; set; }
		
		public enum StatusType
		{
			[RosValue("bound")]
			Bound,
			[RosValue("error")]
			Error,
			[RosValue("stopped")]
			Stopped,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
	}
	[RosObject("ip dhcp-relay", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpRelay : RosItemObject<IpDhcpRelay>
	{
		public enum ConstEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct DelayThresholdType 
                { 
                    private object value; 
                    public ConstEnum? Const 
                { 
                    get { return value as ConstEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DelayThresholdType(ConstEnum? value) 
                { 
                    return new DelayThresholdType() { value = value }; 
                }public TimeSpan? Time 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DelayThresholdType(TimeSpan? value) 
                { 
                    return new DelayThresholdType() { value = value }; 
                } 
                    public static DelayThresholdType Parse(string str, Connection conn)
                    {
                        
                try { return new DelayThresholdType() { value = TypeFormatter.ConvertFromString<ConstEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new DelayThresholdType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// If secs field in DHCP packet is smaller than delay-threshold, then this packet is ignored
		[RosValue("delay-threshold")]
		public DelayThresholdType? DelayThreshold { get; set; }
		
		// List of DHCP servers
		[RosValue("dhcp-server")]
		public IPAddress[] DhcpServer { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Local address for relay identification
		[RosValue("local-address")]
		public IPAddress LocalAddress { get; set; }
		
		// Name of DHCP relay
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		public static IpDhcpRelay Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDhcpRelay obj = new IpDhcpRelay();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip dhcp-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpServer : RosItemObject<IpDhcpServer>
	{
		// Defines whether to add dynamic ARP entry
		[RosValue("add-arp")]
		public Boolean? AddArp { get; set; }
		
		// IP address pool
		[RosValue("address-pool")]
		public String AddressPool { get; set; }
		
		// Send all replies as broadcast
		[RosValue("always-broadcast")]
		public Boolean? AlwaysBroadcast { get; set; }
		
		public enum AuthoritativeEnum
		{
			[RosValue("after-10sec-delay")]
			After10secDelay,
			[RosValue("after-2sec-delay")]
			After2secDelay,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		// This is the only DHCP server for the network
		[RosValue("authoritative")]
		public AuthoritativeEnum? Authoritative { get; set; }
		
		public enum EnumEnum
		{
			[RosValue("forever")]
			Forever,
			[RosValue("lease-time")]
			LeaseTime,
		}
		
		public struct BootpLeaseTimeType 
                { 
                    private object value; 
                    public EnumEnum? Enum 
                { 
                    get { return value as EnumEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BootpLeaseTimeType(EnumEnum? value) 
                { 
                    return new BootpLeaseTimeType() { value = value }; 
                }public TimeSpan? Time 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BootpLeaseTimeType(TimeSpan? value) 
                { 
                    return new BootpLeaseTimeType() { value = value }; 
                } 
                    public static BootpLeaseTimeType Parse(string str, Connection conn)
                    {
                        
                try { return new BootpLeaseTimeType() { value = TypeFormatter.ConvertFromString<EnumEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new BootpLeaseTimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bootp-lease-time")]
		public BootpLeaseTimeType? BootpLeaseTime { get; set; }
		
		public enum BootpSupportEnum
		{
			[RosValue("dynamic")]
			Dynamic,
			[RosValue("none")]
			None,
			[RosValue("static")]
			Static,
		}
		
		// Support for BOOTP clients
		[RosValue("bootp-support")]
		public BootpSupportEnum? BootpSupport { get; set; }
		
		public struct DelayThresholdType 
                { 
                    private object value; 
                    public EnumEnum? Enum 
                { 
                    get { return value as EnumEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DelayThresholdType(EnumEnum? value) 
                { 
                    return new DelayThresholdType() { value = value }; 
                }public TimeSpan? Time 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DelayThresholdType(TimeSpan? value) 
                { 
                    return new DelayThresholdType() { value = value }; 
                } 
                    public static DelayThresholdType Parse(string str, Connection conn)
                    {
                        
                try { return new DelayThresholdType() { value = TypeFormatter.ConvertFromString<EnumEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new DelayThresholdType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// If secs field in DHCP packet is smaller than delay-threshold, then this packet is ignored
		[RosValue("delay-threshold")]
		public DelayThresholdType? DelayThreshold { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Lease time
		[RosValue("lease-time")]
		public TimeSpan? LeaseTime { get; set; }
		
		// Name of DHCP server
		[RosValue("name")]
		public String Name { get; set; }
		
		// DHCP relay address
		[RosValue("relay")]
		public IPAddress Relay { get; set; }
		
		// Source address
		[RosValue("src-address")]
		public IPAddress SrcAddress { get; set; }
		
		// Use RADIUS server for authentication
		[RosValue("use-radius")]
		public Boolean? UseRadius { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		public static IpDhcpServer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDhcpServer obj = new IpDhcpServer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip dhcp-server alert", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpServerAlert : RosItemObject<IpDhcpServerAlert>
	{
		public enum SymbolicNamesEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct AlertTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AlertTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new AlertTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AlertTimeoutType(TimeSpan? value) 
                { 
                    return new AlertTimeoutType() { value = value }; 
                } 
                    public static AlertTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new AlertTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new AlertTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Time, after which alert will be forgotten
		[RosValue("alert-timeout")]
		public AlertTimeoutType? AlertTimeout { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface, on which to run rogue DHCP server finder
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Script to run, when an unknown DHCP server is detected
		[RosValue("on-alert")]
		public String OnAlert { get; set; }
		
		// List of MAC addresses of valid DHCP servers
		[RosValue("valid-server")]
		public MACAddress[] ValidServer { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("unknown-server", IsDynamic=true)]
		public MACAddress? UnknownServer { get; set; }
		
	}
	[RosObject("ip dhcp-server config", CanSet=true, CanGet=true)]
	public class IpDhcpServerConfig : RosValueObject
	{
		public enum SymbolicNamesEnum
		{
			[RosValue("immediately")]
			Immediately,
			[RosValue("never")]
			Never,
		}
		
		public struct StoreLeasesDiskType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StoreLeasesDiskType(SymbolicNamesEnum? value) 
                { 
                    return new StoreLeasesDiskType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StoreLeasesDiskType(TimeSpan? value) 
                { 
                    return new StoreLeasesDiskType() { value = value }; 
                } 
                    public static StoreLeasesDiskType Parse(string str, Connection conn)
                    {
                        
                try { return new StoreLeasesDiskType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new StoreLeasesDiskType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// How frequently leases should be stored on disk
		[RosValue("store-leases-disk")]
		public StoreLeasesDiskType? StoreLeasesDisk { get; set; }
		
	}
	[RosObject("ip dhcp-server lease", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpServerLease : RosItemObject<IpDhcpServerLease>
	{
		// Assigns an individual address to the client
		[RosValue("address")]
		public String Address { get; set; }
		
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		// Send all replies as broadcast
		[RosValue("always-broadcast")]
		public Boolean? AlwaysBroadcast { get; set; }
		
		// Block access for this client
		[RosValue("block-access")]
		public Boolean? BlockAccess { get; set; }
		
		// Client identifier
		[RosValue("client-id")]
		public String ClientId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Lease time
		[RosValue("lease-time")]
		public TimeSpan? LeaseTime { get; set; }
		
		// MAC address
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// Bit rate limit for the client
		[RosValue("rate-limit")]
		public String RateLimit { get; set; }
		
		public enum ServerEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct ServerType 
                { 
                    private object value; 
                    public ServerEnum? Server 
                { 
                    get { return value as ServerEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ServerType(ServerEnum? value) 
                { 
                    return new ServerType() { value = value }; 
                }public RouterOS.IpDhcpServer E7 
                { 
                    get { return value as RouterOS.IpDhcpServer; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ServerType(RouterOS.IpDhcpServer value) 
                { 
                    return new ServerType() { value = value }; 
                } 
                    public static ServerType Parse(string str, Connection conn)
                    {
                        
                try { return new ServerType() { value = TypeFormatter.ConvertFromString<ServerEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new ServerType() { value = TypeFormatter.ConvertFromString<RouterOS.IpDhcpServer>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Server name which serves this client
		[RosValue("server")]
		public ServerType? Server { get; set; }
		
		// Use source mac address instead
		[RosValue("use-src-mac")]
		public Boolean? UseSrcMac { get; set; }
		
		[RosValue("active-address", IsDynamic=true)]
		public IPAddress ActiveAddress { get; set; }
		
		[RosValue("active-client-id", IsDynamic=true)]
		public String ActiveClientId { get; set; }
		
		[RosValue("active-mac-address", IsDynamic=true)]
		public MACAddress? ActiveMacAddress { get; set; }
		
		[RosValue("active-server", IsDynamic=true)]
		public RouterOS.IpDhcpServer ActiveServer { get; set; }
		
		[RosValue("agent-circuit-id", IsDynamic=true)]
		public String AgentCircuitId { get; set; }
		
		[RosValue("agent-remote-id", IsDynamic=true)]
		public String AgentRemoteId { get; set; }
		
		[RosValue("blocked", IsDynamic=true)]
		public String Blocked { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("expires-after", IsDynamic=true)]
		public TimeSpan? ExpiresAfter { get; set; }
		
		[RosValue("host-name", IsDynamic=true)]
		public String HostName { get; set; }
		
		[RosValue("last-seen", IsDynamic=true)]
		public TimeSpan? LastSeen { get; set; }
		
		[RosValue("radius", IsDynamic=true)]
		public String Radius { get; set; }
		
		[RosValue("src-mac-address", IsDynamic=true)]
		public MACAddress? SrcMacAddress { get; set; }
		
		public enum StatusType
		{
			[RosValue("authorizing")]
			Authorizing,
			[RosValue("bound")]
			Bound,
			[RosValue("busy")]
			Busy,
			[RosValue("offered")]
			Offered,
			[RosValue("testing")]
			Testing,
			[RosValue("waiting")]
			Waiting,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
	}
	[RosObject("ip dhcp-server network", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpServerNetwork : RosItemObject<IpDhcpServerNetwork>
	{
		// Network address
		[RosValue("address")]
		public String Address { get; set; }
		
		// Boot file name
		[RosValue("boot-file-name")]
		public String BootFileName { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("dhcp-option")]
		public RouterOS.IpDhcpServerOption[] DhcpOption { get; set; }
		
		// DNS server address
		[RosValue("dns-server")]
		public IPAddress[] DnsServer { get; set; }
		
		// Domain name
		[RosValue("domain")]
		public String Domain { get; set; }
		
		// Default gateway
		[RosValue("gateway")]
		public IPAddress[] Gateway { get; set; }
		
		// Network mask to give out
		[RosValue("netmask")]
		public String Netmask { get; set; }
		
		// IP address of next server to use in bootstrap
		[RosValue("next-server")]
		public IPAddress NextServer { get; set; }
		
		// NTP server
		[RosValue("ntp-server")]
		public IPAddress[] NtpServer { get; set; }
		
		// WINS server
		[RosValue("wins-server")]
		public IPAddress[] WinsServer { get; set; }
		
	}
	[RosObject("ip dhcp-server option", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpDhcpServerOption : RosItemObject<IpDhcpServerOption>
	{
		// DHCP option code
		[RosValue("code")]
		public Int32? Code { get; set; }
		
		// Descriptive name of the option
		[RosValue("name")]
		public String Name { get; set; }
		
		// Parameter's value in form of a string
		[RosValue("value")]
		public String Value { get; set; }
		
		public static IpDhcpServerOption Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDhcpServerOption obj = new IpDhcpServerOption();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip dns", CanSet=true, CanGet=true)]
	public class IpDns : RosValueObject
	{
		// Allow requests from network
		[RosValue("allow-remote-requests")]
		public Boolean? AllowRemoteRequests { get; set; }
		
		// Specifies maximum time-to-live for cache records
		[RosValue("cache-max-ttl")]
		public TimeSpan? CacheMaxTtl { get; set; }
		
		// DNS cache size in kB
		[RosValue("cache-size")]
		public String CacheSize { get; set; }
		
		[RosValue("max-udp-packet-size")]
		public Int32? MaxUdpPacketSize { get; set; }
		
		public struct ServerType 
                { 
                    private object value; 
                    public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ServerType(IPAddress value) 
                { 
                    return new ServerType() { value = value }; 
                }public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ServerType(IPv6Address value) 
                { 
                    return new ServerType() { value = value }; 
                } 
                    public static ServerType Parse(string str, Connection conn)
                    {
                        
                try { return new ServerType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new ServerType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("servers")]
		public ServerType[] Servers { get; set; }
		
	}
	[RosObject("ip dns cache", CanSet=true, CanGet=true)]
	public class IpDnsCache : RosItemObject<IpDnsCache>
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPAddress value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Address value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address of DNS host
		[RosValue("address")]
		public AddressType? Address { get; set; }
		
		// The name of DNS host
		[RosValue("name")]
		public String Name { get; set; }
		
		// Time To Live
		[RosValue("ttl")]
		public TimeSpan? Ttl { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		public static IpDnsCache Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDnsCache obj = new IpDnsCache();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip dns cache all", CanSet=true, CanGet=true)]
	public class IpDnsCacheAll : RosItemObject<IpDnsCacheAll>
	{
		// The data which is in the specific resource record
		[RosValue("data")]
		public String Data { get; set; }
		
		// Time value that specifies the upper limit on the time interval that can elapse before the zone is no longer authoritative
		[RosValue("expire")]
		public Int32? Expire { get; set; }
		
		// Minimum TTL field that should be exported with any record from this zone
		[RosValue("minimum")]
		public Int32? Minimum { get; set; }
		
		// The name of the DNS entry
		[RosValue("name")]
		public String Name { get; set; }
		
		// Time interval before the zone should be refreshed
		[RosValue("refresh")]
		public Int32? Refresh { get; set; }
		
		// Person responsible for this zone
		[RosValue("responsible")]
		public String Responsible { get; set; }
		
		// Time interval that should elapse before a failed refresh should be retried
		[RosValue("retry")]
		public Int32? Retry { get; set; }
		
		// Version number of original copy of the zone
		[RosValue("serial")]
		public Int32? Serial { get; set; }
		
		// Time To Live
		[RosValue("ttl")]
		public TimeSpan? Ttl { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("A")]
			A,
			[RosValue("AAAA")]
			AAAA,
			[RosValue("CNAME")]
			CNAME,
			[RosValue("HINFO")]
			HINFO,
			[RosValue("MB")]
			MB,
		}
		
		// The type of the resource record
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("negative", IsDynamic=true)]
		public String Negative { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		public static IpDnsCacheAll Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDnsCacheAll obj = new IpDnsCacheAll();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip dns static", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class IpDnsStatic : RosItemObject<IpDnsStatic>
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPAddress value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Address value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address
		[RosValue("address")]
		public AddressType? Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Host name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Time To Live
		[RosValue("ttl")]
		public TimeSpan? Ttl { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("regexp", IsDynamic=true)]
		public String Regexp { get; set; }
		
		public static IpDnsStatic Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpDnsStatic obj = new IpDnsStatic();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip firewall address-list", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpFirewallAddressList : RosItemObject<IpFirewallAddressList>
	{
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("list")]
		public String List { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
	}
	[RosObject("ip firewall calea", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpFirewallCalea : RosItemObject<IpFirewallCalea>
	{
		public enum ActionEnum
		{
			[RosValue("sniff")]
			Sniff,
			[RosValue("sniff-pc")]
			SniffPc,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
			[RosValue("postrouting")]
			Postrouting,
			[RosValue("prerouting")]
			Prerouting,
		}
		
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		[RosValue("dst-address-type", CanUnset=true)]
		public String DstAddressType { get; set; }
		
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("fragment", CanUnset=true)]
		public Boolean? Fragment { get; set; }
		
		[RosValue("hotspot", CanUnset=true)]
		public String Hotspot { get; set; }
		
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum Ipv4OptionsEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("loose-source-routing")]
			LooseSourceRouting,
			[RosValue("no-record-route")]
			NoRecordRoute,
			[RosValue("no-router-alert")]
			NoRouterAlert,
			[RosValue("no-source-routing")]
			NoSourceRouting,
		}
		
		[RosValue("ipv4-options", CanUnset=true)]
		public Ipv4OptionsEnum? Ipv4Options { get; set; }
		
		[RosValue("layer7-protocol", CanUnset=true)]
		public String Layer7Protocol { get; set; }
		
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		[RosValue("psd", CanUnset=true)]
		public String Psd { get; set; }
		
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("routing-table", CanUnset=true)]
		public String RoutingTable { get; set; }
		
		[RosValue("sniff-id")]
		public Int32? SniffId { get; set; }
		
		[RosValue("sniff-target")]
		public IPAddress SniffTarget { get; set; }
		
		[RosValue("sniff-target-port")]
		public Int32? SniffTargetPort { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		[RosValue("src-address-type", CanUnset=true)]
		public String SrcAddressType { get; set; }
		
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		[RosValue("ttl", CanUnset=true)]
		public String Ttl { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
	}
	[RosObject("ip firewall connection", CanGet=true, CanRemove=true)]
	public class IpFirewallConnection : RosItemObject<IpFirewallConnection>
	{
		[RosValue("assured", IsDynamic=true)]
		public String Assured { get; set; }
		
		[RosValue("connection-mark", IsDynamic=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-type", IsDynamic=true)]
		public String ConnectionType { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPAddress DstAddress { get; set; }
		
		[RosValue("gre-key", IsDynamic=true)]
		public Int32? GreKey { get; set; }
		
		[RosValue("gre-protocol", IsDynamic=true)]
		public Int32? GreProtocol { get; set; }
		
		[RosValue("gre-version", IsDynamic=true)]
		public Int32? GreVersion { get; set; }
		
		[RosValue("icmp-code", IsDynamic=true)]
		public Int32? IcmpCode { get; set; }
		
		[RosValue("icmp-id", IsDynamic=true)]
		public Int32? IcmpId { get; set; }
		
		[RosValue("icmp-type", IsDynamic=true)]
		public Int32? IcmpType { get; set; }
		
		public enum P2pType
		{
			[RosValue("bit-torrent")]
			BitTorrent,
			[RosValue("blubster")]
			Blubster,
			[RosValue("direct-connect")]
			DirectConnect,
			[RosValue("edonkey")]
			Edonkey,
			[RosValue("fasttrack")]
			Fasttrack,
			[RosValue("gnutella")]
			Gnutella,
			[RosValue("none")]
			None,
			[RosValue("soulseek")]
			Soulseek,
			[RosValue("winmx")]
			Winmx,
		}
		
		[RosValue("p2p", IsDynamic=true)]
		public P2pType? P2p { get; set; }
		
		public enum ProtocolType
		{
			[RosValue("ddp")]
			Ddp,
			[RosValue("egp")]
			Egp,
			[RosValue("encap")]
			Encap,
			[RosValue("ggp")]
			Ggp,
			[RosValue("gre")]
			Gre,
			[RosValue("hmp")]
			Hmp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("icmpv6")]
			Icmpv6,
			[RosValue("idpr-cmtp")]
			IdprCmtp,
			[RosValue("igmp")]
			Igmp,
			[RosValue("ipencap")]
			Ipencap,
			[RosValue("ipip")]
			Ipip,
			[RosValue("ipsec-ah")]
			IpsecAh,
			[RosValue("ipsec-esp")]
			IpsecEsp,
			[RosValue("iso-tp4")]
			IsoTp4,
			[RosValue("ospf")]
			Ospf,
			[RosValue("pim")]
			Pim,
			[RosValue("pup")]
			Pup,
			[RosValue("rdp")]
			Rdp,
			[RosValue("rspf")]
			Rspf,
			[RosValue("st")]
			St,
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
			[RosValue("vmtp")]
			Vmtp,
			[RosValue("vrrp")]
			Vrrp,
			[RosValue("xns-idp")]
			XnsIdp,
			[RosValue("xtp")]
			Xtp,
		}
		
		[RosValue("protocol", IsDynamic=true)]
		public ProtocolType? Protocol { get; set; }
		
		[RosValue("reply-dst-address", IsDynamic=true)]
		public IPAddress ReplyDstAddress { get; set; }
		
		[RosValue("reply-src-address", IsDynamic=true)]
		public IPAddress ReplySrcAddress { get; set; }
		
		public enum SeenType
		{
			[RosValue("assured")]
			Assured,
			[RosValue("connection-mark")]
			ConnectionMark,
			[RosValue("connection-type")]
			ConnectionType,
			[RosValue("dst-address")]
			DstAddress,
			[RosValue("gre-key")]
			GreKey,
			[RosValue("gre-protocol")]
			GreProtocol,
			[RosValue("gre-version")]
			GreVersion,
			[RosValue("icmp-code")]
			IcmpCode,
			[RosValue("icmp-id")]
			IcmpId,
			[RosValue("icmp-type")]
			IcmpType,
			[RosValue("p2p")]
			P2p,
			[RosValue("protocol")]
			Protocol,
			[RosValue("reply-dst-address")]
			ReplyDstAddress,
			[RosValue("reply-src-address")]
			ReplySrcAddress,
			[RosValue("seen")]
			Seen,
			[RosValue("reply")]
			Reply,
			[RosValue("src-address")]
			SrcAddress,
			[RosValue("tcp-state")]
			TcpState,
			[RosValue("timeout")]
			Timeout,
		}
		
		[RosValue("seen", IsDynamic=true)]
		public SeenType? Seen { get; set; }
		
		public enum ReplyType
		{
			[RosValue("assured")]
			Assured,
			[RosValue("connection-mark")]
			ConnectionMark,
			[RosValue("connection-type")]
			ConnectionType,
			[RosValue("dst-address")]
			DstAddress,
			[RosValue("gre-key")]
			GreKey,
			[RosValue("gre-protocol")]
			GreProtocol,
			[RosValue("gre-version")]
			GreVersion,
			[RosValue("icmp-code")]
			IcmpCode,
			[RosValue("icmp-id")]
			IcmpId,
			[RosValue("icmp-type")]
			IcmpType,
			[RosValue("p2p")]
			P2p,
			[RosValue("protocol")]
			Protocol,
			[RosValue("reply-dst-address")]
			ReplyDstAddress,
			[RosValue("reply-src-address")]
			ReplySrcAddress,
			[RosValue("seen")]
			Seen,
			[RosValue("reply")]
			Reply,
			[RosValue("src-address")]
			SrcAddress,
			[RosValue("tcp-state")]
			TcpState,
			[RosValue("timeout")]
			Timeout,
		}
		
		[RosValue("reply", IsDynamic=true)]
		public ReplyType? Reply { get; set; }
		
		[RosValue("src-address", IsDynamic=true)]
		public IPAddress SrcAddress { get; set; }
		
		public enum TcpStateType
		{
			[RosValue("close")]
			Close,
			[RosValue("close-wait")]
			CloseWait,
			[RosValue("established")]
			Established,
			[RosValue("fin-wait")]
			FinWait,
			[RosValue("last-ack")]
			LastAck,
			[RosValue("listen")]
			Listen,
			[RosValue("none")]
			None,
			[RosValue("syn-recv")]
			SynRecv,
			[RosValue("syn-sent")]
			SynSent,
			[RosValue("time-wait")]
			TimeWait,
		}
		
		[RosValue("tcp-state", IsDynamic=true)]
		public TcpStateType? TcpState { get; set; }
		
		[RosValue("timeout", IsDynamic=true)]
		public TimeSpan? Timeout { get; set; }
		
	}
	[RosObject("ip firewall connection tracking", CanSet=true, CanGet=true)]
	public class IpFirewallConnectionTracking : RosValueObject
	{
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		[RosValue("generic-timeout")]
		public TimeSpan? GenericTimeout { get; set; }
		
		[RosValue("icmp-timeout")]
		public TimeSpan? IcmpTimeout { get; set; }
		
		[RosValue("tcp-close-timeout")]
		public TimeSpan? TcpCloseTimeout { get; set; }
		
		[RosValue("tcp-close-wait-timeout")]
		public TimeSpan? TcpCloseWaitTimeout { get; set; }
		
		[RosValue("tcp-established-timeout")]
		public TimeSpan? TcpEstablishedTimeout { get; set; }
		
		[RosValue("tcp-fin-wait-timeout")]
		public TimeSpan? TcpFinWaitTimeout { get; set; }
		
		[RosValue("tcp-last-ack-timeout")]
		public TimeSpan? TcpLastAckTimeout { get; set; }
		
		[RosValue("tcp-syn-received-timeout")]
		public TimeSpan? TcpSynReceivedTimeout { get; set; }
		
		[RosValue("tcp-syn-sent-timeout")]
		public TimeSpan? TcpSynSentTimeout { get; set; }
		
		[RosValue("tcp-syncookie")]
		public Boolean? TcpSyncookie { get; set; }
		
		[RosValue("tcp-time-wait-timeout")]
		public TimeSpan? TcpTimeWaitTimeout { get; set; }
		
		[RosValue("udp-stream-timeout")]
		public TimeSpan? UdpStreamTimeout { get; set; }
		
		[RosValue("udp-timeout")]
		public TimeSpan? UdpTimeout { get; set; }
		
	}
	[RosObject("ip firewall filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpFirewallFilter : RosItemObject<IpFirewallFilter>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("add-dst-to-address-list")]
			AddDstToAddressList,
			[RosValue("add-src-to-address-list")]
			AddSrcToAddressList,
			[RosValue("drop")]
			Drop,
			[RosValue("jump")]
			Jump,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Address list in which marked address put
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		// Time interval after which address remove from address list
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		// The name of the chain through which packets are traversing
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		// Match packets with given bytes or byte range
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		// Restrict connection limit per address or address block
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		// Matches packets marked via mangle facility with particular connection mark
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		public enum ConnectionStateEnum
		{
			[RosValue("established")]
			Established,
			[RosValue("invalid")]
			Invalid,
			[RosValue("new")]
			New,
			[RosValue("related")]
			Related,
		}
		
		// Interprets the connection tracking analysis data for a particular packet
		[RosValue("connection-state", CanUnset=true)]
		public ConnectionStateEnum? ConnectionState { get; set; }
		
		// Match packets with given connection type
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		// The text packets should contain in order to match the rule
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		// Destination address with mask
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination address list name in which packet place
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		// Destination address type
		[RosValue("dst-address-type", CanUnset=true)]
		public String DstAddressType { get; set; }
		
		// Packet limitation per time with burst to dst-address, dst-port or src-address
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		// Destination port number or range
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("fragment", CanUnset=true)]
		public Boolean? Fragment { get; set; }
		
		// Matches packets received from clients against various Hot-Spot
		[RosValue("hotspot", CanUnset=true)]
		public String Hotspot { get; set; }
		
		// Matches ICMP Type:Code fields
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		// Interface the packet has entered the router through
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum Ipv4OptionsEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("loose-source-routing")]
			LooseSourceRouting,
			[RosValue("no-record-route")]
			NoRecordRoute,
			[RosValue("no-router-alert")]
			NoRouterAlert,
			[RosValue("no-source-routing")]
			NoSourceRouting,
		}
		
		// Match ipv4 header options
		[RosValue("ipv4-options", CanUnset=true)]
		public Ipv4OptionsEnum? Ipv4Options { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		// Name of the target chain, if the action=jump is used
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("layer7-protocol", CanUnset=true)]
		public String Layer7Protocol { get; set; }
		
		// Setup burst, how many times to use it in during time interval measured in seconds
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Creates all logs with specific prefix
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Match nth packets received by the rule
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		// Matches the bridge port physical output device added to a bridge device
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		// Interface the packet has leaved the router through
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		// P2P program to match
		[RosValue("p2p", CanUnset=true)]
		public String P2p { get; set; }
		
		// Matches packets marked via mangle facility with particular packet mark
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet size or range in bytes
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		// Protocol name or number
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		// Detect TCP un UDP scans
		[RosValue("psd", CanUnset=true)]
		public String Psd { get; set; }
		
		// Match packets randomly with given propability
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		public enum RejectWithEnum
		{
			[RosValue("icmp-admin-prohibited")]
			IcmpAdminProhibited,
			[RosValue("icmp-host-prohibited")]
			IcmpHostProhibited,
			[RosValue("icmp-host-unreachable")]
			IcmpHostUnreachable,
			[RosValue("icmp-net-prohibited")]
			IcmpNetProhibited,
			[RosValue("icmp-network-unreachable")]
			IcmpNetworkUnreachable,
		}
		
		// Alters the reply packet of reject action
		[RosValue("reject-with")]
		public RejectWithEnum? RejectWith { get; set; }
		
		// Matches packets marked by mangle facility with particular routing mark
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("routing-table", CanUnset=true)]
		public String RoutingTable { get; set; }
		
		// Source address with mask
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Matches source address of a packet against user-defined address list
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		// Source IP address type
		[RosValue("src-address-type", CanUnset=true)]
		public String SrcAddressType { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source-port number
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// TCP flags to match
		[RosValue("tcp-flags", CanUnset=true)]
		public String TcpFlags { get; set; }
		
		// TCP Maximum Segment Size value
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		// Packet arrival time and date or locally generated packets departure time and date
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		[RosValue("ttl", CanUnset=true)]
		public String Ttl { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
	}
	[RosObject("ip firewall layer7-protocol", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpFirewallLayer7Protocol : RosItemObject<IpFirewallLayer7Protocol>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("regexp")]
		public String Regexp { get; set; }
		
		public static IpFirewallLayer7Protocol Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpFirewallLayer7Protocol obj = new IpFirewallLayer7Protocol();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip firewall mangle", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpFirewallMangle : RosItemObject<IpFirewallMangle>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("add-dst-to-address-list")]
			AddDstToAddressList,
			[RosValue("add-src-to-address-list")]
			AddSrcToAddressList,
			[RosValue("change-dscp")]
			ChangeDscp,
			[RosValue("change-mss")]
			ChangeMss,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Address list in which marked address put
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		// Time interval after which address remove from address list
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
			[RosValue("postrouting")]
			Postrouting,
			[RosValue("prerouting")]
			Prerouting,
		}
		
		// Chains through which packets are traversing
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		// Match packets with given bytes or byte range
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		// Restrict connection limit per address or address block
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		// Mark connection
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		public enum ConnectionStateEnum
		{
			[RosValue("established")]
			Established,
			[RosValue("invalid")]
			Invalid,
			[RosValue("new")]
			New,
			[RosValue("related")]
			Related,
		}
		
		[RosValue("connection-state", CanUnset=true)]
		public ConnectionStateEnum? ConnectionState { get; set; }
		
		// Match packets with given connection type
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		// The text packets should contain in order to match the rule
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		// Destination address with mask
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination address list name in which packet will be placed
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		// Destination address type
		[RosValue("dst-address-type", CanUnset=true)]
		public String DstAddressType { get; set; }
		
		// Packet limitation per time with burst to dst-address, dst-port or src-address
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		// Destination port number or range
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("fragment", CanUnset=true)]
		public Boolean? Fragment { get; set; }
		
		// Matches packets received from clients against various Hot-Spot
		[RosValue("hotspot", CanUnset=true)]
		public String Hotspot { get; set; }
		
		// Matches ICMP Type:Code fields
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		// Matches the bridge port physical input device added to a bridge device
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		// Interface the packet has entered the router through
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum Ipv4OptionsEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("loose-source-routing")]
			LooseSourceRouting,
			[RosValue("no-record-route")]
			NoRecordRoute,
			[RosValue("no-router-alert")]
			NoRouterAlert,
			[RosValue("no-source-routing")]
			NoSourceRouting,
		}
		
		// Match ipv4 header options
		[RosValue("ipv4-options", CanUnset=true)]
		public Ipv4OptionsEnum? Ipv4Options { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
			[RosValue("postrouting")]
			Postrouting,
			[RosValue("prerouting")]
			Prerouting,
		}
		
		// Name of the target chain, if the action=jump is used
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("layer7-protocol", CanUnset=true)]
		public String Layer7Protocol { get; set; }
		
		// Setup burst, how many times to use it in during time interval measured in seconds
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Creates all logs with specific prefix
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Specify the new value of the connection mark to be used in conjunction with action=mark-connection
		[RosValue("new-connection-mark")]
		public String NewConnectionMark { get; set; }
		
		[RosValue("new-dscp")]
		public Int32? NewDscp { get; set; }
		
		public enum MssActionEnum
		{
			[RosValue("clamp-to-pmtu")]
			ClampToPmtu,
		}
		
		public struct NewMssType 
                { 
                    private object value; 
                    public MssActionEnum? MssAction 
                { 
                    get { return value as MssActionEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewMssType(MssActionEnum? value) 
                { 
                    return new NewMssType() { value = value }; 
                }public Int32? MssValue 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewMssType(Int32? value) 
                { 
                    return new NewMssType() { value = value }; 
                } 
                    public static NewMssType Parse(string str, Connection conn)
                    {
                        
                try { return new NewMssType() { value = TypeFormatter.ConvertFromString<MssActionEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new NewMssType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Specify MSS value to be used in conjunction with action=change-mss
		[RosValue("new-mss")]
		public NewMssType? NewMss { get; set; }
		
		// Specify the new value of the packet mark to be used in conjunction with action=mark-packet
		[RosValue("new-packet-mark")]
		public String NewPacketMark { get; set; }
		
		public enum NewPriorityEnum
		{
			[RosValue("from-dscp")]
			FromDscp,
			[RosValue("from-ingress")]
			FromIngress,
		}
		
		[RosValue("new-priority")]
		public NewPriorityEnum? NewPriority { get; set; }
		
		// Specify the new value of the routing mark used in conjunction with action=mark-routing
		[RosValue("new-routing-mark")]
		public String NewRoutingMark { get; set; }
		
		// Specify the new TTL field value used in conjunction with action=change-ttl
		[RosValue("new-ttl")]
		public String NewTtl { get; set; }
		
		// Match nth packets received by the rule
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		// Matches the bridge port physical output device added to a bridge device
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		// Interface the packet will leave the router through
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		// P2P program to match
		[RosValue("p2p", CanUnset=true)]
		public String P2p { get; set; }
		
		// Matches packets marked via mangle facility with particular packet mark
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet size or range in bytes
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("passthrough")]
		public Boolean? Passthrough { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		// Protocol name or number
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		// Detect TCP un UDP scans
		[RosValue("psd", CanUnset=true)]
		public String Psd { get; set; }
		
		// Match packets randomly with given propability
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		// Mark name for policy routing
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("routing-table", CanUnset=true)]
		public String RoutingTable { get; set; }
		
		// Source address with mask
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Source address list name in which packet will be placed
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		// Source IP address type
		[RosValue("src-address-type", CanUnset=true)]
		public String SrcAddressType { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source-port number
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// TCP flags to match
		[RosValue("tcp-flags", CanUnset=true)]
		public String TcpFlags { get; set; }
		
		// TCP Maximum Segment Size value
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		// Packet arrival time and date or locally generated packets departure time and date
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		[RosValue("ttl", CanUnset=true)]
		public String Ttl { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
	}
	[RosObject("ip firewall nat", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpFirewallNat : RosItemObject<IpFirewallNat>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("add-dst-to-address-list")]
			AddDstToAddressList,
			[RosValue("add-src-to-address-list")]
			AddSrcToAddressList,
			[RosValue("dst-nat")]
			DstNat,
			[RosValue("jump")]
			Jump,
		}
		
		// Action to undertake if the packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Specifies the name of the address list to collect IP addresses from rules having
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		// Time interval after which the address will be removed from the address list specified by address-list parameter
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("dstnat")]
			Dstnat,
			[RosValue("srcnat")]
			Srcnat,
		}
		
		// The name of the chain through which packets are traversing
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		// Match packets with given bytes or byte range
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		// Restrict connection limit per address or address block
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		// Matches packets marked via mangle facility with particular connection mark
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		// Match packets with given connection type
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		// The text packets should contain in order to match the rule
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		// Destination address with mask
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination address list name in which packet place
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		// Destination address type
		[RosValue("dst-address-type", CanUnset=true)]
		public String DstAddressType { get; set; }
		
		// Packet limitation per time with burst to dst-address, dst-port or src-address
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		// Destination port number or range
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("fragment", CanUnset=true)]
		public Boolean? Fragment { get; set; }
		
		// Matches packets received from clients against various Hot-Spot
		[RosValue("hotspot", CanUnset=true)]
		public String Hotspot { get; set; }
		
		// Matches ICMP Type:Code fields
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		// Matches the bridge port physical input device added to a bridge device
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		// Interface the packet has entered the router through
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum Ipv4OptionsEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("loose-source-routing")]
			LooseSourceRouting,
			[RosValue("no-record-route")]
			NoRecordRoute,
			[RosValue("no-router-alert")]
			NoRouterAlert,
			[RosValue("no-source-routing")]
			NoSourceRouting,
		}
		
		// Match ipv4 header options
		[RosValue("ipv4-options", CanUnset=true)]
		public Ipv4OptionsEnum? Ipv4Options { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("dstnat")]
			Dstnat,
			[RosValue("srcnat")]
			Srcnat,
		}
		
		// Name of the target chain, if the action=jump is used
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("layer7-protocol", CanUnset=true)]
		public String Layer7Protocol { get; set; }
		
		// Setup burst, how many times to use it in during time interval measured in seconds
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		// Creates all logs with specific prefix
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		// Match nth packets received by the rule
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		// Matches the bridge port physical output device added to a bridge device
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		// Interface the packet has leaved the router through
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		// Matches packets marked via mangle facility with particular packet mark
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		// Packet size or range in bytes
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		// Destination port
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		// Protocol name or number
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		// Detect TCP un UDP scans
		[RosValue("psd", CanUnset=true)]
		public String Psd { get; set; }
		
		// Match packets randomly with given propability
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		// Matches packets marked by mangle facility with particular routing mark
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("routing-table", CanUnset=true)]
		public String RoutingTable { get; set; }
		
		// Specifies whether to account or not to account for destination IP address when selecting a new source IP address for packets matched by rules with action=same
		[RosValue("same-not-by-dst")]
		public Boolean? SameNotByDst { get; set; }
		
		// Source address with mask
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Matches source address of a packet against user-defined address list
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		// Source IP address type
		[RosValue("src-address-type", CanUnset=true)]
		public String SrcAddressType { get; set; }
		
		// Source MAC address
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		// Source-port number
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		// TCP Maximum Segment Size value
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		// Packet arrival time and date or locally generated packets departure time and date
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		public struct ToAddressesType
                {
                    public IPAddress Min { get; set; }
                    public IPAddress Max { get; set; }

                    public static ToAddressesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ToAddressesType() { Min = TypeFormatter.ConvertFromString<IPAddress>(strs[0], null), Max = TypeFormatter.ConvertFromString<IPAddress>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ToAddressesType() { Min = TypeFormatter.ConvertFromString<IPAddress>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(Max != null)
                            return String.Concat(Min, '-', Max);
                        else
                            return Min.ToString();
                    }
                }
		// Address or address range to replace original address of an IP packet with
		[RosValue("to-addresses", CanUnset=true)]
		public ToAddressesType? ToAddresses { get; set; }
		
		public struct ToPortsType
                {
                    public Int32 Min { get; set; }
                    public Int32 Max { get; set; }

                    public static ToPortsType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ToPortsType() { Min = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Max = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ToPortsType() { Min = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(Max != null)
                            return String.Concat(Min, '-', Max);
                        else
                            return Min.ToString();
                    }
                }
		// Port or port range to replace original port of an IP packet with
		[RosValue("to-ports", CanUnset=true)]
		public ToPortsType? ToPorts { get; set; }
		
		[RosValue("ttl", CanUnset=true)]
		public String Ttl { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
	}
	[RosObject("ip firewall service-port", CanSet=true, CanGet=true)]
	public class IpFirewallServicePort : RosItemObject<IpFirewallServicePort>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Port number
		[RosValue("ports")]
		public Int32[] Ports { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public static IpFirewallServicePort FtpServicePort
		{
			get { return new IpFirewallServicePort() { Id = 1 }; }
		}
		
		public static IpFirewallServicePort TftpServicePort
		{
			get { return new IpFirewallServicePort() { Id = 2 }; }
		}
		
		public static IpFirewallServicePort IrcServicePort
		{
			get { return new IpFirewallServicePort() { Id = 3 }; }
		}
		
		public static IpFirewallServicePort H323ServicePort
		{
			get { return new IpFirewallServicePort() { Id = 4 }; }
		}
		
		public static IpFirewallServicePort SipServicePort
		{
			get { return new IpFirewallServicePort() { Id = 5 }; }
		}
		
		public static IpFirewallServicePort PptpServicePort
		{
			get { return new IpFirewallServicePort() { Id = 6 }; }
		}
		
		public static IpFirewallServicePort Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpFirewallServicePort obj = new IpFirewallServicePort();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpHotspot : RosItemObject<IpHotspot>
	{
		// IP address pool name
		[RosValue("address-pool")]
		public String AddressPool { get; set; }
		
		public enum AddressesPerMacEnum
		{
			[RosValue("unlimited")]
			Unlimited,
		}
		
		// Maximum count of IP addresses for one MAC address
		[RosValue("addresses-per-mac")]
		public AddressesPerMacEnum? AddressesPerMac { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum SymbolicNamesEnum
		{
			[RosValue("none")]
			None,
		}
		
		public struct IdleTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IdleTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new IdleTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IdleTimeoutType(TimeSpan? value) 
                { 
                    return new IdleTimeoutType() { value = value }; 
                } 
                    public static IdleTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new IdleTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new IdleTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Maximal period of inactivity for unauthorized clients
		[RosValue("idle-timeout")]
		public IdleTimeoutType? IdleTimeout { get; set; }
		
		// Interface to run HotSpot service on
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		public struct KeepaliveTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator KeepaliveTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new KeepaliveTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator KeepaliveTimeoutType(TimeSpan? value) 
                { 
                    return new KeepaliveTimeoutType() { value = value }; 
                } 
                    public static KeepaliveTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new KeepaliveTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new KeepaliveTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Keepalive timeout for unauthorized clients
		[RosValue("keepalive-timeout")]
		public KeepaliveTimeoutType? KeepaliveTimeout { get; set; }
		
		// HotSpot name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Configuration for hotspot server
		[RosValue("profile")]
		public RouterOS.InterfaceWirelessSecurityProfiles Profile { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("ip-of-dns-name", IsDynamic=true)]
		public IPAddress IpOfDnsName { get; set; }
		
		[RosValue("proxy-status", IsDynamic=true)]
		public String ProxyStatus { get; set; }
		
		public static IpHotspot Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpHotspot obj = new IpHotspot();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot active", CanAdd=true, CanGet=true, CanRemove=true)]
	public class IpHotspotActive : RosItemObject<IpHotspotActive>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		public enum AdvertisementType
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("pending")]
			Pending,
			[RosValue("pending,block")]
			PendingBlock,
			[RosValue("sleeping")]
			Sleeping,
		}
		
		[RosValue("advertisement", IsDynamic=true)]
		public AdvertisementType? Advertisement { get; set; }
		
		[RosValue("blocked", IsDynamic=true)]
		public String Blocked { get; set; }
		
		[RosValue("bytes-in", IsDynamic=true)]
		public Int64? BytesIn { get; set; }
		
		[RosValue("bytes-out", IsDynamic=true)]
		public Int64? BytesOut { get; set; }
		
		[RosValue("domain", IsDynamic=true)]
		public String Domain { get; set; }
		
		[RosValue("idle-time", IsDynamic=true)]
		public TimeSpan? IdleTime { get; set; }
		
		[RosValue("idle-timeout", IsDynamic=true)]
		public TimeSpan? IdleTimeout { get; set; }
		
		[RosValue("keepalive-timeout", IsDynamic=true)]
		public TimeSpan? KeepaliveTimeout { get; set; }
		
		[RosValue("limit-bytes-in", IsDynamic=true)]
		public Int64? LimitBytesIn { get; set; }
		
		[RosValue("limit-bytes-out", IsDynamic=true)]
		public Int64? LimitBytesOut { get; set; }
		
		[RosValue("limit-bytes-total", IsDynamic=true)]
		public Int64? LimitBytesTotal { get; set; }
		
		[RosValue("login-by", IsDynamic=true)]
		public String LoginBy { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("packets-in", IsDynamic=true)]
		public Int64? PacketsIn { get; set; }
		
		[RosValue("packets-out", IsDynamic=true)]
		public Int64? PacketsOut { get; set; }
		
		[RosValue("radius", IsDynamic=true)]
		public String Radius { get; set; }
		
		[RosValue("server", IsDynamic=true)]
		public String Server { get; set; }
		
		[RosValue("session-time-left", IsDynamic=true)]
		public TimeSpan? SessionTimeLeft { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		[RosValue("user", IsDynamic=true)]
		public String User { get; set; }
		
	}
	[RosObject("ip hotspot cookie", CanAdd=true, CanGet=true, CanRemove=true)]
	public class IpHotspotCookie : RosItemObject<IpHotspotCookie>
	{
		[RosValue("domain", IsDynamic=true)]
		public String Domain { get; set; }
		
		[RosValue("expires-in", IsDynamic=true)]
		public TimeSpan? ExpiresIn { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("user", IsDynamic=true)]
		public String User { get; set; }
		
	}
	[RosObject("ip hotspot host", CanGet=true, CanRemove=true)]
	public class IpHotspotHost : RosItemObject<IpHotspotHost>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("authorized", IsDynamic=true)]
		public String Authorized { get; set; }
		
		[RosValue("bridge-port", IsDynamic=true)]
		public RouterOS.Interface BridgePort { get; set; }
		
		[RosValue("bypassed", IsDynamic=true)]
		public String Bypassed { get; set; }
		
		[RosValue("bytes-in", IsDynamic=true)]
		public Int64? BytesIn { get; set; }
		
		[RosValue("bytes-out", IsDynamic=true)]
		public Int64? BytesOut { get; set; }
		
		[RosValue("comment", IsDynamic=true)]
		public String Comment { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("found-by", IsDynamic=true)]
		public String FoundBy { get; set; }
		
		[RosValue("host-dead-time", IsDynamic=true)]
		public TimeSpan? HostDeadTime { get; set; }
		
		[RosValue("http-proxy", IsDynamic=true)]
		public IPAddress HttpProxy { get; set; }
		
		[RosValue("idle-time", IsDynamic=true)]
		public TimeSpan? IdleTime { get; set; }
		
		[RosValue("idle-timeout", IsDynamic=true)]
		public TimeSpan? IdleTimeout { get; set; }
		
		[RosValue("keepalive-timeout", IsDynamic=true)]
		public TimeSpan? KeepaliveTimeout { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("packets-in", IsDynamic=true)]
		public Int64? PacketsIn { get; set; }
		
		[RosValue("packets-out", IsDynamic=true)]
		public Int64? PacketsOut { get; set; }
		
		[RosValue("server", IsDynamic=true)]
		public String Server { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		[RosValue("to-address", IsDynamic=true)]
		public IPAddress ToAddress { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
	}
	[RosObject("ip hotspot ip-binding", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class IpHotspotIpBinding : RosItemObject<IpHotspotIpBinding>
	{
		// IP address of client
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// MAC address of client
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// The name of the server the client is connecting to
		[RosValue("server")]
		public String Server { get; set; }
		
		// IP address to translate to
		[RosValue("to-address")]
		public IPAddress ToAddress { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("blocked")]
			Blocked,
			[RosValue("bypassed")]
			Bypassed,
			[RosValue("regular")]
			Regular,
		}
		
		// Type of the static binding entry
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("blocked", IsDynamic=true)]
		public String Blocked { get; set; }
		
		[RosValue("bypassed", IsDynamic=true)]
		public String Bypassed { get; set; }
		
	}
	[RosObject("ip hotspot profile", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpHotspotProfile : RosItemObject<IpHotspotProfile>
	{
		// DNS name of the HotSpot server
		[RosValue("dns-name")]
		public String DnsName { get; set; }
		
		// IP address for HotSpot service
		[RosValue("hotspot-address")]
		public IPAddress HotspotAddress { get; set; }
		
		// Name of the directory, which stores the HTML servlet pages
		[RosValue("html-directory")]
		public String HtmlDirectory { get; set; }
		
		// Validity time of HTTP cookies
		[RosValue("http-cookie-lifetime")]
		public TimeSpan? HttpCookieLifetime { get; set; }
		
		// IP of the proxy server the HotSpot service will use as a proxy server for all those clients configured to use some unknown proxy servers
		[RosValue("http-proxy")]
		public String HttpProxy { get; set; }
		
		// Authentication method to use
		[RosValue("login-by")]
		public String LoginBy { get; set; }
		
		[RosValue("mac-auth-password")]
		public String MacAuthPassword { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("nas-port-type")]
		public String NasPortType { get; set; }
		
		// Enable or disable accounting
		[RosValue("radius-accounting")]
		public Boolean? RadiusAccounting { get; set; }
		
		[RosValue("radius-default-domain")]
		public String RadiusDefaultDomain { get; set; }
		
		public enum SymbolicNamesEnum
		{
			[RosValue("received")]
			Received,
		}
		
		public struct RadiusInterimUpdateType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RadiusInterimUpdateType(SymbolicNamesEnum? value) 
                { 
                    return new RadiusInterimUpdateType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RadiusInterimUpdateType(TimeSpan? value) 
                { 
                    return new RadiusInterimUpdateType() { value = value }; 
                } 
                    public static RadiusInterimUpdateType Parse(string str, Connection conn)
                    {
                        
                try { return new RadiusInterimUpdateType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new RadiusInterimUpdateType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Interim-Update time interval
		[RosValue("radius-interim-update")]
		public RadiusInterimUpdateType? RadiusInterimUpdate { get; set; }
		
		[RosValue("radius-location-id")]
		public String RadiusLocationId { get; set; }
		
		[RosValue("radius-location-name")]
		public String RadiusLocationName { get; set; }
		
		[RosValue("radius-mac-format")]
		public String RadiusMacFormat { get; set; }
		
		// Data rate limitation
		[RosValue("rate-limit")]
		public String RateLimit { get; set; }
		
		// Default SMTP server to be used to redirect unconditionally all user SMTP requests to
		[RosValue("smtp-server")]
		public IPAddress SmtpServer { get; set; }
		
		// Whether to split username from domain name when the username is given in "user@domain" or in "domain\user" format
		[RosValue("split-user-domain")]
		public Boolean? SplitUserDomain { get; set; }
		
		// Name of the SSL certificate to use for HTTPS authentication
		[RosValue("ssl-certificate")]
		public String SslCertificate { get; set; }
		
		[RosValue("trial-uptime")]
		public String TrialUptime { get; set; }
		
		[RosValue("trial-user-profile")]
		public RouterOS.InterfaceWirelessSecurityProfiles TrialUserProfile { get; set; }
		
		// Use RADIUS for AAA
		[RosValue("use-radius")]
		public Boolean? UseRadius { get; set; }
		
		[RosValue("default", IsDynamic=true)]
		public String Default { get; set; }
		
		public static IpHotspotProfile DefaultProfile
		{
			get { return new IpHotspotProfile() { Id = 0 }; }
		}
		
		public static IpHotspotProfile Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpHotspotProfile obj = new IpHotspotProfile();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot service-port", CanSet=true, CanGet=true)]
	public class IpHotspotServicePort : RosItemObject<IpHotspotServicePort>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// List of ports for this service
		[RosValue("ports")]
		public Int32[] Ports { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public static IpHotspotServicePort FtpServicePort
		{
			get { return new IpHotspotServicePort() { Id = 1 }; }
		}
		
		public static IpHotspotServicePort Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpHotspotServicePort obj = new IpHotspotServicePort();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot user", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpHotspotUser : RosItemObject<IpHotspotUser>
	{
		// Static IP address
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("email")]
		public String Email { get; set; }
		
		// Total uploaded byte limit for user
		[RosValue("limit-bytes-in")]
		public Int64? LimitBytesIn { get; set; }
		
		// Total downloaded byte limit for user
		[RosValue("limit-bytes-out")]
		public Int64? LimitBytesOut { get; set; }
		
		// Total transferred byte limit for user
		[RosValue("limit-bytes-total")]
		public Int64? LimitBytesTotal { get; set; }
		
		// Total uptime limit for user
		[RosValue("limit-uptime")]
		public TimeSpan? LimitUptime { get; set; }
		
		// Static MAC address
		[RosValue("mac-address")]
		public MACAddress? MacAddress { get; set; }
		
		// User name
		[RosValue("name")]
		public String Name { get; set; }
		
		// User password
		[RosValue("password")]
		public String Password { get; set; }
		
		// List of profiles for local HotSpot users
		[RosValue("profile")]
		public RouterOS.InterfaceWirelessSecurityProfiles Profile { get; set; }
		
		// User routes
		[RosValue("routes")]
		public String Routes { get; set; }
		
		// Which server is this user allowed to log in to
		[RosValue("server")]
		public String Server { get; set; }
		
		[RosValue("bytes-in", IsDynamic=true)]
		public Int64? BytesIn { get; set; }
		
		[RosValue("bytes-out", IsDynamic=true)]
		public Int64? BytesOut { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("packets-in", IsDynamic=true)]
		public Int64? PacketsIn { get; set; }
		
		[RosValue("packets-out", IsDynamic=true)]
		public Int64? PacketsOut { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		public static IpHotspotUser Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpHotspotUser obj = new IpHotspotUser();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot user profile", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpHotspotUserProfile : RosItemObject<IpHotspotUserProfile>
	{
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		// The IP poll name which the users will be given IP addresses from
		[RosValue("address-pool")]
		public String AddressPool { get; set; }
		
		// Whether to enable forced advertisement popups for this profile
		[RosValue("advertise")]
		public Boolean? Advertise { get; set; }
		
		// Set of intervals between showing advertisement popups
		[RosValue("advertise-interval")]
		public String AdvertiseInterval { get; set; }
		
		public enum SymbolicNamesEnum
		{
			[RosValue("immediately")]
			Immediately,
			[RosValue("never")]
			Never,
		}
		
		public struct AdvertiseTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AdvertiseTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new AdvertiseTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AdvertiseTimeoutType(TimeSpan? value) 
                { 
                    return new AdvertiseTimeoutType() { value = value }; 
                } 
                    public static AdvertiseTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new AdvertiseTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new AdvertiseTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// How long to wait for advertisement to be shown, before blocking network access with walled-garden
		[RosValue("advertise-timeout")]
		public AdvertiseTimeoutType? AdvertiseTimeout { get; set; }
		
		// List of URLs to show as advertisement popups
		[RosValue("advertise-url")]
		public String AdvertiseUrl { get; set; }
		
		public struct IdleTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IdleTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new IdleTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IdleTimeoutType(TimeSpan? value) 
                { 
                    return new IdleTimeoutType() { value = value }; 
                } 
                    public static IdleTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new IdleTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new IdleTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Maximal period of inactivity for authorized clients
		[RosValue("idle-timeout", CanUnset=true)]
		public IdleTimeoutType? IdleTimeout { get; set; }
		
		// Name of the firewall chain applied to incoming packets of the users of this profile
		[RosValue("incoming-filter")]
		public String IncomingFilter { get; set; }
		
		// packet mark put on all the packets from every user of this profile automatically
		[RosValue("incoming-packet-mark")]
		public String IncomingPacketMark { get; set; }
		
		public struct KeepaliveTimeoutType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator KeepaliveTimeoutType(SymbolicNamesEnum? value) 
                { 
                    return new KeepaliveTimeoutType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator KeepaliveTimeoutType(TimeSpan? value) 
                { 
                    return new KeepaliveTimeoutType() { value = value }; 
                } 
                    public static KeepaliveTimeoutType Parse(string str, Connection conn)
                    {
                        
                try { return new KeepaliveTimeoutType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new KeepaliveTimeoutType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Keepalive timeout for authorized clients
		[RosValue("keepalive-timeout", CanUnset=true)]
		public KeepaliveTimeoutType? KeepaliveTimeout { get; set; }
		
		// Profile reference name
		[RosValue("name")]
		public String Name { get; set; }
		
		// script name to launch after a user has logged in
		[RosValue("on-login")]
		public String OnLogin { get; set; }
		
		// script name to launch after a user has logged out
		[RosValue("on-logout")]
		public String OnLogout { get; set; }
		
		public enum OpenStatusPageEnum
		{
			[RosValue("always")]
			Always,
			[RosValue("http-login")]
			HttpLogin,
		}
		
		// Whether to show status page also for users authenticated using mac login method
		[RosValue("open-status-page")]
		public OpenStatusPageEnum? OpenStatusPage { get; set; }
		
		// Name of the firewall chain applied to outgoing packets from the users of this profile
		[RosValue("outgoing-filter")]
		public String OutgoingFilter { get; set; }
		
		// packet mark put on all the packets to every user of this profile automatically
		[RosValue("outgoing-packet-mark")]
		public String OutgoingPacketMark { get; set; }
		
		// Data rate limitation
		[RosValue("rate-limit")]
		public String RateLimit { get; set; }
		
		// Maximal allowed session time for the client
		[RosValue("session-timeout")]
		public TimeSpan? SessionTimeout { get; set; }
		
		public enum SharedUsersEnum
		{
			[RosValue("unlimited")]
			Unlimited,
		}
		
		// Maximal number of simultaneously logged in users with the same username
		[RosValue("shared-users")]
		public SharedUsersEnum? SharedUsers { get; set; }
		
		public struct StatusAutorefreshType 
                { 
                    private object value; 
                    public SymbolicNamesEnum? SymbolicNames 
                { 
                    get { return value as SymbolicNamesEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StatusAutorefreshType(SymbolicNamesEnum? value) 
                { 
                    return new StatusAutorefreshType() { value = value }; 
                }public TimeSpan? TimeInterval 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StatusAutorefreshType(TimeSpan? value) 
                { 
                    return new StatusAutorefreshType() { value = value }; 
                } 
                    public static StatusAutorefreshType Parse(string str, Connection conn)
                    {
                        
                try { return new StatusAutorefreshType() { value = TypeFormatter.ConvertFromString<SymbolicNamesEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new StatusAutorefreshType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// HotSpot servlet status page autorefresh interval
		[RosValue("status-autorefresh")]
		public StatusAutorefreshType? StatusAutorefresh { get; set; }
		
		// Whether to use transparent HTTP proxy for the authorized users of this profile
		[RosValue("transparent-proxy")]
		public Boolean? TransparentProxy { get; set; }
		
		[RosValue("default", IsDynamic=true)]
		public String Default { get; set; }
		
		public static IpHotspotUserProfile DefaultProfile
		{
			get { return new IpHotspotUserProfile() { Id = 0 }; }
		}
		
		public static IpHotspotUserProfile Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpHotspotUserProfile obj = new IpHotspotUserProfile();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip hotspot walled-garden", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpHotspotWalledGarden : RosItemObject<IpHotspotWalledGarden>
	{
		public enum ActionEnum
		{
			[RosValue("deny")]
			Deny,
			[RosValue("allow")]
			Allow,
		}
		
		// Rule action
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// For example, "(^
		[RosValue("dst-host", CanUnset=true)]
		public String DstHost { get; set; }
		
		// TCP destination port
		[RosValue("dst-port")]
		public String DstPort { get; set; }
		
		// HTTP method of the request
		[RosValue("method", CanUnset=true)]
		public String Method { get; set; }
		
		// Regex of requested path on dst-host
		[RosValue("path", CanUnset=true)]
		public String Path { get; set; }
		
		// Name of the HotSpot server this rule appliet to
		[RosValue("server", CanUnset=true)]
		public String Server { get; set; }
		
		// IP address of the user sending the request
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("hits", IsDynamic=true)]
		public Int32? Hits { get; set; }
		
	}
	[RosObject("ip hotspot walled-garden ip", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpHotspotWalledGardenIp : RosItemObject<IpHotspotWalledGardenIp>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("drop")]
			Drop,
			[RosValue("reject")]
			Reject,
		}
		
		// Action to undertake if a packet matches the rule
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// IP address of the destination web server
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Domain name of the destination web server
		[RosValue("dst-host")]
		public String DstHost { get; set; }
		
		// The TCP or UDP port a client has send the request to
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		// IP protocol name
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		// Name of the HotSpot server this rule appliet to
		[RosValue("server", CanUnset=true)]
		public String Server { get; set; }
		
		// IP address of the user sending the request
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ip ipsec installed-sa", CanSet=true, CanGet=true)]
	public class IpIpsecInstalledSa : RosItemObject<IpIpsecInstalledSa>
	{
		// Expiration time counted from installation of SA
		[RosValue("add-lifetime")]
		public String AddLifetime { get; set; }
		
		[RosValue("addtime")]
		public DateTime? Addtime { get; set; }
		
		public enum AuthAlgorithmEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("none")]
			None,
			[RosValue("sha1")]
			Sha1,
		}
		
		// Authentication algorithm
		[RosValue("auth-algorithm")]
		public AuthAlgorithmEnum? AuthAlgorithm { get; set; }
		
		// Authentication key
		[RosValue("auth-key")]
		public String AuthKey { get; set; }
		
		// Amount of data processed by this SA's crypto algorithms
		[RosValue("current-bytes")]
		public Int32? CurrentBytes { get; set; }
		
		public struct DstAddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstAddressType(IPv6Address value) 
                { 
                    return new DstAddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstAddressType(IPAddress value) 
                { 
                    return new DstAddressType() { value = value }; 
                } 
                    public static DstAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new DstAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new DstAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Destination of SA from policy configuration
		[RosValue("dst-address")]
		public DstAddressType? DstAddress { get; set; }
		
		public enum EncAlgorithmEnum
		{
			[RosValue("3des")]
			E3des,
			[RosValue("aes")]
			Aes,
			[RosValue("blowfish")]
			Blowfish,
			[RosValue("camellia")]
			Camellia,
			[RosValue("des")]
			Des,
			[RosValue("null")]
			Null,
		}
		
		// Encryption algorithm
		[RosValue("enc-algorithm")]
		public EncAlgorithmEnum? EncAlgorithm { get; set; }
		
		// Encryption key
		[RosValue("enc-key")]
		public String EncKey { get; set; }
		
		public struct LifebytesType
                {
                    public Int32 Soft { get; set; }
                    public Int32 Hard { get; set; }

                    public static LifebytesType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new LifebytesType() { Soft = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Hard = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Soft, '/', Hard);
                    }
                }
		// Expiration threshold for amount of processed data
		[RosValue("lifebytes")]
		public LifebytesType? Lifebytes { get; set; }
		
		// Size of replay window in bytes
		[RosValue("replay")]
		public Int32? Replay { get; set; }
		
		// SPI value of SA in hexadecimal
		[RosValue("spi")]
		public Int32? Spi { get; set; }
		
		public struct SrcAddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPv6Address value) 
                { 
                    return new SrcAddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPAddress value) 
                { 
                    return new SrcAddressType() { value = value }; 
                } 
                    public static SrcAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Source of SA fro policy configuration
		[RosValue("src-address")]
		public SrcAddressType? SrcAddress { get; set; }
		
		public enum StateEnum
		{
			[RosValue("dead")]
			Dead,
			[RosValue("dying")]
			Dying,
			[RosValue("larval")]
			Larval,
			[RosValue("mature")]
			Mature,
		}
		
		// State of the SA
		[RosValue("state")]
		public StateEnum? State { get; set; }
		
		// Expiration time counter from the first use of SA
		[RosValue("use-lifetime")]
		public String UseLifetime { get; set; }
		
		[RosValue("usetime")]
		public DateTime? Usetime { get; set; }
		
		[RosValue("pfs", IsDynamic=true)]
		public String Pfs { get; set; }
		
	}
	[RosObject("ip ipsec peer", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpIpsecPeer : RosItemObject<IpIpsecPeer>
	{
		// Address of the remote router
		[RosValue("address")]
		public String Address { get; set; }
		
		public enum AuthMethodEnum
		{
			[RosValue("pre-shared-key")]
			PreSharedKey,
			[RosValue("rsa-signature")]
			RsaSignature,
		}
		
		[RosValue("auth-method")]
		public AuthMethodEnum? AuthMethod { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum DhGroupEnum
		{
			[RosValue("ec2n155")]
			Ec2n155,
			[RosValue("ec2n185")]
			Ec2n185,
			[RosValue("modp1024")]
			Modp1024,
			[RosValue("modp1536")]
			Modp1536,
			[RosValue("modp768")]
			Modp768,
		}
		
		// Diffie-Hellman key exchange protocol group
		[RosValue("dh-group")]
		public DhGroupEnum? DhGroup { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("disable-dpd")]
			DisableDpd,
		}
		
		public enum DpdIntervalEnum
		{
			[RosValue("Num")]
			Num,
		}
		
		public struct DpdIntervalType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DpdIntervalType(SpecialEnum? value) 
                { 
                    return new DpdIntervalType() { value = value }; 
                }public DpdIntervalEnum? State 
                { 
                    get { return value as DpdIntervalEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DpdIntervalType(DpdIntervalEnum? value) 
                { 
                    return new DpdIntervalType() { value = value }; 
                } 
                    public static DpdIntervalType Parse(string str, Connection conn)
                    {
                        
                try { return new DpdIntervalType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new DpdIntervalType() { value = TypeFormatter.ConvertFromString<DpdIntervalEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("dpd-interval")]
		public DpdIntervalType? DpdInterval { get; set; }
		
		[RosValue("dpd-maximum-failures")]
		public Int32? DpdMaximumFailures { get; set; }
		
		public enum EncAlgorithmEnum
		{
			[RosValue("3des")]
			E3des,
			[RosValue("aes-128")]
			Aes128,
			[RosValue("aes-192")]
			Aes192,
			[RosValue("aes-256")]
			Aes256,
			[RosValue("blowfish")]
			Blowfish,
		}
		
		// Encryption algorithm
		[RosValue("enc-algorithm")]
		public EncAlgorithmEnum? EncAlgorithm { get; set; }
		
		public enum ExchangeModeEnum
		{
			[RosValue("aggressive")]
			Aggressive,
			[RosValue("base")]
			Base,
			[RosValue("main")]
			Main,
		}
		
		// Exchange mode
		[RosValue("exchange-mode")]
		public ExchangeModeEnum? ExchangeMode { get; set; }
		
		// Generate policy
		[RosValue("generate-policy")]
		public Boolean? GeneratePolicy { get; set; }
		
		public enum HashAlgorithmEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("sha1")]
			Sha1,
		}
		
		// Hashing algorithm
		[RosValue("hash-algorithm")]
		public HashAlgorithmEnum? HashAlgorithm { get; set; }
		
		// Expiration threshold for amount of processed data
		[RosValue("lifebytes")]
		public Int32? Lifebytes { get; set; }
		
		// Expiration time counter from the first use of SA
		[RosValue("lifetime")]
		public TimeSpan? Lifetime { get; set; }
		
		[RosValue("my-id-user-fqdn")]
		public String MyIdUserFqdn { get; set; }
		
		[RosValue("nat-traversal")]
		public Boolean? NatTraversal { get; set; }
		
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		public enum ProposalCheckEnum
		{
			[RosValue("claim")]
			Claim,
			[RosValue("exact")]
			Exact,
			[RosValue("obey")]
			Obey,
			[RosValue("strict")]
			Strict,
		}
		
		// Lifetime check logic
		[RosValue("proposal-check")]
		public ProposalCheckEnum? ProposalCheck { get; set; }
		
		[RosValue("remote-certificate")]
		public String RemoteCertificate { get; set; }
		
		// Secret string
		[RosValue("secret")]
		public String Secret { get; set; }
		
		// Send or not initial contact
		[RosValue("send-initial-contact")]
		public Boolean? SendInitialContact { get; set; }
		
	}
	[RosObject("ip ipsec policy", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class IpIpsecPolicy : RosItemObject<IpIpsecPolicy>
	{
		public enum ActionEnum
		{
			[RosValue("discard")]
			Discard,
			[RosValue("encrypt")]
			Encrypt,
			[RosValue("none")]
			None,
		}
		
		// Action type
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Destination IP address
		[RosValue("dst-address")]
		public String DstAddress { get; set; }
		
		public enum DstPortEnum
		{
			[RosValue("any")]
			Any,
		}
		
		public struct DstPortType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstPortType(Int32? value) 
                { 
                    return new DstPortType() { value = value }; 
                }public DstPortEnum? State 
                { 
                    get { return value as DstPortEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstPortType(DstPortEnum? value) 
                { 
                    return new DstPortType() { value = value }; 
                } 
                    public static DstPortType Parse(string str, Connection conn)
                    {
                        
                try { return new DstPortType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new DstPortType() { value = TypeFormatter.ConvertFromString<DstPortEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("dst-port")]
		public DstPortType? DstPort { get; set; }
		
		// IP security protocols
		[RosValue("ipsec-protocols")]
		public String IpsecProtocols { get; set; }
		
		public enum LevelEnum
		{
			[RosValue("require")]
			Require,
			[RosValue("unique")]
			Unique,
			[RosValue("use")]
			Use,
		}
		
		// What to do if some of the SAs for this policy cannot be found
		[RosValue("level")]
		public LevelEnum? Level { get; set; }
		
		[RosValue("priority")]
		public String Priority { get; set; }
		
		// Name of the proposal info
		[RosValue("proposal")]
		public RouterOS.InterfaceWirelessSecurityProfiles Proposal { get; set; }
		
		public enum ProtocolEnum
		{
			[RosValue("all")]
			All,
			[RosValue("egp")]
			Egp,
			[RosValue("ggp")]
			Ggp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("igmp")]
			Igmp,
			[RosValue("ip-encap")]
			IpEncap,
			[RosValue("ip-sec")]
			IpSec,
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
		}
		
		// Name or number of the protocol
		[RosValue("protocol")]
		public ProtocolEnum? Protocol { get; set; }
		
		public struct SaDstAddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SaDstAddressType(IPv6Address value) 
                { 
                    return new SaDstAddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SaDstAddressType(IPAddress value) 
                { 
                    return new SaDstAddressType() { value = value }; 
                } 
                    public static SaDstAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new SaDstAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new SaDstAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// SA destination
		[RosValue("sa-dst-address")]
		public SaDstAddressType? SaDstAddress { get; set; }
		
		public struct SaSrcAddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SaSrcAddressType(IPv6Address value) 
                { 
                    return new SaSrcAddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SaSrcAddressType(IPAddress value) 
                { 
                    return new SaSrcAddressType() { value = value }; 
                } 
                    public static SaSrcAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new SaSrcAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new SaSrcAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// SA source
		[RosValue("sa-src-address")]
		public SaSrcAddressType? SaSrcAddress { get; set; }
		
		// Source IP address
		[RosValue("src-address")]
		public String SrcAddress { get; set; }
		
		public enum SrcPortEnum
		{
			[RosValue("any")]
			Any,
		}
		
		public struct SrcPortType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcPortType(Int32? value) 
                { 
                    return new SrcPortType() { value = value }; 
                }public SrcPortEnum? State 
                { 
                    get { return value as SrcPortEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcPortType(SrcPortEnum? value) 
                { 
                    return new SrcPortType() { value = value }; 
                } 
                    public static SrcPortType Parse(string str, Connection conn)
                    {
                        
                try { return new SrcPortType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new SrcPortType() { value = TypeFormatter.ConvertFromString<SrcPortEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("src-port")]
		public SrcPortType? SrcPort { get; set; }
		
		// Defines whether tunnel mode is enabled or disabled
		[RosValue("tunnel")]
		public Boolean? Tunnel { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("in-accepted", IsDynamic=true)]
		public Int32? InAccepted { get; set; }
		
		[RosValue("in-dropped", IsDynamic=true)]
		public Int32? InDropped { get; set; }
		
		[RosValue("in-transformed", IsDynamic=true)]
		public Int32? InTransformed { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		[RosValue("out-accepted", IsDynamic=true)]
		public Int32? OutAccepted { get; set; }
		
		[RosValue("out-dropped", IsDynamic=true)]
		public Int32? OutDropped { get; set; }
		
		[RosValue("out-transformed", IsDynamic=true)]
		public Int32? OutTransformed { get; set; }
		
		public enum Ph2StateType
		{
			[RosValue("adding-sa")]
			AddingSa,
			[RosValue("commiting")]
			Commiting,
			[RosValue("established")]
			Established,
			[RosValue("expired")]
			Expired,
			[RosValue("getspi-done")]
			GetspiDone,
			[RosValue("getspi-sent")]
			GetspiSent,
			[RosValue("msg1-sent")]
			Msg1Sent,
			[RosValue("no-phase2")]
			NoPhase2,
			[RosValue("ready-to-establish")]
			ReadyToEstablish,
			[RosValue("ready-to-send")]
			ReadyToSend,
			[RosValue("spawning")]
			Spawning,
			[RosValue("starting")]
			Starting,
		}
		
		[RosValue("ph2-state", IsDynamic=true)]
		public Ph2StateType? Ph2State { get; set; }
		
	}
	[RosObject("ip ipsec proposal", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpIpsecProposal : RosItemObject<IpIpsecProposal>
	{
		// Allowed algorithms for authorization
		[RosValue("auth-algorithms")]
		public String AuthAlgorithms { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Allowed algorithms and key lengths to use for SAs
		[RosValue("enc-algorithms")]
		public String EncAlgorithms { get; set; }
		
		// How long to use SA before throwing it out
		[RosValue("lifetime")]
		public TimeSpan? Lifetime { get; set; }
		
		// Name of proposal for referencing it from policy
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum PfsGroupEnum
		{
			[RosValue("ec2n155")]
			Ec2n155,
			[RosValue("ec2n185")]
			Ec2n185,
			[RosValue("modp1024")]
			Modp1024,
			[RosValue("modp1536")]
			Modp1536,
			[RosValue("modp768")]
			Modp768,
		}
		
		// Diffie-Helman group used for Perfect Forward Secrecy
		[RosValue("pfs-group")]
		public PfsGroupEnum? PfsGroup { get; set; }
		
		public static IpIpsecProposal DefaultProposal
		{
			get { return new IpIpsecProposal() { Id = 0 }; }
		}
		
		public static IpIpsecProposal Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpIpsecProposal obj = new IpIpsecProposal();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip ipsec remote-peers", CanGet=true)]
	public class IpIpsecRemotePeers : RosItemObject<IpIpsecRemotePeers>
	{
		[RosValue("established", IsDynamic=true)]
		public TimeSpan? Established { get; set; }
		
		[RosValue("local-address", IsDynamic=true)]
		public IPv6Address LocalAddress { get; set; }
		
		[RosValue("remote-address", IsDynamic=true)]
		public IPv6Address RemoteAddress { get; set; }
		
		public enum SideType
		{
			[RosValue("initiator")]
			Initiator,
			[RosValue("responder")]
			Responder,
		}
		
		[RosValue("side", IsDynamic=true)]
		public SideType? Side { get; set; }
		
		public enum StateType
		{
			[RosValue("established")]
			Established,
			[RosValue("expired")]
			Expired,
			[RosValue("no-phase1")]
			NoPhase1,
			[RosValue("spawning")]
			Spawning,
			[RosValue("starting")]
			Starting,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("ip neighbor", CanGet=true)]
	public class IpNeighbor : RosItemObject<IpNeighbor>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("address6", IsDynamic=true)]
		public String Address6 { get; set; }
		
		[RosValue("age", IsDynamic=true)]
		public TimeSpan? Age { get; set; }
		
		[RosValue("board", IsDynamic=true)]
		public String Board { get; set; }
		
		[RosValue("identity", IsDynamic=true)]
		public String Identity { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("ipv6", IsDynamic=true)]
		public String Ipv6 { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("platform", IsDynamic=true)]
		public String Platform { get; set; }
		
		[RosValue("software-id", IsDynamic=true)]
		public String SoftwareId { get; set; }
		
		public enum UnpackType
		{
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
			[RosValue("uncompress-all")]
			UncompressAll,
			[RosValue("uncompress-headers")]
			UncompressHeaders,
		}
		
		[RosValue("unpack", IsDynamic=true)]
		public UnpackType? Unpack { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		[RosValue("version", IsDynamic=true)]
		public String Version { get; set; }
		
	}
	[RosObject("ip neighbor discovery", CanSet=true, CanGet=true)]
	public class IpNeighborDiscovery : RosItemObject<IpNeighborDiscovery>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines if discover is enabled or disabled
		[RosValue("discover")]
		public Boolean? Discover { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public RouterOS.Interface Name { get; set; }
		
		public static IpNeighborDiscovery Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpNeighborDiscovery obj = new IpNeighborDiscovery();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip packing", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpPacking : RosItemObject<IpPacking>
	{
		// The maximum size of the aggregated packet
		[RosValue("aggregated-size")]
		public Int32? AggregatedSize { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface settings
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		public enum PackingEnum
		{
			[RosValue("compress-all")]
			CompressAll,
			[RosValue("compress-headers")]
			CompressHeaders,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		// Specifies the packing mode
		[RosValue("packing")]
		public PackingEnum? Packing { get; set; }
		
		public enum UnpackingEnum
		{
			[RosValue("compress-all")]
			CompressAll,
			[RosValue("compress-headers")]
			CompressHeaders,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		// Specifies the unpacking mode
		[RosValue("unpacking")]
		public UnpackingEnum? Unpacking { get; set; }
		
	}
	[RosObject("ip pool", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpPool : RosItemObject<IpPool>
	{
		// Name of the IP pool
		[RosValue("name")]
		public String Name { get; set; }
		
		// Next pool range
		[RosValue("next-pool")]
		public String NextPool { get; set; }
		
		// IP pool's ranges
		[RosValue("ranges")]
		public IPAddress[] Ranges { get; set; }
		
		public static IpPool Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpPool obj = new IpPool();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip pool used", CanSet=true, CanGet=true)]
	public class IpPoolUsed : RosItemObject<IpPoolUsed>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("info", IsDynamic=true)]
		public String Info { get; set; }
		
		[RosValue("owner", IsDynamic=true)]
		public String Owner { get; set; }
		
		[RosValue("pool", IsDynamic=true)]
		public String Pool { get; set; }
		
	}
	[RosObject("ip proxy", CanSet=true, CanGet=true)]
	public class IpProxy : RosValueObject
	{
		[RosValue("always-from-cache")]
		public Boolean? AlwaysFromCache { get; set; }
		
		[RosValue("cache-administrator")]
		public String CacheAdministrator { get; set; }
		
		[RosValue("cache-hit-dscp")]
		public Int32? CacheHitDscp { get; set; }
		
		[RosValue("cache-on-disk")]
		public Boolean? CacheOnDisk { get; set; }
		
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		[RosValue("max-cache-size")]
		public String MaxCacheSize { get; set; }
		
		[RosValue("max-client-connections")]
		public Int32? MaxClientConnections { get; set; }
		
		[RosValue("max-fresh-time")]
		public TimeSpan? MaxFreshTime { get; set; }
		
		[RosValue("max-server-connections")]
		public Int32? MaxServerConnections { get; set; }
		
		[RosValue("parent-proxy")]
		public String ParentProxy { get; set; }
		
		[RosValue("parent-proxy-port")]
		public Int32? ParentProxyPort { get; set; }
		
		[RosValue("port")]
		public Int32[] Port { get; set; }
		
		[RosValue("serialize-connections")]
		public Boolean? SerializeConnections { get; set; }
		
		[RosValue("src-address")]
		public String SrcAddress { get; set; }
		
	}
	[RosObject("ip proxy access", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpProxyAccess : RosItemObject<IpProxyAccess>
	{
		public enum ActionEnum
		{
			[RosValue("deny")]
			Deny,
			[RosValue("allow")]
			Allow,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-host", CanUnset=true)]
		public String DstHost { get; set; }
		
		[RosValue("dst-port")]
		public String DstPort { get; set; }
		
		[RosValue("local-port", CanUnset=true)]
		public String LocalPort { get; set; }
		
		[RosValue("method", CanUnset=true)]
		public String Method { get; set; }
		
		[RosValue("path", CanUnset=true)]
		public String Path { get; set; }
		
		[RosValue("redirect-to", CanUnset=true)]
		public String RedirectTo { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("hits", IsDynamic=true)]
		public Int32? Hits { get; set; }
		
	}
	[RosObject("ip proxy cache", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpProxyCache : RosItemObject<IpProxyCache>
	{
		public enum ActionEnum
		{
			[RosValue("deny")]
			Deny,
			[RosValue("allow")]
			Allow,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-host", CanUnset=true)]
		public String DstHost { get; set; }
		
		[RosValue("dst-port")]
		public String DstPort { get; set; }
		
		[RosValue("local-port", CanUnset=true)]
		public String LocalPort { get; set; }
		
		[RosValue("method", CanUnset=true)]
		public String Method { get; set; }
		
		[RosValue("path", CanUnset=true)]
		public String Path { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("hits", IsDynamic=true)]
		public Int32? Hits { get; set; }
		
	}
	[RosObject("ip proxy cache-contents", CanGet=true)]
	public class IpProxyCacheContents : RosItemObject<IpProxyCacheContents>
	{
		[RosValue("file-size", IsDynamic=true)]
		public String FileSize { get; set; }
		
		[RosValue("last-accessed", IsDynamic=true)]
		public DateTime? LastAccessed { get; set; }
		
		[RosValue("last-accessed-time", IsDynamic=true)]
		public DateTime? LastAccessedTime { get; set; }
		
		[RosValue("last-modified", IsDynamic=true)]
		public DateTime? LastModified { get; set; }
		
		[RosValue("last-modified-time", IsDynamic=true)]
		public DateTime? LastModifiedTime { get; set; }
		
		[RosValue("uri", IsDynamic=true)]
		public String Uri { get; set; }
		
	}
	[RosObject("ip proxy connections", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpProxyConnections : RosItemObject<IpProxyConnections>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("client", IsDynamic=true)]
		public String Client { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPAddress DstAddress { get; set; }
		
		[RosValue("protocol", IsDynamic=true)]
		public String Protocol { get; set; }
		
		[RosValue("rx-bytes", IsDynamic=true)]
		public Int32? RxBytes { get; set; }
		
		[RosValue("server", IsDynamic=true)]
		public String Server { get; set; }
		
		[RosValue("src-address", IsDynamic=true)]
		public IPAddress SrcAddress { get; set; }
		
		public enum StateType
		{
			[RosValue("connecting")]
			Connecting,
			[RosValue("idle")]
			Idle,
			[RosValue("resolving")]
			Resolving,
			[RosValue("rx-body")]
			RxBody,
			[RosValue("rx-header")]
			RxHeader,
			[RosValue("tx-body")]
			TxBody,
			[RosValue("tx-header")]
			TxHeader,
			[RosValue("waiting")]
			Waiting,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		[RosValue("tx-bytes", IsDynamic=true)]
		public Int32? TxBytes { get; set; }
		
	}
	[RosObject("ip proxy direct", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpProxyDirect : RosItemObject<IpProxyDirect>
	{
		public enum ActionEnum
		{
			[RosValue("deny")]
			Deny,
			[RosValue("allow")]
			Allow,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-host", CanUnset=true)]
		public String DstHost { get; set; }
		
		[RosValue("dst-port")]
		public String DstPort { get; set; }
		
		[RosValue("local-port", CanUnset=true)]
		public String LocalPort { get; set; }
		
		[RosValue("method", CanUnset=true)]
		public String Method { get; set; }
		
		[RosValue("path", CanUnset=true)]
		public String Path { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("hits", IsDynamic=true)]
		public Int32? Hits { get; set; }
		
	}
	[RosObject("ip route", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpRoute : RosItemObject<IpRoute>
	{
		[RosValue("bgp-as-path", CanUnset=true)]
		public String BgpAsPath { get; set; }
		
		[RosValue("bgp-atomic-aggregate", CanUnset=true)]
		public Boolean? BgpAtomicAggregate { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("local-as")]
			LocalAs,
			[RosValue("no-advertise")]
			NoAdvertise,
			[RosValue("no-export")]
			NoExport,
		}
		
		public struct NumbersType
                {
                    public Int32 As { get; set; }
                    public Int32 Num { get; set; }

                    public static NumbersType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new NumbersType() { As = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Num = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(As, ':', Num);
                    }
                }
		public struct CommunityType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(SpecialEnum? value) 
                { 
                    return new CommunityType() { value = value }; 
                }public NumbersType? Numbers 
                { 
                    get { return value as NumbersType?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(NumbersType? value) 
                { 
                    return new CommunityType() { value = value }; 
                } 
                    public static CommunityType Parse(string str, Connection conn)
                    {
                        
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<NumbersType>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bgp-communities", CanUnset=true)]
		public CommunityType[] BgpCommunities { get; set; }
		
		[RosValue("bgp-local-pref", CanUnset=true)]
		public Int32? BgpLocalPref { get; set; }
		
		[RosValue("bgp-med", CanUnset=true)]
		public Int32? BgpMed { get; set; }
		
		public enum BgpOriginEnum
		{
			[RosValue("egp")]
			Egp,
			[RosValue("igp")]
			Igp,
			[RosValue("incomplete")]
			Incomplete,
		}
		
		[RosValue("bgp-origin", CanUnset=true)]
		public BgpOriginEnum? BgpOrigin { get; set; }
		
		public struct BgpPrependType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BgpPrependType(Int32? value) 
                { 
                    return new BgpPrependType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BgpPrependType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new BgpPrependType() { value = value }; 
                } 
                    public static BgpPrependType Parse(string str, Connection conn)
                    {
                        
                try { return new BgpPrependType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new BgpPrependType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bgp-prepend", CanUnset=true)]
		public BgpPrependType? BgpPrepend { get; set; }
		
		public enum CheckGatewayEnum
		{
			[RosValue("arp")]
			Arp,
			[RosValue("ping")]
			Ping,
		}
		
		// Whether all nexthops of this route are checking reachability of gateway by sending arp requests every 10 seconds
		[RosValue("check-gateway", CanUnset=true)]
		public CheckGatewayEnum? CheckGateway { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Administrative distance of the route
		[RosValue("distance", CanUnset=true)]
		public Int32? Distance { get; set; }
		
		// Destination address
		[RosValue("dst-address")]
		public IPPrefix? DstAddress { get; set; }
		
		[RosValue("gateway")]
		public String Gateway { get; set; }
		
		[RosValue("pref-src", CanUnset=true)]
		public IPAddress PrefSrc { get; set; }
		
		[RosValue("route-tag", CanUnset=true)]
		public Int32? RouteTag { get; set; }
		
		// It's used for policy-routing
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("scope")]
		public Int32? Scope { get; set; }
		
		[RosValue("target-scope")]
		public Int32? TargetScope { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("blackhole")]
			Blackhole,
			[RosValue("prohibit")]
			Prohibit,
			[RosValue("unicast")]
			Unicast,
			[RosValue("unreachable")]
			Unreachable,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("vrf-interface")]
		public RouterOS.Interface VrfInterface { get; set; }
		
		[RosValue("bgp-ext-communities", CanUnset=true)]
		public String BgpExtCommunities { get; set; }
		
		[RosValue("bgp-weight", CanUnset=true)]
		public String BgpWeight { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("bgp", IsDynamic=true)]
		public String Bgp { get; set; }
		
		[RosValue("blackhole", IsDynamic=true)]
		public String Blackhole { get; set; }
		
		[RosValue("connect", IsDynamic=true)]
		public String Connect { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("gateway-status", IsDynamic=true)]
		public RouterOS.Interface GatewayStatus { get; set; }
		
		[RosValue("mme", IsDynamic=true)]
		public String Mme { get; set; }
		
		[RosValue("ospf", IsDynamic=true)]
		public String Ospf { get; set; }
		
		[RosValue("ospf-metric", IsDynamic=true)]
		public Int32? OspfMetric { get; set; }
		
		public enum OspfTypeType
		{
			[RosValue("external-type-1")]
			ExternalType1,
			[RosValue("external-type-2")]
			ExternalType2,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
			[RosValue("nssa-external-type-1")]
			NssaExternalType1,
			[RosValue("nssa-external-type-2")]
			NssaExternalType2,
		}
		
		[RosValue("ospf-type", IsDynamic=true)]
		public OspfTypeType? OspfType { get; set; }
		
		[RosValue("prohibit", IsDynamic=true)]
		public String Prohibit { get; set; }
		
		[RosValue("received-from", IsDynamic=true)]
		public String ReceivedFrom { get; set; }
		
		[RosValue("rip", IsDynamic=true)]
		public String Rip { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		[RosValue("unreachable", IsDynamic=true)]
		public String Unreachable { get; set; }
		
	}
	[RosObject("ip route nexthop", CanGet=true)]
	public class IpRouteNexthop : RosItemObject<IpRouteNexthop>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		public enum CheckGatewayType
		{
			[RosValue("arp")]
			Arp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("none")]
			None,
		}
		
		[RosValue("check-gateway", IsDynamic=true)]
		public CheckGatewayType? CheckGateway { get; set; }
		
		[RosValue("forwarding-nexthop", IsDynamic=true)]
		public IPAddress ForwardingNexthop { get; set; }
		
		[RosValue("gw-check-ok", IsDynamic=true)]
		public String GwCheckOk { get; set; }
		
		public enum GwStateType
		{
			[RosValue("reachable")]
			Reachable,
			[RosValue("recursive")]
			Recursive,
			[RosValue("unreachable")]
			Unreachable,
		}
		
		[RosValue("gw-state", IsDynamic=true)]
		public GwStateType? GwState { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("scope", IsDynamic=true)]
		public Int32? Scope { get; set; }
		
		[RosValue("table", IsDynamic=true)]
		public String Table { get; set; }
		
	}
	[RosObject("ip route rule", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpRouteRule : RosItemObject<IpRouteRule>
	{
		public enum ActionEnum
		{
			[RosValue("drop")]
			Drop,
			[RosValue("lookup")]
			Lookup,
			[RosValue("lookup-only-in-table")]
			LookupOnlyInTable,
			[RosValue("unreachable")]
			Unreachable,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public IPPrefix? DstAddress { get; set; }
		
		// Interface through which the gateway can be reached
		[RosValue("interface", CanUnset=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public IPPrefix? SrcAddress { get; set; }
		
		[RosValue("table")]
		public String Table { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
	}
	[RosObject("ip route vrf", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpRouteVrf : RosItemObject<IpRouteVrf>
	{
		[RosValue("bgp-nexthop", CanUnset=true)]
		public RouterOS.Interface BgpNexthop { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("export-route-targets", CanUnset=true)]
		public String ExportRouteTargets { get; set; }
		
		[RosValue("import-route-targets", CanUnset=true)]
		public String ImportRouteTargets { get; set; }
		
		[RosValue("interfaces")]
		public RouterOS.Interface[] Interfaces { get; set; }
		
		[RosValue("route-distinguisher", CanUnset=true)]
		public String RouteDistinguisher { get; set; }
		
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
	}
	[RosObject("ip service", CanSet=true, CanGet=true)]
	public class IpService : RosItemObject<IpService>
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPPrefix? Address 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPPrefix? value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPPrefix? Address2 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPPrefix? value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address from which the service is accessible
		[RosValue("address")]
		public AddressType[] Address { get; set; }
		
		[RosValue("certificate")]
		public String Certificate { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Port for specific IP service
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public static IpService TelnetService
		{
			get { return new IpService() { Id = 0 }; }
		}
		
		public static IpService FtpService
		{
			get { return new IpService() { Id = 1 }; }
		}
		
		public static IpService WwwService
		{
			get { return new IpService() { Id = 2 }; }
		}
		
		public static IpService SshService
		{
			get { return new IpService() { Id = 4 }; }
		}
		
		public static IpService WwwSslService
		{
			get { return new IpService() { Id = 6 }; }
		}
		
		public static IpService ApiService
		{
			get { return new IpService() { Id = 7 }; }
		}
		
		public static IpService WinboxService
		{
			get { return new IpService() { Id = 8 }; }
		}
		
		public static IpService Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			IpService obj = new IpService();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ip socks", CanSet=true, CanGet=true)]
	public class IpSocks : RosValueObject
	{
		// Connection is discarded if no data is passed for some time
		[RosValue("connection-idle-timeout")]
		public TimeSpan? ConnectionIdleTimeout { get; set; }
		
		// Defines whether SOCKS are enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// Max number of simultaneous connections
		[RosValue("max-connections")]
		public Int32? MaxConnections { get; set; }
		
		// Client connections are accepted on this port
		[RosValue("port")]
		public Int32? Port { get; set; }
		
	}
	[RosObject("ip socks access", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class IpSocksAccess : RosItemObject<IpSocksAccess>
	{
		public enum ActionEnum
		{
			[RosValue("allow")]
			Allow,
			[RosValue("deny")]
			Deny,
		}
		
		// Rule action
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Server's IP address
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		// Destination port
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		// Client's IP address
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		// Source port
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
	}
	[RosObject("ip socks connections", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpSocksConnections : RosItemObject<IpSocksConnections>
	{
		// Server's IP address
		[RosValue("dst-address")]
		public String DstAddress { get; set; }
		
		// Bytes received
		[RosValue("rx")]
		public Int32? Rx { get; set; }
		
		// Client's IP address
		[RosValue("src-address")]
		public String SrcAddress { get; set; }
		
		// Bytes transmitted
		[RosValue("tx")]
		public Int32? Tx { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("in")]
			In,
			[RosValue("out")]
			Out,
			[RosValue("unknown")]
			Unknown,
		}
		
		// Connection type
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
	}
	[RosObject("ip ssh", CanSet=true, CanGet=true)]
	public class IpSsh : RosValueObject
	{
		[RosValue("forwarding-enabled")]
		public Boolean? ForwardingEnabled { get; set; }
		
	}
	[RosObject("ip tftp", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class IpTftp : RosItemObject<IpTftp>
	{
		[RosValue("allow")]
		public Boolean? Allow { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct IpAddressesType 
                { 
                    private object value; 
                    public IPv6Prefix? Ip6 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpAddressesType(IPv6Prefix? value) 
                { 
                    return new IpAddressesType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpAddressesType(IPAddress value) 
                { 
                    return new IpAddressesType() { value = value }; 
                } 
                    public static IpAddressesType Parse(string str, Connection conn)
                    {
                        
                try { return new IpAddressesType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                try { return new IpAddressesType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("ip-addresses")]
		public IpAddressesType[] IpAddresses { get; set; }
		
		[RosValue("read-only")]
		public Boolean? ReadOnly { get; set; }
		
		[RosValue("real-filename")]
		public String RealFilename { get; set; }
		
		[RosValue("req-filename")]
		public String ReqFilename { get; set; }
		
		[RosValue("hits", IsDynamic=true)]
		public Int32? Hits { get; set; }
		
	}
	[RosObject("ip traffic-flow", CanSet=true, CanGet=true)]
	public class IpTrafficFlow : RosValueObject
	{
		public enum ActiveFlowTimeoutEnum
		{
			[RosValue("ActiveFlowTimeout")]
			ActiveFlowTimeout,
		}
		
		[RosValue("active-flow-timeout")]
		public ActiveFlowTimeoutEnum? ActiveFlowTimeout { get; set; }
		
		public enum CacheEntriesEnum
		{
			[RosValue("128k")]
			E128k,
			[RosValue("16k")]
			E16k,
			[RosValue("1k")]
			E1k,
			[RosValue("256k")]
			E256k,
			[RosValue("2k")]
			E2k,
		}
		
		[RosValue("cache-entries")]
		public CacheEntriesEnum? CacheEntries { get; set; }
		
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		public enum InactiveFlowTimeoutEnum
		{
			[RosValue("InactiveFlowTimeout")]
			InactiveFlowTimeout,
		}
		
		[RosValue("inactive-flow-timeout")]
		public InactiveFlowTimeoutEnum? InactiveFlowTimeout { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interfaces")]
		public InterfaceType[] Interfaces { get; set; }
		
	}
	[RosObject("ip traffic-flow target", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpTrafficFlowTarget : RosItemObject<IpTrafficFlowTarget>
	{
		[RosValue("address")]
		public String Address { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("v9-template-refresh")]
		public Int32? V9TemplateRefresh { get; set; }
		
		public enum V9TemplateTimeoutEnum
		{
			[RosValue("V9TemplateTimeout")]
			V9TemplateTimeout,
		}
		
		[RosValue("v9-template-timeout")]
		public V9TemplateTimeoutEnum? V9TemplateTimeout { get; set; }
		
		public enum VersionEnum
		{
			[RosValue("1")]
			E1,
			[RosValue("5")]
			E5,
			[RosValue("9")]
			E9,
		}
		
		[RosValue("version")]
		public VersionEnum? Version { get; set; }
		
	}
	[RosObject("ip upnp", CanSet=true, CanGet=true)]
	public class IpUpnp : RosValueObject
	{
		// whether or not should the users be allowed to disable router's external interface
		[RosValue("allow-disable-external-interface")]
		public Boolean? AllowDisableExternalInterface { get; set; }
		
		// Enable or not UPnP
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// This is to enable a workaround for some broken implementations, which are handling the absence of UPnP rules inincorrectly
		[RosValue("show-dummy-rule")]
		public Boolean? ShowDummyRule { get; set; }
		
	}
	[RosObject("ip upnp interfaces", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class IpUpnpInterfaces : RosItemObject<IpUpnpInterfaces>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface name
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("external")]
			External,
			[RosValue("internal")]
			Internal,
		}
		
		// Interface type
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
	}
	[RosObject("ipv6 address", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Ipv6Address : RosItemObject<Ipv6Address>
	{
		[RosValue("address")]
		public String Address { get; set; }
		
		[RosValue("advertise")]
		public Boolean? Advertise { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("eui-64")]
		public Boolean? Eui64 { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("actual-interface", IsDynamic=true)]
		public RouterOS.Interface ActualInterface { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("global", IsDynamic=true)]
		public String Global { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("link-local", IsDynamic=true)]
		public String LinkLocal { get; set; }
		
	}
	[RosObject("ipv6 firewall address-list", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Ipv6FirewallAddressList : RosItemObject<Ipv6FirewallAddressList>
	{
		[RosValue("address")]
		public IPv6Prefix? Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("list")]
		public String List { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
	}
	[RosObject("ipv6 firewall connection", CanGet=true, CanRemove=true)]
	public class Ipv6FirewallConnection : RosItemObject<Ipv6FirewallConnection>
	{
		[RosValue("assured", IsDynamic=true)]
		public String Assured { get; set; }
		
		[RosValue("connection-mark", IsDynamic=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-type", IsDynamic=true)]
		public String ConnectionType { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-port", IsDynamic=true)]
		public Int32? DstPort { get; set; }
		
		[RosValue("gre-key", IsDynamic=true)]
		public Int32? GreKey { get; set; }
		
		[RosValue("gre-protocol", IsDynamic=true)]
		public Int32? GreProtocol { get; set; }
		
		[RosValue("gre-version", IsDynamic=true)]
		public Int32? GreVersion { get; set; }
		
		[RosValue("icmp-code", IsDynamic=true)]
		public Int32? IcmpCode { get; set; }
		
		[RosValue("icmp-id", IsDynamic=true)]
		public Int32? IcmpId { get; set; }
		
		[RosValue("icmp-type", IsDynamic=true)]
		public Int32? IcmpType { get; set; }
		
		public enum P2pType
		{
			[RosValue("bit-torrent")]
			BitTorrent,
			[RosValue("blubster")]
			Blubster,
			[RosValue("direct-connect")]
			DirectConnect,
			[RosValue("edonkey")]
			Edonkey,
			[RosValue("fasttrack")]
			Fasttrack,
			[RosValue("gnutella")]
			Gnutella,
			[RosValue("none")]
			None,
			[RosValue("soulseek")]
			Soulseek,
			[RosValue("winmx")]
			Winmx,
		}
		
		[RosValue("p2p", IsDynamic=true)]
		public P2pType? P2p { get; set; }
		
		public enum ProtocolType
		{
			[RosValue("ddp")]
			Ddp,
			[RosValue("egp")]
			Egp,
			[RosValue("encap")]
			Encap,
			[RosValue("ggp")]
			Ggp,
			[RosValue("gre")]
			Gre,
			[RosValue("hmp")]
			Hmp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("icmpv6")]
			Icmpv6,
			[RosValue("idpr-cmtp")]
			IdprCmtp,
			[RosValue("igmp")]
			Igmp,
			[RosValue("ipencap")]
			Ipencap,
			[RosValue("ipip")]
			Ipip,
			[RosValue("ipsec-ah")]
			IpsecAh,
			[RosValue("ipsec-esp")]
			IpsecEsp,
			[RosValue("iso-tp4")]
			IsoTp4,
			[RosValue("ospf")]
			Ospf,
			[RosValue("pim")]
			Pim,
			[RosValue("pup")]
			Pup,
			[RosValue("rdp")]
			Rdp,
			[RosValue("rspf")]
			Rspf,
			[RosValue("st")]
			St,
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
			[RosValue("vmtp")]
			Vmtp,
			[RosValue("vrrp")]
			Vrrp,
			[RosValue("xns-idp")]
			XnsIdp,
			[RosValue("xtp")]
			Xtp,
		}
		
		[RosValue("protocol", IsDynamic=true)]
		public ProtocolType? Protocol { get; set; }
		
		[RosValue("reply-dst-address", IsDynamic=true)]
		public String ReplyDstAddress { get; set; }
		
		[RosValue("reply-dst-port", IsDynamic=true)]
		public Int32? ReplyDstPort { get; set; }
		
		[RosValue("reply-src-address", IsDynamic=true)]
		public String ReplySrcAddress { get; set; }
		
		[RosValue("reply-src-port", IsDynamic=true)]
		public Int32? ReplySrcPort { get; set; }
		
		public enum SeenType
		{
			[RosValue("assured")]
			Assured,
			[RosValue("connection-mark")]
			ConnectionMark,
			[RosValue("connection-type")]
			ConnectionType,
			[RosValue("dst-address")]
			DstAddress,
			[RosValue("dst-port")]
			DstPort,
			[RosValue("gre-key")]
			GreKey,
			[RosValue("gre-protocol")]
			GreProtocol,
			[RosValue("gre-version")]
			GreVersion,
			[RosValue("icmp-code")]
			IcmpCode,
			[RosValue("icmp-id")]
			IcmpId,
			[RosValue("icmp-type")]
			IcmpType,
			[RosValue("p2p")]
			P2p,
			[RosValue("protocol")]
			Protocol,
			[RosValue("reply-dst-address")]
			ReplyDstAddress,
			[RosValue("reply-dst-port")]
			ReplyDstPort,
			[RosValue("reply-src-address")]
			ReplySrcAddress,
			[RosValue("reply-src-port")]
			ReplySrcPort,
			[RosValue("seen")]
			Seen,
			[RosValue("reply")]
			Reply,
			[RosValue("src-address")]
			SrcAddress,
			[RosValue("src-port")]
			SrcPort,
			[RosValue("tcp-state")]
			TcpState,
			[RosValue("timeout")]
			Timeout,
		}
		
		[RosValue("seen", IsDynamic=true)]
		public SeenType? Seen { get; set; }
		
		public enum ReplyType
		{
			[RosValue("assured")]
			Assured,
			[RosValue("connection-mark")]
			ConnectionMark,
			[RosValue("connection-type")]
			ConnectionType,
			[RosValue("dst-address")]
			DstAddress,
			[RosValue("dst-port")]
			DstPort,
			[RosValue("gre-key")]
			GreKey,
			[RosValue("gre-protocol")]
			GreProtocol,
			[RosValue("gre-version")]
			GreVersion,
			[RosValue("icmp-code")]
			IcmpCode,
			[RosValue("icmp-id")]
			IcmpId,
			[RosValue("icmp-type")]
			IcmpType,
			[RosValue("p2p")]
			P2p,
			[RosValue("protocol")]
			Protocol,
			[RosValue("reply-dst-address")]
			ReplyDstAddress,
			[RosValue("reply-dst-port")]
			ReplyDstPort,
			[RosValue("reply-src-address")]
			ReplySrcAddress,
			[RosValue("reply-src-port")]
			ReplySrcPort,
			[RosValue("seen")]
			Seen,
			[RosValue("reply")]
			Reply,
			[RosValue("src-address")]
			SrcAddress,
			[RosValue("src-port")]
			SrcPort,
			[RosValue("tcp-state")]
			TcpState,
			[RosValue("timeout")]
			Timeout,
		}
		
		[RosValue("reply", IsDynamic=true)]
		public ReplyType? Reply { get; set; }
		
		[RosValue("src-address", IsDynamic=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("src-port", IsDynamic=true)]
		public Int32? SrcPort { get; set; }
		
		public enum TcpStateType
		{
			[RosValue("close")]
			Close,
			[RosValue("close-wait")]
			CloseWait,
			[RosValue("established")]
			Established,
			[RosValue("fin-wait")]
			FinWait,
			[RosValue("last-ack")]
			LastAck,
			[RosValue("listen")]
			Listen,
			[RosValue("none")]
			None,
			[RosValue("syn-recv")]
			SynRecv,
			[RosValue("syn-sent")]
			SynSent,
			[RosValue("time-wait")]
			TimeWait,
		}
		
		[RosValue("tcp-state", IsDynamic=true)]
		public TcpStateType? TcpState { get; set; }
		
		[RosValue("timeout", IsDynamic=true)]
		public TimeSpan? Timeout { get; set; }
		
	}
	[RosObject("ipv6 firewall filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class Ipv6FirewallFilter : RosItemObject<Ipv6FirewallFilter>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("add-dst-to-address-list")]
			AddDstToAddressList,
			[RosValue("add-src-to-address-list")]
			AddSrcToAddressList,
			[RosValue("drop")]
			Drop,
			[RosValue("jump")]
			Jump,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		public enum ConnectionStateEnum
		{
			[RosValue("established")]
			Established,
			[RosValue("invalid")]
			Invalid,
			[RosValue("new")]
			New,
			[RosValue("related")]
			Related,
		}
		
		[RosValue("connection-state", CanUnset=true)]
		public ConnectionStateEnum? ConnectionState { get; set; }
		
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("headers", CanUnset=true)]
		public String Headers { get; set; }
		
		[RosValue("hop-limit", CanUnset=true)]
		public String HopLimit { get; set; }
		
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
		}
		
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		public enum RejectWithEnum
		{
			[RosValue("icmp-address-unreachable")]
			IcmpAddressUnreachable,
			[RosValue("icmp-admin-prohibited")]
			IcmpAdminProhibited,
			[RosValue("icmp-no-route")]
			IcmpNoRoute,
			[RosValue("icmp-not-neighbour")]
			IcmpNotNeighbour,
			[RosValue("icmp-port-unreachable")]
			IcmpPortUnreachable,
		}
		
		[RosValue("reject-with")]
		public RejectWithEnum? RejectWith { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		[RosValue("tcp-flags", CanUnset=true)]
		public String TcpFlags { get; set; }
		
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ipv6 firewall mangle", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class Ipv6FirewallMangle : RosItemObject<Ipv6FirewallMangle>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("add-dst-to-address-list")]
			AddDstToAddressList,
			[RosValue("add-src-to-address-list")]
			AddSrcToAddressList,
			[RosValue("change-dscp")]
			ChangeDscp,
			[RosValue("change-hop-limit")]
			ChangeHopLimit,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		[RosValue("address-list")]
		public String AddressList { get; set; }
		
		[RosValue("address-list-timeout")]
		public TimeSpan? AddressListTimeout { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
			[RosValue("postrouting")]
			Postrouting,
			[RosValue("prerouting")]
			Prerouting,
		}
		
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public struct ConnectionBytesType
                {
                    public Int32 From { get; set; }
                    public Int32 To { get; set; }

                    public static ConnectionBytesType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null), To = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new ConnectionBytesType() { From = TypeFormatter.ConvertFromString<Int32>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(To != null)
                            return String.Concat(From, '-', To);
                        else
                            return From.ToString();
                    }
                }
		[RosValue("connection-bytes", CanUnset=true)]
		public ConnectionBytesType? ConnectionBytes { get; set; }
		
		[RosValue("connection-limit", CanUnset=true)]
		public String ConnectionLimit { get; set; }
		
		[RosValue("connection-mark", CanUnset=true)]
		public String ConnectionMark { get; set; }
		
		[RosValue("connection-rate", CanUnset=true)]
		public String ConnectionRate { get; set; }
		
		public enum ConnectionStateEnum
		{
			[RosValue("established")]
			Established,
			[RosValue("invalid")]
			Invalid,
			[RosValue("new")]
			New,
			[RosValue("related")]
			Related,
		}
		
		[RosValue("connection-state", CanUnset=true)]
		public ConnectionStateEnum? ConnectionState { get; set; }
		
		[RosValue("connection-type", CanUnset=true)]
		public String ConnectionType { get; set; }
		
		[RosValue("content", CanUnset=true)]
		public String Content { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dscp", CanUnset=true)]
		public String Dscp { get; set; }
		
		[RosValue("dst-address", CanUnset=true)]
		public String DstAddress { get; set; }
		
		[RosValue("dst-address-list", CanUnset=true)]
		public String DstAddressList { get; set; }
		
		[RosValue("dst-limit", CanUnset=true)]
		public String DstLimit { get; set; }
		
		[RosValue("dst-port", CanUnset=true)]
		public String DstPort { get; set; }
		
		[RosValue("headers", CanUnset=true)]
		public String Headers { get; set; }
		
		[RosValue("hop-limit", CanUnset=true)]
		public String HopLimit { get; set; }
		
		[RosValue("icmp-options", CanUnset=true)]
		public String IcmpOptions { get; set; }
		
		[RosValue("in-bridge-port", CanUnset=true)]
		public String InBridgePort { get; set; }
		
		[RosValue("in-interface", CanUnset=true)]
		public String InInterface { get; set; }
		
		[RosValue("ingress-priority", CanUnset=true)]
		public String IngressPriority { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("forward")]
			Forward,
			[RosValue("input")]
			Input,
			[RosValue("output")]
			Output,
			[RosValue("postrouting")]
			Postrouting,
			[RosValue("prerouting")]
			Prerouting,
		}
		
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("limit", CanUnset=true)]
		public String Limit { get; set; }
		
		[RosValue("log-prefix")]
		public String LogPrefix { get; set; }
		
		[RosValue("new-connection-mark")]
		public String NewConnectionMark { get; set; }
		
		[RosValue("new-dscp")]
		public Int32? NewDscp { get; set; }
		
		[RosValue("new-hop-limit")]
		public String NewHopLimit { get; set; }
		
		public enum MssActionEnum
		{
			[RosValue("clamp-to-pmtu")]
			ClampToPmtu,
		}
		
		public struct NewMssType 
                { 
                    private object value; 
                    public MssActionEnum? MssAction 
                { 
                    get { return value as MssActionEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewMssType(MssActionEnum? value) 
                { 
                    return new NewMssType() { value = value }; 
                }public Int32? MssValue 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NewMssType(Int32? value) 
                { 
                    return new NewMssType() { value = value }; 
                } 
                    public static NewMssType Parse(string str, Connection conn)
                    {
                        
                try { return new NewMssType() { value = TypeFormatter.ConvertFromString<MssActionEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new NewMssType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("new-mss")]
		public NewMssType? NewMss { get; set; }
		
		[RosValue("new-packet-mark")]
		public String NewPacketMark { get; set; }
		
		[RosValue("new-routing-mark")]
		public String NewRoutingMark { get; set; }
		
		[RosValue("nth", CanUnset=true)]
		public String Nth { get; set; }
		
		[RosValue("out-bridge-port", CanUnset=true)]
		public String OutBridgePort { get; set; }
		
		[RosValue("out-interface", CanUnset=true)]
		public String OutInterface { get; set; }
		
		[RosValue("packet-mark", CanUnset=true)]
		public String PacketMark { get; set; }
		
		[RosValue("packet-size", CanUnset=true)]
		public String PacketSize { get; set; }
		
		[RosValue("passthrough")]
		public Boolean? Passthrough { get; set; }
		
		[RosValue("per-connection-classifier", CanUnset=true)]
		public String PerConnectionClassifier { get; set; }
		
		[RosValue("port", CanUnset=true)]
		public String Port { get; set; }
		
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		[RosValue("random", CanUnset=true)]
		public Int32? Random { get; set; }
		
		[RosValue("src-address", CanUnset=true)]
		public String SrcAddress { get; set; }
		
		[RosValue("src-address-list", CanUnset=true)]
		public String SrcAddressList { get; set; }
		
		[RosValue("src-mac-address", CanUnset=true)]
		public String SrcMacAddress { get; set; }
		
		[RosValue("src-port", CanUnset=true)]
		public String SrcPort { get; set; }
		
		[RosValue("tcp-flags", CanUnset=true)]
		public String TcpFlags { get; set; }
		
		[RosValue("tcp-mss", CanUnset=true)]
		public String TcpMss { get; set; }
		
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int64? Packets { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ipv6 nd", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Ipv6Nd : RosItemObject<Ipv6Nd>
	{
		[RosValue("advertise-dns")]
		public Boolean? AdvertiseDns { get; set; }
		
		[RosValue("advertise-mac-address")]
		public Boolean? AdvertiseMacAddress { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("unspecified")]
			Unspecified,
		}
		
		public struct HopLimitType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HopLimitType(SpecialEnum? value) 
                { 
                    return new HopLimitType() { value = value }; 
                }public Int32? Value 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HopLimitType(Int32? value) 
                { 
                    return new HopLimitType() { value = value }; 
                } 
                    public static HopLimitType Parse(string str, Connection conn)
                    {
                        
                try { return new HopLimitType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new HopLimitType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("hop-limit")]
		public HopLimitType? HopLimit { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("managed-address-configuration")]
		public Boolean? ManagedAddressConfiguration { get; set; }
		
		public struct MtuType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MtuType(SpecialEnum? value) 
                { 
                    return new MtuType() { value = value }; 
                }public Int32? Value 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MtuType(Int32? value) 
                { 
                    return new MtuType() { value = value }; 
                } 
                    public static MtuType Parse(string str, Connection conn)
                    {
                        
                try { return new MtuType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new MtuType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("mtu")]
		public MtuType? Mtu { get; set; }
		
		[RosValue("other-configuration")]
		public Boolean? OtherConfiguration { get; set; }
		
		[RosValue("ra-delay")]
		public TimeSpan? RaDelay { get; set; }
		
		[RosValue("ra-interval")]
		public String RaInterval { get; set; }
		
		public enum RaLifetimeEnum
		{
			[RosValue("Value")]
			Value,
		}
		
		public struct RaLifetimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RaLifetimeType(SpecialEnum? value) 
                { 
                    return new RaLifetimeType() { value = value }; 
                }public RaLifetimeEnum? State 
                { 
                    get { return value as RaLifetimeEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RaLifetimeType(RaLifetimeEnum? value) 
                { 
                    return new RaLifetimeType() { value = value }; 
                } 
                    public static RaLifetimeType Parse(string str, Connection conn)
                    {
                        
                try { return new RaLifetimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new RaLifetimeType() { value = TypeFormatter.ConvertFromString<RaLifetimeEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("ra-lifetime")]
		public RaLifetimeType? RaLifetime { get; set; }
		
		public enum ReachableTimeEnum
		{
			[RosValue("Value")]
			Value,
		}
		
		public struct ReachableTimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ReachableTimeType(SpecialEnum? value) 
                { 
                    return new ReachableTimeType() { value = value }; 
                }public ReachableTimeEnum? State 
                { 
                    get { return value as ReachableTimeEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ReachableTimeType(ReachableTimeEnum? value) 
                { 
                    return new ReachableTimeType() { value = value }; 
                } 
                    public static ReachableTimeType Parse(string str, Connection conn)
                    {
                        
                try { return new ReachableTimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new ReachableTimeType() { value = TypeFormatter.ConvertFromString<ReachableTimeEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("reachable-time")]
		public ReachableTimeType? ReachableTime { get; set; }
		
		public struct RetransmitIntervalType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RetransmitIntervalType(SpecialEnum? value) 
                { 
                    return new RetransmitIntervalType() { value = value }; 
                }public TimeSpan? Value 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RetransmitIntervalType(TimeSpan? value) 
                { 
                    return new RetransmitIntervalType() { value = value }; 
                } 
                    public static RetransmitIntervalType Parse(string str, Connection conn)
                    {
                        
                try { return new RetransmitIntervalType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new RetransmitIntervalType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("retransmit-interval")]
		public RetransmitIntervalType? RetransmitInterval { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ipv6 nd prefix", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Ipv6NdPrefix : RosItemObject<Ipv6NdPrefix>
	{
		[RosValue("autonomous")]
		public Boolean? Autonomous { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("on-link")]
		public Boolean? OnLink { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("infinity")]
			Infinity,
		}
		
		public struct PreferredLifetimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PreferredLifetimeType(SpecialEnum? value) 
                { 
                    return new PreferredLifetimeType() { value = value }; 
                }public TimeSpan? Value 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PreferredLifetimeType(TimeSpan? value) 
                { 
                    return new PreferredLifetimeType() { value = value }; 
                } 
                    public static PreferredLifetimeType Parse(string str, Connection conn)
                    {
                        
                try { return new PreferredLifetimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new PreferredLifetimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("preferred-lifetime")]
		public PreferredLifetimeType? PreferredLifetime { get; set; }
		
		[RosValue("prefix")]
		public IPv6Prefix? Prefix { get; set; }
		
		public struct ValidLifetimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ValidLifetimeType(SpecialEnum? value) 
                { 
                    return new ValidLifetimeType() { value = value }; 
                }public TimeSpan? Value 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ValidLifetimeType(TimeSpan? value) 
                { 
                    return new ValidLifetimeType() { value = value }; 
                } 
                    public static ValidLifetimeType Parse(string str, Connection conn)
                    {
                        
                try { return new ValidLifetimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new ValidLifetimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("valid-lifetime")]
		public ValidLifetimeType? ValidLifetime { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("ipv6 nd prefix default", CanSet=true, CanGet=true)]
	public class Ipv6NdPrefixDefault : RosValueObject
	{
		[RosValue("autonomous")]
		public Boolean? Autonomous { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("infinity")]
			Infinity,
		}
		
		public struct PreferredLifetimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PreferredLifetimeType(SpecialEnum? value) 
                { 
                    return new PreferredLifetimeType() { value = value }; 
                }public TimeSpan? Value 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PreferredLifetimeType(TimeSpan? value) 
                { 
                    return new PreferredLifetimeType() { value = value }; 
                } 
                    public static PreferredLifetimeType Parse(string str, Connection conn)
                    {
                        
                try { return new PreferredLifetimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new PreferredLifetimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("preferred-lifetime")]
		public PreferredLifetimeType? PreferredLifetime { get; set; }
		
		public struct ValidLifetimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ValidLifetimeType(SpecialEnum? value) 
                { 
                    return new ValidLifetimeType() { value = value }; 
                }public TimeSpan? Value 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ValidLifetimeType(TimeSpan? value) 
                { 
                    return new ValidLifetimeType() { value = value }; 
                } 
                    public static ValidLifetimeType Parse(string str, Connection conn)
                    {
                        
                try { return new ValidLifetimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new ValidLifetimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("valid-lifetime")]
		public ValidLifetimeType? ValidLifetime { get; set; }
		
	}
	[RosObject("ipv6 neighbor", CanSet=true, CanGet=true)]
	public class Ipv6Neighbor : RosItemObject<Ipv6Neighbor>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("address", IsDynamic=true)]
		public String Address { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("mac-address", IsDynamic=true)]
		public MACAddress? MacAddress { get; set; }
		
		[RosValue("router", IsDynamic=true)]
		public String Router { get; set; }
		
		[RosValue("status", IsDynamic=true)]
		public String Status { get; set; }
		
	}
	[RosObject("ipv6 route", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class Ipv6Route : RosItemObject<Ipv6Route>
	{
		[RosValue("bgp-as-path", CanUnset=true)]
		public String BgpAsPath { get; set; }
		
		[RosValue("bgp-atomic-aggregate", CanUnset=true)]
		public Boolean? BgpAtomicAggregate { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("internet")]
			Internet,
			[RosValue("local-as")]
			LocalAs,
			[RosValue("no-advertise")]
			NoAdvertise,
			[RosValue("no-export")]
			NoExport,
		}
		
		public struct NumbersType
                {
                    public Int32 As { get; set; }
                    public Int32 Num { get; set; }

                    public static NumbersType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new NumbersType() { As = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Num = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(As, ':', Num);
                    }
                }
		public struct CommunityType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(SpecialEnum? value) 
                { 
                    return new CommunityType() { value = value }; 
                }public NumbersType? Numbers 
                { 
                    get { return value as NumbersType?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(NumbersType? value) 
                { 
                    return new CommunityType() { value = value }; 
                } 
                    public static CommunityType Parse(string str, Connection conn)
                    {
                        
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<NumbersType>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bgp-communities", CanUnset=true)]
		public CommunityType[] BgpCommunities { get; set; }
		
		[RosValue("bgp-local-pref", CanUnset=true)]
		public Int32? BgpLocalPref { get; set; }
		
		[RosValue("bgp-med", CanUnset=true)]
		public Int32? BgpMed { get; set; }
		
		public enum BgpOriginEnum
		{
			[RosValue("egp")]
			Egp,
			[RosValue("igp")]
			Igp,
			[RosValue("incomplete")]
			Incomplete,
		}
		
		[RosValue("bgp-origin", CanUnset=true)]
		public BgpOriginEnum? BgpOrigin { get; set; }
		
		public struct BgpPrependType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BgpPrependType(Int32? value) 
                { 
                    return new BgpPrependType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator BgpPrependType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new BgpPrependType() { value = value }; 
                } 
                    public static BgpPrependType Parse(string str, Connection conn)
                    {
                        
                try { return new BgpPrependType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new BgpPrependType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("bgp-prepend", CanUnset=true)]
		public BgpPrependType? BgpPrepend { get; set; }
		
		public enum CheckGatewayEnum
		{
			[RosValue("ping")]
			Ping,
		}
		
		[RosValue("check-gateway", CanUnset=true)]
		public CheckGatewayEnum? CheckGateway { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("distance", CanUnset=true)]
		public Int32? Distance { get; set; }
		
		[RosValue("dst-address")]
		public IPv6Prefix? DstAddress { get; set; }
		
		[RosValue("gateway")]
		public String Gateway { get; set; }
		
		[RosValue("route-tag", CanUnset=true)]
		public Int32? RouteTag { get; set; }
		
		[RosValue("scope")]
		public Int32? Scope { get; set; }
		
		[RosValue("target-scope")]
		public Int32? TargetScope { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("unicast")]
			Unicast,
			[RosValue("unreachable")]
			Unreachable,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("bgp-weight", CanUnset=true)]
		public String BgpWeight { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("bgp", IsDynamic=true)]
		public String Bgp { get; set; }
		
		[RosValue("connect", IsDynamic=true)]
		public String Connect { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("gateway-status", IsDynamic=true)]
		public String GatewayStatus { get; set; }
		
		[RosValue("ospf", IsDynamic=true)]
		public String Ospf { get; set; }
		
		[RosValue("ospf-metric", IsDynamic=true)]
		public Int32? OspfMetric { get; set; }
		
		public enum OspfTypeType
		{
			[RosValue("external-type-1")]
			ExternalType1,
			[RosValue("external-type-2")]
			ExternalType2,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
		}
		
		[RosValue("ospf-type", IsDynamic=true)]
		public OspfTypeType? OspfType { get; set; }
		
		[RosValue("received-from", IsDynamic=true)]
		public String ReceivedFrom { get; set; }
		
		[RosValue("rip", IsDynamic=true)]
		public String Rip { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		[RosValue("unreachable", IsDynamic=true)]
		public String Unreachable { get; set; }
		
	}
	[RosObject("log", CanGet=true)]
	public class Log : RosItemObject<Log>
	{
		[RosValue("buffer", IsDynamic=true)]
		public RouterOS.SystemLoggingAction Buffer { get; set; }
		
		[RosValue("message", IsDynamic=true)]
		public String Message { get; set; }
		
		[RosValue("time", IsDynamic=true)]
		public DateTime? Time { get; set; }
		
		public enum TopicEnum
		{
			[RosValue("account")]
			Account,
			[RosValue("async")]
			Async,
			[RosValue("backup")]
			Backup,
			[RosValue("bfd")]
			Bfd,
			[RosValue("bgp")]
			Bgp,
		}
		
		[RosValue("topics", IsDynamic=true)]
		public TopicEnum? Topics { get; set; }
		
	}
	[RosObject("mpls", CanSet=true, CanGet=true)]
	public class Mpls : RosValueObject
	{
		[RosValue("dynamic-label-range")]
		public String DynamicLabelRange { get; set; }
		
		public enum PropagateTtlEnum
		{
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		[RosValue("propagate-ttl")]
		public PropagateTtlEnum? PropagateTtl { get; set; }
		
	}
	[RosObject("mpls forwarding-table", CanGet=true)]
	public class MplsForwardingTable : RosItemObject<MplsForwardingTable>
	{
		[RosValue("bytes", IsDynamic=true)]
		public Int32? Bytes { get; set; }
		
		[RosValue("destination", IsDynamic=true)]
		public IPPrefix? Destination { get; set; }
		
		[RosValue("in-label", IsDynamic=true)]
		public Int32? InLabel { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("ldp", IsDynamic=true)]
		public String Ldp { get; set; }
		
		[RosValue("nexthop", IsDynamic=true)]
		public IPAddress Nexthop { get; set; }
		
		[RosValue("out-labels", IsDynamic=true)]
		public Int32? OutLabels { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("traffic-eng", IsDynamic=true)]
		public String TrafficEng { get; set; }
		
		[RosValue("vpls", IsDynamic=true)]
		public String Vpls { get; set; }
		
	}
	[RosObject("mpls interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsInterface : RosItemObject<MplsInterface>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum InterfaceEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public InterfaceEnum? State 
                { 
                    get { return value as InterfaceEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(InterfaceEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<InterfaceEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("mpls-mtu")]
		public Int32? MplsMtu { get; set; }
		
	}
	[RosObject("mpls ldp", CanSet=true, CanGet=true)]
	public class MplsLdp : RosValueObject
	{
		[RosValue("distribute-for-default-route")]
		public Boolean? DistributeForDefaultRoute { get; set; }
		
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		[RosValue("hop-limit")]
		public Int32? HopLimit { get; set; }
		
		[RosValue("loop-detect")]
		public Boolean? LoopDetect { get; set; }
		
		[RosValue("lsr-id")]
		public IPAddress LsrId { get; set; }
		
		[RosValue("path-vector-limit")]
		public Int32? PathVectorLimit { get; set; }
		
		[RosValue("transport-address")]
		public IPAddress TransportAddress { get; set; }
		
		[RosValue("use-explicit-null")]
		public Boolean? UseExplicitNull { get; set; }
		
	}
	[RosObject("mpls ldp accept-filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class MplsLdpAcceptFilter : RosItemObject<MplsLdpAcceptFilter>
	{
		[RosValue("accept")]
		public Boolean? Accept { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("neighbor")]
		public IPAddress Neighbor { get; set; }
		
		[RosValue("prefix")]
		public IPPrefix? Prefix { get; set; }
		
	}
	[RosObject("mpls ldp advertise-filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class MplsLdpAdvertiseFilter : RosItemObject<MplsLdpAdvertiseFilter>
	{
		[RosValue("advertise")]
		public Boolean? Advertise { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("neighbor")]
		public IPAddress Neighbor { get; set; }
		
		[RosValue("prefix")]
		public IPPrefix? Prefix { get; set; }
		
	}
	[RosObject("mpls ldp interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsLdpInterface : RosItemObject<MplsLdpInterface>
	{
		[RosValue("accept-dynamic-neighbors")]
		public Boolean? AcceptDynamicNeighbors { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("hello-interval")]
		public TimeSpan? HelloInterval { get; set; }
		
		[RosValue("hold-time")]
		public TimeSpan? HoldTime { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("transport-address")]
		public IPAddress TransportAddress { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("mpls ldp neighbor", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsLdpNeighbor : RosItemObject<MplsLdpNeighbor>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("send-targeted")]
		public Boolean? SendTargeted { get; set; }
		
		[RosValue("transport")]
		public IPAddress Transport { get; set; }
		
		[RosValue("addresses", IsDynamic=true)]
		public IPAddress Addresses { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("local-transport", IsDynamic=true)]
		public IPAddress LocalTransport { get; set; }
		
		[RosValue("operational", IsDynamic=true)]
		public String Operational { get; set; }
		
		[RosValue("peer", IsDynamic=true)]
		public IPAddress Peer { get; set; }
		
		[RosValue("sending-targeted-hello", IsDynamic=true)]
		public String SendingTargetedHello { get; set; }
		
		[RosValue("vpls", IsDynamic=true)]
		public String Vpls { get; set; }
		
	}
	[RosObject("mpls local-bindings", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsLocalBindings : RosItemObject<MplsLocalBindings>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address")]
		public IPPrefix? DstAddress { get; set; }
		
		public enum LabelEnumEnum
		{
			[RosValue("alert")]
			Alert,
			[RosValue("expl-null")]
			ExplNull,
			[RosValue("expl-null6")]
			ExplNull6,
			[RosValue("impl-null")]
			ImplNull,
			[RosValue("none")]
			None,
		}
		
		public struct LabelType 
                { 
                    private object value; 
                    public LabelEnumEnum? LabelEnum 
                { 
                    get { return value as LabelEnumEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator LabelType(LabelEnumEnum? value) 
                { 
                    return new LabelType() { value = value }; 
                }public Int32? LabelNum 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator LabelType(Int32? value) 
                { 
                    return new LabelType() { value = value }; 
                } 
                    public static LabelType Parse(string str, Connection conn)
                    {
                        
                try { return new LabelType() { value = TypeFormatter.ConvertFromString<LabelEnumEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new LabelType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("label")]
		public LabelType? Label { get; set; }
		
		[RosValue("adv-path", IsDynamic=true)]
		public String AdvPath { get; set; }
		
		[RosValue("advertised", IsDynamic=true)]
		public String Advertised { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("egress", IsDynamic=true)]
		public String Egress { get; set; }
		
		[RosValue("gateway-route", IsDynamic=true)]
		public String GatewayRoute { get; set; }
		
		[RosValue("local-route", IsDynamic=true)]
		public String LocalRoute { get; set; }
		
		[RosValue("peers", IsDynamic=true)]
		public IPAddress Peers { get; set; }
		
	}
	[RosObject("mpls remote-bindings", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsRemoteBindings : RosItemObject<MplsRemoteBindings>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address")]
		public IPPrefix? DstAddress { get; set; }
		
		public enum LabelEnumEnum
		{
			[RosValue("alert")]
			Alert,
			[RosValue("expl-null")]
			ExplNull,
			[RosValue("expl-null6")]
			ExplNull6,
			[RosValue("impl-null")]
			ImplNull,
			[RosValue("none")]
			None,
		}
		
		public struct LabelType 
                { 
                    private object value; 
                    public LabelEnumEnum? LabelEnum 
                { 
                    get { return value as LabelEnumEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator LabelType(LabelEnumEnum? value) 
                { 
                    return new LabelType() { value = value }; 
                }public Int32? LabelNum 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator LabelType(Int32? value) 
                { 
                    return new LabelType() { value = value }; 
                } 
                    public static LabelType Parse(string str, Connection conn)
                    {
                        
                try { return new LabelType() { value = TypeFormatter.ConvertFromString<LabelEnumEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new LabelType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("label")]
		public LabelType? Label { get; set; }
		
		[RosValue("nexthop")]
		public IPAddress Nexthop { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("path", IsDynamic=true)]
		public String Path { get; set; }
		
		[RosValue("peer", IsDynamic=true)]
		public IPAddress Peer { get; set; }
		
	}
	[RosObject("mpls traffic-eng interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class MplsTrafficEngInterface : RosItemObject<MplsTrafficEngInterface>
	{
		[RosValue("bandwidth")]
		public Bits Bandwidth { get; set; }
		
		[RosValue("blockade-k-factor")]
		public Int32? BlockadeKFactor { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("down-flood-thresholds")]
		public Int32[] DownFloodThresholds { get; set; }
		
		[RosValue("igp-flood-period")]
		public TimeSpan? IgpFloodPeriod { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("k-factor")]
		public Int32? KFactor { get; set; }
		
		[RosValue("refresh-time")]
		public TimeSpan? RefreshTime { get; set; }
		
		[RosValue("resource-class")]
		public Int32? ResourceClass { get; set; }
		
		[RosValue("te-metric")]
		public Int32? TeMetric { get; set; }
		
		[RosValue("up-flood-thresholds")]
		public Int32[] UpFloodThresholds { get; set; }
		
		[RosValue("use-udp")]
		public Boolean? UseUdp { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("remaining-bw", IsDynamic=true)]
		public Bits RemainingBw { get; set; }
		
	}
	[RosObject("mpls traffic-eng path-state", CanGet=true)]
	public class MplsTrafficEngPathState : RosItemObject<MplsTrafficEngPathState>
	{
		[RosValue("bandwidth", IsDynamic=true)]
		public Bits Bandwidth { get; set; }
		
		[RosValue("dst", IsDynamic=true)]
		public IPAddress Dst { get; set; }
		
		[RosValue("egress", IsDynamic=true)]
		public String Egress { get; set; }
		
		[RosValue("forwarding", IsDynamic=true)]
		public String Forwarding { get; set; }
		
		[RosValue("in-interface", IsDynamic=true)]
		public RouterOS.Interface InInterface { get; set; }
		
		[RosValue("in-previous-hop", IsDynamic=true)]
		public IPAddress InPreviousHop { get; set; }
		
		[RosValue("label", IsDynamic=true)]
		public Int32? Label { get; set; }
		
		[RosValue("locally-originated", IsDynamic=true)]
		public String LocallyOriginated { get; set; }
		
		[RosValue("out-interface", IsDynamic=true)]
		public RouterOS.Interface OutInterface { get; set; }
		
		[RosValue("out-label", IsDynamic=true)]
		public Int32? OutLabel { get; set; }
		
		[RosValue("out-next-hop", IsDynamic=true)]
		public IPAddress OutNextHop { get; set; }
		
		[RosValue("path-in-explicit-route", IsDynamic=true)]
		public String PathInExplicitRoute { get; set; }
		
		[RosValue("path-in-record-route", IsDynamic=true)]
		public String PathInRecordRoute { get; set; }
		
		[RosValue("path-out-explicit-route", IsDynamic=true)]
		public String PathOutExplicitRoute { get; set; }
		
		[RosValue("path-out-record-route", IsDynamic=true)]
		public String PathOutRecordRoute { get; set; }
		
		[RosValue("resv-bandwidth", IsDynamic=true)]
		public Bits ResvBandwidth { get; set; }
		
		[RosValue("resv-out-record-route", IsDynamic=true)]
		public String ResvOutRecordRoute { get; set; }
		
		[RosValue("sending-path", IsDynamic=true)]
		public String SendingPath { get; set; }
		
		[RosValue("sending-resv", IsDynamic=true)]
		public String SendingResv { get; set; }
		
		[RosValue("src", IsDynamic=true)]
		public IPAddress Src { get; set; }
		
	}
	[RosObject("mpls traffic-eng resv-state", CanGet=true)]
	public class MplsTrafficEngResvState : RosItemObject<MplsTrafficEngResvState>
	{
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("bandwidth", IsDynamic=true)]
		public Bits Bandwidth { get; set; }
		
		[RosValue("dst", IsDynamic=true)]
		public IPAddress Dst { get; set; }
		
		[RosValue("egress", IsDynamic=true)]
		public String Egress { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("label", IsDynamic=true)]
		public Int32? Label { get; set; }
		
		[RosValue("next-hop", IsDynamic=true)]
		public IPAddress NextHop { get; set; }
		
		[RosValue("non-output", IsDynamic=true)]
		public String NonOutput { get; set; }
		
		[RosValue("recorded-route", IsDynamic=true)]
		public String RecordedRoute { get; set; }
		
		[RosValue("shared", IsDynamic=true)]
		public String Shared { get; set; }
		
		[RosValue("src", IsDynamic=true)]
		public IPAddress Src { get; set; }
		
	}
	[RosObject("mpls traffic-eng tunnel-path", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class MplsTrafficEngTunnelPath : RosItemObject<MplsTrafficEngTunnelPath>
	{
		[RosValue("affinity-exclude", CanUnset=true)]
		public Int32? AffinityExclude { get; set; }
		
		[RosValue("affinity-include-all", CanUnset=true)]
		public Int32? AffinityIncludeAll { get; set; }
		
		[RosValue("affinity-include-any", CanUnset=true)]
		public Int32? AffinityIncludeAny { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("holding-priority", CanUnset=true)]
		public Int32? HoldingPriority { get; set; }
		
		public enum StrictEnum
		{
			[RosValue("strict")]
			Strict,
			[RosValue("loose")]
			Loose,
		}
		
		public struct HopType
                {
                    public IPAddress Address { get; set; }
                    public StrictEnum Strict { get; set; }

                    public static HopType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new HopType() { Address = TypeFormatter.ConvertFromString<IPAddress>(strs[0], null), Strict = TypeFormatter.ConvertFromString<StrictEnum>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Strict);
                    }
                }
		[RosValue("hops")]
		public HopType[] Hops { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("record-route", CanUnset=true)]
		public Boolean? RecordRoute { get; set; }
		
		[RosValue("reoptimize-interval", CanUnset=true)]
		public TimeSpan? ReoptimizeInterval { get; set; }
		
		[RosValue("setup-priority", CanUnset=true)]
		public Int32? SetupPriority { get; set; }
		
		[RosValue("use-cspf")]
		public Boolean? UseCspf { get; set; }
		
		public static MplsTrafficEngTunnelPath Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			MplsTrafficEngTunnelPath obj = new MplsTrafficEngTunnelPath();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("port", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Port : RosItemObject<Port>
	{
		public enum BaudRateEnum
		{
			[RosValue("1000000")]
			E1000000,
			[RosValue("110")]
			E110,
			[RosValue("115200")]
			E115200,
			[RosValue("1152000")]
			E1152000,
			[RosValue("1200")]
			E1200,
		}
		
		// Baud rate (bits/s)
		[RosValue("baud-rate")]
		public BaudRateEnum? BaudRate { get; set; }
		
		public enum DataBitsEnum
		{
			[RosValue("7")]
			E7,
			[RosValue("8")]
			E8,
		}
		
		// Number of bits for data
		[RosValue("data-bits")]
		public DataBitsEnum? DataBits { get; set; }
		
		public enum DtrEnum
		{
			[RosValue("on")]
			On,
			[RosValue("off")]
			Off,
		}
		
		[RosValue("dtr")]
		public DtrEnum? Dtr { get; set; }
		
		public enum FlowControlEnum
		{
			[RosValue("hardware")]
			Hardware,
			[RosValue("none")]
			None,
			[RosValue("xon-xoff")]
			XonXoff,
		}
		
		// Type of flow control
		[RosValue("flow-control")]
		public FlowControlEnum? FlowControl { get; set; }
		
		// New port name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum ParityEnum
		{
			[RosValue("even")]
			Even,
			[RosValue("none")]
			None,
			[RosValue("odd")]
			Odd,
		}
		
		// Parity
		[RosValue("parity")]
		public ParityEnum? Parity { get; set; }
		
		public enum RtsEnum
		{
			[RosValue("on")]
			On,
			[RosValue("off")]
			Off,
		}
		
		[RosValue("rts")]
		public RtsEnum? Rts { get; set; }
		
		public enum StopBitsEnum
		{
			[RosValue("1")]
			E1,
			[RosValue("2")]
			E2,
		}
		
		// Number of bits for control
		[RosValue("stop-bits")]
		public StopBitsEnum? StopBits { get; set; }
		
		[RosValue("channels", IsDynamic=true)]
		public Int32? Channels { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		public enum LineStateType
		{
			[RosValue("cts")]
			Cts,
			[RosValue("dcd")]
			Dcd,
			[RosValue("dsr")]
			Dsr,
			[RosValue("dtr")]
			Dtr,
			[RosValue("ri")]
			Ri,
			[RosValue("rts")]
			Rts,
		}
		
		[RosValue("line-state", IsDynamic=true)]
		public LineStateType? LineState { get; set; }
		
		[RosValue("used-by", IsDynamic=true)]
		public String UsedBy { get; set; }
		
		public static Port Serial0Port
		{
			get { return new Port() { Id = 1 }; }
		}
		
		public static Port Serial1Port
		{
			get { return new Port() { Id = 2 }; }
		}
		
		public static Port Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			Port obj = new Port();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("port firmware", CanSet=true, CanGet=true)]
	public class PortFirmware : RosValueObject
	{
		[RosValue("directory")]
		public String Directory { get; set; }
		
	}
	[RosObject("port remote-access", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class PortRemoteAccess : RosItemObject<PortRemoteAccess>
	{
		[RosValue("allowed-addresses")]
		public IPAddress AllowedAddresses { get; set; }
		
		[RosValue("channel")]
		public Int32? Channel { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("local-address", CanUnset=true)]
		public IPAddress LocalAddress { get; set; }
		
		[RosValue("log-file")]
		public String LogFile { get; set; }
		
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		public enum ProtocolEnum
		{
			[RosValue("raw")]
			Raw,
			[RosValue("rfc2217")]
			Rfc2217,
		}
		
		[RosValue("protocol")]
		public ProtocolEnum? Protocol { get; set; }
		
		[RosValue("tcp-port")]
		public Int32? TcpPort { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("busy", IsDynamic=true)]
		public String Busy { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		[RosValue("logging-active", IsDynamic=true)]
		public String LoggingActive { get; set; }
		
		[RosValue("remote-address", IsDynamic=true)]
		public IPAddress RemoteAddress { get; set; }
		
	}
	[RosObject("ppp aaa", CanSet=true, CanGet=true)]
	public class PppAaa : RosValueObject
	{
		// Enable/Disable accounting
		[RosValue("accounting")]
		public Boolean? Accounting { get; set; }
		
		// Defines time interval between communications with the router
		[RosValue("interim-update")]
		public TimeSpan? InterimUpdate { get; set; }
		
		// Use or not radius
		[RosValue("use-radius")]
		public Boolean? UseRadius { get; set; }
		
	}
	[RosObject("ppp active", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class PppActive : RosItemObject<PppActive>
	{
		// User address
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// User's IP address for PPTP or MAC address for PPoE
		[RosValue("caller-id")]
		public String CallerId { get; set; }
		
		// Encoding method
		[RosValue("encoding")]
		public String Encoding { get; set; }
		
		// maximum amount of bytes user can transmit
		[RosValue("limit-bytes-in")]
		public Int64? LimitBytesIn { get; set; }
		
		// maximum amount of bytes user can receive
		[RosValue("limit-bytes-out")]
		public Int64? LimitBytesOut { get; set; }
		
		// User name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum ServiceEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("async")]
			Async,
			[RosValue("isdn")]
			Isdn,
			[RosValue("l2tp")]
			L2tp,
			[RosValue("ovpn")]
			Ovpn,
		}
		
		// Service name
		[RosValue("service")]
		public ServiceEnum? Service { get; set; }
		
		// Specific session identifier
		[RosValue("session-id")]
		public Int32? SessionId { get; set; }
		
		// Time when user is up
		[RosValue("uptime")]
		public TimeSpan? Uptime { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int32? Bytes { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("radius", IsDynamic=true)]
		public String Radius { get; set; }
		
		public static PppActive Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			PppActive obj = new PppActive();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ppp profile", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class PppProfile : RosItemObject<PppProfile>
	{
		[RosValue("address-list", CanUnset=true)]
		public String AddressList { get; set; }
		
		[RosValue("bridge", CanUnset=true)]
		public RouterOS.Interface Bridge { get; set; }
		
		public enum ChangeTcpMssEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		// Change or not TCP protocol's Maximum Segment Size
		[RosValue("change-tcp-mss")]
		public ChangeTcpMssEnum? ChangeTcpMss { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// DNS server address
		[RosValue("dns-server", CanUnset=true)]
		public IPAddress[] DnsServer { get; set; }
		
		// The time limit when the link will be terminated if there is no activity
		[RosValue("idle-timeout", CanUnset=true)]
		public TimeSpan? IdleTimeout { get; set; }
		
		// Firewall chain name for incoming packets
		[RosValue("incoming-filter", CanUnset=true)]
		public String IncomingFilter { get; set; }
		
		// Assigns an individual address to the PPP-server
		[RosValue("local-address", CanUnset=true)]
		public String LocalAddress { get; set; }
		
		// Profile name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum OnlyOneEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		// Allow only one connection at a time
		[RosValue("only-one")]
		public OnlyOneEnum? OnlyOne { get; set; }
		
		// Firewall chain name for outgoing packets
		[RosValue("outgoing-filter", CanUnset=true)]
		public String OutgoingFilter { get; set; }
		
		// Data rate limitations for the client
		[RosValue("rate-limit", CanUnset=true)]
		public String RateLimit { get; set; }
		
		// Assigns an individual address to the PPP
		[RosValue("remote-address", CanUnset=true)]
		public String RemoteAddress { get; set; }
		
		// The maximum time the connection can stay up
		[RosValue("session-timeout", CanUnset=true)]
		public TimeSpan? SessionTimeout { get; set; }
		
		public enum UseCompressionEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		// Defines whether compress traffic or not
		[RosValue("use-compression")]
		public UseCompressionEnum? UseCompression { get; set; }
		
		public enum UseEncryptionEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("required")]
			Required,
			[RosValue("yes")]
			Yes,
		}
		
		// Defines whether encrypt traffic or not
		[RosValue("use-encryption")]
		public UseEncryptionEnum? UseEncryption { get; set; }
		
		public enum UseIpv6Enum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("required")]
			Required,
			[RosValue("yes")]
			Yes,
		}
		
		[RosValue("use-ipv6")]
		public UseIpv6Enum? UseIpv6 { get; set; }
		
		public enum UseMplsEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("required")]
			Required,
			[RosValue("yes")]
			Yes,
		}
		
		[RosValue("use-mpls")]
		public UseMplsEnum? UseMpls { get; set; }
		
		public enum UseVjCompressionEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("no")]
			No,
			[RosValue("yes")]
			Yes,
		}
		
		// Use Van Jacobson header  compression
		[RosValue("use-vj-compression")]
		public UseVjCompressionEnum? UseVjCompression { get; set; }
		
		// Windows Internet Naming Service server
		[RosValue("wins-server", CanUnset=true)]
		public IPAddress[] WinsServer { get; set; }
		
		[RosValue("default", IsDynamic=true)]
		public String Default { get; set; }
		
		public static PppProfile DefaultProfile
		{
			get { return new PppProfile() { Id = 0 }; }
		}
		
		public static PppProfile DefaultEncryptionProfile
		{
			get { return new PppProfile() { Id = -2 }; }
		}
		
		public static PppProfile Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			PppProfile obj = new PppProfile();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("ppp secret", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class PppSecret : RosItemObject<PppSecret>
	{
		// Sets IP address for PPTP, L2TP or MAC address for PPPoE
		[RosValue("caller-id")]
		public String CallerId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// maximum amount of bytes user can transmit
		[RosValue("limit-bytes-in")]
		public Int64? LimitBytesIn { get; set; }
		
		// maximum amount of bytes user can receive
		[RosValue("limit-bytes-out")]
		public Int64? LimitBytesOut { get; set; }
		
		// Assigns an individual address to the PPP-server
		[RosValue("local-address", CanUnset=true)]
		public IPAddress LocalAddress { get; set; }
		
		// Name of the user
		[RosValue("name")]
		public String Name { get; set; }
		
		// User password
		[RosValue("password")]
		public String Password { get; set; }
		
		// Profile name for the user
		[RosValue("profile")]
		public RouterOS.PppProfile Profile { get; set; }
		
		// Assigns an individual address to the PPP-client
		[RosValue("remote-address", CanUnset=true)]
		public IPAddress RemoteAddress { get; set; }
		
		[RosValue("remote-ipv6-prefix", CanUnset=true)]
		public IPv6Prefix? RemoteIpv6Prefix { get; set; }
		
		// Routes that appear on the server when the client is connected
		[RosValue("routes")]
		public String Routes { get; set; }
		
		public enum ServiceEnum
		{
			[RosValue("any")]
			Any,
			[RosValue("async")]
			Async,
			[RosValue("isdn")]
			Isdn,
			[RosValue("l2tp")]
			L2tp,
			[RosValue("ovpn")]
			Ovpn,
		}
		
		// Specifies service that will use this user
		[RosValue("service")]
		public ServiceEnum? Service { get; set; }
		
		public static PppSecret Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			PppSecret obj = new PppSecret();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("queue interface", CanSet=true, CanGet=true)]
	public class QueueInterface : RosItemObject<QueueInterface>
	{
		// Queue type which will be used for the interface
		[RosValue("queue")]
		public RouterOS.QueueType Queue { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
	}
	[RosObject("queue simple", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class QueueSimple : RosItemObject<QueueSimple>
	{
		public struct BurstLimitType
                {
                    public Int32 Out { get; set; }
                    public Int32 In { get; set; }

                    public static BurstLimitType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new BurstLimitType() { Out = TypeFormatter.ConvertFromString<Int32>(strs[0], null), In = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Out, '/', In);
                    }
                }
		// Maximal allowed burst of data rate in form of in/out
		[RosValue("burst-limit")]
		public BurstLimitType? BurstLimit { get; set; }
		
		public struct BurstThresholdType
                {
                    public Int32 Out { get; set; }
                    public Int32 In { get; set; }

                    public static BurstThresholdType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new BurstThresholdType() { Out = TypeFormatter.ConvertFromString<Int32>(strs[0], null), In = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Out, '/', In);
                    }
                }
		// Average burst threshold in form of in/out
		[RosValue("burst-threshold")]
		public BurstThresholdType? BurstThreshold { get; set; }
		
		// Burst time in form in/out
		[RosValue("burst-time")]
		public String BurstTime { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum DirectionEnum
		{
			[RosValue("both")]
			Both,
			[RosValue("download")]
			Download,
			[RosValue("upload")]
			Upload,
		}
		
		[RosValue("direction")]
		public DirectionEnum? Direction { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct DstAddressType 
                { 
                    private object value; 
                    public IPPrefix? IpAddress 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstAddressType(IPPrefix? value) 
                { 
                    return new DstAddressType() { value = value }; 
                }public IPv6Prefix? Ipv6Address 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DstAddressType(IPv6Prefix? value) 
                { 
                    return new DstAddressType() { value = value }; 
                } 
                    public static DstAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new DstAddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new DstAddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Destination IP address
		[RosValue("dst-address")]
		public DstAddressType? DstAddress { get; set; }
		
		// Destination netmask
		[RosValue("dst-netmask")]
		public IPAddress DstNetmask { get; set; }
		
		public enum AllEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public AllEnum? All 
                { 
                    get { return value as AllEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(AllEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<AllEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Interface to which this queue applies
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		public struct LimitAtType
                {
                    public Int32 Out { get; set; }
                    public Int32 In { get; set; }

                    public static LimitAtType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new LimitAtType() { Out = TypeFormatter.ConvertFromString<Int32>(strs[0], null), In = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Out, '/', In);
                    }
                }
		// Guaranteed data rate to this queue
		[RosValue("limit-at")]
		public LimitAtType? LimitAt { get; set; }
		
		public struct MaxLimitType
                {
                    public Int32 Out { get; set; }
                    public Int32 In { get; set; }

                    public static MaxLimitType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new MaxLimitType() { Out = TypeFormatter.ConvertFromString<Int32>(strs[0], null), In = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Out, '/', In);
                    }
                }
		// Data rate which can be reached if there is enough bandwidth available
		[RosValue("max-limit")]
		public MaxLimitType? MaxLimit { get; set; }
		
		// Queue name
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum P2pEnum
		{
			[RosValue("all-p2p")]
			AllP2p,
			[RosValue("bit-torrent")]
			BitTorrent,
			[RosValue("blubster")]
			Blubster,
			[RosValue("direct-connect")]
			DirectConnect,
			[RosValue("edonkey")]
			Edonkey,
		}
		
		// Which type of P2P traffic to match
		[RosValue("p2p", CanUnset=true)]
		public P2pEnum? P2p { get; set; }
		
		[RosValue("packet-marks")]
		public String[] PacketMarks { get; set; }
		
		// Name of the parent queue in the hierarchy
		[RosValue("parent")]
		public String Parent { get; set; }
		
		// Flow priority
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		public struct QueueType
                {
                    public RouterOS.QueueType Out { get; set; }
                    public RouterOS.QueueType In { get; set; }

                    public static QueueType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new QueueType() { Out = TypeFormatter.ConvertFromString<RouterOS.QueueType>(strs[0], null), In = TypeFormatter.ConvertFromString<RouterOS.QueueType>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Out, '/', In);
                    }
                }
		// Queue type
		[RosValue("queue")]
		public QueueType? Queue { get; set; }
		
		public struct TargetAddressType 
                { 
                    private object value; 
                    public IPPrefix? IpAddress 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TargetAddressType(IPPrefix? value) 
                { 
                    return new TargetAddressType() { value = value }; 
                }public IPv6Prefix? Ipv6Address 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TargetAddressType(IPv6Prefix? value) 
                { 
                    return new TargetAddressType() { value = value }; 
                } 
                    public static TargetAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new TargetAddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new TargetAddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Limitation source IP addresses
		[RosValue("target-addresses")]
		public TargetAddressType[] TargetAddresses { get; set; }
		
		// At which time and day of week to allow this queue
		[RosValue("time", CanUnset=true)]
		public String Time { get; set; }
		
		// Maximal allowed total (bidirectional) burst of data rate (bits/s)
		[RosValue("total-burst-limit")]
		public Int32? TotalBurstLimit { get; set; }
		
		// Total (bidirectional) average burst threshold (bits/s)
		[RosValue("total-burst-threshold")]
		public Int32? TotalBurstThreshold { get; set; }
		
		// Total burst (bidirectional) time
		[RosValue("total-burst-time")]
		public TimeSpan? TotalBurstTime { get; set; }
		
		// Allocated total (bidirectional) stream data rate (bits/s)
		[RosValue("total-limit-at")]
		public Int32? TotalLimitAt { get; set; }
		
		// Maximal total (bidirectional) stream data rate (bits/s)
		[RosValue("total-max-limit")]
		public Int32? TotalMaxLimit { get; set; }
		
		// Queuing discipline to use for upload and download traffic
		[RosValue("total-queue")]
		public RouterOS.QueueType TotalQueue { get; set; }
		
		[RosValue("borrows", IsDynamic=true)]
		public Int32? Borrows { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dropped", IsDynamic=true)]
		public Int32? Dropped { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("lends", IsDynamic=true)]
		public Int32? Lends { get; set; }
		
		[RosValue("packet-rate", IsDynamic=true)]
		public Int32? PacketRate { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("pcq-queues", IsDynamic=true)]
		public Int32? PcqQueues { get; set; }
		
		[RosValue("queued-bytes", IsDynamic=true)]
		public Int32? QueuedBytes { get; set; }
		
		[RosValue("queued-packets", IsDynamic=true)]
		public Int32? QueuedPackets { get; set; }
		
		[RosValue("rate", IsDynamic=true)]
		public Int32? Rate { get; set; }
		
		[RosValue("total-borrows", IsDynamic=true)]
		public Int32? TotalBorrows { get; set; }
		
		[RosValue("total-bytes", IsDynamic=true)]
		public Int64? TotalBytes { get; set; }
		
		[RosValue("total-dropped", IsDynamic=true)]
		public Int32? TotalDropped { get; set; }
		
		[RosValue("total-lends", IsDynamic=true)]
		public Int32? TotalLends { get; set; }
		
		[RosValue("total-packet-rate", IsDynamic=true)]
		public Int32? TotalPacketRate { get; set; }
		
		[RosValue("total-packets", IsDynamic=true)]
		public Int32? TotalPackets { get; set; }
		
		[RosValue("total-pcq-queues", IsDynamic=true)]
		public Int32? TotalPcqQueues { get; set; }
		
		[RosValue("total-queued-bytes", IsDynamic=true)]
		public Int32? TotalQueuedBytes { get; set; }
		
		[RosValue("total-queued-packets", IsDynamic=true)]
		public Int32? TotalQueuedPackets { get; set; }
		
		[RosValue("total-rate", IsDynamic=true)]
		public Int32? TotalRate { get; set; }
		
		public static QueueSimple Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			QueueSimple obj = new QueueSimple();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("queue tree", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class QueueTree : RosItemObject<QueueTree>
	{
		// Maximal allowed burst of the data rate
		[RosValue("burst-limit")]
		public Int32? BurstLimit { get; set; }
		
		// Average burst threshold
		[RosValue("burst-threshold")]
		public Int32? BurstThreshold { get; set; }
		
		// Burst time
		[RosValue("burst-time")]
		public TimeSpan? BurstTime { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Maximum stream bit rate
		[RosValue("limit-at")]
		public Int32? LimitAt { get; set; }
		
		// Maximal limit
		[RosValue("max-limit")]
		public Int32? MaxLimit { get; set; }
		
		// Queue name
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("packet-mark")]
		public String[] PacketMark { get; set; }
		
		// Interface which queue uses
		[RosValue("parent")]
		public String Parent { get; set; }
		
		// Flow priority
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		// Queue type
		[RosValue("queue")]
		public RouterOS.QueueType Queue { get; set; }
		
		[RosValue("borrows", IsDynamic=true)]
		public Int32? Borrows { get; set; }
		
		[RosValue("bytes", IsDynamic=true)]
		public Int64? Bytes { get; set; }
		
		[RosValue("dropped", IsDynamic=true)]
		public Int32? Dropped { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("lends", IsDynamic=true)]
		public Int32? Lends { get; set; }
		
		[RosValue("packet-rate", IsDynamic=true)]
		public Int32? PacketRate { get; set; }
		
		[RosValue("packets", IsDynamic=true)]
		public Int32? Packets { get; set; }
		
		[RosValue("pcq-queues", IsDynamic=true)]
		public Int32? PcqQueues { get; set; }
		
		[RosValue("queued-bytes", IsDynamic=true)]
		public Int32? QueuedBytes { get; set; }
		
		[RosValue("queued-packets", IsDynamic=true)]
		public Int32? QueuedPackets { get; set; }
		
		[RosValue("rate", IsDynamic=true)]
		public Int32? Rate { get; set; }
		
		public static QueueTree Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			QueueTree obj = new QueueTree();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("queue type", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class QueueType : RosItemObject<QueueType>
	{
		// Maximum number of bytes that the BFIFO queue can hold
		[RosValue("bfifo-limit")]
		public Int32? BfifoLimit { get; set; }
		
		public enum KindEnum
		{
			[RosValue("bfifo")]
			Bfifo,
			[RosValue("pcq")]
			Pcq,
			[RosValue("pfifo")]
			Pfifo,
			[RosValue("red")]
			Red,
			[RosValue("sfq")]
			Sfq,
		}
		
		// Queue type
		[RosValue("kind")]
		public KindEnum? Kind { get; set; }
		
		// Associative name of the queue type
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("pcq-burst-rate")]
		public Int32? PcqBurstRate { get; set; }
		
		[RosValue("pcq-burst-threshold")]
		public Int32? PcqBurstThreshold { get; set; }
		
		public enum PcqBurstTimeEnum
		{
			[RosValue("PcqBurstTime")]
			PcqBurstTime,
		}
		
		[RosValue("pcq-burst-time")]
		public PcqBurstTimeEnum? PcqBurstTime { get; set; }
		
		// The classifier of grouping traffic flow
		[RosValue("pcq-classifier")]
		public String PcqClassifier { get; set; }
		
		[RosValue("pcq-dst-address-mask")]
		public String PcqDstAddressMask { get; set; }
		
		[RosValue("pcq-dst-address6-mask")]
		public Int32? PcqDstAddress6Mask { get; set; }
		
		// PCQ queue limit
		[RosValue("pcq-limit")]
		public Int32? PcqLimit { get; set; }
		
		// Maximal data rate assigned to one group (bits/s)
		[RosValue("pcq-rate")]
		public Int32? PcqRate { get; set; }
		
		[RosValue("pcq-src-address-mask")]
		public String PcqSrcAddressMask { get; set; }
		
		[RosValue("pcq-src-address6-mask")]
		public Int32? PcqSrcAddress6Mask { get; set; }
		
		// Number of packets that can hold the whole PCQ queue
		[RosValue("pcq-total-limit")]
		public Int32? PcqTotalLimit { get; set; }
		
		// Maximum number of packets that the PFIFO queue can hold
		[RosValue("pfifo-limit")]
		public Int32? PfifoLimit { get; set; }
		
		// Used by RED for average queue size calculations
		[RosValue("red-avg-packet")]
		public Int32? RedAvgPacket { get; set; }
		
		// Number of packets allowed for bursts of packets when there are no packets in the queue
		[RosValue("red-burst")]
		public Int32? RedBurst { get; set; }
		
		// RED queue limit in bytes
		[RosValue("red-limit")]
		public Int32? RedLimit { get; set; }
		
		// The average queue size at which packet marking probability is the highest
		[RosValue("red-max-threshold")]
		public Int32? RedMaxThreshold { get; set; }
		
		// Average queue size in bytes
		[RosValue("red-min-threshold")]
		public Int32? RedMinThreshold { get; set; }
		
		// How often to change hash function
		[RosValue("sfq-allot")]
		public Int32? SfqAllot { get; set; }
		
		// Amount of data in bytes that can be sent in one round-robin round
		[RosValue("sfq-perturb")]
		public Int32? SfqPerturb { get; set; }
		
		public static QueueType DefaultType
		{
			get { return new QueueType() { Id = 1 }; }
		}
		
		public static QueueType EthernetDefaultType
		{
			get { return new QueueType() { Id = 2 }; }
		}
		
		public static QueueType WirelessDefaultType
		{
			get { return new QueueType() { Id = 3 }; }
		}
		
		public static QueueType SynchronousDefaultType
		{
			get { return new QueueType() { Id = 4 }; }
		}
		
		public static QueueType HotspotDefaultType
		{
			get { return new QueueType() { Id = 5 }; }
		}
		
		public static QueueType DefaultSmallType
		{
			get { return new QueueType() { Id = -2 }; }
		}
		
		public static QueueType Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			QueueType obj = new QueueType();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("radius", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class Radius : RosItemObject<Radius>
	{
		// Radius accounting backup
		[RosValue("accounting-backup")]
		public Boolean? AccountingBackup { get; set; }
		
		// Radius accounting port
		[RosValue("accounting-port")]
		public Int32? AccountingPort { get; set; }
		
		public struct AddressType 
                { 
                    private object value; 
                    public IPAddress IpAddress 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPAddress value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPv6Address Ipv6Address 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Address value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// The address of radius
		[RosValue("address")]
		public AddressType? Address { get; set; }
		
		// Default port 1645 to RFC
		[RosValue("authentication-port")]
		public Int32? AuthenticationPort { get; set; }
		
		// Called identity
		[RosValue("called-id")]
		public String CalledId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// The domain of the radius
		[RosValue("domain")]
		public String Domain { get; set; }
		
		// Explicitly stated realm (user domain)
		[RosValue("realm")]
		public String Realm { get; set; }
		
		// PPP secret name
		[RosValue("secret")]
		public String Secret { get; set; }
		
		// Name of the service
		[RosValue("service")]
		public String Service { get; set; }
		
		public struct SrcAddressType 
                { 
                    private object value; 
                    public IPAddress IpAddress 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPAddress value) 
                { 
                    return new SrcAddressType() { value = value }; 
                }public IPv6Address Ipv6Address 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPv6Address value) 
                { 
                    return new SrcAddressType() { value = value }; 
                } 
                    public static SrcAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("src-address")]
		public SrcAddressType? SrcAddress { get; set; }
		
		public enum TimeoutEnum
		{
			[RosValue("Timeout")]
			Timeout,
		}
		
		// Time limit how long the radius client will try to connect to the radius server
		[RosValue("timeout")]
		public TimeoutEnum? Timeout { get; set; }
		
	}
	[RosObject("radius incoming", CanSet=true, CanGet=true)]
	public class RadiusIncoming : RosValueObject
	{
		// Whether to accept the unsolicited messages
		[RosValue("accept")]
		public Boolean? Accept { get; set; }
		
		// The port number to listen for the requests on
		[RosValue("port")]
		public Int32? Port { get; set; }
		
	}
	[RosObject("routing bfd interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingBfdInterface : RosItemObject<RoutingBfdInterface>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("interval")]
		public String Interval { get; set; }
		
		[RosValue("min-rx")]
		public String MinRx { get; set; }
		
		[RosValue("multiplier")]
		public Int32? Multiplier { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
	}
	[RosObject("routing bfd neighbor", CanGet=true)]
	public class RoutingBfdNeighbor : RosItemObject<RoutingBfdNeighbor>
	{
		[RosValue("actual-tx-interval", IsDynamic=true)]
		public String ActualTxInterval { get; set; }
		
		[RosValue("address", IsDynamic=true)]
		public IPv6Address Address { get; set; }
		
		[RosValue("desired-tx-interval", IsDynamic=true)]
		public String DesiredTxInterval { get; set; }
		
		[RosValue("hold-time", IsDynamic=true)]
		public String HoldTime { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("multihop", IsDynamic=true)]
		public String Multihop { get; set; }
		
		[RosValue("multiplier", IsDynamic=true)]
		public Int32? Multiplier { get; set; }
		
		[RosValue("packets-rx", IsDynamic=true)]
		public Int32? PacketsRx { get; set; }
		
		[RosValue("packets-tx", IsDynamic=true)]
		public Int32? PacketsTx { get; set; }
		
		[RosValue("protocols", IsDynamic=true)]
		public String Protocols { get; set; }
		
		[RosValue("remote-min-rx", IsDynamic=true)]
		public String RemoteMinRx { get; set; }
		
		[RosValue("required-min-rx", IsDynamic=true)]
		public String RequiredMinRx { get; set; }
		
		public enum StateType
		{
			[RosValue("admin-down")]
			AdminDown,
			[RosValue("down")]
			Down,
			[RosValue("init")]
			Init,
			[RosValue("up")]
			Up,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		[RosValue("state-changes", IsDynamic=true)]
		public Int32? StateChanges { get; set; }
		
		[RosValue("up", IsDynamic=true)]
		public String Up { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
	}
	[RosObject("routing bgp advertisements", CanGet=true, CanUnset=true)]
	public class RoutingBgpAdvertisements : RosItemObject<RoutingBgpAdvertisements>
	{
		[RosValue("aggregator", IsDynamic=true)]
		public Int32? Aggregator { get; set; }
		
		[RosValue("as-path", IsDynamic=true)]
		public String AsPath { get; set; }
		
		[RosValue("atomic-aggregate", IsDynamic=true)]
		public String AtomicAggregate { get; set; }
		
		[RosValue("bgp-ext-communities", IsDynamic=true)]
		public String BgpExtCommunities { get; set; }
		
		[RosValue("cluster-list", IsDynamic=true)]
		public String ClusterList { get; set; }
		
		[RosValue("communities", IsDynamic=true)]
		public Int32? Communities { get; set; }
		
		[RosValue("local-pref", IsDynamic=true)]
		public Int32? LocalPref { get; set; }
		
		[RosValue("med", IsDynamic=true)]
		public Int32? Med { get; set; }
		
		[RosValue("nexthop", IsDynamic=true)]
		public IPAddress Nexthop { get; set; }
		
		public enum OriginType
		{
			[RosValue("egp")]
			Egp,
			[RosValue("igp")]
			Igp,
			[RosValue("incomplete")]
			Incomplete,
		}
		
		[RosValue("origin", IsDynamic=true)]
		public OriginType? Origin { get; set; }
		
		[RosValue("originator-id", IsDynamic=true)]
		public IPAddress OriginatorId { get; set; }
		
		[RosValue("peer", IsDynamic=true)]
		public String Peer { get; set; }
		
		[RosValue("prefix", IsDynamic=true)]
		public IPPrefix? Prefix { get; set; }
		
	}
	[RosObject("routing bgp aggregate", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingBgpAggregate : RosItemObject<RoutingBgpAggregate>
	{
		public enum AdvertiseFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("advertise-filter")]
		public AdvertiseFilterEnum? AdvertiseFilter { get; set; }
		
		public enum AttributeFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("attribute-filter")]
		public AttributeFilterEnum? AttributeFilter { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("include-igp")]
		public Boolean? IncludeIgp { get; set; }
		
		[RosValue("inherit-attributes")]
		public Boolean? InheritAttributes { get; set; }
		
		[RosValue("instance")]
		public RouterOS.RoutingBgpInstance Instance { get; set; }
		
		public struct PrefixType 
                { 
                    private object value; 
                    public IPPrefix? Prefix4 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrefixType(IPPrefix? value) 
                { 
                    return new PrefixType() { value = value }; 
                }public IPv6Prefix? Prefix6 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrefixType(IPv6Prefix? value) 
                { 
                    return new PrefixType() { value = value }; 
                } 
                    public static PrefixType Parse(string str, Connection conn)
                    {
                        
                try { return new PrefixType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new PrefixType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("prefix")]
		public PrefixType? Prefix { get; set; }
		
		[RosValue("summary-only")]
		public Boolean? SummaryOnly { get; set; }
		
		public enum SuppressFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("suppress-filter")]
		public SuppressFilterEnum? SuppressFilter { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		[RosValue("routes-used", IsDynamic=true)]
		public Int32? RoutesUsed { get; set; }
		
	}
	[RosObject("routing bgp instance", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class RoutingBgpInstance : RosItemObject<RoutingBgpInstance>
	{
		[RosValue("as")]
		public String As { get; set; }
		
		[RosValue("client-to-client-reflection")]
		public Boolean? ClientToClientReflection { get; set; }
		
		[RosValue("cluster-id", CanUnset=true)]
		public IPAddress ClusterId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("confederation", CanUnset=true)]
		public String Confederation { get; set; }
		
		[RosValue("confederation-peers")]
		public String ConfederationPeers { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("ignore-as-path-len")]
		public Boolean? IgnoreAsPathLen { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum OutFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("out-filter")]
		public OutFilterEnum? OutFilter { get; set; }
		
		[RosValue("redistribute-connected")]
		public Boolean? RedistributeConnected { get; set; }
		
		[RosValue("redistribute-ospf")]
		public Boolean? RedistributeOspf { get; set; }
		
		[RosValue("redistribute-other-bgp")]
		public Boolean? RedistributeOtherBgp { get; set; }
		
		[RosValue("redistribute-rip")]
		public Boolean? RedistributeRip { get; set; }
		
		[RosValue("redistribute-static")]
		public Boolean? RedistributeStatic { get; set; }
		
		[RosValue("router-id")]
		public IPAddress RouterId { get; set; }
		
		[RosValue("routing-table")]
		public String RoutingTable { get; set; }
		
		public static RoutingBgpInstance DefaultInstance
		{
			get { return new RoutingBgpInstance() { Id = 0 }; }
		}
		
		public static RoutingBgpInstance Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingBgpInstance obj = new RoutingBgpInstance();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing bgp instance vrf", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class RoutingBgpInstanceVrf : RosItemObject<RoutingBgpInstanceVrf>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum InFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("in-filter")]
		public InFilterEnum? InFilter { get; set; }
		
		[RosValue("instance")]
		public RouterOS.RoutingBgpInstance Instance { get; set; }
		
		public enum OutFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("out-filter")]
		public OutFilterEnum? OutFilter { get; set; }
		
		[RosValue("redistribute-connected")]
		public Boolean? RedistributeConnected { get; set; }
		
		[RosValue("redistribute-ospf")]
		public Boolean? RedistributeOspf { get; set; }
		
		[RosValue("redistribute-other-bgp")]
		public Boolean? RedistributeOtherBgp { get; set; }
		
		[RosValue("redistribute-rip")]
		public Boolean? RedistributeRip { get; set; }
		
		[RosValue("redistribute-static")]
		public Boolean? RedistributeStatic { get; set; }
		
		[RosValue("routing-mark")]
		public String RoutingMark { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("routing bgp network", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingBgpNetwork : RosItemObject<RoutingBgpNetwork>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct NetworkType 
                { 
                    private object value; 
                    public IPPrefix? Network4 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NetworkType(IPPrefix? value) 
                { 
                    return new NetworkType() { value = value }; 
                }public IPv6Prefix? Network6 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator NetworkType(IPv6Prefix? value) 
                { 
                    return new NetworkType() { value = value }; 
                } 
                    public static NetworkType Parse(string str, Connection conn)
                    {
                        
                try { return new NetworkType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new NetworkType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("network")]
		public NetworkType? Network { get; set; }
		
		[RosValue("synchronize")]
		public Boolean? Synchronize { get; set; }
		
	}
	[RosObject("routing bgp peer", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class RoutingBgpPeer : RosItemObject<RoutingBgpPeer>
	{
		[RosValue("address-families")]
		public String AddressFamilies { get; set; }
		
		[RosValue("allow-as-in", CanUnset=true)]
		public Int32? AllowAsIn { get; set; }
		
		[RosValue("as-override")]
		public Boolean? AsOverride { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum DefaultOriginateEnum
		{
			[RosValue("always")]
			Always,
			[RosValue("if-installed")]
			IfInstalled,
			[RosValue("never")]
			Never,
		}
		
		[RosValue("default-originate")]
		public DefaultOriginateEnum? DefaultOriginate { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("infinity")]
			Infinity,
		}
		
		public enum HoldTimeEnum
		{
			[RosValue("Number")]
			Number,
		}
		
		public struct HoldTimeType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HoldTimeType(SpecialEnum? value) 
                { 
                    return new HoldTimeType() { value = value }; 
                }public HoldTimeEnum? State 
                { 
                    get { return value as HoldTimeEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator HoldTimeType(HoldTimeEnum? value) 
                { 
                    return new HoldTimeType() { value = value }; 
                } 
                    public static HoldTimeType Parse(string str, Connection conn)
                    {
                        
                try { return new HoldTimeType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new HoldTimeType() { value = TypeFormatter.ConvertFromString<HoldTimeEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("hold-time")]
		public HoldTimeType? HoldTime { get; set; }
		
		public enum InFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("in-filter")]
		public InFilterEnum? InFilter { get; set; }
		
		[RosValue("instance")]
		public RouterOS.RoutingBgpInstance Instance { get; set; }
		
		public enum KeepaliveTimeEnum
		{
			[RosValue("KeepaliveTime")]
			KeepaliveTime,
		}
		
		[RosValue("keepalive-time", CanUnset=true)]
		public KeepaliveTimeEnum? KeepaliveTime { get; set; }
		
		[RosValue("max-prefix-limit", CanUnset=true)]
		public Int32? MaxPrefixLimit { get; set; }
		
		public enum SpecvalEnum
		{
			[RosValue("infinity")]
			Infinity,
		}
		
		public enum MaxPrefixRestartTimeEnum
		{
			[RosValue("Timeval")]
			Timeval,
		}
		
		public struct MaxPrefixRestartTimeType 
                { 
                    private object value; 
                    public SpecvalEnum? Specval 
                { 
                    get { return value as SpecvalEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MaxPrefixRestartTimeType(SpecvalEnum? value) 
                { 
                    return new MaxPrefixRestartTimeType() { value = value }; 
                }public MaxPrefixRestartTimeEnum? State 
                { 
                    get { return value as MaxPrefixRestartTimeEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MaxPrefixRestartTimeType(MaxPrefixRestartTimeEnum? value) 
                { 
                    return new MaxPrefixRestartTimeType() { value = value }; 
                } 
                    public static MaxPrefixRestartTimeType Parse(string str, Connection conn)
                    {
                        
                try { return new MaxPrefixRestartTimeType() { value = TypeFormatter.ConvertFromString<SpecvalEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new MaxPrefixRestartTimeType() { value = TypeFormatter.ConvertFromString<MaxPrefixRestartTimeEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("max-prefix-restart-time", CanUnset=true)]
		public MaxPrefixRestartTimeType? MaxPrefixRestartTime { get; set; }
		
		[RosValue("multihop")]
		public Boolean? Multihop { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum NexthopChoiceEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("force-self")]
			ForceSelf,
			[RosValue("propagate")]
			Propagate,
		}
		
		[RosValue("nexthop-choice")]
		public NexthopChoiceEnum? NexthopChoice { get; set; }
		
		public enum OutFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("out-filter")]
		public OutFilterEnum? OutFilter { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		[RosValue("remote-address")]
		public String RemoteAddress { get; set; }
		
		[RosValue("remote-as")]
		public String RemoteAs { get; set; }
		
		[RosValue("remote-port")]
		public Int32? RemotePort { get; set; }
		
		[RosValue("remove-private-as")]
		public Boolean? RemovePrivateAs { get; set; }
		
		[RosValue("route-reflect")]
		public Boolean? RouteReflect { get; set; }
		
		[RosValue("tcp-md5-key")]
		public String TcpMd5Key { get; set; }
		
		public struct TtlType 
                { 
                    private object value; 
                    public RouterOS.InterfaceWirelessSecurityProfiles Special 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TtlType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new TtlType() { value = value }; 
                }public Int32? Number 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TtlType(Int32? value) 
                { 
                    return new TtlType() { value = value }; 
                } 
                    public static TtlType Parse(string str, Connection conn)
                    {
                        
                try { return new TtlType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                try { return new TtlType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("ttl")]
		public TtlType? Ttl { get; set; }
		
		[RosValue("update-source")]
		public String UpdateSource { get; set; }
		
		[RosValue("use-bfd")]
		public Boolean? UseBfd { get; set; }
		
		[RosValue("local-address", IsDynamic=true)]
		public IPAddress LocalAddress { get; set; }
		
		[RosValue("prefix-count", IsDynamic=true)]
		public Int32? PrefixCount { get; set; }
		
		[RosValue("remote-hold-time", IsDynamic=true)]
		public TimeSpan? RemoteHoldTime { get; set; }
		
		[RosValue("updates-sent", IsDynamic=true)]
		public Int32? UpdatesSent { get; set; }
		
		[RosValue("used-keepalive-time", IsDynamic=true)]
		public TimeSpan? UsedKeepaliveTime { get; set; }
		
		[RosValue("withdrawn-sent", IsDynamic=true)]
		public Int32? WithdrawnSent { get; set; }
		
		[RosValue("as4-capability", IsDynamic=true)]
		public String As4Capability { get; set; }
		
		[RosValue("established", IsDynamic=true)]
		public String Established { get; set; }
		
		[RosValue("refresh-capability", IsDynamic=true)]
		public String RefreshCapability { get; set; }
		
		[RosValue("remote-id", IsDynamic=true)]
		public IPAddress RemoteId { get; set; }
		
		public enum StateType
		{
			[RosValue("active")]
			Active,
			[RosValue("connect")]
			Connect,
			[RosValue("established")]
			Established,
			[RosValue("idle")]
			Idle,
			[RosValue("openconfirm")]
			Openconfirm,
			[RosValue("opensent")]
			Opensent,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		[RosValue("updates-received", IsDynamic=true)]
		public Int32? UpdatesReceived { get; set; }
		
		[RosValue("uptime", IsDynamic=true)]
		public TimeSpan? Uptime { get; set; }
		
		[RosValue("used-hold-time", IsDynamic=true)]
		public TimeSpan? UsedHoldTime { get; set; }
		
		[RosValue("withdrawn-received", IsDynamic=true)]
		public Int32? WithdrawnReceived { get; set; }
		
		public static RoutingBgpPeer Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingBgpPeer obj = new RoutingBgpPeer();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing bgp vpnv4-route", CanGet=true, CanUnset=true)]
	public class RoutingBgpVpnv4Route : RosItemObject<RoutingBgpVpnv4Route>
	{
		[RosValue("bgp-as-path", CanUnset=true)]
		public String BgpAsPath { get; set; }
		
		[RosValue("bgp-atomic-aggregate", CanUnset=true)]
		public String BgpAtomicAggregate { get; set; }
		
		[RosValue("bgp-communities", CanUnset=true)]
		public String BgpCommunities { get; set; }
		
		[RosValue("bgp-ext-communities", CanUnset=true)]
		public String BgpExtCommunities { get; set; }
		
		[RosValue("bgp-local-pref", CanUnset=true)]
		public String BgpLocalPref { get; set; }
		
		[RosValue("bgp-med", CanUnset=true)]
		public String BgpMed { get; set; }
		
		[RosValue("bgp-origin", CanUnset=true)]
		public String BgpOrigin { get; set; }
		
		[RosValue("bgp-prepend", CanUnset=true)]
		public String BgpPrepend { get; set; }
		
		[RosValue("bgp-weight", CanUnset=true)]
		public String BgpWeight { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPPrefix? DstAddress { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public RouterOS.Interface Gateway { get; set; }
		
		[RosValue("in-label", IsDynamic=true)]
		public Int32? InLabel { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("label-present", IsDynamic=true)]
		public String LabelPresent { get; set; }
		
		[RosValue("out-label", IsDynamic=true)]
		public Int32? OutLabel { get; set; }
		
		[RosValue("route-distinguisher", IsDynamic=true)]
		public String RouteDistinguisher { get; set; }
		
	}
	[RosObject("routing filter", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class RoutingFilter : RosItemObject<RoutingFilter>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("discard")]
			Discard,
			[RosValue("jump")]
			Jump,
			[RosValue("log")]
			Log,
			[RosValue("passthrough")]
			Passthrough,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		[RosValue("address-family", CanUnset=true)]
		public String AddressFamily { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("local-as")]
			LocalAs,
			[RosValue("no-advertise")]
			NoAdvertise,
			[RosValue("no-export")]
			NoExport,
		}
		
		public struct NumbersType
                {
                    public Int32 As { get; set; }
                    public Int32 Num { get; set; }

                    public static NumbersType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new NumbersType() { As = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Num = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(As, ':', Num);
                    }
                }
		public struct CommunityType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(SpecialEnum? value) 
                { 
                    return new CommunityType() { value = value }; 
                }public NumbersType? Numbers 
                { 
                    get { return value as NumbersType?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CommunityType(NumbersType? value) 
                { 
                    return new CommunityType() { value = value }; 
                } 
                    public static CommunityType Parse(string str, Connection conn)
                    {
                        
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new CommunityType() { value = TypeFormatter.ConvertFromString<NumbersType>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("append-bgp-communities", CanUnset=true)]
		public CommunityType[] AppendBgpCommunities { get; set; }
		
		[RosValue("append-route-targets", CanUnset=true)]
		public String AppendRouteTargets { get; set; }
		
		[RosValue("bgp-as-path", CanUnset=true)]
		public String BgpAsPath { get; set; }
		
		[RosValue("bgp-as-path-length", CanUnset=true)]
		public String BgpAsPathLength { get; set; }
		
		public enum BgpAtomicAggregateEnum
		{
			[RosValue("absent")]
			Absent,
			[RosValue("present")]
			Present,
		}
		
		[RosValue("bgp-atomic-aggregate", CanUnset=true)]
		public BgpAtomicAggregateEnum? BgpAtomicAggregate { get; set; }
		
		[RosValue("bgp-communities", CanUnset=true)]
		public String BgpCommunities { get; set; }
		
		[RosValue("bgp-local-pref", CanUnset=true)]
		public String BgpLocalPref { get; set; }
		
		[RosValue("bgp-med", CanUnset=true)]
		public String BgpMed { get; set; }
		
		[RosValue("bgp-origin", CanUnset=true)]
		public String BgpOrigin { get; set; }
		
		[RosValue("bgp-weight", CanUnset=true)]
		public String BgpWeight { get; set; }
		
		public enum ChainEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("chain")]
		public ChainEnum? Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("distance", CanUnset=true)]
		public String Distance { get; set; }
		
		[RosValue("invert-match")]
		public Boolean? InvertMatch { get; set; }
		
		public enum JumpTargetEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("jump-target")]
		public JumpTargetEnum? JumpTarget { get; set; }
		
		[RosValue("locally-originated-bgp", CanUnset=true)]
		public Boolean? LocallyOriginatedBgp { get; set; }
		
		[RosValue("match-chain", CanUnset=true)]
		public String MatchChain { get; set; }
		
		public enum OspfTypeEnum
		{
			[RosValue("external-type-1")]
			ExternalType1,
			[RosValue("external-type-2")]
			ExternalType2,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
			[RosValue("nssa-external-type-1")]
			NssaExternalType1,
		}
		
		[RosValue("ospf-type", CanUnset=true)]
		public OspfTypeEnum? OspfType { get; set; }
		
		[RosValue("pref-src", CanUnset=true)]
		public String PrefSrc { get; set; }
		
		[RosValue("prefix")]
		public String Prefix { get; set; }
		
		[RosValue("prefix-length", CanUnset=true)]
		public String PrefixLength { get; set; }
		
		[RosValue("protocol", CanUnset=true)]
		public String Protocol { get; set; }
		
		[RosValue("route-comment", CanUnset=true)]
		public String RouteComment { get; set; }
		
		[RosValue("route-tag", CanUnset=true)]
		public String RouteTag { get; set; }
		
		[RosValue("route-targets", CanUnset=true)]
		public String RouteTargets { get; set; }
		
		[RosValue("routing-mark", CanUnset=true)]
		public String RoutingMark { get; set; }
		
		[RosValue("scope", CanUnset=true)]
		public String Scope { get; set; }
		
		[RosValue("set-bgp-communities", CanUnset=true)]
		public CommunityType[] SetBgpCommunities { get; set; }
		
		[RosValue("set-bgp-local-pref", CanUnset=true)]
		public Int32? SetBgpLocalPref { get; set; }
		
		[RosValue("set-bgp-med", CanUnset=true)]
		public Int32? SetBgpMed { get; set; }
		
		public struct SetBgpPrependType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SetBgpPrependType(Int32? value) 
                { 
                    return new SetBgpPrependType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SetBgpPrependType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new SetBgpPrependType() { value = value }; 
                } 
                    public static SetBgpPrependType Parse(string str, Connection conn)
                    {
                        
                try { return new SetBgpPrependType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new SetBgpPrependType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("set-bgp-prepend", CanUnset=true)]
		public SetBgpPrependType? SetBgpPrepend { get; set; }
		
		[RosValue("set-bgp-prepend-path")]
		public String SetBgpPrependPath { get; set; }
		
		[RosValue("set-bgp-weight", CanUnset=true)]
		public String SetBgpWeight { get; set; }
		
		public enum SetCheckGatewayEnum
		{
			[RosValue("arp")]
			Arp,
			[RosValue("none")]
			None,
			[RosValue("ping")]
			Ping,
		}
		
		[RosValue("set-check-gateway", CanUnset=true)]
		public SetCheckGatewayEnum? SetCheckGateway { get; set; }
		
		[RosValue("set-disabled", CanUnset=true)]
		public Boolean? SetDisabled { get; set; }
		
		[RosValue("set-distance", CanUnset=true)]
		public Int32? SetDistance { get; set; }
		
		[RosValue("set-in-nexthop", CanUnset=true)]
		public IPAddress[] SetInNexthop { get; set; }
		
		[RosValue("set-in-nexthop-direct", CanUnset=true)]
		public RouterOS.Interface[] SetInNexthopDirect { get; set; }
		
		[RosValue("set-in-nexthop-ipv6", CanUnset=true)]
		public String SetInNexthopIpv6 { get; set; }
		
		[RosValue("set-in-nexthop-linklocal", CanUnset=true)]
		public String SetInNexthopLinklocal { get; set; }
		
		[RosValue("set-out-nexthop", CanUnset=true)]
		public IPAddress SetOutNexthop { get; set; }
		
		[RosValue("set-out-nexthop-ipv6", CanUnset=true)]
		public String SetOutNexthopIpv6 { get; set; }
		
		[RosValue("set-out-nexthop-linklocal", CanUnset=true)]
		public String SetOutNexthopLinklocal { get; set; }
		
		[RosValue("set-pref-src", CanUnset=true)]
		public IPAddress SetPrefSrc { get; set; }
		
		[RosValue("set-route-comment", CanUnset=true)]
		public String SetRouteComment { get; set; }
		
		[RosValue("set-route-tag", CanUnset=true)]
		public Int32? SetRouteTag { get; set; }
		
		[RosValue("set-route-targets", CanUnset=true)]
		public String SetRouteTargets { get; set; }
		
		[RosValue("set-routing-mark", CanUnset=true)]
		public String SetRoutingMark { get; set; }
		
		[RosValue("set-scope", CanUnset=true)]
		public Int32? SetScope { get; set; }
		
		[RosValue("set-site-of-origin", CanUnset=true)]
		public String SetSiteOfOrigin { get; set; }
		
		[RosValue("set-target-scope", CanUnset=true)]
		public Int32? SetTargetScope { get; set; }
		
		public enum SetTypeEnum
		{
			[RosValue("blackhole")]
			Blackhole,
			[RosValue("prohibit")]
			Prohibit,
			[RosValue("unicast")]
			Unicast,
			[RosValue("unreachable")]
			Unreachable,
		}
		
		[RosValue("set-type", CanUnset=true)]
		public SetTypeEnum? SetType { get; set; }
		
		[RosValue("set-use-te-nexthop", CanUnset=true)]
		public Boolean? SetUseTeNexthop { get; set; }
		
		[RosValue("site-of-origin", CanUnset=true)]
		public String SiteOfOrigin { get; set; }
		
		[RosValue("target-scope", CanUnset=true)]
		public String TargetScope { get; set; }
		
	}
	[RosObject("routing mme", CanSet=true, CanGet=true)]
	public class RoutingMme : RosValueObject
	{
		[RosValue("bidirectional-timeout")]
		public Int32? BidirectionalTimeout { get; set; }
		
		public enum ClassEnum
		{
			[RosValue("1-MBit")]
			E1MBit,
			[RosValue("128-KBit")]
			E128KBit,
			[RosValue("2-MBit")]
			E2MBit,
			[RosValue("256-KBit")]
			E256KBit,
			[RosValue("3-MBit")]
			E3MBit,
		}
		
		public struct GatewayClassType 
                { 
                    private object value; 
                    public ClassEnum? Class 
                { 
                    get { return value as ClassEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator GatewayClassType(ClassEnum? value) 
                { 
                    return new GatewayClassType() { value = value }; 
                }public Int32? Number 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator GatewayClassType(Int32? value) 
                { 
                    return new GatewayClassType() { value = value }; 
                } 
                    public static GatewayClassType Parse(string str, Connection conn)
                    {
                        
                try { return new GatewayClassType() { value = TypeFormatter.ConvertFromString<ClassEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new GatewayClassType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("gateway-class")]
		public GatewayClassType? GatewayClass { get; set; }
		
		public enum GatewayKeepaliveEnum
		{
			[RosValue("GatewayKeepalive")]
			GatewayKeepalive,
		}
		
		[RosValue("gateway-keepalive")]
		public GatewayKeepaliveEnum? GatewayKeepalive { get; set; }
		
		public enum GatewaySelectionEnum
		{
			[RosValue("best-adjusted")]
			BestAdjusted,
			[RosValue("best-statistic")]
			BestStatistic,
			[RosValue("no-gateway")]
			NoGateway,
		}
		
		[RosValue("gateway-selection")]
		public GatewaySelectionEnum? GatewaySelection { get; set; }
		
		[RosValue("origination-interval")]
		public TimeSpan? OriginationInterval { get; set; }
		
		[RosValue("preferred-gateway")]
		public IPAddress PreferredGateway { get; set; }
		
		[RosValue("timeout")]
		public TimeSpan? Timeout { get; set; }
		
		[RosValue("ttl")]
		public Int32? Ttl { get; set; }
		
	}
	[RosObject("routing mme interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingMmeInterface : RosItemObject<RoutingMmeInterface>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		[RosValue("primary")]
		public Boolean? Primary { get; set; }
		
		[RosValue("messages-rx", IsDynamic=true)]
		public Int32? MessagesRx { get; set; }
		
		[RosValue("messages-tx", IsDynamic=true)]
		public Int32? MessagesTx { get; set; }
		
	}
	[RosObject("routing mme network", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingMmeNetwork : RosItemObject<RoutingMmeNetwork>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("network")]
		public IPPrefix? Network { get; set; }
		
	}
	[RosObject("routing mme originators", CanGet=true)]
	public class RoutingMmeOriginators : RosItemObject<RoutingMmeOriginators>
	{
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("gateway-class", IsDynamic=true)]
		public Int32? GatewayClass { get; set; }
		
		[RosValue("last-packet-before", IsDynamic=true)]
		public TimeSpan? LastPacketBefore { get; set; }
		
		[RosValue("originator", IsDynamic=true)]
		public IPAddress Originator { get; set; }
		
	}
	[RosObject("routing ospf area", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfArea : RosItemObject<RoutingOspfArea>
	{
		[RosValue("area-id")]
		public IPAddress AreaId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum DefaultCostEnum
		{
			[RosValue("infinity")]
			Infinity,
		}
		
		public struct DefaultCostType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DefaultCostType(Int32? value) 
                { 
                    return new DefaultCostType() { value = value }; 
                }public DefaultCostEnum? State 
                { 
                    get { return value as DefaultCostEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator DefaultCostType(DefaultCostEnum? value) 
                { 
                    return new DefaultCostType() { value = value }; 
                } 
                    public static DefaultCostType Parse(string str, Connection conn)
                    {
                        
                try { return new DefaultCostType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new DefaultCostType() { value = TypeFormatter.ConvertFromString<DefaultCostEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("default-cost")]
		public DefaultCostType? DefaultCost { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("inject-summary-lsas")]
		public Boolean? InjectSummaryLsas { get; set; }
		
		[RosValue("instance")]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum TranslatorRoleEnum
		{
			[RosValue("translate-always")]
			TranslateAlways,
			[RosValue("translate-candidate")]
			TranslateCandidate,
			[RosValue("translate-never")]
			TranslateNever,
		}
		
		[RosValue("translator-role")]
		public TranslatorRoleEnum? TranslatorRole { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("nssa")]
			Nssa,
			[RosValue("stub")]
			Stub,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("active-interfaces", IsDynamic=true)]
		public Int32? ActiveInterfaces { get; set; }
		
		[RosValue("adjacent-neighbors", IsDynamic=true)]
		public Int32? AdjacentNeighbors { get; set; }
		
		[RosValue("interfaces", IsDynamic=true)]
		public Int32? Interfaces { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("neighbors", IsDynamic=true)]
		public Int32? Neighbors { get; set; }
		
		public static RoutingOspfArea BackboneArea
		{
			get { return new RoutingOspfArea() { Id = 1 }; }
		}
		
		public static RoutingOspfArea Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingOspfArea obj = new RoutingOspfArea();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing ospf area range", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfAreaRange : RosItemObject<RoutingOspfAreaRange>
	{
		[RosValue("advertise")]
		public Boolean? Advertise { get; set; }
		
		[RosValue("area")]
		public RouterOS.RoutingOspfArea Area { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum CostEnum
		{
			[RosValue("calculated")]
			Calculated,
		}
		
		[RosValue("cost")]
		public CostEnum? Cost { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("range")]
		public IPPrefix? Range { get; set; }
		
	}
	[RosObject("routing ospf area-border-router", CanGet=true)]
	public class RoutingOspfAreaBorderRouter : RosItemObject<RoutingOspfAreaBorderRouter>
	{
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfArea Area 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfArea Area2 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		public struct AddressType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(RouterOS.Interface value) 
                { 
                    return new AddressType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(RouterOS.Interface value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public AddressType? Interface { get; set; }
		
		[RosValue("routerid", IsDynamic=true)]
		public IPAddress Routerid { get; set; }
		
		public enum StateType
		{
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf as-border-router", CanGet=true)]
	public class RoutingOspfAsBorderRouter : RosItemObject<RoutingOspfAsBorderRouter>
	{
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		public struct AddressType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(RouterOS.Interface value) 
                { 
                    return new AddressType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(RouterOS.Interface value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public AddressType? Interface { get; set; }
		
		[RosValue("routerid", IsDynamic=true)]
		public IPAddress Routerid { get; set; }
		
		public enum StateType
		{
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf instance", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class RoutingOspfInstance : RosItemObject<RoutingOspfInstance>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum DistributeDefaultEnum
		{
			[RosValue("always-as-type-1")]
			AlwaysAsType1,
			[RosValue("always-as-type-2")]
			AlwaysAsType2,
			[RosValue("if-installed-as-type-1")]
			IfInstalledAsType1,
			[RosValue("if-installed-as-type-2")]
			IfInstalledAsType2,
			[RosValue("never")]
			Never,
		}
		
		[RosValue("distribute-default")]
		public DistributeDefaultEnum? DistributeDefault { get; set; }
		
		[RosValue("domain-id", CanUnset=true)]
		public String DomainId { get; set; }
		
		[RosValue("domain-tag", CanUnset=true)]
		public Int32? DomainTag { get; set; }
		
		public enum InFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("in-filter")]
		public InFilterEnum? InFilter { get; set; }
		
		public enum MetricBgpEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct MetricBgpType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricBgpType(Int32? value) 
                { 
                    return new MetricBgpType() { value = value }; 
                }public MetricBgpEnum? State 
                { 
                    get { return value as MetricBgpEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricBgpType(MetricBgpEnum? value) 
                { 
                    return new MetricBgpType() { value = value }; 
                } 
                    public static MetricBgpType Parse(string str, Connection conn)
                    {
                        
                try { return new MetricBgpType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MetricBgpType() { value = TypeFormatter.ConvertFromString<MetricBgpEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("metric-bgp")]
		public MetricBgpType? MetricBgp { get; set; }
		
		[RosValue("metric-connected")]
		public Int32? MetricConnected { get; set; }
		
		[RosValue("metric-default")]
		public Int32? MetricDefault { get; set; }
		
		public enum MetricOtherOspfEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct MetricOtherOspfType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricOtherOspfType(Int32? value) 
                { 
                    return new MetricOtherOspfType() { value = value }; 
                }public MetricOtherOspfEnum? State 
                { 
                    get { return value as MetricOtherOspfEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricOtherOspfType(MetricOtherOspfEnum? value) 
                { 
                    return new MetricOtherOspfType() { value = value }; 
                } 
                    public static MetricOtherOspfType Parse(string str, Connection conn)
                    {
                        
                try { return new MetricOtherOspfType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MetricOtherOspfType() { value = TypeFormatter.ConvertFromString<MetricOtherOspfEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("metric-other-ospf")]
		public MetricOtherOspfType? MetricOtherOspf { get; set; }
		
		[RosValue("metric-rip")]
		public Int32? MetricRip { get; set; }
		
		[RosValue("metric-static")]
		public Int32? MetricStatic { get; set; }
		
		[RosValue("mpls-te-area", CanUnset=true)]
		public RouterOS.RoutingOspfArea MplsTeArea { get; set; }
		
		[RosValue("mpls-te-router-id", CanUnset=true)]
		public RouterOS.Interface MplsTeRouterId { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum OutFilterEnum
		{
			[RosValue("connected-in")]
			ConnectedIn,
			[RosValue("dynamic-in")]
			DynamicIn,
			[RosValue("ospf-in")]
			OspfIn,
		}
		
		[RosValue("out-filter")]
		public OutFilterEnum? OutFilter { get; set; }
		
		public enum RedistributeBgpEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-bgp")]
		public RedistributeBgpEnum? RedistributeBgp { get; set; }
		
		public enum RedistributeConnectedEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-connected")]
		public RedistributeConnectedEnum? RedistributeConnected { get; set; }
		
		public enum RedistributeOtherOspfEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-other-ospf")]
		public RedistributeOtherOspfEnum? RedistributeOtherOspf { get; set; }
		
		public enum RedistributeRipEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-rip")]
		public RedistributeRipEnum? RedistributeRip { get; set; }
		
		public enum RedistributeStaticEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-static")]
		public RedistributeStaticEnum? RedistributeStatic { get; set; }
		
		[RosValue("router-id")]
		public IPAddress RouterId { get; set; }
		
		[RosValue("routing-table", CanUnset=true)]
		public String RoutingTable { get; set; }
		
		[RosValue("db-exchanges", IsDynamic=true)]
		public Int32? DbExchanges { get; set; }
		
		[RosValue("dijkstras", IsDynamic=true)]
		public Int32? Dijkstras { get; set; }
		
		[RosValue("effective-router-id", IsDynamic=true)]
		public IPAddress EffectiveRouterId { get; set; }
		
		[RosValue("external-imports", IsDynamic=true)]
		public Int32? ExternalImports { get; set; }
		
		public enum StateType
		{
			[RosValue("down")]
			Down,
			[RosValue("running")]
			Running,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		public static RoutingOspfInstance DefaultInstance
		{
			get { return new RoutingOspfInstance() { Id = 0 }; }
		}
		
		public static RoutingOspfInstance Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingOspfInstance obj = new RoutingOspfInstance();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing ospf interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfInterface : RosItemObject<RoutingOspfInterface>
	{
		public enum AuthenticationEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		[RosValue("authentication")]
		public AuthenticationEnum? Authentication { get; set; }
		
		[RosValue("authentication-key")]
		public String AuthenticationKey { get; set; }
		
		[RosValue("authentication-key-id")]
		public Int32? AuthenticationKeyId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("cost")]
		public Int32? Cost { get; set; }
		
		public enum DeadIntervalEnum
		{
			[RosValue("DeadInterval")]
			DeadInterval,
		}
		
		[RosValue("dead-interval")]
		public DeadIntervalEnum? DeadInterval { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum HelloIntervalEnum
		{
			[RosValue("HelloInterval")]
			HelloInterval,
		}
		
		[RosValue("hello-interval")]
		public HelloIntervalEnum? HelloInterval { get; set; }
		
		[RosValue("instance-id")]
		public Int32? InstanceId { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		public enum NetworkTypeEnum
		{
			[RosValue("broadcast")]
			Broadcast,
			[RosValue("default")]
			Default,
			[RosValue("nbma")]
			Nbma,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("ptmp")]
			Ptmp,
		}
		
		[RosValue("network-type")]
		public NetworkTypeEnum? NetworkType { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		public enum RetransmitIntervalEnum
		{
			[RosValue("RetransmitInterval")]
			RetransmitInterval,
		}
		
		[RosValue("retransmit-interval")]
		public RetransmitIntervalEnum? RetransmitInterval { get; set; }
		
		public enum TransmitDelayEnum
		{
			[RosValue("TransmitDelay")]
			TransmitDelay,
		}
		
		[RosValue("transmit-delay")]
		public TransmitDelayEnum? TransmitDelay { get; set; }
		
		[RosValue("use-bfd")]
		public Boolean? UseBfd { get; set; }
		
		[RosValue("adjacent-neighbors", IsDynamic=true)]
		public Int32? AdjacentNeighbors { get; set; }
		
		[RosValue("area", IsDynamic=true)]
		public RouterOS.RoutingOspfArea Area { get; set; }
		
		[RosValue("backup-designated-router", IsDynamic=true)]
		public IPAddress BackupDesignatedRouter { get; set; }
		
		[RosValue("designated-router", IsDynamic=true)]
		public IPAddress DesignatedRouter { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		[RosValue("ip-address", IsDynamic=true)]
		public IPAddress IpAddress { get; set; }
		
		[RosValue("neighbors", IsDynamic=true)]
		public Int32? Neighbors { get; set; }
		
		public enum StateType
		{
			[RosValue("backup")]
			Backup,
			[RosValue("designated-router")]
			DesignatedRouter,
			[RosValue("down")]
			Down,
			[RosValue("dr-other")]
			DrOther,
			[RosValue("loopback")]
			Loopback,
			[RosValue("passive")]
			Passive,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("waiting")]
			Waiting,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		public enum UsedNetworkTypeType
		{
			[RosValue("broadcast")]
			Broadcast,
			[RosValue("nbma")]
			Nbma,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("ptmp")]
			Ptmp,
			[RosValue("sham-link")]
			ShamLink,
			[RosValue("virtual-link")]
			VirtualLink,
		}
		
		[RosValue("used-network-type", IsDynamic=true)]
		public UsedNetworkTypeType? UsedNetworkType { get; set; }
		
	}
	[RosObject("routing ospf lsa", CanGet=true)]
	public class RoutingOspfLsa : RosItemObject<RoutingOspfLsa>
	{
		[RosValue("age", IsDynamic=true)]
		public Int32? Age { get; set; }
		
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfArea Area 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfArea Area2 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("body", IsDynamic=true)]
		public String Body { get; set; }
		
		[RosValue("checksum", IsDynamic=true)]
		public Int32? Checksum { get; set; }
		
		[RosValue("id", IsDynamic=true)]
		public IPAddress Id { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		[RosValue("options", IsDynamic=true)]
		public String Options { get; set; }
		
		[RosValue("originator", IsDynamic=true)]
		public IPAddress Originator { get; set; }
		
		[RosValue("sequence-number", IsDynamic=true)]
		public Int32? SequenceNumber { get; set; }
		
		public enum TypeType
		{
			[RosValue("as-external")]
			AsExternal,
			[RosValue("network")]
			Network,
			[RosValue("opaque-area")]
			OpaqueArea,
			[RosValue("opaque-as")]
			OpaqueAs,
			[RosValue("opaque-link")]
			OpaqueLink,
			[RosValue("router")]
			Router,
			[RosValue("summary-asbr")]
			SummaryAsbr,
			[RosValue("summary-network")]
			SummaryNetwork,
			[RosValue("type-7")]
			Type7,
		}
		
		[RosValue("type", IsDynamic=true)]
		public TypeType? Type { get; set; }
		
	}
	[RosObject("routing ospf nbma-neighbor", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfNbmaNeighbor : RosItemObject<RoutingOspfNbmaNeighbor>
	{
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("instance")]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		public enum PollIntervalEnum
		{
			[RosValue("PollInterval")]
			PollInterval,
		}
		
		[RosValue("poll-interval")]
		public PollIntervalEnum? PollInterval { get; set; }
		
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
	}
	[RosObject("routing ospf neighbor", CanGet=true)]
	public class RoutingOspfNeighbor : RosItemObject<RoutingOspfNeighbor>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("adjacency", IsDynamic=true)]
		public TimeSpan? Adjacency { get; set; }
		
		[RosValue("backup-dr-address", IsDynamic=true)]
		public IPAddress BackupDrAddress { get; set; }
		
		[RosValue("db-summaries", IsDynamic=true)]
		public Int32? DbSummaries { get; set; }
		
		[RosValue("dr-address", IsDynamic=true)]
		public IPAddress DrAddress { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("ls-requests", IsDynamic=true)]
		public Int32? LsRequests { get; set; }
		
		[RosValue("ls-retransmits", IsDynamic=true)]
		public Int32? LsRetransmits { get; set; }
		
		[RosValue("priority", IsDynamic=true)]
		public Int32? Priority { get; set; }
		
		[RosValue("router-id", IsDynamic=true)]
		public IPAddress RouterId { get; set; }
		
		[RosValue("state", IsDynamic=true)]
		public String State { get; set; }
		
		[RosValue("state-changes", IsDynamic=true)]
		public Int32? StateChanges { get; set; }
		
	}
	[RosObject("routing ospf network", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfNetwork : RosItemObject<RoutingOspfNetwork>
	{
		[RosValue("area")]
		public RouterOS.RoutingOspfArea Area { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("network")]
		public IPPrefix? Network { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("routing ospf route", CanGet=true)]
	public class RoutingOspfRoute : RosItemObject<RoutingOspfRoute>
	{
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfArea Area 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfArea Area2 
                { 
                    get { return value as RouterOS.RoutingOspfArea; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfArea value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfArea>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPPrefix? DstAddress { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("instance", IsDynamic=true)]
		public RouterOS.RoutingOspfInstance Instance { get; set; }
		
		public struct IfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IfaceType(RouterOS.Interface value) 
                { 
                    return new IfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IfaceType(RouterOS.Interface value) 
                { 
                    return new IfaceType() { value = value }; 
                } 
                    public static IfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new IfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new IfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public IfaceType? Interface { get; set; }
		
		public enum StateType
		{
			[RosValue("ext-1")]
			Ext1,
			[RosValue("ext-2")]
			Ext2,
			[RosValue("imported-ext-1")]
			ImportedExt1,
			[RosValue("imported-ext-2")]
			ImportedExt2,
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
			[RosValue("nssa-ext-1")]
			NssaExt1,
			[RosValue("nssa-ext-2")]
			NssaExt2,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf sham-link", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfShamLink : RosItemObject<RoutingOspfShamLink>
	{
		[RosValue("area")]
		public RouterOS.RoutingOspfArea Area { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("cost")]
		public Int32? Cost { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("dst-address")]
		public IPAddress DstAddress { get; set; }
		
		[RosValue("src-address")]
		public IPAddress SrcAddress { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("up", IsDynamic=true)]
		public String Up { get; set; }
		
	}
	[RosObject("routing ospf virtual-link", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfVirtualLink : RosItemObject<RoutingOspfVirtualLink>
	{
		public enum AuthenticationEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		[RosValue("authentication")]
		public AuthenticationEnum? Authentication { get; set; }
		
		[RosValue("authentication-key")]
		public String AuthenticationKey { get; set; }
		
		[RosValue("authentication-key-id")]
		public Int32? AuthenticationKeyId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("instance-id")]
		public Int32? InstanceId { get; set; }
		
		[RosValue("neighbor-id")]
		public IPAddress NeighborId { get; set; }
		
		[RosValue("transit-area")]
		public RouterOS.RoutingOspfArea TransitArea { get; set; }
		
		[RosValue("use-bfd")]
		public Boolean? UseBfd { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("routing ospf-v3 area", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3Area : RosItemObject<RoutingOspfV3Area>
	{
		[RosValue("area-id")]
		public IPAddress AreaId { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("default-cost")]
		public Int32? DefaultCost { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("inject-summary-lsas")]
		public Boolean? InjectSummaryLsas { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance")]
		public InstanceType? Instance { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum TranslatorRoleEnum
		{
			[RosValue("translate-always")]
			TranslateAlways,
			[RosValue("translate-candidate")]
			TranslateCandidate,
			[RosValue("translate-never")]
			TranslateNever,
		}
		
		[RosValue("translator-role")]
		public TranslatorRoleEnum? TranslatorRole { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("default")]
			Default,
			[RosValue("nssa")]
			Nssa,
			[RosValue("stub")]
			Stub,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("active-interfaces", IsDynamic=true)]
		public Int32? ActiveInterfaces { get; set; }
		
		[RosValue("adjacent-neighbors", IsDynamic=true)]
		public Int32? AdjacentNeighbors { get; set; }
		
		[RosValue("interfaces", IsDynamic=true)]
		public Int32? Interfaces { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("neighbors", IsDynamic=true)]
		public Int32? Neighbors { get; set; }
		
		public static RoutingOspfV3Area BackboneArea
		{
			get { return new RoutingOspfV3Area() { Id = 1 }; }
		}
		
		public static RoutingOspfV3Area Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingOspfV3Area obj = new RoutingOspfV3Area();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing ospf-v3 area range", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3AreaRange : RosItemObject<RoutingOspfV3AreaRange>
	{
		[RosValue("advertise")]
		public Boolean? Advertise { get; set; }
		
		[RosValue("area")]
		public RouterOS.RoutingOspfV3Area Area { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		public enum CostEnum
		{
			[RosValue("calculated")]
			Calculated,
		}
		
		[RosValue("cost")]
		public CostEnum? Cost { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("range")]
		public IPv6Prefix? Range { get; set; }
		
	}
	[RosObject("routing ospf-v3 as-border-router", CanGet=true)]
	public class RoutingOspfV3AsBorderRouter : RosItemObject<RoutingOspfV3AsBorderRouter>
	{
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public String Gateway { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("router-id", IsDynamic=true)]
		public IPAddress RouterId { get; set; }
		
		public enum StateType
		{
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf-v3 instance", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3Instance : RosItemObject<RoutingOspfV3Instance>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum DistributeDefaultEnum
		{
			[RosValue("always-as-type-1")]
			AlwaysAsType1,
			[RosValue("always-as-type-2")]
			AlwaysAsType2,
			[RosValue("if-installed-as-type-1")]
			IfInstalledAsType1,
			[RosValue("if-installed-as-type-2")]
			IfInstalledAsType2,
			[RosValue("never")]
			Never,
		}
		
		[RosValue("distribute-default")]
		public DistributeDefaultEnum? DistributeDefault { get; set; }
		
		public enum MetricBgpEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct MetricBgpType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricBgpType(Int32? value) 
                { 
                    return new MetricBgpType() { value = value }; 
                }public MetricBgpEnum? State 
                { 
                    get { return value as MetricBgpEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricBgpType(MetricBgpEnum? value) 
                { 
                    return new MetricBgpType() { value = value }; 
                } 
                    public static MetricBgpType Parse(string str, Connection conn)
                    {
                        
                try { return new MetricBgpType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MetricBgpType() { value = TypeFormatter.ConvertFromString<MetricBgpEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("metric-bgp")]
		public MetricBgpType? MetricBgp { get; set; }
		
		[RosValue("metric-connected")]
		public Int32? MetricConnected { get; set; }
		
		[RosValue("metric-default")]
		public Int32? MetricDefault { get; set; }
		
		public enum MetricOtherOspfEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct MetricOtherOspfType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricOtherOspfType(Int32? value) 
                { 
                    return new MetricOtherOspfType() { value = value }; 
                }public MetricOtherOspfEnum? State 
                { 
                    get { return value as MetricOtherOspfEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MetricOtherOspfType(MetricOtherOspfEnum? value) 
                { 
                    return new MetricOtherOspfType() { value = value }; 
                } 
                    public static MetricOtherOspfType Parse(string str, Connection conn)
                    {
                        
                try { return new MetricOtherOspfType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new MetricOtherOspfType() { value = TypeFormatter.ConvertFromString<MetricOtherOspfEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("metric-other-ospf")]
		public MetricOtherOspfType? MetricOtherOspf { get; set; }
		
		[RosValue("metric-rip")]
		public Int32? MetricRip { get; set; }
		
		[RosValue("metric-static")]
		public Int32? MetricStatic { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum RedistributeBgpEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-bgp")]
		public RedistributeBgpEnum? RedistributeBgp { get; set; }
		
		public enum RedistributeConnectedEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-connected")]
		public RedistributeConnectedEnum? RedistributeConnected { get; set; }
		
		public enum RedistributeOtherOspfEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-other-ospf")]
		public RedistributeOtherOspfEnum? RedistributeOtherOspf { get; set; }
		
		public enum RedistributeRipEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-rip")]
		public RedistributeRipEnum? RedistributeRip { get; set; }
		
		public enum RedistributeStaticEnum
		{
			[RosValue("as-type-1")]
			AsType1,
			[RosValue("as-type-2")]
			AsType2,
			[RosValue("no")]
			No,
		}
		
		[RosValue("redistribute-static")]
		public RedistributeStaticEnum? RedistributeStatic { get; set; }
		
		[RosValue("router-id")]
		public IPAddress RouterId { get; set; }
		
		[RosValue("db-exchanges", IsDynamic=true)]
		public Int32? DbExchanges { get; set; }
		
		[RosValue("dijkstras", IsDynamic=true)]
		public Int32? Dijkstras { get; set; }
		
		[RosValue("effective-router-id", IsDynamic=true)]
		public IPAddress EffectiveRouterId { get; set; }
		
		[RosValue("external-imports", IsDynamic=true)]
		public Int32? ExternalImports { get; set; }
		
		public enum StateType
		{
			[RosValue("down")]
			Down,
			[RosValue("running")]
			Running,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		public static RoutingOspfV3Instance DefaultInstance
		{
			get { return new RoutingOspfV3Instance() { Id = 0 }; }
		}
		
		public static RoutingOspfV3Instance Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			RoutingOspfV3Instance obj = new RoutingOspfV3Instance();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("routing ospf-v3 interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3Interface : RosItemObject<RoutingOspfV3Interface>
	{
		[RosValue("area")]
		public RouterOS.RoutingOspfV3Area Area { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("cost")]
		public Int32? Cost { get; set; }
		
		public enum DeadIntervalEnum
		{
			[RosValue("DeadInterval")]
			DeadInterval,
		}
		
		[RosValue("dead-interval")]
		public DeadIntervalEnum? DeadInterval { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum HelloIntervalEnum
		{
			[RosValue("HelloInterval")]
			HelloInterval,
		}
		
		[RosValue("hello-interval")]
		public HelloIntervalEnum? HelloInterval { get; set; }
		
		[RosValue("instance-id")]
		public Int32? InstanceId { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		public enum NetworkTypeEnum
		{
			[RosValue("broadcast")]
			Broadcast,
			[RosValue("default")]
			Default,
			[RosValue("nbma")]
			Nbma,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("ptmp")]
			Ptmp,
		}
		
		[RosValue("network-type")]
		public NetworkTypeEnum? NetworkType { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
		public enum RetransmitIntervalEnum
		{
			[RosValue("RetransmitInterval")]
			RetransmitInterval,
		}
		
		[RosValue("retransmit-interval")]
		public RetransmitIntervalEnum? RetransmitInterval { get; set; }
		
		public enum TransmitDelayEnum
		{
			[RosValue("TransmitDelay")]
			TransmitDelay,
		}
		
		[RosValue("transmit-delay")]
		public TransmitDelayEnum? TransmitDelay { get; set; }
		
		[RosValue("use-bfd")]
		public Boolean? UseBfd { get; set; }
		
		[RosValue("adjacent-neighbors", IsDynamic=true)]
		public Int32? AdjacentNeighbors { get; set; }
		
		[RosValue("backup-designated-router", IsDynamic=true)]
		public IPAddress BackupDesignatedRouter { get; set; }
		
		[RosValue("designated-router", IsDynamic=true)]
		public IPAddress DesignatedRouter { get; set; }
		
		[RosValue("dynamic", IsDynamic=true)]
		public String Dynamic { get; set; }
		
		[RosValue("inactive", IsDynamic=true)]
		public String Inactive { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		[RosValue("neighbors", IsDynamic=true)]
		public Int32? Neighbors { get; set; }
		
		public enum StateType
		{
			[RosValue("backup")]
			Backup,
			[RosValue("designated-router")]
			DesignatedRouter,
			[RosValue("down")]
			Down,
			[RosValue("dr-other")]
			DrOther,
			[RosValue("loopback")]
			Loopback,
			[RosValue("passive")]
			Passive,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("waiting")]
			Waiting,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		public enum UsedNetworkTypeType
		{
			[RosValue("broadcast")]
			Broadcast,
			[RosValue("nbma")]
			Nbma,
			[RosValue("point-to-point")]
			PointToPoint,
			[RosValue("ptmp")]
			Ptmp,
			[RosValue("virtual-link")]
			VirtualLink,
		}
		
		[RosValue("used-network-type", IsDynamic=true)]
		public UsedNetworkTypeType? UsedNetworkType { get; set; }
		
	}
	[RosObject("routing ospf-v3 lsa", CanGet=true)]
	public class RoutingOspfV3Lsa : RosItemObject<RoutingOspfV3Lsa>
	{
		[RosValue("age", IsDynamic=true)]
		public Int32? Age { get; set; }
		
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfV3Area Area2 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("body", IsDynamic=true)]
		public String Body { get; set; }
		
		[RosValue("checksum", IsDynamic=true)]
		public Int32? Checksum { get; set; }
		
		[RosValue("id", IsDynamic=true)]
		public IPAddress Id { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		[RosValue("options", IsDynamic=true)]
		public String Options { get; set; }
		
		[RosValue("originator", IsDynamic=true)]
		public IPAddress Originator { get; set; }
		
		[RosValue("sequence-number", IsDynamic=true)]
		public Int32? SequenceNumber { get; set; }
		
		public enum TypeType
		{
			[RosValue("as-external")]
			AsExternal,
			[RosValue("inter-area-prefix")]
			InterAreaPrefix,
			[RosValue("inter-area-router")]
			InterAreaRouter,
			[RosValue("intra-area-prefix")]
			IntraAreaPrefix,
			[RosValue("link")]
			Link,
			[RosValue("network")]
			Network,
			[RosValue("opaque-area")]
			OpaqueArea,
			[RosValue("opaque-as")]
			OpaqueAs,
			[RosValue("router")]
			Router,
			[RosValue("type-7")]
			Type7,
		}
		
		[RosValue("type", IsDynamic=true)]
		public TypeType? Type { get; set; }
		
	}
	[RosObject("routing ospf-v3 nbma-neighbor", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3NbmaNeighbor : RosItemObject<RoutingOspfV3NbmaNeighbor>
	{
		[RosValue("address")]
		public String Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance")]
		public InstanceType? Instance { get; set; }
		
		public enum PollIntervalEnum
		{
			[RosValue("PollInterval")]
			PollInterval,
		}
		
		[RosValue("poll-interval")]
		public PollIntervalEnum? PollInterval { get; set; }
		
		[RosValue("priority")]
		public Int32? Priority { get; set; }
		
	}
	[RosObject("routing ospf-v3 neighbor", CanGet=true)]
	public class RoutingOspfV3Neighbor : RosItemObject<RoutingOspfV3Neighbor>
	{
		[RosValue("address", IsDynamic=true)]
		public String Address { get; set; }
		
		[RosValue("adjacency", IsDynamic=true)]
		public TimeSpan? Adjacency { get; set; }
		
		[RosValue("backup-dr", IsDynamic=true)]
		public IPAddress BackupDr { get; set; }
		
		[RosValue("db-summaries", IsDynamic=true)]
		public Int32? DbSummaries { get; set; }
		
		[RosValue("dr", IsDynamic=true)]
		public IPAddress Dr { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface", IsDynamic=true)]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("ls-requests", IsDynamic=true)]
		public Int32? LsRequests { get; set; }
		
		[RosValue("ls-retransmits", IsDynamic=true)]
		public Int32? LsRetransmits { get; set; }
		
		[RosValue("priority", IsDynamic=true)]
		public Int32? Priority { get; set; }
		
		[RosValue("router-id", IsDynamic=true)]
		public IPAddress RouterId { get; set; }
		
		[RosValue("state", IsDynamic=true)]
		public String State { get; set; }
		
		[RosValue("state-changes", IsDynamic=true)]
		public Int32? StateChanges { get; set; }
		
	}
	[RosObject("routing ospf-v3 ospf-router", CanGet=true)]
	public class RoutingOspfV3OspfRouter : RosItemObject<RoutingOspfV3OspfRouter>
	{
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfV3Area Area2 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public String Gateway { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("router-id", IsDynamic=true)]
		public IPAddress RouterId { get; set; }
		
		public enum StateType
		{
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf-v3 route", CanGet=true)]
	public class RoutingOspfV3Route : RosItemObject<RoutingOspfV3Route>
	{
		public struct AreaType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                }public RouterOS.RoutingOspfV3Area Area2 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AreaType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new AreaType() { value = value }; 
                } 
                    public static AreaType Parse(string str, Connection conn)
                    {
                        
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new AreaType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("area", IsDynamic=true)]
		public AreaType? Area { get; set; }
		
		[RosValue("cost", IsDynamic=true)]
		public Int32? Cost { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPv6Prefix? DstAddress { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public String Gateway { get; set; }
		
		public struct InstanceType 
                { 
                    private object value; 
                    public RouterOS.RoutingOspfV3Area RoutingOspfV3Area 
                { 
                    get { return value as RouterOS.RoutingOspfV3Area; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.RoutingOspfV3Area value) 
                { 
                    return new InstanceType() { value = value }; 
                }public RouterOS.InterfaceWirelessSecurityProfiles InterfaceWirelessSecurityProfiles 
                { 
                    get { return value as RouterOS.InterfaceWirelessSecurityProfiles; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InstanceType(RouterOS.InterfaceWirelessSecurityProfiles value) 
                { 
                    return new InstanceType() { value = value }; 
                } 
                    public static InstanceType Parse(string str, Connection conn)
                    {
                        
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.RoutingOspfV3Area>(str, conn) }; }
                catch(Exception) { }
                try { return new InstanceType() { value = TypeFormatter.ConvertFromString<RouterOS.InterfaceWirelessSecurityProfiles>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("instance", IsDynamic=true)]
		public InstanceType? Instance { get; set; }
		
		[RosValue("interface", IsDynamic=true)]
		public RouterOS.Interface Interface { get; set; }
		
		public enum StateType
		{
			[RosValue("ext-1")]
			Ext1,
			[RosValue("ext-2")]
			Ext2,
			[RosValue("imported-ext-1")]
			ImportedExt1,
			[RosValue("imported-ext-2")]
			ImportedExt2,
			[RosValue("inactive")]
			Inactive,
			[RosValue("inter-area")]
			InterArea,
			[RosValue("intra-area")]
			IntraArea,
			[RosValue("nssa-ext-1")]
			NssaExt1,
			[RosValue("nssa-ext-2")]
			NssaExt2,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
	}
	[RosObject("routing ospf-v3 virtual-link", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingOspfV3VirtualLink : RosItemObject<RoutingOspfV3VirtualLink>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("instance-id")]
		public Int32? InstanceId { get; set; }
		
		[RosValue("neighbor-id")]
		public IPAddress NeighborId { get; set; }
		
		[RosValue("transit-area")]
		public RouterOS.RoutingOspfV3Area TransitArea { get; set; }
		
		[RosValue("use-bfd")]
		public Boolean? UseBfd { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("routing prefix-lists", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true, CanUnset=true)]
	public class RoutingPrefixLists : RosItemObject<RoutingPrefixLists>
	{
		public enum ActionEnum
		{
			[RosValue("accept")]
			Accept,
			[RosValue("discard")]
			Discard,
		}
		
		[RosValue("action")]
		public ActionEnum? Action { get; set; }
		
		[RosValue("chain")]
		public String Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("invert-match")]
		public Boolean? InvertMatch { get; set; }
		
		public struct PrefixType 
                { 
                    private object value; 
                    public IPPrefix? Ip4 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrefixType(IPPrefix? value) 
                { 
                    return new PrefixType() { value = value }; 
                }public IPv6Prefix? Ip6 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrefixType(IPv6Prefix? value) 
                { 
                    return new PrefixType() { value = value }; 
                } 
                    public static PrefixType Parse(string str, Connection conn)
                    {
                        
                try { return new PrefixType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new PrefixType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("prefix")]
		public PrefixType? Prefix { get; set; }
		
		[RosValue("prefix-length", CanUnset=true)]
		public String PrefixLength { get; set; }
		
		[RosValue("set-metric", CanUnset=true)]
		public Int32? SetMetric { get; set; }
		
	}
	[RosObject("routing rip", CanSet=true, CanGet=true)]
	public class RoutingRip : RosValueObject
	{
		public enum DistributeDefaultEnum
		{
			[RosValue("always")]
			Always,
			[RosValue("if-installed")]
			IfInstalled,
			[RosValue("never")]
			Never,
		}
		
		[RosValue("distribute-default")]
		public DistributeDefaultEnum? DistributeDefault { get; set; }
		
		public enum GarbageTimerEnum
		{
			[RosValue("GarbageTimer")]
			GarbageTimer,
		}
		
		[RosValue("garbage-timer")]
		public GarbageTimerEnum? GarbageTimer { get; set; }
		
		[RosValue("metric-bgp")]
		public Int32? MetricBgp { get; set; }
		
		[RosValue("metric-connected")]
		public Int32? MetricConnected { get; set; }
		
		[RosValue("metric-default")]
		public Int32? MetricDefault { get; set; }
		
		[RosValue("metric-ospf")]
		public Int32? MetricOspf { get; set; }
		
		[RosValue("metric-static")]
		public Int32? MetricStatic { get; set; }
		
		[RosValue("redistribute-bgp")]
		public Boolean? RedistributeBgp { get; set; }
		
		[RosValue("redistribute-connected")]
		public Boolean? RedistributeConnected { get; set; }
		
		[RosValue("redistribute-ospf")]
		public Boolean? RedistributeOspf { get; set; }
		
		[RosValue("redistribute-static")]
		public Boolean? RedistributeStatic { get; set; }
		
		[RosValue("routing-table")]
		public String RoutingTable { get; set; }
		
		public enum TimeoutTimerEnum
		{
			[RosValue("TimeoutTimer")]
			TimeoutTimer,
		}
		
		[RosValue("timeout-timer")]
		public TimeoutTimerEnum? TimeoutTimer { get; set; }
		
		public enum UpdateTimerEnum
		{
			[RosValue("UpdateTimer")]
			UpdateTimer,
		}
		
		[RosValue("update-timer")]
		public UpdateTimerEnum? UpdateTimer { get; set; }
		
	}
	[RosObject("routing rip interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingRipInterface : RosItemObject<RoutingRipInterface>
	{
		public enum AuthenticationEnum
		{
			[RosValue("md5")]
			Md5,
			[RosValue("none")]
			None,
			[RosValue("simple")]
			Simple,
		}
		
		[RosValue("authentication")]
		public AuthenticationEnum? Authentication { get; set; }
		
		[RosValue("authentication-key")]
		public String AuthenticationKey { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("in-prefix-list")]
		public String InPrefixList { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("key-chain")]
		public String KeyChain { get; set; }
		
		[RosValue("out-prefix-list")]
		public String OutPrefixList { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		public enum ReceiveEnum
		{
			[RosValue("v1")]
			V1,
			[RosValue("v1-2")]
			V12,
			[RosValue("v2")]
			V2,
		}
		
		[RosValue("receive")]
		public ReceiveEnum? Receive { get; set; }
		
		public enum SendEnum
		{
			[RosValue("v1")]
			V1,
			[RosValue("v1-2")]
			V12,
			[RosValue("v2")]
			V2,
		}
		
		[RosValue("send")]
		public SendEnum? Send { get; set; }
		
		[RosValue("bad-packets", IsDynamic=true)]
		public Int32? BadPackets { get; set; }
		
		[RosValue("bad-routes", IsDynamic=true)]
		public Int32? BadRoutes { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("updates-rx", IsDynamic=true)]
		public Int32? UpdatesRx { get; set; }
		
		[RosValue("updates-tx", IsDynamic=true)]
		public Int32? UpdatesTx { get; set; }
		
	}
	[RosObject("routing rip keys", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class RoutingRipKeys : RosItemObject<RoutingRipKeys>
	{
		[RosValue("chain")]
		public String Chain { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("from-date")]
		public DateTime? FromDate { get; set; }
		
		[RosValue("from-time")]
		public DateTime? FromTime { get; set; }
		
		[RosValue("key")]
		public String Key { get; set; }
		
		[RosValue("key-id")]
		public Int32? KeyId { get; set; }
		
		public enum SpecialEnum
		{
			[RosValue("forever")]
			Forever,
		}
		
		public struct ToDateType 
                { 
                    private object value; 
                    public SpecialEnum? Special 
                { 
                    get { return value as SpecialEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ToDateType(SpecialEnum? value) 
                { 
                    return new ToDateType() { value = value }; 
                }public DateTime? Value 
                { 
                    get { return value as DateTime?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ToDateType(DateTime? value) 
                { 
                    return new ToDateType() { value = value }; 
                } 
                    public static ToDateType Parse(string str, Connection conn)
                    {
                        
                try { return new ToDateType() { value = TypeFormatter.ConvertFromString<SpecialEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new ToDateType() { value = TypeFormatter.ConvertFromString<DateTime>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("to-date")]
		public ToDateType? ToDate { get; set; }
		
		[RosValue("to-time")]
		public DateTime? ToTime { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
	}
	[RosObject("routing rip neighbor", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingRipNeighbor : RosItemObject<RoutingRipNeighbor>
	{
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
	}
	[RosObject("routing rip network", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingRipNetwork : RosItemObject<RoutingRipNetwork>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("network")]
		public IPPrefix? Network { get; set; }
		
	}
	[RosObject("routing rip route", CanGet=true)]
	public class RoutingRipRoute : RosItemObject<RoutingRipRoute>
	{
		[RosValue("bgp", IsDynamic=true)]
		public String Bgp { get; set; }
		
		[RosValue("connect", IsDynamic=true)]
		public String Connect { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPPrefix? DstAddress { get; set; }
		
		[RosValue("from", IsDynamic=true)]
		public IPAddress From { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public IPAddress Gateway { get; set; }
		
		[RosValue("metric", IsDynamic=true)]
		public Int32? Metric { get; set; }
		
		[RosValue("ospf", IsDynamic=true)]
		public String Ospf { get; set; }
		
		[RosValue("rip", IsDynamic=true)]
		public String Rip { get; set; }
		
		[RosValue("rtag", IsDynamic=true)]
		public Int32? Rtag { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		[RosValue("timeout", IsDynamic=true)]
		public TimeSpan? Timeout { get; set; }
		
	}
	[RosObject("routing ripng", CanSet=true, CanGet=true)]
	public class RoutingRipng : RosValueObject
	{
		public enum DistributeDefaultEnum
		{
			[RosValue("always")]
			Always,
			[RosValue("if-installed")]
			IfInstalled,
			[RosValue("never")]
			Never,
		}
		
		[RosValue("distribute-default")]
		public DistributeDefaultEnum? DistributeDefault { get; set; }
		
		public enum GarbageTimerEnum
		{
			[RosValue("GarbageTimer")]
			GarbageTimer,
		}
		
		[RosValue("garbage-timer")]
		public GarbageTimerEnum? GarbageTimer { get; set; }
		
		[RosValue("metric-bgp")]
		public Int32? MetricBgp { get; set; }
		
		[RosValue("metric-connected")]
		public Int32? MetricConnected { get; set; }
		
		[RosValue("metric-default")]
		public Int32? MetricDefault { get; set; }
		
		[RosValue("metric-ospf")]
		public Int32? MetricOspf { get; set; }
		
		[RosValue("metric-static")]
		public Int32? MetricStatic { get; set; }
		
		[RosValue("redistribute-bgp")]
		public Boolean? RedistributeBgp { get; set; }
		
		[RosValue("redistribute-connected")]
		public Boolean? RedistributeConnected { get; set; }
		
		[RosValue("redistribute-ospf")]
		public Boolean? RedistributeOspf { get; set; }
		
		[RosValue("redistribute-static")]
		public Boolean? RedistributeStatic { get; set; }
		
		public enum TimeoutTimerEnum
		{
			[RosValue("TimeoutTimer")]
			TimeoutTimer,
		}
		
		[RosValue("timeout-timer")]
		public TimeoutTimerEnum? TimeoutTimer { get; set; }
		
		public enum UpdateTimerEnum
		{
			[RosValue("UpdateTimer")]
			UpdateTimer,
		}
		
		[RosValue("update-timer")]
		public UpdateTimerEnum? UpdateTimer { get; set; }
		
	}
	[RosObject("routing ripng interface", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class RoutingRipngInterface : RosItemObject<RoutingRipngInterface>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("in-prefix-list")]
		public String InPrefixList { get; set; }
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface2 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		[RosValue("out-prefix-list")]
		public String OutPrefixList { get; set; }
		
		[RosValue("passive")]
		public Boolean? Passive { get; set; }
		
		[RosValue("bad-packets", IsDynamic=true)]
		public Int32? BadPackets { get; set; }
		
		[RosValue("bad-routes", IsDynamic=true)]
		public Int32? BadRoutes { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("updates-rx", IsDynamic=true)]
		public Int32? UpdatesRx { get; set; }
		
		[RosValue("updates-tx", IsDynamic=true)]
		public Int32? UpdatesTx { get; set; }
		
	}
	[RosObject("routing ripng route", CanGet=true)]
	public class RoutingRipngRoute : RosItemObject<RoutingRipngRoute>
	{
		[RosValue("bgp", IsDynamic=true)]
		public String Bgp { get; set; }
		
		[RosValue("connect", IsDynamic=true)]
		public String Connect { get; set; }
		
		[RosValue("dst-address", IsDynamic=true)]
		public IPv6Prefix? DstAddress { get; set; }
		
		[RosValue("from", IsDynamic=true)]
		public String From { get; set; }
		
		[RosValue("gateway", IsDynamic=true)]
		public String Gateway { get; set; }
		
		[RosValue("metric", IsDynamic=true)]
		public Int32? Metric { get; set; }
		
		[RosValue("ospf", IsDynamic=true)]
		public String Ospf { get; set; }
		
		[RosValue("rip", IsDynamic=true)]
		public String Rip { get; set; }
		
		[RosValue("rtag", IsDynamic=true)]
		public Int32? Rtag { get; set; }
		
		[RosValue("static", IsDynamic=true)]
		public String Static { get; set; }
		
		[RosValue("timeout", IsDynamic=true)]
		public TimeSpan? Timeout { get; set; }
		
	}
	[RosObject("snmp", CanSet=true, CanGet=true)]
	public class Snmp : RosValueObject
	{
		// Informative only settings for the NMS
		[RosValue("contact")]
		public String Contact { get; set; }
		
		// Defines whether SNMP service is enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		[RosValue("engine-id")]
		public String EngineId { get; set; }
		
		// Informative only settings for the NMS
		[RosValue("location")]
		public String Location { get; set; }
		
		[RosValue("trap-community")]
		public RouterOS.SnmpCommunity TrapCommunity { get; set; }
		
		public enum GeneratorEnum
		{
			[RosValue("interfaces")]
			Interfaces,
			[RosValue("start-trap")]
			StartTrap,
		}
		
		[RosValue("trap-generators")]
		public GeneratorEnum[] TrapGenerators { get; set; }
		
		public struct TrapTargetType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TrapTargetType(IPv6Address value) 
                { 
                    return new TrapTargetType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator TrapTargetType(IPAddress value) 
                { 
                    return new TrapTargetType() { value = value }; 
                } 
                    public static TrapTargetType Parse(string str, Connection conn)
                    {
                        
                try { return new TrapTargetType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new TrapTargetType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("trap-target")]
		public TrapTargetType? TrapTarget { get; set; }
		
		public enum TrapVersionEnum
		{
			[RosValue("1")]
			E1,
			[RosValue("2")]
			E2,
			[RosValue("3")]
			E3,
		}
		
		[RosValue("trap-version")]
		public TrapVersionEnum? TrapVersion { get; set; }
		
	}
	[RosObject("snmp community", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SnmpCommunity : RosItemObject<SnmpCommunity>
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPv6Prefix? Ipv6 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Prefix? value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPPrefix? Ip 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPPrefix? value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Community IP address
		[RosValue("address")]
		public AddressType? Address { get; set; }
		
		[RosValue("authentication-password")]
		public String AuthenticationPassword { get; set; }
		
		public enum AuthenticationProtocolEnum
		{
			[RosValue("MD5")]
			MD5,
			[RosValue("SHA1")]
			SHA1,
		}
		
		[RosValue("authentication-protocol")]
		public AuthenticationProtocolEnum? AuthenticationProtocol { get; set; }
		
		[RosValue("encryption-password")]
		public String EncryptionPassword { get; set; }
		
		public enum EncryptionProtocolEnum
		{
			[RosValue("DES")]
			DES,
		}
		
		[RosValue("encryption-protocol")]
		public EncryptionProtocolEnum? EncryptionProtocol { get; set; }
		
		// Community name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Enables or disables the read access for the community
		[RosValue("read-access")]
		public Boolean? ReadAccess { get; set; }
		
		public enum SecurityEnum
		{
			[RosValue("authorized")]
			Authorized,
			[RosValue("none")]
			None,
			[RosValue("private")]
			Private,
		}
		
		[RosValue("security")]
		public SecurityEnum? Security { get; set; }
		
		[RosValue("write-access")]
		public Boolean? WriteAccess { get; set; }
		
		public static SnmpCommunity PublicCommunity
		{
			get { return new SnmpCommunity() { Id = 0 }; }
		}
		
		public static SnmpCommunity Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SnmpCommunity obj = new SnmpCommunity();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("special-login", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SpecialLogin : RosItemObject<SpecialLogin>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Name of the port the user will be forwarded to
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		// Name of the user will be forwarded
		[RosValue("user")]
		public RouterOS.User User { get; set; }
		
	}
	[RosObject("store", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class Store : RosItemObject<Store>
	{
		[RosValue("activate")]
		public Boolean? Activate { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("disk")]
		public RouterOS.StoreDisk Disk { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("web-proxy")]
			WebProxy,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
		public enum StatusType
		{
			[RosValue("active")]
			Active,
			[RosValue("backup")]
			Backup,
			[RosValue("copying")]
			Copying,
			[RosValue("invalid")]
			Invalid,
			[RosValue("preparing")]
			Preparing,
			[RosValue("switching")]
			Switching,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
		public static Store Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			Store obj = new Store();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("store disk", CanGet=true)]
	public class StoreDisk : RosItemObject<StoreDisk>
	{
		[RosValue("free-space", IsDynamic=true)]
		public String FreeSpace { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public enum StatusType
		{
			[RosValue("checking")]
			Checking,
			[RosValue("formatting")]
			Formatting,
			[RosValue("invalid")]
			Invalid,
			[RosValue("ready")]
			Ready,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
		[RosValue("system", IsDynamic=true)]
		public String System { get; set; }
		
		[RosValue("total-space", IsDynamic=true)]
		public String TotalSpace { get; set; }
		
		public static StoreDisk Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			StoreDisk obj = new StoreDisk();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system clock", CanSet=true, CanGet=true)]
	public class SystemClock : RosValueObject
	{
		// New system date [month/DD/YYYY]
		[RosValue("date")]
		public DateTime? Date { get; set; }
		
		// New system time [HH:MM:SS]
		[RosValue("time")]
		public DateTime? Time { get; set; }
		
		[RosValue("time-zone-name")]
		public String TimeZoneName { get; set; }
		
	}
	[RosObject("system clock manual", CanSet=true, CanGet=true)]
	public class SystemClockManual : RosValueObject
	{
		[RosValue("dst-delta")]
		public String DstDelta { get; set; }
		
		[RosValue("dst-end")]
		public DateTime? DstEnd { get; set; }
		
		[RosValue("dst-start")]
		public DateTime? DstStart { get; set; }
		
		[RosValue("time-zone")]
		public String TimeZone { get; set; }
		
	}
	[RosObject("system console", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemConsole : RosItemObject<SystemConsole>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Port name
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		// Name of the terminal
		[RosValue("term")]
		public String Term { get; set; }
		
		[RosValue("free", IsDynamic=true)]
		public String Free { get; set; }
		
		[RosValue("used", IsDynamic=true)]
		public String Used { get; set; }
		
		[RosValue("vcno", IsDynamic=true)]
		public Int32? Vcno { get; set; }
		
		[RosValue("wedged", IsDynamic=true)]
		public String Wedged { get; set; }
		
	}
	[RosObject("system console screen", CanSet=true, CanGet=true)]
	public class SystemConsoleScreen : RosValueObject
	{
		public enum LineCountEnum
		{
			[RosValue("25")]
			E25,
			[RosValue("40")]
			E40,
			[RosValue("50")]
			E50,
		}
		
		// Number of lines on the monitor
		[RosValue("line-count")]
		public LineCountEnum? LineCount { get; set; }
		
	}
	[RosObject("system hardware", CanSet=true, CanGet=true)]
	public class SystemHardware : RosValueObject
	{
		[RosValue("multi-cpu")]
		public Boolean? MultiCpu { get; set; }
		
	}
	[RosObject("system health", CanSet=true, CanGet=true)]
	public class SystemHealth : RosValueObject
	{
		public enum StateAfterRebootEnum
		{
			[RosValue("enabled")]
			Enabled,
			[RosValue("disabled")]
			Disabled,
		}
		
		// Whether the health monitoring will be enabled after the reboot or not
		[RosValue("state-after-reboot")]
		public StateAfterRebootEnum? StateAfterReboot { get; set; }
		
	}
	[RosObject("system history", CanSet=true, CanGet=true)]
	public class SystemHistory : RosItemObject<SystemHistory>
	{
		[RosValue("action", IsDynamic=true)]
		public String Action { get; set; }
		
		[RosValue("by", IsDynamic=true)]
		public String By { get; set; }
		
		[RosValue("floating-undo", IsDynamic=true)]
		public String FloatingUndo { get; set; }
		
		public enum PolicyEnum
		{
			[RosValue("api")]
			Api,
			[RosValue("ftp")]
			Ftp,
			[RosValue("local")]
			Local,
			[RosValue("password")]
			Password,
			[RosValue("policy")]
			Policy,
		}
		
		[RosValue("policy", IsDynamic=true)]
		public PolicyEnum? Policy { get; set; }
		
		[RosValue("redoable", IsDynamic=true)]
		public String Redoable { get; set; }
		
		[RosValue("time", IsDynamic=true)]
		public DateTime? Time { get; set; }
		
		[RosValue("undoable", IsDynamic=true)]
		public String Undoable { get; set; }
		
	}
	[RosObject("system identity", CanSet=true, CanGet=true)]
	public class SystemIdentity : RosValueObject
	{
		// Name of the system
		[RosValue("name")]
		public String Name { get; set; }
		
	}
	[RosObject("system leds", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemLeds : RosItemObject<SystemLeds>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		[RosValue("leds")]
		public String Leds { get; set; }
		
		[RosValue("modem-signal-treshold")]
		public String ModemSignalTreshold { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("flash-access")]
			FlashAccess,
			[RosValue("interface-activity")]
			InterfaceActivity,
			[RosValue("interface-receive")]
			InterfaceReceive,
			[RosValue("interface-status")]
			InterfaceStatus,
			[RosValue("interface-transmit")]
			InterfaceTransmit,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
	}
	[RosObject("system logging", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemLogging : RosItemObject<SystemLogging>
	{
		[RosValue("action")]
		public RouterOS.SystemLoggingAction Action { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("prefix")]
		public String Prefix { get; set; }
		
		[RosValue("topics")]
		public String Topics { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
	}
	[RosObject("system logging action", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemLoggingAction : RosItemObject<SystemLoggingAction>
	{
		[RosValue("bsd-syslog")]
		public Boolean? BsdSyslog { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("disk-file-count")]
		public Int32? DiskFileCount { get; set; }
		
		[RosValue("disk-file-name")]
		public String DiskFileName { get; set; }
		
		[RosValue("disk-lines-per-file")]
		public Int32? DiskLinesPerFile { get; set; }
		
		[RosValue("disk-stop-on-full")]
		public Boolean? DiskStopOnFull { get; set; }
		
		[RosValue("email-start-tls")]
		public Boolean? EmailStartTls { get; set; }
		
		[RosValue("email-to")]
		public String EmailTo { get; set; }
		
		[RosValue("memory-lines")]
		public Int32? MemoryLines { get; set; }
		
		[RosValue("memory-stop-on-full")]
		public Boolean? MemoryStopOnFull { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("remember")]
		public Boolean? Remember { get; set; }
		
		public struct RemoteType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RemoteType(IPv6Address value) 
                { 
                    return new RemoteType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator RemoteType(IPAddress value) 
                { 
                    return new RemoteType() { value = value }; 
                } 
                    public static RemoteType Parse(string str, Connection conn)
                    {
                        
                try { return new RemoteType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new RemoteType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("remote")]
		public RemoteType? Remote { get; set; }
		
		[RosValue("remote-port")]
		public Int32? RemotePort { get; set; }
		
		public struct SrcAddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPv6Address value) 
                { 
                    return new SrcAddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SrcAddressType(IPAddress value) 
                { 
                    return new SrcAddressType() { value = value }; 
                } 
                    public static SrcAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new SrcAddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("src-address")]
		public SrcAddressType? SrcAddress { get; set; }
		
		public enum SyslogFacilityEnum
		{
			[RosValue("auth")]
			Auth,
			[RosValue("authpriv")]
			Authpriv,
			[RosValue("cron")]
			Cron,
			[RosValue("daemon")]
			Daemon,
			[RosValue("ftp")]
			Ftp,
		}
		
		[RosValue("syslog-facility")]
		public SyslogFacilityEnum? SyslogFacility { get; set; }
		
		public enum SyslogSeverityEnum
		{
			[RosValue("alert")]
			Alert,
			[RosValue("auto")]
			Auto,
			[RosValue("critical")]
			Critical,
			[RosValue("debug")]
			Debug,
			[RosValue("emergency")]
			Emergency,
		}
		
		[RosValue("syslog-severity")]
		public SyslogSeverityEnum? SyslogSeverity { get; set; }
		
		public enum TargetEnum
		{
			[RosValue("disk")]
			Disk,
			[RosValue("echo")]
			Echo,
			[RosValue("email")]
			Email,
			[RosValue("memory")]
			Memory,
			[RosValue("remote")]
			Remote,
		}
		
		[RosValue("target")]
		public TargetEnum? Target { get; set; }
		
		[RosValue("default", IsDynamic=true)]
		public String Default { get; set; }
		
		public static SystemLoggingAction MemoryAction
		{
			get { return new SystemLoggingAction() { Id = 0 }; }
		}
		
		public static SystemLoggingAction DiskAction
		{
			get { return new SystemLoggingAction() { Id = 1 }; }
		}
		
		public static SystemLoggingAction EchoAction
		{
			get { return new SystemLoggingAction() { Id = 2 }; }
		}
		
		public static SystemLoggingAction RemoteAction
		{
			get { return new SystemLoggingAction() { Id = 3 }; }
		}
		
		public static SystemLoggingAction Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemLoggingAction obj = new SystemLoggingAction();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system note", CanSet=true, CanGet=true)]
	public class SystemNote : RosValueObject
	{
		// Text to show
		[RosValue("note")]
		public String Note { get; set; }
		
		// Whether to show a note at login
		[RosValue("show-at-login")]
		public Boolean? ShowAtLogin { get; set; }
		
	}
	[RosObject("system ntp client", CanSet=true, CanGet=true)]
	public class SystemNtpClient : RosValueObject
	{
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		public enum ModeEnum
		{
			[RosValue("broadcast")]
			Broadcast,
			[RosValue("unicast")]
			Unicast,
		}
		
		[RosValue("mode")]
		public ModeEnum? Mode { get; set; }
		
		public struct PrimaryNtpType 
                { 
                    private object value; 
                    public IPAddress IpAddress 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrimaryNtpType(IPAddress value) 
                { 
                    return new PrimaryNtpType() { value = value }; 
                }public IPv6Address Ipv6Address 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator PrimaryNtpType(IPv6Address value) 
                { 
                    return new PrimaryNtpType() { value = value }; 
                } 
                    public static PrimaryNtpType Parse(string str, Connection conn)
                    {
                        
                try { return new PrimaryNtpType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new PrimaryNtpType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("primary-ntp")]
		public PrimaryNtpType? PrimaryNtp { get; set; }
		
		public struct SecondaryNtpType 
                { 
                    private object value; 
                    public IPAddress IpAddress 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SecondaryNtpType(IPAddress value) 
                { 
                    return new SecondaryNtpType() { value = value }; 
                }public IPv6Address Ipv6Address 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator SecondaryNtpType(IPv6Address value) 
                { 
                    return new SecondaryNtpType() { value = value }; 
                } 
                    public static SecondaryNtpType Parse(string str, Connection conn)
                    {
                        
                try { return new SecondaryNtpType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new SecondaryNtpType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("secondary-ntp")]
		public SecondaryNtpType? SecondaryNtp { get; set; }
		
	}
	[RosObject("system package", CanGet=true)]
	public class SystemPackage : RosItemObject<SystemPackage>
	{
		[RosValue("build-time", IsDynamic=true)]
		public DateTime? BuildTime { get; set; }
		
		[RosValue("disabled", IsDynamic=true)]
		public String Disabled { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		public enum AppendEnum
		{
			[RosValue("append")]
			Append,
			[RosValue("as-value")]
			AsValue,
			[RosValue("brief")]
			Brief,
			[RosValue("count-only")]
			CountOnly,
			[RosValue("detail")]
			Detail,
			[RosValue("file")]
			File,
			[RosValue("follow")]
			Follow,
			[RosValue("follow-only")]
			FollowOnly,
			[RosValue("from")]
			From,
			[RosValue("interval")]
			Interval,
			[RosValue("terse")]
			Terse,
			[RosValue("value-list")]
			ValueList,
			[RosValue("where")]
			Where,
			[RosValue("without-paging")]
			WithoutPaging,
		}
		
		[RosValue("scheduled", IsDynamic=true)]
		public AppendEnum? Scheduled { get; set; }
		
		[RosValue("version", IsDynamic=true)]
		public String Version { get; set; }
		
		public static SystemPackage Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemPackage obj = new SystemPackage();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system resource cpu", CanGet=true)]
	public class SystemResourceCpu : RosItemObject<SystemResourceCpu>
	{
		[RosValue("cpu", IsDynamic=true)]
		public Int32? Cpu { get; set; }
		
		public struct DiskType
                {
                    public Int32 Value { get; set; }

                    public static DiskType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator DiskType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(DiskType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("disk", IsDynamic=true)]
		public DiskType? Disk { get; set; }
		
		public struct IrqType
                {
                    public Int32 Value { get; set; }

                    public static IrqType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator IrqType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(IrqType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("irq", IsDynamic=true)]
		public IrqType? Irq { get; set; }
		
		public struct LoadType
                {
                    public Int32 Value { get; set; }

                    public static LoadType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator LoadType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(LoadType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("load", IsDynamic=true)]
		public LoadType? Load { get; set; }
		
	}
	[RosObject("system resource irq", CanSet=true, CanGet=true)]
	public class SystemResourceIrq : RosItemObject<SystemResourceIrq>
	{
		public enum CpuEnum
		{
			[RosValue("auto")]
			Auto,
		}
		
		public struct CpuType 
                { 
                    private object value; 
                    public Int32? Num 
                { 
                    get { return value as Int32?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CpuType(Int32? value) 
                { 
                    return new CpuType() { value = value }; 
                }public CpuEnum? State 
                { 
                    get { return value as CpuEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator CpuType(CpuEnum? value) 
                { 
                    return new CpuType() { value = value }; 
                } 
                    public static CpuType Parse(string str, Connection conn)
                    {
                        
                try { return new CpuType() { value = TypeFormatter.ConvertFromString<Int32>(str, conn) }; }
                catch(Exception) { }
                try { return new CpuType() { value = TypeFormatter.ConvertFromString<CpuEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("cpu")]
		public CpuType? Cpu { get; set; }
		
		[RosValue("active-cpu", IsDynamic=true)]
		public Int32? ActiveCpu { get; set; }
		
		[RosValue("count", IsDynamic=true)]
		public Int32? Count { get; set; }
		
		[RosValue("irq", IsDynamic=true)]
		public Int32? Irq { get; set; }
		
		[RosValue("users", IsDynamic=true)]
		public RouterOS.Interface Users { get; set; }
		
	}
	[RosObject("system resource irq rps", CanSet=true, CanGet=true)]
	public class SystemResourceIrqRps : RosItemObject<SystemResourceIrqRps>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public RouterOS.Interface Name { get; set; }
		
		public static SystemResourceIrqRps Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemResourceIrqRps obj = new SystemResourceIrqRps();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system resource pci", CanGet=true)]
	public class SystemResourcePci : RosItemObject<SystemResourcePci>
	{
		[RosValue("category", IsDynamic=true)]
		public String Category { get; set; }
		
		[RosValue("device", IsDynamic=true)]
		public String Device { get; set; }
		
		[RosValue("device-id", IsDynamic=true)]
		public String DeviceId { get; set; }
		
		[RosValue("io", IsDynamic=true)]
		public Int32? Io { get; set; }
		
		[RosValue("irq", IsDynamic=true)]
		public Int32? Irq { get; set; }
		
		[RosValue("memory", IsDynamic=true)]
		public Int32? Memory { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("vendor", IsDynamic=true)]
		public String Vendor { get; set; }
		
		[RosValue("vendor-id", IsDynamic=true)]
		public String VendorId { get; set; }
		
		public static SystemResourcePci Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemResourcePci obj = new SystemResourcePci();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system resource usb", CanSet=true, CanGet=true)]
	public class SystemResourceUsb : RosItemObject<SystemResourceUsb>
	{
		[RosValue("device")]
		public String Device { get; set; }
		
		[RosValue("device-id")]
		public String DeviceId { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		[RosValue("ports")]
		public Int32? Ports { get; set; }
		
		[RosValue("serial-number")]
		public String SerialNumber { get; set; }
		
		[RosValue("speed")]
		public String Speed { get; set; }
		
		[RosValue("usb-version")]
		public String UsbVersion { get; set; }
		
		[RosValue("vendor")]
		public String Vendor { get; set; }
		
		[RosValue("vendor-id")]
		public String VendorId { get; set; }
		
		public static SystemResourceUsb Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemResourceUsb obj = new SystemResourceUsb();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system routerboard bios", CanSet=true, CanGet=true)]
	public class SystemRouterboardBios : RosValueObject
	{
		public enum BaudRateEnum
		{
			[RosValue("115200")]
			E115200,
			[RosValue("1200")]
			E1200,
			[RosValue("19200")]
			E19200,
			[RosValue("2400")]
			E2400,
			[RosValue("38400")]
			E38400,
		}
		
		// Data rate of the port
		[RosValue("baud-rate")]
		public BaudRateEnum? BaudRate { get; set; }
		
		// Defines whether to beep or not when routerboard boots
		[RosValue("beep-on-boot")]
		public Boolean? BeepOnBoot { get; set; }
		
		public enum BootDelayEnum
		{
			[RosValue("BootDelay")]
			BootDelay,
		}
		
		// How long to wait for keypress to enter bios setup
		[RosValue("boot-delay")]
		public BootDelayEnum? BootDelay { get; set; }
		
		public enum BootDeviceEnum
		{
			[RosValue("etherboot-ide")]
			EtherbootIde,
			[RosValue("etherboot-only")]
			EtherbootOnly,
			[RosValue("ide-only")]
			IdeOnly,
			[RosValue("try-etherboot-once")]
			TryEtherbootOnce,
		}
		
		// Boot device
		[RosValue("boot-device")]
		public BootDeviceEnum? BootDevice { get; set; }
		
		public enum CpuModeEnum
		{
			[RosValue("regular")]
			Regular,
			[RosValue("power-save")]
			PowerSave,
		}
		
		// CPU power save mode
		[RosValue("cpu-mode")]
		public CpuModeEnum? CpuMode { get; set; }
		
		public enum DebugLevelEnum
		{
			[RosValue("high")]
			High,
			[RosValue("low")]
			Low,
			[RosValue("none")]
			None,
		}
		
		// Information quantity that will be shown when booting up the router
		[RosValue("debug-level")]
		public DebugLevelEnum? DebugLevel { get; set; }
		
		public enum EnterSetupOnEnum
		{
			[RosValue("delete-key")]
			DeleteKey,
			[RosValue("any-key")]
			AnyKey,
		}
		
		// How to enter BIOS setup during boot
		[RosValue("enter-setup-on")]
		public EnterSetupOnEnum? EnterSetupOn { get; set; }
		
		public enum EtherbootTimeoutEnum
		{
			[RosValue("EtherbootTimeout")]
			EtherbootTimeout,
		}
		
		// Timeout for etherboot to boot from server
		[RosValue("etherboot-timeout")]
		public EtherbootTimeoutEnum? EtherbootTimeout { get; set; }
		
		public enum MemorySettingsEnum
		{
			[RosValue("fail-safe")]
			FailSafe,
			[RosValue("optimal")]
			Optimal,
		}
		
		// RAM settings
		[RosValue("memory-settings")]
		public MemorySettingsEnum? MemorySettings { get; set; }
		
		// Do RAM test from BIOS on every boot
		[RosValue("memory-test")]
		public Boolean? MemoryTest { get; set; }
		
		public enum PciBackoffEnum
		{
			[RosValue("disabled")]
			Disabled,
			[RosValue("enabled")]
			Enabled,
		}
		
		// PCI Back Off
		[RosValue("pci-backoff")]
		public PciBackoffEnum? PciBackoff { get; set; }
		
		// Whether to show loading information through serial port
		[RosValue("vga-to-serial")]
		public Boolean? VgaToSerial { get; set; }
		
	}
	[RosObject("system scheduler", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemScheduler : RosItemObject<SystemScheduler>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interval between two script executions
		[RosValue("interval")]
		public TimeSpan? Interval { get; set; }
		
		// Scheduled event name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Script name or the script algorithm to execute on the event
		[RosValue("on-event")]
		public String OnEvent { get; set; }
		
		public enum PolicyEnum
		{
			[RosValue("api")]
			Api,
			[RosValue("ftp")]
			Ftp,
			[RosValue("local")]
			Local,
			[RosValue("password")]
			Password,
			[RosValue("policy")]
			Policy,
			[RosValue("web")]
			Web,
			[RosValue("winbox")]
			Winbox,
			[RosValue("read")]
			Read,
			[RosValue("reboot")]
			Reboot,
			[RosValue("sensitive")]
			Sensitive,
			[RosValue("sniff")]
			Sniff,
			[RosValue("ssh")]
			Ssh,
			[RosValue("telnet")]
			Telnet,
			[RosValue("test")]
			Test,
			[RosValue("write")]
			Write,
		}
		
		[RosValue("policy")]
		public PolicyEnum[] Policy { get; set; }
		
		// Date of first execution
		[RosValue("start-date")]
		public DateTime? StartDate { get; set; }
		
		public enum StartupEnum
		{
			[RosValue("startup")]
			Startup,
		}
		
		public struct StartTimeType 
                { 
                    private object value; 
                    public StartupEnum? Startup 
                { 
                    get { return value as StartupEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StartTimeType(StartupEnum? value) 
                { 
                    return new StartTimeType() { value = value }; 
                }public DateTime? Time 
                { 
                    get { return value as DateTime?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator StartTimeType(DateTime? value) 
                { 
                    return new StartTimeType() { value = value }; 
                } 
                    public static StartTimeType Parse(string str, Connection conn)
                    {
                        
                try { return new StartTimeType() { value = TypeFormatter.ConvertFromString<StartupEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new StartTimeType() { value = TypeFormatter.ConvertFromString<DateTime>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Time of first execution
		[RosValue("start-time")]
		public StartTimeType? StartTime { get; set; }
		
		[RosValue("next-run", IsDynamic=true)]
		public DateTime? NextRun { get; set; }
		
		[RosValue("owner", IsDynamic=true)]
		public String Owner { get; set; }
		
		[RosValue("run-count", IsDynamic=true)]
		public Int32? RunCount { get; set; }
		
		public static SystemScheduler Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemScheduler obj = new SystemScheduler();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system script", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemScript : RosItemObject<SystemScript>
	{
		// Name of the script
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum PolicyEnum
		{
			[RosValue("api")]
			Api,
			[RosValue("ftp")]
			Ftp,
			[RosValue("local")]
			Local,
			[RosValue("password")]
			Password,
			[RosValue("policy")]
			Policy,
			[RosValue("web")]
			Web,
			[RosValue("winbox")]
			Winbox,
			[RosValue("read")]
			Read,
			[RosValue("reboot")]
			Reboot,
			[RosValue("sensitive")]
			Sensitive,
			[RosValue("sniff")]
			Sniff,
			[RosValue("ssh")]
			Ssh,
			[RosValue("telnet")]
			Telnet,
			[RosValue("test")]
			Test,
			[RosValue("write")]
			Write,
		}
		
		// The name of the specific policy
		[RosValue("policy")]
		public PolicyEnum[] Policy { get; set; }
		
		// The script itself
		[RosValue("source")]
		public String Source { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		[RosValue("last-started", IsDynamic=true)]
		public DateTime? LastStarted { get; set; }
		
		[RosValue("owner", IsDynamic=true)]
		public String Owner { get; set; }
		
		[RosValue("run-count", IsDynamic=true)]
		public Int32? RunCount { get; set; }
		
		public static SystemScript Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemScript obj = new SystemScript();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system script environment", CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemScriptEnvironment : RosItemObject<SystemScriptEnvironment>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("value")]
		public String Value { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("user", IsDynamic=true)]
		public String User { get; set; }
		
		public static SystemScriptEnvironment Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemScriptEnvironment obj = new SystemScriptEnvironment();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system script job", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemScriptJob : RosItemObject<SystemScriptJob>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// The script that is already started
		[RosValue("started")]
		public DateTime? Started { get; set; }
		
		public enum TypeEnum
		{
			[RosValue("api-login")]
			ApiLogin,
			[RosValue("command")]
			Command,
			[RosValue("login")]
			Login,
		}
		
		[RosValue("type")]
		public TypeEnum? Type { get; set; }
		
		[RosValue("owner", IsDynamic=true)]
		public String Owner { get; set; }
		
		[RosValue("parent", IsDynamic=true)]
		public Int32? Parent { get; set; }
		
		public enum PolicyEnum
		{
			[RosValue("api")]
			Api,
			[RosValue("ftp")]
			Ftp,
			[RosValue("local")]
			Local,
			[RosValue("password")]
			Password,
			[RosValue("policy")]
			Policy,
			[RosValue("web")]
			Web,
			[RosValue("winbox")]
			Winbox,
			[RosValue("read")]
			Read,
			[RosValue("reboot")]
			Reboot,
			[RosValue("sensitive")]
			Sensitive,
			[RosValue("sniff")]
			Sniff,
			[RosValue("ssh")]
			Ssh,
			[RosValue("telnet")]
			Telnet,
			[RosValue("test")]
			Test,
			[RosValue("write")]
			Write,
		}
		
		[RosValue("policy", IsDynamic=true)]
		public PolicyEnum? Policy { get; set; }
		
		[RosValue("script", IsDynamic=true)]
		public String Script { get; set; }
		
	}
	[RosObject("system upgrade", CanSet=true, CanGet=true)]
	public class SystemUpgrade : RosItemObject<SystemUpgrade>
	{
		// Download a single package
		[RosValue("download")]
		public Boolean? Download { get; set; }
		
		public struct CompletedType
                {
                    public Int32 Value { get; set; }

                    public static CompletedType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator CompletedType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(CompletedType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("completed", IsDynamic=true)]
		public CompletedType? Completed { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("source", IsDynamic=true)]
		public IPAddress Source { get; set; }
		
		public enum StatusType
		{
			[RosValue("available")]
			Available,
			[RosValue("downloaded")]
			Downloaded,
			[RosValue("downloading")]
			Downloading,
			[RosValue("installed")]
			Installed,
			[RosValue("scheduled")]
			Scheduled,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
		[RosValue("version", IsDynamic=true)]
		public String Version { get; set; }
		
		public static SystemUpgrade Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemUpgrade obj = new SystemUpgrade();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system upgrade mirror", CanSet=true, CanGet=true)]
	public class SystemUpgradeMirror : RosValueObject
	{
		public enum CheckIntervalEnum
		{
			[RosValue("CheckInterval")]
			CheckInterval,
		}
		
		// Time period after which to check for the new version
		[RosValue("check-interval")]
		public CheckIntervalEnum? CheckInterval { get; set; }
		
		// Enable or not automatic downloading
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// User password
		[RosValue("password")]
		public String Password { get; set; }
		
		// Server's IP address where all master routers are downloading packages from
		[RosValue("primary-server")]
		public IPAddress PrimaryServer { get; set; }
		
		// Backup server IP address
		[RosValue("secondary-server")]
		public IPAddress SecondaryServer { get; set; }
		
		// The name of the user
		[RosValue("user")]
		public String User { get; set; }
		
	}
	[RosObject("system upgrade upgrade-package-source", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemUpgradeUpgradePackageSource : RosItemObject<SystemUpgradeUpgradePackageSource>
	{
		// IP address of the source router
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		// Username of the source router
		[RosValue("user")]
		public String User { get; set; }
		
	}
	[RosObject("system ups", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class SystemUps : RosItemObject<SystemUps>
	{
		public enum AlarmSettingEnum
		{
			[RosValue("delayed")]
			Delayed,
			[RosValue("immediate")]
			Immediate,
			[RosValue("low-battery")]
			LowBattery,
			[RosValue("none")]
			None,
		}
		
		// UPS sound alarm setting
		[RosValue("alarm-setting")]
		public AlarmSettingEnum? AlarmSetting { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum NeverEnum
		{
			[RosValue("never")]
			Never,
		}
		
		public struct MinRuntimeType 
                { 
                    private object value; 
                    public NeverEnum? Never 
                { 
                    get { return value as NeverEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MinRuntimeType(NeverEnum? value) 
                { 
                    return new MinRuntimeType() { value = value }; 
                }public TimeSpan? MinRuntime 
                { 
                    get { return value as TimeSpan?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator MinRuntimeType(TimeSpan? value) 
                { 
                    return new MinRuntimeType() { value = value }; 
                } 
                    public static MinRuntimeType Parse(string str, Connection conn)
                    {
                        
                try { return new MinRuntimeType() { value = TypeFormatter.ConvertFromString<NeverEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new MinRuntimeType() { value = TypeFormatter.ConvertFromString<TimeSpan>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Minimal run time remaining
		[RosValue("min-runtime")]
		public MinRuntimeType? MinRuntime { get; set; }
		
		[RosValue("name")]
		public String Name { get; set; }
		
		// How long to work on batteries
		[RosValue("offline-time")]
		public TimeSpan? OfflineTime { get; set; }
		
		// Serial port UPS is connected to
		[RosValue("port")]
		public String Port { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		public struct LoadType
                {
                    public Int32 Value { get; set; }

                    public static LoadType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Int32>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator LoadType(Int32 value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Int32(LoadType parser)
                    {
                        return parser.Value;
                    }
                }
		[RosValue("load", IsDynamic=true)]
		public LoadType? Load { get; set; }
		
		[RosValue("manufacture-date", IsDynamic=true)]
		public String ManufactureDate { get; set; }
		
		[RosValue("model", IsDynamic=true)]
		public String Model { get; set; }
		
		[RosValue("nominal-battery-voltage", IsDynamic=true)]
		public String NominalBatteryVoltage { get; set; }
		
		[RosValue("offline-after", IsDynamic=true)]
		public TimeSpan? OfflineAfter { get; set; }
		
		[RosValue("on-line", IsDynamic=true)]
		public String OnLine { get; set; }
		
		[RosValue("serial", IsDynamic=true)]
		public String Serial { get; set; }
		
		[RosValue("version", IsDynamic=true)]
		public String Version { get; set; }
		
		public static SystemUps Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			SystemUps obj = new SystemUps();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("system watchdog", CanSet=true, CanGet=true)]
	public class SystemWatchdog : RosValueObject
	{
		// After the support output file is automatically generated, it can be sent by email
		[RosValue("auto-send-supout")]
		public Boolean? AutoSendSupout { get; set; }
		
		// When software failure happens, a file named "autosupout.rif" is generated automatically
		[RosValue("automatic-supout")]
		public Boolean? AutomaticSupout { get; set; }
		
		// Specifies how long after reboot not to test and ping 'watch-address'
		[RosValue("no-ping-delay")]
		public TimeSpan? NoPingDelay { get; set; }
		
		// E-mail address to send the support output file from
		[RosValue("send-email-from")]
		public String SendEmailFrom { get; set; }
		
		// E-mail address to send the support output file to
		[RosValue("send-email-to")]
		public String SendEmailTo { get; set; }
		
		// SMTP server address to send the support output file through
		[RosValue("send-smtp-server")]
		public IPAddress SendSmtpServer { get; set; }
		
		// The system will reboot in case 6 sequental pings to the given IP address will fail
		[RosValue("watch-address")]
		public String WatchAddress { get; set; }
		
		// Reboot if system is unresponsive for a minute
		[RosValue("watchdog-timer")]
		public Boolean? WatchdogTimer { get; set; }
		
	}
	[RosObject("tool bandwidth-server", CanSet=true, CanGet=true)]
	public class ToolBandwidthServer : RosValueObject
	{
		// Beginning of UDP port range
		[RosValue("allocate-udp-ports-from")]
		public Int32? AllocateUdpPortsFrom { get; set; }
		
		// Communicate only with authenticated clients
		[RosValue("authenticate")]
		public Boolean? Authenticate { get; set; }
		
		// Defines whether bandwidth server is enabled or not
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
		// Maximal simultaneous test count
		[RosValue("max-sessions")]
		public Int32? MaxSessions { get; set; }
		
	}
	[RosObject("tool bandwidth-server session", CanGet=true, CanRemove=true)]
	public class ToolBandwidthServerSession : RosItemObject<ToolBandwidthServerSession>
	{
		[RosValue("client", IsDynamic=true)]
		public IPAddress Client { get; set; }
		
		public enum DirectionType
		{
			[RosValue("both")]
			Both,
			[RosValue("receive")]
			Receive,
			[RosValue("send")]
			Send,
		}
		
		[RosValue("direction", IsDynamic=true)]
		public DirectionType? Direction { get; set; }
		
		public enum ProtocolType
		{
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
		}
		
		[RosValue("protocol", IsDynamic=true)]
		public ProtocolType? Protocol { get; set; }
		
		[RosValue("user", IsDynamic=true)]
		public String User { get; set; }
		
	}
	[RosObject("tool e-mail", CanSet=true, CanGet=true)]
	public class ToolEMail : RosValueObject
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Address value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPAddress value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		[RosValue("address")]
		public AddressType? Address { get; set; }
		
		[RosValue("from")]
		public String From { get; set; }
		
		[RosValue("password")]
		public String Password { get; set; }
		
		[RosValue("port")]
		public Int32? Port { get; set; }
		
		[RosValue("username")]
		public String Username { get; set; }
		
	}
	[RosObject("tool graphing", CanSet=true, CanGet=true)]
	public class ToolGraphing : RosValueObject
	{
		public enum PageRefreshEnum
		{
			[RosValue("never")]
			Never,
		}
		
		// How often to refresh HTML pages (in seconds)
		[RosValue("page-refresh")]
		public PageRefreshEnum? PageRefresh { get; set; }
		
		public enum StoreEveryEnum
		{
			[RosValue("24hours")]
			E24hours,
			[RosValue("5min")]
			E5min,
			[RosValue("hour")]
			Hour,
		}
		
		// How often to store information on system drive
		[RosValue("store-every")]
		public StoreEveryEnum? StoreEvery { get; set; }
		
	}
	[RosObject("tool graphing interface", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class ToolGraphingInterface : RosItemObject<ToolGraphingInterface>
	{
		public struct AllowAddressType 
                { 
                    private object value; 
                    public IPPrefix? IpPrefix 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPPrefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                }public IPv6Prefix? Ipv6Prefix 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPv6Prefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                } 
                    public static AllowAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address range which is allowed to view information about the interface
		[RosValue("allow-address")]
		public AllowAddressType? AllowAddress { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum AllEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public AllEnum? All 
                { 
                    get { return value as AllEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(AllEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<AllEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Name of the interface which will be monitored
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		// Whether to store information about traffic on system drive or not
		[RosValue("store-on-disk")]
		public Boolean? StoreOnDisk { get; set; }
		
	}
	[RosObject("tool graphing queue", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class ToolGraphingQueue : RosItemObject<ToolGraphingQueue>
	{
		public struct AllowAddressType 
                { 
                    private object value; 
                    public IPPrefix? IpPrefix 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPPrefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                }public IPv6Prefix? Ipv6Prefix 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPv6Prefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                } 
                    public static AllowAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address range which is allowed to view information about the queue
		[RosValue("allow-address")]
		public AllowAddressType? AllowAddress { get; set; }
		
		// Whether to allow access to web graphing from IP range specified in target-address parameter
		[RosValue("allow-target")]
		public Boolean? AllowTarget { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Name of simple queue which will be monitored
		[RosValue("simple-queue")]
		public String SimpleQueue { get; set; }
		
		// Whether to store information about traffic on hard drive or not
		[RosValue("store-on-disk")]
		public Boolean? StoreOnDisk { get; set; }
		
	}
	[RosObject("tool graphing resource", CanAdd=true, CanSet=true, CanMove=true, CanGet=true, CanRemove=true)]
	public class ToolGraphingResource : RosItemObject<ToolGraphingResource>
	{
		public struct AllowAddressType 
                { 
                    private object value; 
                    public IPPrefix? IpPrefix 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPPrefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                }public IPv6Prefix? Ipv6Prefix 
                { 
                    get { return value as IPv6Prefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AllowAddressType(IPv6Prefix? value) 
                { 
                    return new AllowAddressType() { value = value }; 
                } 
                    public static AllowAddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AllowAddressType() { value = TypeFormatter.ConvertFromString<IPv6Prefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP address range which is allowed to view information about the resource usage
		[RosValue("allow-address")]
		public AllowAddressType? AllowAddress { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Whether to store information about traffic on hard drive or not
		[RosValue("store-on-disk")]
		public Boolean? StoreOnDisk { get; set; }
		
	}
	[RosObject("tool mac-server", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class ToolMacServer : RosItemObject<ToolMacServer>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum InterfaceEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public InterfaceEnum? State 
                { 
                    get { return value as InterfaceEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(InterfaceEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<InterfaceEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Interface name
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
	}
	[RosObject("tool mac-server mac-winbox", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class ToolMacServerMacWinbox : RosItemObject<ToolMacServerMacWinbox>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		public enum InterfaceEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public InterfaceEnum? State 
                { 
                    get { return value as InterfaceEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(InterfaceEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<InterfaceEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Interface name
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
	}
	[RosObject("tool mac-server ping", CanSet=true, CanGet=true)]
	public class ToolMacServerPing : RosValueObject
	{
		// Enabled or not MAC pinging
		[RosValue("enabled")]
		public Boolean? Enabled { get; set; }
		
	}
	[RosObject("tool mac-server sessions", CanSet=true, CanGet=true)]
	public class ToolMacServerSessions : RosItemObject<ToolMacServerSessions>
	{
		// Interface the client is connected to
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// MAC address the client is connected from
		[RosValue("src-address")]
		public MACAddress? SrcAddress { get; set; }
		
		// How long the client is connected to the server
		[RosValue("uptime")]
		public TimeSpan? Uptime { get; set; }
		
	}
	[RosObject("tool netwatch", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class ToolNetwatch : RosItemObject<ToolNetwatch>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Script that is started when host goes down
		[RosValue("down-script")]
		public String DownScript { get; set; }
		
		// Host IP address
		[RosValue("host")]
		public String Host { get; set; }
		
		// Ping interval in seconds
		[RosValue("interval")]
		public TimeSpan? Interval { get; set; }
		
		// Timeout in seconds after which host is considered down
		[RosValue("timeout")]
		public TimeSpan? Timeout { get; set; }
		
		// Script that is started when host goes up
		[RosValue("up-script")]
		public String UpScript { get; set; }
		
		[RosValue("since", IsDynamic=true)]
		public DateTime? Since { get; set; }
		
		public enum StatusType
		{
			[RosValue("down")]
			Down,
			[RosValue("unknown")]
			Unknown,
			[RosValue("up")]
			Up,
		}
		
		[RosValue("status", IsDynamic=true)]
		public StatusType? Status { get; set; }
		
	}
	[RosObject("tool sigwatch", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class ToolSigwatch : RosItemObject<ToolSigwatch>
	{
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Add or not a message to System-Info facility
		[RosValue("log")]
		public Boolean? Log { get; set; }
		
		// The name of sigwatch item
		[RosValue("name")]
		public String Name { get; set; }
		
		public enum OnConditionEnum
		{
			[RosValue("change")]
			Change,
			[RosValue("off")]
			Off,
			[RosValue("on")]
			On,
		}
		
		// On what condition to trigger actions of this item
		[RosValue("on-condition")]
		public OnConditionEnum? OnCondition { get; set; }
		
		// The number of PIN to monitor
		[RosValue("pin")]
		public Int32? Pin { get; set; }
		
		// Serial port to monitor
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		// Script that is executed whenever this item is triggered
		[RosValue("script")]
		public String Script { get; set; }
		
		[RosValue("count", IsDynamic=true)]
		public Int32? Count { get; set; }
		
		public enum StateType
		{
			[RosValue("off")]
			Off,
			[RosValue("on")]
			On,
			[RosValue("unknown")]
			Unknown,
		}
		
		[RosValue("state", IsDynamic=true)]
		public StateType? State { get; set; }
		
		public static ToolSigwatch Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			ToolSigwatch obj = new ToolSigwatch();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("tool sms", CanSet=true, CanGet=true)]
	public class ToolSms : RosValueObject
	{
		[RosValue("allowed-number")]
		public String AllowedNumber { get; set; }
		
		[RosValue("channel")]
		public Int32? Channel { get; set; }
		
		[RosValue("keep-max-sms")]
		public Int32? KeepMaxSms { get; set; }
		
		[RosValue("port")]
		public RouterOS.Port Port { get; set; }
		
		[RosValue("receive-enabled")]
		public Boolean? ReceiveEnabled { get; set; }
		
		[RosValue("secret")]
		public String Secret { get; set; }
		
	}
	[RosObject("tool sms inbox", CanAdd=true, CanGet=true, CanRemove=true)]
	public class ToolSmsInbox : RosItemObject<ToolSmsInbox>
	{
		[RosValue("src", IsDynamic=true)]
		public String Src { get; set; }
		
		[RosValue("text", IsDynamic=true)]
		public String Text { get; set; }
		
		[RosValue("timestamp", IsDynamic=true)]
		public String Timestamp { get; set; }
		
		public enum TypeType
		{
			[RosValue("src")]
			Src,
			[RosValue("text")]
			Text,
			[RosValue("timestamp")]
			Timestamp,
			[RosValue("type")]
			Type,
		}
		
		[RosValue("type", IsDynamic=true)]
		public TypeType? Type { get; set; }
		
	}
	[RosObject("tool sniffer", CanSet=true, CanGet=true)]
	public class ToolSniffer : RosValueObject
	{
		// The limit of the file in KB
		[RosValue("file-limit")]
		public Int32? FileLimit { get; set; }
		
		// The name of the file
		[RosValue("file-name")]
		public String FileName { get; set; }
		
		public struct PortsType
                {
                    public UInt16 Start { get; set; }
                    public UInt16 End { get; set; }

                    public static PortsType Parse(string str)
                    {
                        string[] strs = str.Split('-');
                        if (strs.Length == 2)
                            return new PortsType() { Start = TypeFormatter.ConvertFromString<UInt16>(strs[0], null), End = TypeFormatter.ConvertFromString<UInt16>(strs[1], null) };
                        else if (strs.Length == 1)
                            return new PortsType() { Start = TypeFormatter.ConvertFromString<UInt16>(strs[0], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        if(End != null)
                            return String.Concat(Start, '-', End);
                        else
                            return Start.ToString();
                    }
                }
		public struct FilterAddress1Type
                {
                    public IPPrefix Address { get; set; }
                    public PortsType Ports { get; set; }

                    public static FilterAddress1Type Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new FilterAddress1Type() { Address = TypeFormatter.ConvertFromString<IPPrefix>(strs[0], null), Ports = TypeFormatter.ConvertFromString<PortsType>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Ports);
                    }
                }
		// The first address to filter
		[RosValue("filter-address1")]
		public FilterAddress1Type? FilterAddress1 { get; set; }
		
		public struct FilterAddress2Type
                {
                    public IPPrefix Address { get; set; }
                    public PortsType Ports { get; set; }

                    public static FilterAddress2Type Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new FilterAddress2Type() { Address = TypeFormatter.ConvertFromString<IPPrefix>(strs[0], null), Ports = TypeFormatter.ConvertFromString<PortsType>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Ports);
                    }
                }
		// The second address to filter
		[RosValue("filter-address2")]
		public FilterAddress2Type? FilterAddress2 { get; set; }
		
		public enum FilterProtocolEnum
		{
			[RosValue("all-frames")]
			AllFrames,
			[RosValue("ip-only")]
			IpOnly,
			[RosValue("mac-only-no-ip")]
			MacOnlyNoIp,
		}
		
		// Filter specific protocol
		[RosValue("filter-protocol")]
		public FilterProtocolEnum? FilterProtocol { get; set; }
		
		// Sniffed packets that are devised for sniffer server are ignored
		[RosValue("filter-stream")]
		public Boolean? FilterStream { get; set; }
		
		public enum InterfaceEnum
		{
			[RosValue("all")]
			All,
		}
		
		public struct InterfaceType 
                { 
                    private object value; 
                    public RouterOS.Interface Interface 
                { 
                    get { return value as RouterOS.Interface; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(RouterOS.Interface value) 
                { 
                    return new InterfaceType() { value = value }; 
                }public InterfaceEnum? State 
                { 
                    get { return value as InterfaceEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator InterfaceType(InterfaceEnum? value) 
                { 
                    return new InterfaceType() { value = value }; 
                } 
                    public static InterfaceType Parse(string str, Connection conn)
                    {
                        
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<RouterOS.Interface>(str, conn) }; }
                catch(Exception) { }
                try { return new InterfaceType() { value = TypeFormatter.ConvertFromString<InterfaceEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Interface management
		[RosValue("interface")]
		public InterfaceType? Interface { get; set; }
		
		// Memory amount reached in KB to stop sniffing
		[RosValue("memory-limit")]
		public Int32? MemoryLimit { get; set; }
		
		[RosValue("memory-scroll")]
		public Boolean? MemoryScroll { get; set; }
		
		// Save in the memory only packet's headers not the whole packet
		[RosValue("only-headers")]
		public Boolean? OnlyHeaders { get; set; }
		
		// Defines whether to send sniffed packets to sniffer's server or not
		[RosValue("streaming-enabled")]
		public Boolean? StreamingEnabled { get; set; }
		
		// Tazmen Sniffer Protocol (TZSP) stream receiver
		[RosValue("streaming-server")]
		public IPAddress StreamingServer { get; set; }
		
	}
	[RosObject("tool sniffer connection", CanSet=true, CanGet=true)]
	public class ToolSnifferConnection : RosItemObject<ToolSnifferConnection>
	{
		public struct BytesType
                {
                    public Int32 In { get; set; }
                    public Int32 Out { get; set; }

                    public static BytesType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new BytesType() { In = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Out = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// Bytes in the current connection
		[RosValue("bytes")]
		public BytesType? Bytes { get; set; }
		
		public enum PortEnum
		{
			[RosValue("acap")]
			Acap,
			[RosValue("activision")]
			Activision,
			[RosValue("afpovertcp")]
			Afpovertcp,
			[RosValue("agentx")]
			Agentx,
			[RosValue("aol")]
			Aol,
			[RosValue("tacacs")]
			Tacacs,
			[RosValue("link")]
			Link,
			[RosValue("arcp")]
			Arcp,
			[RosValue("systat")]
			Systat,
			[RosValue("apple-licman")]
			AppleLicman,
			[RosValue("appleugcontrol")]
			Appleugcontrol,
			[RosValue("asip-webadmin")]
			AsipWebadmin,
			[RosValue("asipregistry")]
			Asipregistry,
			[RosValue("asia")]
			Asia,
			[RosValue("aurp")]
			Aurp,
			[RosValue("auth")]
			Auth,
			[RosValue("bftp")]
			Bftp,
			[RosValue("bootpc")]
			Bootpc,
			[RosValue("bootps")]
			Bootps,
			[RosValue("bgp")]
			Bgp,
			[RosValue("bdp")]
			Bdp,
			[RosValue("chargen")]
			Chargen,
			[RosValue("cfdptkt")]
			Cfdptkt,
			[RosValue("cpq-wbem")]
			CpqWbem,
			[RosValue("epmap")]
			Epmap,
			[RosValue("dlsrpn")]
			Dlsrpn,
			[RosValue("dlswpn")]
			Dlswpn,
			[RosValue("dlsrap")]
			Dlsrap,
			[RosValue("squid")]
			Squid,
			[RosValue("rip")]
			Rip,
			[RosValue("dsp")]
			Dsp,
			[RosValue("dns")]
			Dns,
			[RosValue("esro-emsdp")]
			EsroEmsdp,
			[RosValue("esro-gen")]
			EsroGen,
			[RosValue("etftp")]
			Etftp,
			[RosValue("nfile")]
			Nfile,
			[RosValue("ftp")]
			Ftp,
			[RosValue("ftp-data")]
			FtpData,
			[RosValue("glimpse")]
			Glimpse,
			[RosValue("swat")]
			Swat,
			[RosValue("name")]
			Name,
			[RosValue("hsrp")]
			Hsrp,
			[RosValue("icpv2")]
			Icpv2,
			[RosValue("ircu")]
			Ircu,
			[RosValue("iso-tsap")]
			IsoTsap,
			[RosValue("poppassd")]
			Poppassd,
			[RosValue("imap3")]
			Imap3,
			[RosValue("isakmp")]
			Isakmp,
			[RosValue("ipp")]
			Ipp,
			[RosValue("irc")]
			Irc,
			[RosValue("rap")]
			Rap,
			[RosValue("ldap")]
			Ldap,
			[RosValue("lpr")]
			Lpr,
			[RosValue("matip-type-a")]
			MatipTypeA,
			[RosValue("matip-type-b")]
			MatipTypeB,
			[RosValue("mac-srvr-admin")]
			MacSrvrAdmin,
			[RosValue("mtp")]
			Mtp,
			[RosValue("csnet-ns")]
			CsnetNs,
			[RosValue("mgcp-callagent")]
			MgcpCallagent,
			[RosValue("mgcp-gateway")]
			MgcpGateway,
			[RosValue("smb")]
			Smb,
			[RosValue("ms-sql-m")]
			MsSqlM,
			[RosValue("ms-sql-s")]
			MsSqlS,
			[RosValue("mc-ftp")]
			McFtp,
			[RosValue("btserv")]
			Btserv,
			[RosValue("discovery")]
			Discovery,
			[RosValue("winbox-old-tls")]
			WinboxOldTls,
			[RosValue("winbox-old")]
			WinboxOld,
			[RosValue("mobileip")]
			Mobileip,
			[RosValue("netbios-dgm")]
			NetbiosDgm,
			[RosValue("netbios-ns")]
			NetbiosNs,
			[RosValue("netbios-ssn")]
			NetbiosSsn,
			[RosValue("hostname")]
			Hostname,
			[RosValue("rrp")]
			Rrp,
			[RosValue("net-assistant")]
			NetAssistant,
			[RosValue("mpp")]
			Mpp,
			[RosValue("nfs")]
			Nfs,
			[RosValue("nntp")]
			Nntp,
			[RosValue("ntp")]
			Ntp,
			[RosValue("nextstep")]
			Nextstep,
			[RosValue("odmr")]
			Odmr,
			[RosValue("sql*net")]
			SqlNet,
			[RosValue("pgp5-key")]
			Pgp5Key,
			[RosValue("hotsync-2")]
			Hotsync2,
			[RosValue("hotsync-1")]
			Hotsync1,
			[RosValue("pwdgen")]
			Pwdgen,
			[RosValue("pop2")]
			Pop2,
			[RosValue("pop3")]
			Pop3,
			[RosValue("pdap-np")]
			PdapNp,
			[RosValue("prospero-np")]
			ProsperoNp,
			[RosValue("qotd")]
			Qotd,
			[RosValue("radius")]
			Radius,
			[RosValue("radius-acct")]
			RadiusAcct,
			[RosValue("tcpmux")]
			Tcpmux,
			[RosValue("dixie")]
			Dixie,
			[RosValue("finger")]
			Finger,
			[RosValue("gopher")]
			Gopher,
			[RosValue("kerberos")]
			Kerberos,
			[RosValue("supdup")]
			Supdup,
			[RosValue("echo")]
			Echo,
			[RosValue("discard")]
			Discard,
			[RosValue("daytime")]
			Daytime,
			[RosValue("time")]
			Time,
			[RosValue("ripng")]
			Ripng,
			[RosValue("ms-rpc")]
			MsRpc,
			[RosValue("rsvp-tunnel")]
			RsvpTunnel,
			[RosValue("realsecure")]
			Realsecure,
			[RosValue("rtsp")]
			Rtsp,
			[RosValue("eppc")]
			Eppc,
			[RosValue("rwhois")]
			Rwhois,
			[RosValue("rje")]
			Rje,
			[RosValue("re-mail-ck")]
			ReMailCk,
			[RosValue("rlogin")]
			Rlogin,
			[RosValue("rwp")]
			Rwp,
			[RosValue("rlp")]
			Rlp,
			[RosValue("sip")]
			Sip,
			[RosValue("sql-net")]
			SqlNet,
			[RosValue("ssh")]
			Ssh,
			[RosValue("sunrpc")]
			Sunrpc,
			[RosValue("secureid")]
			Secureid,
			[RosValue("sift-uft")]
			SiftUft,
			[RosValue("sftp")]
			Sftp,
			[RosValue("snmp")]
			Snmp,
			[RosValue("snpp")]
			Snpp,
			[RosValue("smtp")]
			Smtp,
			[RosValue("statsrv")]
			Statsrv,
			[RosValue("t.120")]
			T120,
			[RosValue("timbuktu-srv1")]
			TimbuktuSrv1,
			[RosValue("timbuktu-srv2")]
			TimbuktuSrv2,
			[RosValue("timbuktu-srv3")]
			TimbuktuSrv3,
			[RosValue("timbuktu-srv4")]
			TimbuktuSrv4,
			[RosValue("tftp")]
			Tftp,
			[RosValue("uucp-path")]
			UucpPath,
			[RosValue("unreal")]
			Unreal,
			[RosValue("biff")]
			Biff,
			[RosValue("uls")]
			Uls,
			[RosValue("vemmi")]
			Vemmi,
			[RosValue("vnc-1")]
			Vnc1,
			[RosValue("vnc-2")]
			Vnc2,
			[RosValue("http-alt")]
			HttpAlt,
			[RosValue("nicname")]
			Nicname,
			[RosValue("whois++")]
			Whois,
			[RosValue("ms-streaming")]
			MsStreaming,
			[RosValue("msbd")]
			Msbd,
			[RosValue("http")]
			Http,
			[RosValue("xdmcp")]
			Xdmcp,
			[RosValue("x11")]
			X11,
			[RosValue("yahoo")]
			Yahoo,
			[RosValue("appleqtc")]
			Appleqtc,
			[RosValue("tcp-id-port")]
			TcpIdPort,
			[RosValue("distributed-net")]
			DistributedNet,
			[RosValue("doom")]
			Doom,
			[RosValue("https")]
			Https,
			[RosValue("imaps")]
			Imaps,
			[RosValue("ldaps")]
			Ldaps,
			[RosValue("nntps")]
			Nntps,
			[RosValue("tlisrv")]
			Tlisrv,
			[RosValue("pop3s")]
			Pop3s,
			[RosValue("linuxconf")]
			Linuxconf,
		}
		
		public struct DstAddressType
                {
                    public IPAddress Address { get; set; }
                    public PortEnum Port { get; set; }

                    public static DstAddressType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new DstAddressType() { Address = TypeFormatter.ConvertFromString<IPAddress>(strs[0], null), Port = TypeFormatter.ConvertFromString<PortEnum>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Port);
                    }
                }
		// Destination address
		[RosValue("dst-address")]
		public DstAddressType? DstAddress { get; set; }
		
		public struct MssType
                {
                    public Int32 In { get; set; }
                    public Int32 Out { get; set; }

                    public static MssType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new MssType() { In = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Out = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// Maximum Segment Size
		[RosValue("mss")]
		public MssType? Mss { get; set; }
		
		public struct ResendsType
                {
                    public Int32 In { get; set; }
                    public Int32 Out { get; set; }

                    public static ResendsType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new ResendsType() { In = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Out = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// The number of packets resends in the current connection
		[RosValue("resends")]
		public ResendsType? Resends { get; set; }
		
		public struct SrcAddressType
                {
                    public IPAddress Address { get; set; }
                    public PortEnum Port { get; set; }

                    public static SrcAddressType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new SrcAddressType() { Address = TypeFormatter.ConvertFromString<IPAddress>(strs[0], null), Port = TypeFormatter.ConvertFromString<PortEnum>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Port);
                    }
                }
		// Source address
		[RosValue("src-address")]
		public SrcAddressType? SrcAddress { get; set; }
		
		[RosValue("active", IsDynamic=true)]
		public String Active { get; set; }
		
	}
	[RosObject("tool sniffer host", CanSet=true, CanGet=true)]
	public class ToolSnifferHost : RosItemObject<ToolSnifferHost>
	{
		// The address of the host
		[RosValue("address")]
		public IPAddress Address { get; set; }
		
		public struct PeekRateType
                {
                    public Bits In { get; set; }
                    public Bits Out { get; set; }

                    public static PeekRateType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new PeekRateType() { In = TypeFormatter.ConvertFromString<Bits>(strs[0], null), Out = TypeFormatter.ConvertFromString<Bits>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// The maximum data-rate received/transmitted
		[RosValue("peek-rate")]
		public PeekRateType? PeekRate { get; set; }
		
		public struct RateType
                {
                    public Bits In { get; set; }
                    public Bits Out { get; set; }

                    public static RateType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new RateType() { In = TypeFormatter.ConvertFromString<Bits>(strs[0], null), Out = TypeFormatter.ConvertFromString<Bits>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// Current data-rate received/transmitted
		[RosValue("rate")]
		public RateType? Rate { get; set; }
		
		public struct TotalType
                {
                    public Int32 In { get; set; }
                    public Int32 Out { get; set; }

                    public static TotalType Parse(string str)
                    {
                        string[] strs = str.Split('/');
                        if (strs.Length == 2)
                            return new TotalType() { In = TypeFormatter.ConvertFromString<Int32>(strs[0], null), Out = TypeFormatter.ConvertFromString<Int32>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(In, '/', Out);
                    }
                }
		// Total packets received/transmitted
		[RosValue("total")]
		public TotalType? Total { get; set; }
		
	}
	[RosObject("tool sniffer packet", CanSet=true, CanGet=true)]
	public class ToolSnifferPacket : RosItemObject<ToolSnifferPacket>
	{
		// Search for specified data inclusion in packets
		[RosValue("data")]
		public String Data { get; set; }
		
		public enum DirectionEnum
		{
			[RosValue("in")]
			In,
			[RosValue("out")]
			Out,
		}
		
		[RosValue("direction")]
		public DirectionEnum? Direction { get; set; }
		
		[RosValue("dscp")]
		public Int32? Dscp { get; set; }
		
		public struct AddressType 
                { 
                    private object value; 
                    public IPAddress Ip 
                { 
                    get { return value as IPAddress; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPAddress value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPv6Address Ipv6 
                { 
                    get { return value as IPv6Address; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPv6Address value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPAddress>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPv6Address>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		public enum PortEnum
		{
			[RosValue("acap")]
			Acap,
			[RosValue("activision")]
			Activision,
			[RosValue("afpovertcp")]
			Afpovertcp,
			[RosValue("agentx")]
			Agentx,
			[RosValue("aol")]
			Aol,
			[RosValue("tacacs")]
			Tacacs,
			[RosValue("link")]
			Link,
			[RosValue("arcp")]
			Arcp,
			[RosValue("systat")]
			Systat,
			[RosValue("apple-licman")]
			AppleLicman,
			[RosValue("appleugcontrol")]
			Appleugcontrol,
			[RosValue("asip-webadmin")]
			AsipWebadmin,
			[RosValue("asipregistry")]
			Asipregistry,
			[RosValue("asia")]
			Asia,
			[RosValue("aurp")]
			Aurp,
			[RosValue("auth")]
			Auth,
			[RosValue("bftp")]
			Bftp,
			[RosValue("bootpc")]
			Bootpc,
			[RosValue("bootps")]
			Bootps,
			[RosValue("bgp")]
			Bgp,
			[RosValue("bdp")]
			Bdp,
			[RosValue("chargen")]
			Chargen,
			[RosValue("cfdptkt")]
			Cfdptkt,
			[RosValue("cpq-wbem")]
			CpqWbem,
			[RosValue("epmap")]
			Epmap,
			[RosValue("dlsrpn")]
			Dlsrpn,
			[RosValue("dlswpn")]
			Dlswpn,
			[RosValue("dlsrap")]
			Dlsrap,
			[RosValue("squid")]
			Squid,
			[RosValue("rip")]
			Rip,
			[RosValue("dsp")]
			Dsp,
			[RosValue("dns")]
			Dns,
			[RosValue("esro-emsdp")]
			EsroEmsdp,
			[RosValue("esro-gen")]
			EsroGen,
			[RosValue("etftp")]
			Etftp,
			[RosValue("nfile")]
			Nfile,
			[RosValue("ftp")]
			Ftp,
			[RosValue("ftp-data")]
			FtpData,
			[RosValue("glimpse")]
			Glimpse,
			[RosValue("swat")]
			Swat,
			[RosValue("name")]
			Name,
			[RosValue("hsrp")]
			Hsrp,
			[RosValue("icpv2")]
			Icpv2,
			[RosValue("ircu")]
			Ircu,
			[RosValue("iso-tsap")]
			IsoTsap,
			[RosValue("poppassd")]
			Poppassd,
			[RosValue("imap3")]
			Imap3,
			[RosValue("isakmp")]
			Isakmp,
			[RosValue("ipp")]
			Ipp,
			[RosValue("irc")]
			Irc,
			[RosValue("rap")]
			Rap,
			[RosValue("ldap")]
			Ldap,
			[RosValue("lpr")]
			Lpr,
			[RosValue("matip-type-a")]
			MatipTypeA,
			[RosValue("matip-type-b")]
			MatipTypeB,
			[RosValue("mac-srvr-admin")]
			MacSrvrAdmin,
			[RosValue("mtp")]
			Mtp,
			[RosValue("csnet-ns")]
			CsnetNs,
			[RosValue("mgcp-callagent")]
			MgcpCallagent,
			[RosValue("mgcp-gateway")]
			MgcpGateway,
			[RosValue("smb")]
			Smb,
			[RosValue("ms-sql-m")]
			MsSqlM,
			[RosValue("ms-sql-s")]
			MsSqlS,
			[RosValue("mc-ftp")]
			McFtp,
			[RosValue("btserv")]
			Btserv,
			[RosValue("discovery")]
			Discovery,
			[RosValue("winbox-old-tls")]
			WinboxOldTls,
			[RosValue("winbox-old")]
			WinboxOld,
			[RosValue("mobileip")]
			Mobileip,
			[RosValue("netbios-dgm")]
			NetbiosDgm,
			[RosValue("netbios-ns")]
			NetbiosNs,
			[RosValue("netbios-ssn")]
			NetbiosSsn,
			[RosValue("hostname")]
			Hostname,
			[RosValue("rrp")]
			Rrp,
			[RosValue("net-assistant")]
			NetAssistant,
			[RosValue("mpp")]
			Mpp,
			[RosValue("nfs")]
			Nfs,
			[RosValue("nntp")]
			Nntp,
			[RosValue("ntp")]
			Ntp,
			[RosValue("nextstep")]
			Nextstep,
			[RosValue("odmr")]
			Odmr,
			[RosValue("sql*net")]
			SqlNet,
			[RosValue("pgp5-key")]
			Pgp5Key,
			[RosValue("hotsync-2")]
			Hotsync2,
			[RosValue("hotsync-1")]
			Hotsync1,
			[RosValue("pwdgen")]
			Pwdgen,
			[RosValue("pop2")]
			Pop2,
			[RosValue("pop3")]
			Pop3,
			[RosValue("pdap-np")]
			PdapNp,
			[RosValue("prospero-np")]
			ProsperoNp,
			[RosValue("qotd")]
			Qotd,
			[RosValue("radius")]
			Radius,
			[RosValue("radius-acct")]
			RadiusAcct,
			[RosValue("tcpmux")]
			Tcpmux,
			[RosValue("dixie")]
			Dixie,
			[RosValue("finger")]
			Finger,
			[RosValue("gopher")]
			Gopher,
			[RosValue("kerberos")]
			Kerberos,
			[RosValue("supdup")]
			Supdup,
			[RosValue("echo")]
			Echo,
			[RosValue("discard")]
			Discard,
			[RosValue("daytime")]
			Daytime,
			[RosValue("time")]
			Time,
			[RosValue("ripng")]
			Ripng,
			[RosValue("ms-rpc")]
			MsRpc,
			[RosValue("rsvp-tunnel")]
			RsvpTunnel,
			[RosValue("realsecure")]
			Realsecure,
			[RosValue("rtsp")]
			Rtsp,
			[RosValue("eppc")]
			Eppc,
			[RosValue("rwhois")]
			Rwhois,
			[RosValue("rje")]
			Rje,
			[RosValue("re-mail-ck")]
			ReMailCk,
			[RosValue("rlogin")]
			Rlogin,
			[RosValue("rwp")]
			Rwp,
			[RosValue("rlp")]
			Rlp,
			[RosValue("sip")]
			Sip,
			[RosValue("sql-net")]
			SqlNet,
			[RosValue("ssh")]
			Ssh,
			[RosValue("sunrpc")]
			Sunrpc,
			[RosValue("secureid")]
			Secureid,
			[RosValue("sift-uft")]
			SiftUft,
			[RosValue("sftp")]
			Sftp,
			[RosValue("snmp")]
			Snmp,
			[RosValue("snpp")]
			Snpp,
			[RosValue("smtp")]
			Smtp,
			[RosValue("statsrv")]
			Statsrv,
			[RosValue("t.120")]
			T120,
			[RosValue("timbuktu-srv1")]
			TimbuktuSrv1,
			[RosValue("timbuktu-srv2")]
			TimbuktuSrv2,
			[RosValue("timbuktu-srv3")]
			TimbuktuSrv3,
			[RosValue("timbuktu-srv4")]
			TimbuktuSrv4,
			[RosValue("tftp")]
			Tftp,
			[RosValue("uucp-path")]
			UucpPath,
			[RosValue("unreal")]
			Unreal,
			[RosValue("biff")]
			Biff,
			[RosValue("uls")]
			Uls,
			[RosValue("vemmi")]
			Vemmi,
			[RosValue("vnc-1")]
			Vnc1,
			[RosValue("vnc-2")]
			Vnc2,
			[RosValue("http-alt")]
			HttpAlt,
			[RosValue("nicname")]
			Nicname,
			[RosValue("whois++")]
			Whois,
			[RosValue("ms-streaming")]
			MsStreaming,
			[RosValue("msbd")]
			Msbd,
			[RosValue("http")]
			Http,
			[RosValue("xdmcp")]
			Xdmcp,
			[RosValue("x11")]
			X11,
			[RosValue("yahoo")]
			Yahoo,
			[RosValue("appleqtc")]
			Appleqtc,
			[RosValue("tcp-id-port")]
			TcpIdPort,
			[RosValue("distributed-net")]
			DistributedNet,
			[RosValue("doom")]
			Doom,
			[RosValue("https")]
			Https,
			[RosValue("imaps")]
			Imaps,
			[RosValue("ldaps")]
			Ldaps,
			[RosValue("nntps")]
			Nntps,
			[RosValue("tlisrv")]
			Tlisrv,
			[RosValue("pop3s")]
			Pop3s,
			[RosValue("linuxconf")]
			Linuxconf,
		}
		
		public struct DstAddressType
                {
                    public AddressType Address { get; set; }
                    public PortEnum Port { get; set; }

                    public static DstAddressType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new DstAddressType() { Address = TypeFormatter.ConvertFromString<AddressType>(strs[0], null), Port = TypeFormatter.ConvertFromString<PortEnum>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Port);
                    }
                }
		// IP destination address
		[RosValue("dst-address")]
		public DstAddressType? DstAddress { get; set; }
		
		[RosValue("dst-mac")]
		public MACAddress? DstMac { get; set; }
		
		// IP fragment offset
		[RosValue("fragment-offset")]
		public Int32? FragmentOffset { get; set; }
		
		// IP identification
		[RosValue("identification")]
		public Int32? Identification { get; set; }
		
		// Interface on which heard of this packet
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// The size of IP header
		[RosValue("ip-header-size")]
		public Int32? IpHeaderSize { get; set; }
		
		// The size of IP packet
		[RosValue("ip-packet-size")]
		public Int32? IpPacketSize { get; set; }
		
		public enum IpProtocolEnum
		{
			[RosValue("ddp")]
			Ddp,
			[RosValue("egp")]
			Egp,
			[RosValue("encap")]
			Encap,
			[RosValue("ggp")]
			Ggp,
			[RosValue("gre")]
			Gre,
			[RosValue("ipsec-ah")]
			IpsecAh,
			[RosValue("ipsec-esp")]
			IpsecEsp,
			[RosValue("hmp")]
			Hmp,
			[RosValue("idpr-cmtp")]
			IdprCmtp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("icmpv6")]
			Icmpv6,
			[RosValue("igmp")]
			Igmp,
			[RosValue("ipencap")]
			Ipencap,
			[RosValue("ipip")]
			Ipip,
			[RosValue("iso-tp4")]
			IsoTp4,
			[RosValue("ospf")]
			Ospf,
			[RosValue("pup")]
			Pup,
			[RosValue("pim")]
			Pim,
			[RosValue("rspf")]
			Rspf,
			[RosValue("rdp")]
			Rdp,
			[RosValue("st")]
			St,
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
			[RosValue("vmtp")]
			Vmtp,
			[RosValue("vrrp")]
			Vrrp,
			[RosValue("xns-idp")]
			XnsIdp,
			[RosValue("xtp")]
			Xtp,
		}
		
		public struct IpProtocolType 
                { 
                    private object value; 
                    public IpProtocolEnum? IpProtocol 
                { 
                    get { return value as IpProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpProtocolType(IpProtocolEnum? value) 
                { 
                    return new IpProtocolType() { value = value }; 
                }public IpProtocolEnum? IpProtocol2 
                { 
                    get { return value as IpProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpProtocolType(IpProtocolEnum? value) 
                { 
                    return new IpProtocolType() { value = value }; 
                } 
                    public static IpProtocolType Parse(string str, Connection conn)
                    {
                        
                try { return new IpProtocolType() { value = TypeFormatter.ConvertFromString<IpProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new IpProtocolType() { value = TypeFormatter.ConvertFromString<IpProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// The name/number of IP protocol
		[RosValue("ip-protocol")]
		public IpProtocolType? IpProtocol { get; set; }
		
		public enum MacProtocolEnum
		{
			[RosValue("arp")]
			Arp,
			[RosValue("ip")]
			Ip,
			[RosValue("ipv6")]
			Ipv6,
			[RosValue("ipx")]
			Ipx,
			[RosValue("pppoe")]
			Pppoe,
			[RosValue("rarp")]
			Rarp,
		}
		
		public struct ProtocolType 
                { 
                    private object value; 
                    public Byte? Protocol 
                { 
                    get { return value as Byte?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ProtocolType(Byte? value) 
                { 
                    return new ProtocolType() { value = value }; 
                }public MacProtocolEnum? MacProtocol 
                { 
                    get { return value as MacProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ProtocolType(MacProtocolEnum? value) 
                { 
                    return new ProtocolType() { value = value }; 
                } 
                    public static ProtocolType Parse(string str, Connection conn)
                    {
                        
                try { return new ProtocolType() { value = TypeFormatter.ConvertFromString<Byte>(str, conn) }; }
                catch(Exception) { }
                try { return new ProtocolType() { value = TypeFormatter.ConvertFromString<MacProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// The name/number of ethernet protocol
		[RosValue("protocol")]
		public ProtocolType? Protocol { get; set; }
		
		// Size of packet
		[RosValue("size")]
		public Int32? Size { get; set; }
		
		public struct SrcAddressType
                {
                    public AddressType Address { get; set; }
                    public PortEnum Port { get; set; }

                    public static SrcAddressType Parse(string str)
                    {
                        string[] strs = str.Split(':');
                        if (strs.Length == 2)
                            return new SrcAddressType() { Address = TypeFormatter.ConvertFromString<AddressType>(strs[0], null), Port = TypeFormatter.ConvertFromString<PortEnum>(strs[1], null) };
                        else
                            throw new InvalidOperationException();
                    }

                    public override string ToString()
                    {
                        return String.Concat(Address, ':', Port);
                    }
                }
		// Source address
		[RosValue("src-address")]
		public SrcAddressType? SrcAddress { get; set; }
		
		[RosValue("src-mac")]
		public MACAddress? SrcMac { get; set; }
		
		public enum FlagEnum
		{
			[RosValue("ack")]
			Ack,
			[RosValue("cwr")]
			Cwr,
			[RosValue("ece")]
			Ece,
			[RosValue("fin")]
			Fin,
			[RosValue("psh")]
			Psh,
		}
		
		[RosValue("tcp-flags")]
		public FlagEnum[] TcpFlags { get; set; }
		
		// Time when packet arrived
		[RosValue("time")]
		public Single? Time { get; set; }
		
		// IP Time To Live
		[RosValue("ttl")]
		public Int32? Ttl { get; set; }
		
		[RosValue("vlan-id")]
		public Int32? VlanId { get; set; }
		
		[RosValue("vlan-priority")]
		public Int32? VlanPriority { get; set; }
		
	}
	[RosObject("tool sniffer protocol", CanSet=true, CanGet=true)]
	public class ToolSnifferProtocol : RosItemObject<ToolSnifferProtocol>
	{
		// Total number of data bytes
		[RosValue("bytes")]
		public Int32? Bytes { get; set; }
		
		public enum IpProtocolEnum
		{
			[RosValue("ddp")]
			Ddp,
			[RosValue("egp")]
			Egp,
			[RosValue("encap")]
			Encap,
			[RosValue("ggp")]
			Ggp,
			[RosValue("gre")]
			Gre,
			[RosValue("ipsec-ah")]
			IpsecAh,
			[RosValue("ipsec-esp")]
			IpsecEsp,
			[RosValue("hmp")]
			Hmp,
			[RosValue("idpr-cmtp")]
			IdprCmtp,
			[RosValue("icmp")]
			Icmp,
			[RosValue("icmpv6")]
			Icmpv6,
			[RosValue("igmp")]
			Igmp,
			[RosValue("ipencap")]
			Ipencap,
			[RosValue("ipip")]
			Ipip,
			[RosValue("iso-tp4")]
			IsoTp4,
			[RosValue("ospf")]
			Ospf,
			[RosValue("pup")]
			Pup,
			[RosValue("pim")]
			Pim,
			[RosValue("rspf")]
			Rspf,
			[RosValue("rdp")]
			Rdp,
			[RosValue("st")]
			St,
			[RosValue("tcp")]
			Tcp,
			[RosValue("udp")]
			Udp,
			[RosValue("vmtp")]
			Vmtp,
			[RosValue("vrrp")]
			Vrrp,
			[RosValue("xns-idp")]
			XnsIdp,
			[RosValue("xtp")]
			Xtp,
		}
		
		public struct IpProtocolType 
                { 
                    private object value; 
                    public IpProtocolEnum? IpProtocol 
                { 
                    get { return value as IpProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpProtocolType(IpProtocolEnum? value) 
                { 
                    return new IpProtocolType() { value = value }; 
                }public IpProtocolEnum? IpProtocol2 
                { 
                    get { return value as IpProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator IpProtocolType(IpProtocolEnum? value) 
                { 
                    return new IpProtocolType() { value = value }; 
                } 
                    public static IpProtocolType Parse(string str, Connection conn)
                    {
                        
                try { return new IpProtocolType() { value = TypeFormatter.ConvertFromString<IpProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                try { return new IpProtocolType() { value = TypeFormatter.ConvertFromString<IpProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// IP protocol
		[RosValue("ip-protocol")]
		public IpProtocolType? IpProtocol { get; set; }
		
		// The number of packets
		[RosValue("packets")]
		public Int32? Packets { get; set; }
		
		public enum PortEnum
		{
			[RosValue("acap")]
			Acap,
			[RosValue("activision")]
			Activision,
			[RosValue("afpovertcp")]
			Afpovertcp,
			[RosValue("agentx")]
			Agentx,
			[RosValue("aol")]
			Aol,
			[RosValue("tacacs")]
			Tacacs,
			[RosValue("link")]
			Link,
			[RosValue("arcp")]
			Arcp,
			[RosValue("systat")]
			Systat,
			[RosValue("apple-licman")]
			AppleLicman,
			[RosValue("appleugcontrol")]
			Appleugcontrol,
			[RosValue("asip-webadmin")]
			AsipWebadmin,
			[RosValue("asipregistry")]
			Asipregistry,
			[RosValue("asia")]
			Asia,
			[RosValue("aurp")]
			Aurp,
			[RosValue("auth")]
			Auth,
			[RosValue("bftp")]
			Bftp,
			[RosValue("bootpc")]
			Bootpc,
			[RosValue("bootps")]
			Bootps,
			[RosValue("bgp")]
			Bgp,
			[RosValue("bdp")]
			Bdp,
			[RosValue("chargen")]
			Chargen,
			[RosValue("cfdptkt")]
			Cfdptkt,
			[RosValue("cpq-wbem")]
			CpqWbem,
			[RosValue("epmap")]
			Epmap,
			[RosValue("dlsrpn")]
			Dlsrpn,
			[RosValue("dlswpn")]
			Dlswpn,
			[RosValue("dlsrap")]
			Dlsrap,
			[RosValue("squid")]
			Squid,
			[RosValue("rip")]
			Rip,
			[RosValue("dsp")]
			Dsp,
			[RosValue("dns")]
			Dns,
			[RosValue("esro-emsdp")]
			EsroEmsdp,
			[RosValue("esro-gen")]
			EsroGen,
			[RosValue("etftp")]
			Etftp,
			[RosValue("nfile")]
			Nfile,
			[RosValue("ftp")]
			Ftp,
			[RosValue("ftp-data")]
			FtpData,
			[RosValue("glimpse")]
			Glimpse,
			[RosValue("swat")]
			Swat,
			[RosValue("name")]
			Name,
			[RosValue("hsrp")]
			Hsrp,
			[RosValue("icpv2")]
			Icpv2,
			[RosValue("ircu")]
			Ircu,
			[RosValue("iso-tsap")]
			IsoTsap,
			[RosValue("poppassd")]
			Poppassd,
			[RosValue("imap3")]
			Imap3,
			[RosValue("isakmp")]
			Isakmp,
			[RosValue("ipp")]
			Ipp,
			[RosValue("irc")]
			Irc,
			[RosValue("rap")]
			Rap,
			[RosValue("ldap")]
			Ldap,
			[RosValue("lpr")]
			Lpr,
			[RosValue("matip-type-a")]
			MatipTypeA,
			[RosValue("matip-type-b")]
			MatipTypeB,
			[RosValue("mac-srvr-admin")]
			MacSrvrAdmin,
			[RosValue("mtp")]
			Mtp,
			[RosValue("csnet-ns")]
			CsnetNs,
			[RosValue("mgcp-callagent")]
			MgcpCallagent,
			[RosValue("mgcp-gateway")]
			MgcpGateway,
			[RosValue("smb")]
			Smb,
			[RosValue("ms-sql-m")]
			MsSqlM,
			[RosValue("ms-sql-s")]
			MsSqlS,
			[RosValue("mc-ftp")]
			McFtp,
			[RosValue("btserv")]
			Btserv,
			[RosValue("discovery")]
			Discovery,
			[RosValue("winbox-old-tls")]
			WinboxOldTls,
			[RosValue("winbox-old")]
			WinboxOld,
			[RosValue("mobileip")]
			Mobileip,
			[RosValue("netbios-dgm")]
			NetbiosDgm,
			[RosValue("netbios-ns")]
			NetbiosNs,
			[RosValue("netbios-ssn")]
			NetbiosSsn,
			[RosValue("hostname")]
			Hostname,
			[RosValue("rrp")]
			Rrp,
			[RosValue("net-assistant")]
			NetAssistant,
			[RosValue("mpp")]
			Mpp,
			[RosValue("nfs")]
			Nfs,
			[RosValue("nntp")]
			Nntp,
			[RosValue("ntp")]
			Ntp,
			[RosValue("nextstep")]
			Nextstep,
			[RosValue("odmr")]
			Odmr,
			[RosValue("sql*net")]
			SqlNet,
			[RosValue("pgp5-key")]
			Pgp5Key,
			[RosValue("hotsync-2")]
			Hotsync2,
			[RosValue("hotsync-1")]
			Hotsync1,
			[RosValue("pwdgen")]
			Pwdgen,
			[RosValue("pop2")]
			Pop2,
			[RosValue("pop3")]
			Pop3,
			[RosValue("pdap-np")]
			PdapNp,
			[RosValue("prospero-np")]
			ProsperoNp,
			[RosValue("qotd")]
			Qotd,
			[RosValue("radius")]
			Radius,
			[RosValue("radius-acct")]
			RadiusAcct,
			[RosValue("tcpmux")]
			Tcpmux,
			[RosValue("dixie")]
			Dixie,
			[RosValue("finger")]
			Finger,
			[RosValue("gopher")]
			Gopher,
			[RosValue("kerberos")]
			Kerberos,
			[RosValue("supdup")]
			Supdup,
			[RosValue("echo")]
			Echo,
			[RosValue("discard")]
			Discard,
			[RosValue("daytime")]
			Daytime,
			[RosValue("time")]
			Time,
			[RosValue("ripng")]
			Ripng,
			[RosValue("ms-rpc")]
			MsRpc,
			[RosValue("rsvp-tunnel")]
			RsvpTunnel,
			[RosValue("realsecure")]
			Realsecure,
			[RosValue("rtsp")]
			Rtsp,
			[RosValue("eppc")]
			Eppc,
			[RosValue("rwhois")]
			Rwhois,
			[RosValue("rje")]
			Rje,
			[RosValue("re-mail-ck")]
			ReMailCk,
			[RosValue("rlogin")]
			Rlogin,
			[RosValue("rwp")]
			Rwp,
			[RosValue("rlp")]
			Rlp,
			[RosValue("sip")]
			Sip,
			[RosValue("ssh")]
			Ssh,
			[RosValue("sunrpc")]
			Sunrpc,
			[RosValue("secureid")]
			Secureid,
			[RosValue("sift-uft")]
			SiftUft,
			[RosValue("sftp")]
			Sftp,
			[RosValue("snmp")]
			Snmp,
			[RosValue("snpp")]
			Snpp,
			[RosValue("smtp")]
			Smtp,
			[RosValue("statsrv")]
			Statsrv,
			[RosValue("t.120")]
			T120,
			[RosValue("timbuktu-srv1")]
			TimbuktuSrv1,
			[RosValue("timbuktu-srv2")]
			TimbuktuSrv2,
			[RosValue("timbuktu-srv3")]
			TimbuktuSrv3,
			[RosValue("timbuktu-srv4")]
			TimbuktuSrv4,
			[RosValue("tftp")]
			Tftp,
			[RosValue("uucp-path")]
			UucpPath,
			[RosValue("unreal")]
			Unreal,
			[RosValue("biff")]
			Biff,
			[RosValue("uls")]
			Uls,
			[RosValue("vemmi")]
			Vemmi,
			[RosValue("vnc-1")]
			Vnc1,
			[RosValue("vnc-2")]
			Vnc2,
			[RosValue("http-alt")]
			HttpAlt,
			[RosValue("nicname")]
			Nicname,
			[RosValue("whois++")]
			Whois,
			[RosValue("ms-streaming")]
			MsStreaming,
			[RosValue("msbd")]
			Msbd,
			[RosValue("http")]
			Http,
			[RosValue("xdmcp")]
			Xdmcp,
			[RosValue("x11")]
			X11,
			[RosValue("yahoo")]
			Yahoo,
			[RosValue("appleqtc")]
			Appleqtc,
			[RosValue("tcp-id-port")]
			TcpIdPort,
			[RosValue("distributed-net")]
			DistributedNet,
			[RosValue("doom")]
			Doom,
			[RosValue("https")]
			Https,
			[RosValue("imaps")]
			Imaps,
			[RosValue("ldaps")]
			Ldaps,
			[RosValue("nntps")]
			Nntps,
			[RosValue("tlisrv")]
			Tlisrv,
			[RosValue("pop3s")]
			Pop3s,
			[RosValue("linuxconf")]
			Linuxconf,
		}
		
		// The port of TCP/UDP protocol
		[RosValue("port")]
		public PortEnum? Port { get; set; }
		
		public enum MacProtocolEnum
		{
			[RosValue("arp")]
			Arp,
			[RosValue("ip")]
			Ip,
			[RosValue("ipv6")]
			Ipv6,
			[RosValue("ipx")]
			Ipx,
			[RosValue("pppoe")]
			Pppoe,
			[RosValue("rarp")]
			Rarp,
		}
		
		public struct ProtocolType 
                { 
                    private object value; 
                    public Byte? Protocol 
                { 
                    get { return value as Byte?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ProtocolType(Byte? value) 
                { 
                    return new ProtocolType() { value = value }; 
                }public MacProtocolEnum? MacProtocol 
                { 
                    get { return value as MacProtocolEnum?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator ProtocolType(MacProtocolEnum? value) 
                { 
                    return new ProtocolType() { value = value }; 
                } 
                    public static ProtocolType Parse(string str, Connection conn)
                    {
                        
                try { return new ProtocolType() { value = TypeFormatter.ConvertFromString<Byte>(str, conn) }; }
                catch(Exception) { }
                try { return new ProtocolType() { value = TypeFormatter.ConvertFromString<MacProtocolEnum>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// The name/number of the protocol
		[RosValue("protocol")]
		public ProtocolType? Protocol { get; set; }
		
		public struct ShareType
                {
                    public Single Value { get; set; }

                    public static ShareType Parse(string str)
                    {
                        if (str.EndsWith("%"))
                            str = str.Substring(0, str.Length - "%".Length);
                        return new Parser() { Value = TypeFormatter.ConvertFromString<Single>(str, null) };
                    }

                    public override string ToString()
                    {
                        return String.Concat(Value, "%");
                    }

                    public static implicit operator ShareType(Single value)
                    {
                        return new Parser() { Value = value };
                    }

                    public static implicit operator Single(ShareType parser)
                    {
                        return parser.Value;
                    }
                }
		// Specific type of traffic compared to all traffic in bytes
		[RosValue("share")]
		public ShareType? Share { get; set; }
		
	}
	[RosObject("tool traffic-monitor", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class ToolTrafficMonitor : RosItemObject<ToolTrafficMonitor>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Interface to monitor
		[RosValue("interface")]
		public RouterOS.Interface Interface { get; set; }
		
		// Name of the traffic monitor item
		[RosValue("name")]
		public String Name { get; set; }
		
		// Script source
		[RosValue("on-event")]
		public String OnEvent { get; set; }
		
		// Traffic threshold, in bits per second
		[RosValue("threshold")]
		public Int32? Threshold { get; set; }
		
		public enum TrafficEnum
		{
			[RosValue("received")]
			Received,
			[RosValue("transmitted")]
			Transmitted,
		}
		
		// Type of traffic to monitor
		[RosValue("traffic")]
		public TrafficEnum? Traffic { get; set; }
		
		public enum TriggerEnum
		{
			[RosValue("above")]
			Above,
			[RosValue("always")]
			Always,
			[RosValue("below")]
			Below,
		}
		
		// Condition on which to execute script
		[RosValue("trigger")]
		public TriggerEnum? Trigger { get; set; }
		
		[RosValue("invalid", IsDynamic=true)]
		public String Invalid { get; set; }
		
		public static ToolTrafficMonitor Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			ToolTrafficMonitor obj = new ToolTrafficMonitor();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("user", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class User : RosItemObject<User>
	{
		public struct AddressType 
                { 
                    private object value; 
                    public IPPrefix? Address 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPPrefix? value) 
                { 
                    return new AddressType() { value = value }; 
                }public IPPrefix? Address2 
                { 
                    get { return value as IPPrefix?; } 
                    set { this.value = value; } 
                } 
                public static implicit operator AddressType(IPPrefix? value) 
                { 
                    return new AddressType() { value = value }; 
                } 
                    public static AddressType Parse(string str, Connection conn)
                    {
                        
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                try { return new AddressType() { value = TypeFormatter.ConvertFromString<IPPrefix>(str, conn) }; }
                catch(Exception) { }
                        throw new NotSupportedException();
                    }
                    public override string ToString() { return value != null ? value.ToString() : null; } 
                }
		// Network address part of addresses user is allowed to use
		[RosValue("address")]
		public AddressType[] Address { get; set; }
		
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// Defines whether item is ignored or used
		[RosValue("disabled")]
		public Boolean? Disabled { get; set; }
		
		// Group management
		[RosValue("group")]
		public RouterOS.UserGroup Group { get; set; }
		
		// User name
		[RosValue("name")]
		public String Name { get; set; }
		
		// User password
		[RosValue("password")]
		public String Password { get; set; }
		
		public static User AdminUser
		{
			get { return new User() { Id = 1 }; }
		}
		
		public static User Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			User obj = new User();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("user aaa", CanSet=true, CanGet=true)]
	public class UserAaa : RosValueObject
	{
		// Status of aaa (yes/no)
		[RosValue("accounting")]
		public Boolean? Accounting { get; set; }
		
		// Set type of the default group
		[RosValue("default-group")]
		public RouterOS.UserGroup DefaultGroup { get; set; }
		
		[RosValue("exclude-groups")]
		public RouterOS.UserGroup[] ExcludeGroups { get; set; }
		
		// Defines time interval between communications with the router
		[RosValue("interim-update")]
		public TimeSpan? InterimUpdate { get; set; }
		
		// Use or not radius
		[RosValue("use-radius")]
		public Boolean? UseRadius { get; set; }
		
	}
	[RosObject("user active", CanGet=true)]
	public class UserActive : RosItemObject<UserActive>
	{
		[RosValue("address", IsDynamic=true)]
		public IPAddress Address { get; set; }
		
		[RosValue("group", IsDynamic=true)]
		public RouterOS.UserGroup Group { get; set; }
		
		[RosValue("name", IsDynamic=true)]
		public String Name { get; set; }
		
		[RosValue("radius", IsDynamic=true)]
		public String Radius { get; set; }
		
		public enum ViaType
		{
			[RosValue("api")]
			Api,
			[RosValue("bandwidth-test")]
			BandwidthTest,
			[RosValue("console")]
			Console,
			[RosValue("ftp")]
			Ftp,
			[RosValue("mac-telnet")]
			MacTelnet,
			[RosValue("ssh")]
			Ssh,
			[RosValue("telnet")]
			Telnet,
			[RosValue("unknown")]
			Unknown,
			[RosValue("web")]
			Web,
			[RosValue("winbox")]
			Winbox,
		}
		
		[RosValue("via", IsDynamic=true)]
		public ViaType? Via { get; set; }
		
		[RosValue("when", IsDynamic=true)]
		public DateTime? When { get; set; }
		
		public static UserActive Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			UserActive obj = new UserActive();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("user group", CanAdd=true, CanSet=true, CanGet=true, CanRemove=true)]
	public class UserGroup : RosItemObject<UserGroup>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		// New group name
		[RosValue("name")]
		public String Name { get; set; }
		
		// Group policy
		[RosValue("policy")]
		public String Policy { get; set; }
		
		public static UserGroup ReadGroup
		{
			get { return new UserGroup() { Id = 1 }; }
		}
		
		public static UserGroup WriteGroup
		{
			get { return new UserGroup() { Id = 2 }; }
		}
		
		public static UserGroup FullGroup
		{
			get { return new UserGroup() { Id = 3 }; }
		}
		
		public static UserGroup Parse(string str, Connection conn)
		{
			if(string.IsNullOrEmpty(str))
				return null;
			UserGroup obj = new UserGroup();
			obj.FindByName(conn, str);
			return obj;
		}
		
		public override string ToString() { return Name.ToString(); }
		
	}
	[RosObject("user ssh-keys", CanSet=true, CanGet=true, CanRemove=true)]
	public class UserSshKeys : RosItemObject<UserSshKeys>
	{
		// Short description of the item
		[RosValue("comment")]
		public String Comment { get; set; }
		
		[RosValue("user")]
		public RouterOS.User User { get; set; }
		
		[RosValue("key-owner", IsDynamic=true)]
		public String KeyOwner { get; set; }
		
	}
}
