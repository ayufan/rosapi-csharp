﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;

namespace RouterOS.DataTypes
{
#if ENABLE_REF
    public struct Ref<T> where T
    {
        private string m_Name;
        private object m_Object;
        private Connection m_Connection;

        public string Name
        {
            get
            {
                if (m_Object != null)
                    return TypeFormatter.ConvertToString(typeof(T), m_Object, m_Connection);
                return m_Name;
            }
            set
            {
                m_Name = value;
                m_Object = null;
                m_Connection = null;
            }
        }

        private object Object
        {
            get
            {
                if (m_Object == null)
                {
                    if (m_Connection == null || m_Name == null)
                        throw new InvalidOperationException();
                    m_Object = TypeFormatter.ConvertFromString(typeof(T), m_Name, m_Connection);
                }
                return m_Object;
            }
            set
            {
                m_Object = value;
                m_Connection = null;
                m_Name = null;
            }
        }

        public Connection Connection
        {
            get { return m_Connection; }
            internal set { m_Connection = value; }
        }

        public static Ref<T> Parse(string name, Connection conn)
        {
            Ref<T> r = new Ref<T>();
            r.m_Name = name;
            r.m_Connection = conn;
            return r;
        }

        public override string ToString()
        {
            return Name;
        }

        public static implicit operator Ref<T>(string name)
        {
            Ref<T> r = new Ref<T>();
            r.m_Name = name;
            return r;
        }

        public static implicit operator string(Ref<T> r)
        {
            return r.ToString();
        }

        public static implicit operator Ref<T>(T obj)
        {
            Ref<T> r = new Ref<T>();
            r.m_Object = obj;
            return r;
        }

        public static implicit operator T(Ref<T> r)
        {
            return (T)r.Object;
        }
    }
#endif
}
