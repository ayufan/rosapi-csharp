﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace RouterOS.DataTypes
{
    public struct MACAddress
    {
        private byte[] m_MacAddress;

        public byte[] Address
        {
            get { return m_MacAddress; }
            set
            {
                if (value != null && value.Length != 6)
                    throw new ArgumentException();
                m_MacAddress = value;
            }
        }

        public MACAddress(byte[] macAddress)
        {
            m_MacAddress = null;
            Address = macAddress;
        }

        public static MACAddress Parse(string macAddress)
        {
            string str = macAddress.Replace("-", "").Replace(":", "").ToLower();
            if (str.Length != 6 * 2)
                throw new ArgumentException();

            byte[] macAddressBytes = new byte[6];
            for (int i = 0; i < 6; i++)
                byte.TryParse(str.Substring(i * 2, 2), NumberStyles.HexNumber, null, out macAddressBytes[i]);
            return new MACAddress(macAddressBytes);
        }

        public override string ToString()
        {
            if(m_MacAddress == null)
                return "00:00:00:00:00:00";
            return String.Format("{1:X}:{2:X}:{3:X}:{4:X}:{5:X}:{6:X}", m_MacAddress);
        }
    }
}
