﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterOS.DataTypes
{
    public struct Bits
    {
        public const long Kilobit = 1000;
        public const long Megabit = 1000 * Kilobit;
        public const long Gigabit = 1000 * Megabit;
        public static readonly Bits Unlimited = new Bits(-1);

        public long Count { get; set; }
        public bool IsUnlimited { get { return Count == -1; } }

        public Bits(long bits)
            : this()
        {
            Count = bits;
        }

        public static Bits Parse(string str)
        {
            if (str == "unlimited")
            {
                return Unlimited;
            }
            else if (str.Contains("k"))
            {
                return new Bits(long.Parse(str.Split('k')[0]) * Kilobit);
            }
            else if (str.Contains("M"))
            {
                return new Bits( long.Parse(str.Split('M')[0]) * Megabit);
            }
            else if (str.Contains("G"))
            {
                return new Bits(long.Parse(str.Split('G')[0]) * Gigabit);
            }
            else
            {
                return new Bits(long.Parse(str));
            }
        }

        public override string ToString()
        {
            if (IsUnlimited)
                return "unlimited";
            if ((Count % Gigabit) == 0)
                return (Count / Gigabit) + "G";
            if ((Count % Megabit) == 0)
                return (Count / Megabit) + "M";
            if ((Count % Kilobit) == 0)
                return (Count / Kilobit) + "k";
            return Count.ToString();
        }
    }
}
