﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace RouterOS.DataTypes
{
    public struct IPPrefix
    {
        public IPAddress IP { get; set; }
        public int Mask { get; set; }

        public uint Network
        {
            get { return (uint)~(((1 << (32 - Mask)) - 1) << Mask); }
        }

        public IPPrefix(IPAddress ip, int mask)
            : this()
        {
            IP = ip;
            Mask = mask;

            if (Mask < 0 || Mask > 32)
                throw new ArgumentException();
        }

        public static IPPrefix Parse(string mask)
        {
            string[] ip = mask.Split(new char[] { '/' }, 2);
            return new IPPrefix(IPAddress.Parse(ip[0]), ip.Length == 2 ? int.Parse(ip[1]) : 32);
        }

        public override string ToString()
        {
            return String.Format("{0}/{1}", IP, Mask);
        }
    }
}
