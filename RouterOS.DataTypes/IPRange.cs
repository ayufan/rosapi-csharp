﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace RouterOS.DataTypes
{
    public struct IPRange
    {
        private IPAddress start, end;

        public IPAddress Start
        {
            get { return start; }
            set { start = value; if (end == null || end.Address < start.Address) end = value; }
        }

        public IPAddress End
        {
            get { return end; }
            set { end = value; if (start == null || end.Address < start.Address) start = value; }
        }

        public IPRange(IPAddress ip)
            : this()
        {
            start = ip;
            end = ip;
        }

        public static IPRange Parse(string str)
        {
            IPRange range = new IPRange();

            string[] ips = str.Split(new char[] { '-' }, 2);
            if (ips.Length == 1 || ips.Length == 2)
            {
                range.Start = IPAddress.Parse(ips[0]);
            }
            else
            {
                throw new ArgumentException("str");
            }
            if (ips.Length == 2)
            {
                range.End = IPAddress.Parse(ips[1]);
            }
            return range;
        }

        public override string ToString()
        {
            if (start == end)
                return start.ToString();
            else
                return String.Format("{0}-{1}", start, end);
        }
    }
}
