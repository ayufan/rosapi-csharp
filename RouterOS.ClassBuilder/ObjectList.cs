﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Tamir.SharpSsh;

namespace RouterOS.ClassBuilder
{
    [XmlRoot("objects")]
    public class ObjectList
    {
        [XmlElement("object")]
        public ObjectDefinition[] Objects { get; set; }

        public void BuildCode(StringBuilder sb, int level)
        {
            sb.WriteLine(level, "using System;");
            sb.WriteLine(level, "using System.Net;");
            sb.WriteLine(level, "using RouterOS.API;");
            sb.WriteLine(level, "using RouterOS.DataTypes;");
            sb.WriteLine(level, "using RouterOS.Serialization;");
            sb.WriteLine(level, "");

            sb.WriteLine(level, "namespace RouterOS");
            sb.WriteLine(level, "{{");

            foreach (ObjectDefinition objectInfo in Objects)
            {
                ObjectDefinition baseObject = Objects.FirstOrDefault(o => o.ClassScope == objectInfo.Namespace);
                if (baseObject != null)
                    continue;

                if (objectInfo.Namespace != "")
                {
                    sb.WriteLine(level + 1, "namespace {0}", objectInfo.Namespace);
                    sb.WriteLine(level + 1, "{{");
                    objectInfo.BuildCode(sb, level + 2, Objects);
                    sb.WriteLine(level + 1, "}}");
                }
                else
                {
                    objectInfo.BuildCode(sb, level + 1, Objects);
                }
            }

            sb.WriteLine(level, "}}");
        }

        private static string[] actionList = new string[] {
            "add",
            "append",
            "for",
            "if",
            "set",
            "move",
            "find",
            "remove",
            "unset",
            "listen",
            "enable",
            "disable",
            "comment",
            "print",
            "edit",
            "get",
            "import",
            "export",
            "foreach",
            "do",
            "monitor",
            "monitor-traffic",
            "without-paging",
            "as-value",
            "interval",
            "once",
            ".."
        };

        private void Crawl(RosSshStream shell, string scope, Dictionary<string, ObjectDefinition> objectDictionary)
        {
            var cmdList = shell.TypeDescription(scope);
            if (cmdList.ContainsKey("print") || cmdList.ContainsKey("set"))
            {
                Console.WriteLine("[TEST] {0}", scope);
                ObjectDefinition objectInfo;
                if (!objectDictionary.TryGetValue(scope, out objectInfo))
                    objectInfo = objectDictionary[scope] = new ObjectDefinition() { Scope = scope };
                objectInfo.BuildObject(shell);
                if (objectInfo.Variables.Length == 0)
                    objectDictionary.Remove(scope);
            }

            foreach (var cmd in cmdList)
            {
                if (actionList.Contains(cmd.Key))
                    continue;
                if (VariableDefinition.IsSpecialName(cmd.Key))
                    continue;
                Crawl(shell, scope != "" ? scope + " " + cmd.Key : cmd.Key, objectDictionary);
            }
        }

        public void Crawl(string host, string user, string password)
        {
            // get objects
            Dictionary<string, ObjectDefinition> objectDictionary;
            if (Objects == null)
                objectDictionary = new Dictionary<string, ObjectDefinition>();
            else
                objectDictionary = Objects.ToDictionary(o => o.Scope);

            // initialize shell
            var shell = new RosSshStream(host, user, password);

            // find all objects
            Crawl(shell, "", objectDictionary);
            shell.Close();

            // save objects
            Objects = objectDictionary.Values.ToArray();
        }
    }
}
