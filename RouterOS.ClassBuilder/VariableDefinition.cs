﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Diagnostics;
using Tamir.SharpSsh;

namespace RouterOS.ClassBuilder
{
    public class EnumValueDefinition
    {
        [XmlText]
        public string Name { get; set; }

        [XmlAttribute("description")]
        [DefaultValue("")]
        public string Description { get; set; }
    }

    public static class EnumTypeHelper
    {
        public static void BuildEnum(this IEnumerable<EnumValueDefinition> enumTypes, StringBuilder sb, int level, string name, bool alsoNegative)
        {
            sb.WriteLine(level, "public enum {0}", name.ConvertToName());
            sb.WriteLine(level, "{{");
            foreach (EnumValueDefinition enumType in enumTypes)
            {
                if (enumType.Name.EndsWith("...") || enumType.Name.Contains("&"))
                {
                    Trace.WriteLine("[INVALID_ENUM] {0}", enumType.Name);
                    continue;
                }
                sb.WriteLine(level + 1, "[RosValue(\"{0}\")]", enumType.Name.Replace("\"", ""));
                sb.WriteLine(level + 1, "{0},", enumType.Name.ConvertToName());
                if (alsoNegative)
                {
                    sb.WriteLine(level + 1, "[RosValue(\"!{0}\")]", enumType.Name.Replace("\"", ""));
                    sb.WriteLine(level + 1, "Not{0},", enumType.Name.ConvertToName());
                }
            }
            sb.WriteLine(level, "}}");
            sb.WriteLine(level, "");
        }

        public static IEnumerable<EnumValueDefinition> ToEnumTypes(this IDictionary<string, string> enumList)
        {
            return enumList.Select(kv => new EnumValueDefinition() { Name = kv.Key, Description = kv.Value }).ToArray();
        }
    }

    public class VariableDefinition
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("arch")]
        [DefaultValue(Arch.None)]
        public Arch Arch { get; set; }

        [XmlElement("type")]
        [DefaultValue("")]
        public string Type { get; set; }

        [XmlElement("enum")]
        public EnumValueDefinition[] Enums { get; set; }

        [XmlElement("typedescription")]
        [DefaultValue("")]
        public string TypeDescription { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlAttribute("key")]
        [DefaultValue(false)]
        public bool IsKey { get; set; }

        [XmlAttribute("required")]
        [DefaultValue(false)]
        public bool IsRequired { get; set; }

        [XmlAttribute("dynamic")]
        [DefaultValue(false)]
        public bool IsDynamic { get; set; }

        [XmlAttribute("unset")]
        [DefaultValue(false)]
        public bool CanUnset { get; set; }

        public string VarName
        {
            get
            {
                return Name.ConvertToName();
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public static bool IsSpecialName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return true;
            if (name[0] == '<') // ignore all variables starting from <
                return true;
            if (name[0] == char.ToUpper(name[0])) // ignore all variables starting from upper name
                return true;
            if (name[0] == '!')
                return true;
            if (name == "value-name")
                return true;
            return false;
        }

        public void DeduceVariable(IEnumerable<ObjectDefinition> allObjects)
        {
            if (TypeDescription == null)
            {
                return;
            }

            try
            {
                TypeDefinition defInfo = TypeDefinition.Parse(TypeDescription.ToLines());
                TypeInfo type = defInfo.BuildType(new StringBuilder(), 0, new SortedDictionary<string,bool>(), allObjects);
                Console.WriteLine("[OK: " + type + "] " + TypeDescription);
            }
            catch (Exception e)
            {
                Console.WriteLine("[FAIL] " + TypeDescription);
            }
        }

        internal void BuildVariable(RosSshStream shell, ObjectDefinition objectInfo, string method)
        {
            Arch |= shell.Arch;

            if (TypeDescription != null)
                return;

            // get type definition
            string[] lines = shell.PeekCommand(String.Format("{0} {1} {2}=?", objectInfo.Scope, method, Name));

            try
            {
                // try to parse type definition
                TypeDefinition defInfo = TypeDefinition.Parse(lines);

                if (defInfo != null)
                {
                    TypeDescription = string.Join("\n", lines);
                    return;
                }
            }
            catch (Exception)
            {
            }

            // get type enumeration
            lines = shell.PeekCommand(String.Format("{0} {1} {2}=\t\t\t\t", objectInfo.Scope, method, Name));
            lines = lines.SelectMany(line => line.ToTokens()).Distinct().ToArray();
            if (lines.Length == 0)
            {
                Console.WriteLine("[NO_TYPE] {0} : {1}", objectInfo.Scope, Name);
                return;
            }

            // is boolean?
            if (lines.Length == 2 && lines.Contains("yes") && lines.Contains("no"))
                Type = "Boolean";

            // special type
            else if (lines.Contains("!use-string"))
                Type = "String";

            // split to enumeration
            else
                Enums = lines.Select(line => new EnumValueDefinition() { Name = line }).ToArray();
            TypeDescription = null;
        }

        public void BuildCode(StringBuilder sb, int level, ObjectDefinition objectInfo, SortedDictionary<string, bool> typeList, IEnumerable<ObjectDefinition> allObjects)
        {
            // write type
            string type = "String";

            if (Enums != null)
            {
                ObjectDefinition objectTypeInfo = allObjects.FirstOrDefault(o => Enums.All(sv => o.Actual != null && o.Actual.FirstOrDefault(oa => oa.Name == sv.Name) != null));
                if (objectTypeInfo != null)
                {
                    type = objectTypeInfo.ClassScope;
                }
                else
                {
                    Enums.BuildEnum(sb, level, VarName + "Type", false);
                    type = VarName + "Type?";
                }
            }
            else if(TypeDescription != null)
            {
                try
                {
                    StringBuilder sb2 = new StringBuilder();
                    type = TypeDefinition.Parse(TypeDescription.ToLines()).BuildType(sb2, level, typeList, allObjects).ToString();
                    sb.Append(sb2);
                }
                catch (Exception)
                {
                }
            }

            // write description
            if (Description != null)
            {
                foreach (string line in Description.ToLines())
                    sb.WriteLine(level, "// {0}", line);
            }

            // write attribute            
            List<string> flags = new List<string>();
            flags.Add(String.Format("\"{0}\"", Name));
            if (IsDynamic)
                flags.Add("IsDynamic=true");
            if (IsRequired)
                flags.Add("IsRequired=true");
            if (IsKey)
                flags.Add("IsKey=true");
            if (CanUnset)
                flags.Add("CanUnset=true");
            sb.WriteLine(level, "[RosValue({0})]", string.Join(", ", flags.ToArray()));

            // write variable
            sb.WriteLine(level, "public {0} {1} {{ get; set; }}", type, VarName == objectInfo.ClassName ? VarName + "Name" : VarName);
            sb.WriteLine(level, "");
        }
    }
}
