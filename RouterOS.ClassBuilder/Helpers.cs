﻿#define USE_READLINE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using Tamir.SharpSsh;
using System.Threading;

namespace RouterOS.ClassBuilder
{
    public static class Helpers
    {
        public static string[] SplitScope(this string name)
        {
            return name.Split(' ', '/').ToArray();
        }

        public static string ConvertToName(this string name)
        {
            string newName = string.Concat(name.Split(new char[] {'-', ' ', '/', '*', '.', ',', '+', '!', '"'}, StringSplitOptions.RemoveEmptyEntries).Select(str => char.ToUpper(str[0]) + str.Substring(1)).ToArray());
            if (char.IsDigit(newName[0]))
                newName = "E" + newName;
            return newName;
        }

        public static string[] ToLines(this string str)
        {
            return str.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static IEnumerable<string> ToTokens(this string str)
        {
            return str.ToTokens(true);
        }

        public static IEnumerable<string> ToTokens(this string str, bool removeEmpty)
        {
            char[] spaces = new char[] { ' ', '\t', '\r', '\n', '|' }; 
            int start = 0;
            while (start < str.Length)
            {
                // handle quotes
                if(str[start] == '"')
                {
                    // look for end quote
                    int end = str.IndexOf('"', start + 1);
                    if(end == -1)
                        throw new InvalidDataException("no maching end quote");
                    
                    // check token
                    int length = end - start - 1;
                    if (!removeEmpty || length != 0)
                        yield return str.Substring(start + 1, length);

                    // move to next string
                    start = end + 1;
                }
                
                // ignore whitespaces
                else if (!spaces.Contains(str[start]))
                {
                    // find next whitespace
                    int end = str.IndexOfAny(spaces, start + 1);
                    if (end == -1)
                        end = str.Length;

                    // return token
                    yield return str.Substring(start, end - start);

                    // move to next string
                    start = end + 1;
                }
                else
                {
                    ++start;
                }
            }
        }

        public static void Indent(this StringBuilder sb, int level)
        {
            sb.Append(new string('\t', level));
        }

        public static void WriteLine(this StringBuilder sb, int level, string format, params object[] args)
        {
            sb.Indent(level);
            sb.AppendLine(String.Format(format, args));
        }

        public static void WriteLine(this StringBuilder sb, int level, string[] lines)
        {
            foreach (string line in lines)
            {
                sb.Indent(level);
                sb.AppendLine(line);
            }
        }

        public static void WriteLine(this StringBuilder sb, int level, StringBuilder otherSb)
        {
            foreach (string line in otherSb.ToString().ToLines())
            {
                sb.Indent(level);
                sb.AppendLine(line);
            }
        }
    }
}
