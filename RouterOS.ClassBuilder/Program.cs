﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Tamir.SharpSsh;
using Microsoft.Test.CommandLineParsing;

namespace RouterOS.ClassBuilder
{
    class Program
    {
        public string XmlFile { get; set; }
        public string CodeFile { get; set; }
        public string XHostName { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        Program()
        {
            UserName = "admin";
            Password = "";
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.ParseArguments(args);

#if !USE_TRACE
            Trace.Listeners.Clear();
#endif

            // read from file
            XmlSerializer serializer = new XmlSerializer(typeof(ObjectList));
            ObjectList objectList = new ObjectList();

            // deserialize objects
            if (program.XmlFile != null)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(program.XmlFile))
                    {
                        objectList = (ObjectList)serializer.Deserialize(sr);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            if (program.HostName != null || program.XHostName != null)
            {
                if(program.HostName !=null || objectList.Objects == null || objectList.Objects.Length == 0)
                    objectList.Crawl(program.HostName != null ? program.HostName : program.XHostName, program.UserName + "+cte500w", program.Password);

                if(program.XmlFile != null)
                {
                    serializer.Serialize(new StreamWriter(program.XmlFile), objectList);
                }
            }

#if XX
            // try to deduce types
            foreach (var objectInfo in objectList.Objects)
            {
                foreach (var varInfo in objectInfo.Variables)
                {
                    varInfo.DeduceVariable(objectList.Objects);
                }
            }
#endif

            if (program.CodeFile != null)
            {
                StringBuilder sb = new StringBuilder();
                objectList.BuildCode(sb, 0); 
                File.WriteAllText(program.CodeFile, sb.ToString());
            }
        }
    }
}
