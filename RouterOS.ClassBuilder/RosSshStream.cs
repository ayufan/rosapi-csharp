﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO;
using Tamir.SharpSsh;

namespace RouterOS.ClassBuilder
{
    [Flags]
    public enum Arch
    {
        None = 0,
        x86 = 1,
        mipsle = 2,
        mispbe = 4,
        powerpc = 8
    }

    public class RosSshStream : SshStream
    {
        public string Version { get; private set; }
        public Arch Arch { get; private set; }

        public RosSshStream(string host, string username, string password)
            : base(host, username, password)
        {
            // initialize shell
            while (!ReadLine().Contains("Please press \"Enter\" to continue!")) ;
            WriteLine("");
            ReadByte();

            // get version
            string[] results = ExecuteCommand(":put [system resource get version]; :put [system resource get architecture-name];");
            Version = results[0];
            Arch = (Arch)Enum.Parse(typeof(Arch), results[1]);
        }

        public static readonly Regex PromptRegex = new Regex(@"\[[A-Za-z0-9_-]*@[A-Z0-9a-z_-]*\] \>");

        private string m_lineBuffer = "";

        public void WriteLine(string format)
        {
            Write(format + "\r\n");
        }

        public string ReadLine()
        {
            do
            {
                // try to find line
                int eol = m_lineBuffer.IndexOf('\n');
                if (eol >= 0)
                {
                    // get line
                    string line = m_lineBuffer.Substring(0, eol);
                    if (RemoveTerminalEmulationCharacters)
                        line = SshShell.HandleTerminalChars(line);

                    // shift buffer
                    m_lineBuffer = m_lineBuffer.Substring(eol + 1);
                    return line;
                }

                // get data from stream and remove carriage return
                byte[] buffer = new byte[1000];
                int length = Read(buffer);
                m_lineBuffer += Encoding.ASCII.GetString(buffer, 0, length).Replace("\r", "");
            }
            while (true);
        }

        public string[] ReadLines(string ignoreText, bool ignoreEmpty)
        {
            // generate & send synctask
            string syncTaskName = "sync-task-" + DateTime.Now.Ticks;
            WriteLine(String.Format(":put (\"{0}\")", syncTaskName));

            List<string> lines = new List<string>();
            while (true)
            {
                string line = ReadLine();
                if (line == syncTaskName)
                    break;
                //if (line.Contains(syncTaskName))
                //    continue;
                if (PromptRegex.IsMatch(line))
                    continue;
                //if (ignoreText.Contains(line))
                //    continue;
                if (ignoreEmpty && line.Length == 0)
                    continue;
                Trace.WriteLine(">> " + line);
                lines.Add(line);
            }
            return lines.ToArray();
        }

        public string[] ReadLines()
        {
            return ReadLines("", true);
        }

        public string[] PeekCommand(string cmd)
        {
            // push command with backspaces
            Trace.WriteLine("<< " + cmd);
            Write(cmd + new string('\b', 200));
            return ReadLines(cmd, true);
        }

        public string[] ExecuteCommand(string cmd)
        {
            // write command line
            Trace.WriteLine("<< " + cmd);
            WriteLine(cmd);
            return ReadLines(cmd, true);
        }
    }

    internal static class RosSshStreamHelpers
    {
        internal static IEnumerable<string> AllNames(this RosSshStream shell, string context)
        {
            string[] names = shell.PeekCommand(context + "\t\t\t\t");
            return names.SelectMany(p => 
                p.ToTokens().SelectMany(q => 
                    q.EndsWith("...") ? shell.AllNames(context + q.Replace("...", "")) : new string[] { q })
                );
        }

        internal static Dictionary<string, string> TypeDescription(this RosSshStream shell, string scope)
        {
            return TypeDescription(shell, scope, false);
        }

        internal static Dictionary<string, string> ValueList(this RosSshStream shell, string scope, string value)
        {
            return TypeDescription(shell, String.Format(":foreach i in=[{0} find] do={{:put (\"$i -- \" . [{0} get $i {1}])}}", scope, value), true);
        }

        internal static Dictionary<string, string> ValueList(this RosSshStream shell, string scope, string key, string value)
        {
            return TypeDescription(shell, String.Format(":foreach i in=[{0} find] do={{:put ([{0} get $i {1}] . \" -- \" . [{0} get $i {2}])}}", scope, key, value), true);
        }

        internal static readonly Regex KeyValueRegex = new Regex(@"^([^ ]*) -- (.*)$"); // <name> - <description>

        private static Dictionary<string, string> TypeDescription(this RosSshStream shell, string scope, bool execute)
        {
            Dictionary<string, string> cmdList = new Dictionary<string, string>();
            IEnumerable<string> lines = execute ? shell.ExecuteCommand(scope) : shell.PeekCommand(scope + " ?");

            foreach (string line in lines)
            {
                // ignore empty and duplicates
                if (line == "" || scope.Contains(line))
                    continue;

                // ignore non matching - probably description
                if (!KeyValueRegex.IsMatch(line))
                    continue;

                Match match = KeyValueRegex.Match(line);
                // ignore '...'
                if (match.Groups[1].Value == "...")
                    continue;

                cmdList[match.Groups[1].Value] = match.Groups[2].Value;
            }
            return cmdList;
        }
    }
}
