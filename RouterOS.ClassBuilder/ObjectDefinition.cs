﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;
using Tamir.SharpSsh;
using System.Threading;

namespace RouterOS.ClassBuilder
{
    public enum ObjectType
    {
        Item,
        Value
    }

    public class PredefinedDefinition
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("arch")]
        [DefaultValue(Arch.None)]
        public Arch Arch { get; set; }

        public string VarName
        {
            get
            {
                return Name.ConvertToName();
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public void BuildCode(StringBuilder sb, int level, ObjectDefinition objectInfo)
        {
            string extraName = objectInfo.Scope.ToTokens().LastOrDefault();
            if (!string.IsNullOrEmpty(extraName))
                extraName = extraName.ConvertToName();
            string varName = VarName + extraName;
            sb.WriteLine(level, "public static {0} {1}", objectInfo.ClassName, varName, Id);
            sb.WriteLine(level, "{{");
            sb.WriteLine(level + 1, "get {{ return new {0}() {{ Id = {2} }}; }}", objectInfo.ClassName, VarName, Id);
            sb.WriteLine(level, "}}");
            sb.WriteLine(level, "");
        }
    }

    public class ObjectDefinition
    {
        [XmlAttribute("scope")]
        public string Scope { get; set; }

        [XmlAttribute("arch")]
        [DefaultValue(Arch.None)]
        public Arch Arch { get; set; }

        [XmlAttribute("add")]
        [DefaultValue(false)]
        public bool CanAdd { get; set; }

        [XmlAttribute("find")]
        [DefaultValue(false)]
        public bool CanFind { get; set; }

        [XmlAttribute("set")]
        [DefaultValue(false)]
        public bool CanSet { get; set; }

        [XmlAttribute("move")]
        [DefaultValue(false)]
        public bool CanMove { get; set; }

        [XmlAttribute("remove")]
        [DefaultValue(false)]
        public bool CanRemove { get; set; }

        [XmlAttribute("unset")]
        [DefaultValue(false)]
        public bool CanUnset { get; set; }

        [XmlAttribute("listen")]
        [DefaultValue(false)]
        public bool CanListen { get; set; }

        [XmlAttribute("enable")]
        [DefaultValue(false)]
        public bool CanEnable { get; set; }

        [XmlAttribute("disable")]
        [DefaultValue(false)]
        public bool CanDisable { get; set; }

        [XmlAttribute("comment")]
        [DefaultValue(false)]
        public bool CanComment { get; set; }

        [XmlAttribute("ref")]
        [DefaultValue(false)]
        public bool IsReferencable { get; set; }

        [XmlElement("variable")]
        public VariableDefinition[] Variables { get; set; }

        [XmlElement("predefined")]
        public PredefinedDefinition[] Predefined { get; set; }

        [XmlElement("actual")]
        public PredefinedDefinition[] Actual { get; set; }

        public ObjectType Type 
        {
            get { return CanFind ? ObjectType.Item : ObjectType.Value; }
        }

        public string ClassScope
        {
            get
            {
                return "RouterOS." + ClassName;
            }
        }

        public string Namespace
        {
            get
            {
                return "";
            }
        }

        public string ClassName
        {
            get
            {
                return Scope.ConvertToName();
            }
        }

        public ObjectDefinition()
        {
            Variables = new VariableDefinition[0];
            Predefined = new PredefinedDefinition[0];
        }

        public override string ToString()
        {
            return Scope;
        }

        internal void BuildObject(RosSshStream shell)
        {
            var cmdList = shell.TypeDescription(Scope);

            // build object
            Arch |= shell.Arch;
            CanAdd = cmdList.ContainsKey("add");
            CanSet = cmdList.ContainsKey("set");
            CanMove = cmdList.ContainsKey("move");
            CanFind = cmdList.ContainsKey("find");
            CanRemove = cmdList.ContainsKey("remove");
            CanUnset = cmdList.ContainsKey("unset");
            CanListen = cmdList.ContainsKey("listen");
            CanEnable = cmdList.ContainsKey("enable");
            CanDisable = cmdList.ContainsKey("disable");
            CanComment = cmdList.ContainsKey("comment");

            // build list of variables
            Dictionary<string, VariableDefinition> varList;
            if (Variables != null)
                varList = Variables.ToDictionary(v => v.ToString());
            else
                varList = new Dictionary<string, VariableDefinition>();

            // get possible print commands
            var printCmdList = shell.AllNames(Scope + " print ");

            if (CanSet)
            {
                // get static variables
                string cmdNameSet;
                if (CanFind)
                    cmdNameSet = Scope + " set 0 ";
                else
                    cmdNameSet = Scope + " set ";
                var staticVarList = shell.TypeDescription(cmdNameSet);

                // add static variables
                foreach (var varName in staticVarList)
                {
                    if (VariableDefinition.IsSpecialName(varName.Key))
                        continue;
                    VariableDefinition varInfo;
                    if (!varList.TryGetValue(varName.Key, out varInfo))
                        varInfo = varList[varName.Key] = new VariableDefinition() { Name = varName.Key, Description = varName.Value };
                    varInfo.BuildVariable(shell, this, CanFind ? "set 0" : "set");
                }
            }

            if (CanUnset)
            {
                // get static variables
                string cmdNameSet;
                if (CanFind)
                    cmdNameSet = Scope + " unset 0 ";
                else
                    cmdNameSet = Scope + " unset ";
                var staticVarList = shell.AllNames(cmdNameSet);

                // add static variables
                foreach (var varName in staticVarList)
                {
                    if (VariableDefinition.IsSpecialName(varName))
                        continue;
                    VariableDefinition varInfo;
                    if (!varList.TryGetValue(varName, out varInfo))
                        varInfo = varList[varName] = new VariableDefinition() { Name = varName };
                    varInfo.CanUnset = true;
                    varInfo.BuildVariable(shell, this, CanFind ? "set 0" : "set");
                }
            }

            if (printCmdList.Contains("where"))
            {
                // get dynamic variables
                var dynamicVarList = shell.AllNames(Scope + " print where ");

                // add dynamic variables
                foreach (var varName in dynamicVarList)
                {
                    if (varList.ContainsKey(varName))
                    {
                        varList[varName].BuildVariable(shell, this, "print where");
                        continue;
                    }
                    if (VariableDefinition.IsSpecialName(varName))
                        continue;
                    VariableDefinition varInfo;
                    if (!varList.TryGetValue(varName, out varInfo))
                        varInfo = varList[varName] = new VariableDefinition() { Name = varName };
                    varInfo.IsDynamic = true;
                    varInfo.BuildVariable(shell, this, "print where");
                }
            }

            if (varList.ContainsKey("name"))
            {
                var valueList = shell.ValueList(Scope, "name");
                Actual = valueList.Select(v => new PredefinedDefinition() { Id = int.Parse(v.Key.Replace("*", ""), System.Globalization.NumberStyles.HexNumber), Name = v.Value, Arch = Arch }).ToArray();
                if (CanSet)
                {
                    Predefined = Actual.Where(p => p.Name[0] != '!' && p.Id < 10).ToArray(); // mostly there are not more than 10 predefined objects
                }
            }

            Variables = varList.Values.ToArray();
        }

        public void BuildCode(StringBuilder sb, int level, IEnumerable<ObjectDefinition> allObjects)
        {
            // write attribute            
            List<string> flags = new List<string>();
            flags.Add(String.Format("\"{0}\"", Scope));
            if (CanAdd)
                flags.Add("CanAdd=true");
            if (CanSet)
                flags.Add("CanSet=true");
            if (CanMove)
                flags.Add("CanMove=true");
            if (CanFind || CanSet)
                flags.Add("CanGet=true");
            //if (CanFind)
            //    flags.Add("CanFind=true");
            if (CanRemove)
                flags.Add("CanRemove=true");
            if (CanUnset)
                flags.Add("CanUnset=true");
            if (CanListen)
                flags.Add("CanListen=true");
            sb.WriteLine(level, "[RosObject({0})]", string.Join(", ", flags.ToArray()));

            if (Type == ObjectType.Item)
                sb.WriteLine(level, "public class {0} : RosItemObject<{0}>", ClassName);
            else
                sb.WriteLine(level, "public class {0} : RosValueObject", ClassName);
            sb.WriteLine(level, "{{");

            // write subobjects
            foreach (ObjectDefinition objectInfo in allObjects.Where(o => o.Namespace == ClassScope))
            {
                objectInfo.BuildCode(sb, level + 1, allObjects);
                sb.WriteLine(level + 1, "");
            }

            // write variables
            if (Variables != null)
            {
                SortedDictionary<string, bool> typeList = new SortedDictionary<string, bool>();
                foreach (VariableDefinition varInfo in Variables)
                    varInfo.BuildCode(sb, level + 1, this, typeList, allObjects);
            }

            // write predefined
            if (Predefined != null)
            {
                foreach (PredefinedDefinition preInfo in Predefined)
                    preInfo.BuildCode(sb, level + 1, this);
            }

            bool containsName = Variables != null && Variables.FirstOrDefault(v => v.Name == "name") != null;

            // write special functions
            if (containsName && CanFind)
            {
                sb.WriteLine(level + 1, "public static {0} Parse(string str, Connection conn)", ClassName);
                sb.WriteLine(level + 1, "{{");
                sb.WriteLine(level + 2, "if(string.IsNullOrEmpty(str))");
                sb.WriteLine(level + 3, "return null;");
                sb.WriteLine(level + 2, "{0} obj = new {0}();", ClassName);
                sb.WriteLine(level + 2, "obj.FindByName(conn, str);");
                sb.WriteLine(level + 2, "return obj;");
                sb.WriteLine(level + 1, "}}");
                sb.WriteLine(level + 1, "");
            }

            if (containsName && CanFind)
            {
                sb.WriteLine(level + 1, "public override string ToString() {{ return Name.ToString(); }}");
                sb.WriteLine(level + 1, "");
            }

            sb.WriteLine(level, "}}");
        }
    }
}
