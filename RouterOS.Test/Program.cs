﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Net;
using RouterOS.DataTypes;
using RouterOS;

namespace RouterOS.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var conn = new RouterOS.API.Connection("home.ayufan.eu", "admin", "dupamaryna"))
            {
                var queueSimple = QueueSimple.GetAll(conn);
                var dhcpLeases = IpDhcpServerLease.GetAll(conn);
                var interfaces = Interface.GetAll(conn);

                IpService ftpService = IpService.FtpService;
                ftpService.Disabled = false;
                ftpService.Update(conn);
                ftpService.Disabled = true;
                ftpService.Update(conn);
            }
        }
    }
}
