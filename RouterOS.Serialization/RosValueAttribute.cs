﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using RouterOS.API;
using System.Runtime.Serialization;

namespace RouterOS.Serialization
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RosObjectAttribute : Attribute
    {
        public string[] Scope { get; internal set; }
        public bool CanAdd { get; set; }
        public bool CanGet { get; set; }
        public bool CanUnset { get; set; }
        public bool CanSet { get; set; }
        public bool CanRemove { get; set; }
        public bool CanDisable { get; set; }
        public bool CanEnable { get; set; }
        public bool CanMove { get; set; }
        public bool CanListen { get; set; }

        public RosObjectAttribute(string scope)
        {
            Scope = scope.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class RosValueAttribute : Attribute
    {
        public string Name { get; set; }

        public bool IsDynamic { get; set; }
        public bool IsKey { get; set; }
        public bool IsRequired { get; set; }
        public bool CanUnset { get; set; }

        public bool ReadOnly { get; set; }
        public string Default { get; set; }
        public Type Formatter { get; set; }

        public RosValueAttribute(string valueName)
        {
            Name = valueName;
        }

        public override bool Equals(object obj)
        {
            if (obj is RosValueAttribute)
                return Name.Equals(((RosValueAttribute)obj).Name);
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }

    public class RosValueMember
    {
        public MemberInfo Member { get; set; }
        public RosValueAttribute Attribute { get; set; }

        public string Name
        {
            get { return Attribute.Name; }
        }

        public string Default
        {
            get { return Attribute.Default; }
        }

        public Type GetMemberType()
        {
            if (Member is PropertyInfo)
            {
                PropertyInfo property = Member as PropertyInfo;
                return property.PropertyType;
            }
            else if (Member is FieldInfo)
            {
                FieldInfo field = Member as FieldInfo;
                return field.FieldType;
            }
            else
            {
                return null;
            }
        }

        public object GetValue(RosObject obj)
        {
            if (Member is PropertyInfo)
            {
                PropertyInfo property = Member as PropertyInfo;
                return property.GetValue(obj, null);
            }
            else if (Member is FieldInfo)
            {
                FieldInfo field = Member as FieldInfo;
                return field.GetValue(obj);
            }
            else
            {
                return null;
            }
        }

        public void SetValue(RosObject obj, object value)
        {
            if (Member is PropertyInfo)
            {
                PropertyInfo property = Member as PropertyInfo;
                property.SetValue(obj, value, null);
            }
            else if (Member is FieldInfo)
            {
                FieldInfo field = Member as FieldInfo;
                field.SetValue(obj, value);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public void SetValue(RosObject obj, string value, Connection connection)
        {
            SetValue(obj, GetObject(value, connection));
        }

        public string GetString(object value, Connection connection)
        {
            return TypeFormatter.ConvertToString(GetMemberType(), value, connection);
        }

        public object GetObject(string str, Connection connection)
        {
            return TypeFormatter.ConvertFromString(GetMemberType(), str, connection);
        }
    }
}
