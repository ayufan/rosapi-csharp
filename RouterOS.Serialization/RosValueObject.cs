﻿using System;
using System.Collections.Generic;
using System.Text;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public abstract class RosValueObject : RosObject
    {
        public void Get(Connection conn)
        {
            Record record = conn.ExecuteOneRecord(new GetAllCommand(Attributes.Scope));

            if (record != null)
            {
                record.CopyTo(this, conn);
            }
        }

        public void Save(Connection conn)
        {
            // try to update object
            if (SavedState.Count == 0)
            {
                if(Attributes.CanGet)
                    Get(conn);
            }

            // create command
            SetCommand command = new SetCommand(Attributes.Scope);

            // get changes
            foreach (var keyValue in GetChangedMembers())
                command.KeyValues[keyValue.Name] = keyValue.GetValue(this).ToString(conn);
            if (command.KeyValues.Count == 0)
                return;

            // execute command
            conn.ExecuteCommand(command);

            // save changes
            SaveChanges();
        }
    }
}
