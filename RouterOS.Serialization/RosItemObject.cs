﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public class RosItemObject : RosObject
    {
        [RosValue(".id")]
        public RosId? Id { get; protected set; }

        public Query[] BuildQuery()
        {
            List<Query> query = new List<Query>();
            foreach (var keyValue in GetMembers(true, false))
                query.Add(new EqualQuery(keyValue.Name, keyValue.GetValue(this).ToString()));
            return query.ToArray();
        }

        public Query[] BuildKeyQuery()
        {
            List<Query> query = new List<Query>();
            foreach (var member in GetKeyMembers())
                query.Add(new EqualQuery(member.Name, member.GetValue(this).ToString()));
            return query.ToArray();
        }

        public void Remove(Connection conn)
        {
            FindByKeys(conn);
            conn.ExecuteCommand(new RemoveCommand(Attributes.Scope, Id));
        }

        public void FindByQuery(Connection connection, string[] propList, Query[] query)
        {
            if (!Attributes.CanGet)
                throw new NotSupportedException();
            if (query != null && query.Length == 0)
                throw new InvalidOperationException();
            GetAllCommand command = new GetAllCommand(Attributes.Scope);
            if (query != null)
                command.Query.AddRange(query);
            if (propList != null)
                command.PropList.AddRange(propList);
            Response response = connection.ExecuteCommand(command);
            if (response.Records.Length == 0)
                throw new KeyNotFoundException();
            else if (response.Records.Length > 1)
                throw new OverflowException();
            response.Records[0].CopyTo(this, connection);
        }

        public void FindByQuery(Connection connection, params Query[] query)
        {
            FindByQuery(connection, null, query);
        }

        public void FindByName(Connection connection, string value)
        {
            FindByQuery(connection, null, new Query[] { new EqualQuery("name", value) });
        }

        public void FindByKeys(Connection conn)
        {
            if (Id != null)
                return;
            FindByQuery(conn, new string[] { ".id" }, BuildKeyQuery());
        }

        public void Get(Connection conn)
        {
            GetAllCommand command = new GetAllCommand(Attributes.Scope);

            // try to find item
            if (Id == null)
            {
                command.Query.AddRange(BuildKeyQuery());

                if (command.Query.Count == 0)
                    throw new Exception("Object doesn't contain any keys, so can't be updated!");
            }

            // got object of id
            else
            {
                command.Query.Add(new EqualQuery(".id", Id.ToString(conn)));
            }

            // save values
            Record record = conn.ExecuteOneRecord(command);
            if (record != null)
                record.CopyTo(this, conn);
        }

        protected void Add(Connection conn)
        {
            if (!Attributes.CanAdd)
                throw new InvalidOperationException("item can't be added!");

            AddCommand command = new AddCommand(Attributes.Scope);

            // build command
            foreach (var keyValue in GetMembers(false, false))
                command.KeyValues[keyValue.Name] = keyValue.GetValue(this).ToString(conn);

            // execute command
            Response response = conn.ExecuteCommand(command);

            // save object id
            if (Id == null && response.Ret != null)
                Id = RosId.Parse(response.Ret, conn);

            // apply changes to internal list
            SaveChanges();
        }

        protected void Set(Connection conn)
        {
            if (!Attributes.CanSet)
                throw new InvalidOperationException("item can't be set!");

            SetCommand setCommand = new SetCommand(Attributes.Scope);
            UnsetCommand unsetCommand = new UnsetCommand(Attributes.Scope);

            // try to find item
            if (Id == null)
            {
                FindByKeys(conn);
            }

            // build command
            foreach (var member in GetChangedMembers())
            {
                string value = member.GetValue(this).ToString(conn);

                if (member.Attribute.CanUnset)
                    if (value == member.Default)
                        unsetCommand.ValueNames.Add(member.Name);
                    else
                        value = null;
                else
                    setCommand.KeyValues[member.Name] = value;
            }

            // execute command
            if (Attributes.CanSet)
                conn.ExecuteCommand(setCommand);
            if (Attributes.CanUnset)
                conn.ExecuteCommand(unsetCommand);

            // save state
            SaveChanges();
        }

        public void Update(Connection conn)
        {
            if (Id == null)
            {
                bool hasEmptyKey = GetKeyMembers().Any(m => m.GetValue(this) == null);
                if (hasEmptyKey)
                {
                    Add(conn);
                    return;
                }
            }
            Set(conn);
        }
    }

    public class RosItemObject<T> : RosItemObject where T : RosItemObject<T>, new()
    {
        #region GetAll - Sync
        private static T[] GetRecords(Response response)
        {
            T[] objects = new T[response.Records.Length];
            for (int i = 0; i < response.Records.Length; ++i)
            {
                objects[i] = new T();
                response.Records[i].CopyTo(objects[i], response.Connection);
            }
            return objects;
        }

        public static T[] GetAll(Connection connection, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            GetAllCommand command = new GetAllCommand(objectAttribute.Scope, query);
            return GetRecords(connection.ExecuteCommand(command));
        }

        public static T[] GetAll(Connection connection, T queriedObject)
        {
            return GetAll(connection, queriedObject.BuildQuery());
        }
        #endregion

        #region GetAll - Async
        public delegate void GetAllCallback(RecordType type, T[] objects);

        private struct GetAllObject
        {
            public GetAllCallback Callback;
            public List<T> Objects;

            public void OnEvent(Connection conn, Record record)
            {
                if (record.Type == RecordType.Record)
                {
                    T obj = new T();
                    obj.Id = RosId.Parse(record.ItemId, conn);
                    record.CopyTo(obj, conn);
                    Objects.Add(obj);
                }
                else if (record.Type == RecordType.Done)
                {
                    Callback(record.Type, Objects.ToArray());
                }
                else
                {
                    Callback(record.Type, null);
                }
            }
        }
        
        public static object GetAll(Connection connection, GetAllCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            GetAllCommand command = new GetAllCommand(objectAttribute.Scope, query);

            GetAllObject getAll = new GetAllObject() { Callback = callback, Objects = new List<T>() };

            return connection.ExecuteCommand(command, new AsyncResponseCallback(getAll.OnEvent));
        }

        public static object GetAll(Connection connection, GetAllCallback callback, T queriedObject)
        {
            return GetAll(connection, callback, queriedObject.BuildQuery());
        }
        #endregion

        #region Listen - Async
        public delegate void ListenCallback(ListenResults result);

        private struct ListenObject
        {
            public ListenCallback Callback;

            public void OnEvent(Connection conn, Record record)
            {
                ListenResults results = new ListenResults();
                results.Connection = conn;
                results.Tag = record.Tag;
                results.Type = record.Type;

                if (record.Type == RecordType.Record)
                {
                    results.Id = RosId.Parse(record.ItemId, conn);
                    results.Record = new T();
                    record.CopyTo(results.Record, conn);
                }

                Callback(results);
            }
        }

        public struct ListenResults
        {
            public Connection Connection;
            public RecordType Type;
            public object Tag;
            public T Record;
            public RosId Id;
        }

        public static object Listen(Connection connection, ListenCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanListen)
                throw new NotSupportedException();

            ListenCommand command = new ListenCommand(objectAttribute.Scope);
            command.Query.AddRange(query);

            ListenObject listen = new ListenObject();
            listen.Callback = callback;

            return connection.ExecuteCommand(command, new AsyncResponseCallback(listen.OnEvent));
        }

        public static object Listen(Connection connection, ListenCallback callback, T queriedObject)
        {
            return Listen(connection, callback, queriedObject.BuildQuery());
        }
        #endregion
    }
}
