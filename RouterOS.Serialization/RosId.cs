﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public struct RosId
    {
        public Int64 Index { get; internal set; }
        public Connection Connection { get; internal set; }

        public RosId(int index)
            : this()
        {
            Index = index;
        }

        public static RosId Parse(string value, Connection conn)
        {
            if (String.IsNullOrEmpty(value) || value[0] != '*')
                throw new ArgumentException();
            if (conn == null)
                throw new ArgumentException();
            return new RosId()
            {
                Index = Convert.ToInt64(value.Substring(1), 16),
                Connection = conn
            };
        }

        public override string ToString()
        {
            return "*" + Index.ToString("X");
        }

        public static implicit operator RosId(int value)
        {
            return new RosId() { Index = value };
        }
    }
}
