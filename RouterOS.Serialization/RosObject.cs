﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public class RosObject
    {
        private static Dictionary<Type, RosValueMember[]> m_members = new Dictionary<Type, RosValueMember[]>();
        private static Dictionary<Type, RosObjectAttribute> m_attributes = new Dictionary<Type, RosObjectAttribute>();
        private Dictionary<RosValueMember, object> m_savedState = new Dictionary<RosValueMember, object>();

        public RosObject()
        {
        }

        public IDictionary<RosValueMember, object> SavedState
        {
            get { return m_savedState; }
        }

        public RosObjectAttribute Attributes
        {
            get { return GetObjectAttribute(GetType()); }
        }

        public static RosObjectAttribute GetObjectAttribute(Type type)
        {
            RosObjectAttribute attribute;
            if (m_attributes.TryGetValue(type, out attribute))
                return attribute;

            object[] objectAttribute = type.GetCustomAttributes(typeof(RosObjectAttribute), false);
            if (objectAttribute.Length == 0)
                throw new NotSupportedException("Class not supported: " + type.ToString());

            attribute = (RosObjectAttribute)objectAttribute[0];
            m_attributes.Add(type, attribute);
            return attribute;
        }
        
        private static RosValueAttribute GetValueAttribute(MemberInfo member)
        {
            object[] serializableAttribute = member.GetCustomAttributes(typeof(RosValueAttribute), true);
            if (serializableAttribute.Length != 0)
                return ((RosValueAttribute)serializableAttribute[0]);
            return null;
        }

        public IEnumerable<RosValueMember> GetMembers()
        {
            RosValueMember[] retValue;
            Type type = GetType();

            if (m_members.TryGetValue(type, out retValue))
                return retValue;

            List<RosValueMember> members = new List<RosValueMember>();

            foreach (var member in type.FindMembers(MemberTypes.Field | MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null))
            {
                RosValueAttribute serializableAttribute = GetValueAttribute(member);
                if (serializableAttribute == null)
                    continue;
                members.Add(new RosValueMember() { Member = member, Attribute = serializableAttribute });
            }

            retValue = members.ToArray();
            m_members[type] = retValue;
            return retValue;
        }

        public IEnumerable<RosValueMember> GetMembers(bool dynamic, bool undefined)
        {
            return GetMembers().Where(vm =>
                (dynamic || !vm.Attribute.IsDynamic) &&
                (undefined || vm.GetValue(this) != null));
        }

        public IEnumerable<RosValueMember> GetKeyMembers()
        {
            return GetMembers().Where(vm => vm.Attribute.IsKey);
        }

        public IEnumerable<RosValueMember> GetRequiredMembers()
        {
            return GetMembers().Where(vm => vm.Attribute.IsRequired);
        }

        public IEnumerable<RosValueMember> GetChangedMembers()
        {
            return GetMembers().Where(vm => 
                !vm.Attribute.IsDynamic && vm.GetValue(this) != null && SavedState.ContainsKey(vm) && SavedState[vm] != vm.GetValue(this));
        }

        public void SaveChanges()
        {
            foreach (var member in GetMembers())
            {
                object value = member.GetValue(this);
                if (value != null)
                    SavedState[member] = value;
            }
        }
    }

    public static class RosSerializationHelper
    {
        public static void CopyTo(this Record record, RosObject obj, Connection conn)
        {
            foreach (var member in obj.GetMembers())
            {
                // get value from record or get default
                string value;
                if (!record.KeyValues.TryGetValue(member.Attribute.Name, out value))
                {
                    value = member.Default;
                    if (value == null)
                        continue;
                }

                // set value in class
                object valueT = member.GetObject(value, conn);
                member.SetValue(obj, valueT);
                obj.SavedState[member] = valueT;
            }
        }
    }
}
