﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Net;
using System.Globalization;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public interface ITypeFormatter
    {
        object ConvertFromString(string value, Connection connection);
        string ConvertToString(Object value, Connection connection);
    }

    public static class TypeFormatter
    {
        static Dictionary<Type, ITypeFormatter> FormatterDict = new Dictionary<Type, ITypeFormatter>();

        static TypeFormatter()
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => typeof(ITypeFormatter).IsAssignableFrom(p));
            foreach (var type in types)
            {
                TypeFormatterAttribute[] attrs = (TypeFormatterAttribute[])type.GetCustomAttributes(typeof(TypeFormatterAttribute), false);
                foreach(var attr in attrs)
                {
                    FormatterDict[attr.FormattedType] = (ITypeFormatter)Activator.CreateInstance(type);
                }
            }
        }

        public static ITypeFormatter GetFormatterForType(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                System.ComponentModel.NullableConverter nc = new System.ComponentModel.NullableConverter(type);
                type = nc.UnderlyingType;
            }

            ITypeFormatter formatter;
            if (FormatterDict.TryGetValue(type, out formatter))
                return formatter;

            if (type.IsEnum)
            {
                formatter = new EnumTypeFormatter(type);
            }
            else if (type.IsArray)
            {
                formatter = new ArrayTypeFormatter(type.GetElementType());
            }
            else
            {
                formatter = new GenericTypeFormatter(type);
            }
            FormatterDict.Add(type, formatter);
            return formatter;
        }

        public static object ConvertFromString(Type type, string value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(type);
            return typeFormatter.ConvertFromString(value, connection);
        }

        public static string ToString(this Object value, Connection connection)
        {
            if (value == null)
                return null;
            ITypeFormatter typeFormatter = GetFormatterForType(value.GetType());
            return typeFormatter.ConvertToString(value, connection);
        }

        public static string ConvertToString(Type type, Object value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(type);
            return typeFormatter.ConvertToString(value, connection);
        }

        public static T ConvertFromString<T>(string value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(typeof(T));
            return (T)typeFormatter.ConvertFromString(value, connection);
        }

        public static string ConvertToString<T>(T value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(typeof(T));
            return typeFormatter.ConvertToString(value, connection);
        }
    }
    
    [AttributeUsage(AttributeTargets.Class)]
    public class TypeFormatterAttribute : Attribute
    {
        public Type FormattedType { get; set; }

        public TypeFormatterAttribute(Type formattedType)
        {
            FormattedType = formattedType;
        }
    }

    public class EnumTypeFormatter : ITypeFormatter
    {
        private Type Type { get; set; }
        private Dictionary<string, object> From { get; set; }
        private Dictionary<object, string> To { get; set; }

        public EnumTypeFormatter(Type type)
        {
            Type = type;
            From = new Dictionary<string, object>();
            To = new Dictionary<object, string>();

            foreach (var fi in Type.GetFields())
            {
                RosValueAttribute[] attrs = (RosValueAttribute[])fi.GetCustomAttributes(typeof(RosValueAttribute), false);
                if (attrs.Length > 0)
                {
                    string e = attrs[0].Name;
                    object o = Enum.Parse(Type, fi.Name);
                    From[e] = o;
                    To[o] = e;
                }
            }
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return From[value];
        }

        public string ConvertToString(object value, Connection connection)
        {
            return To[value];
        }
    }

    public class ArrayTypeFormatter : ITypeFormatter
    {
        private Type Type { get; set; }

        public ArrayTypeFormatter(Type type)
        {
            Type = type;
        }

        public object ConvertFromString(string value, Connection connection)
        {
            string[] strs = value.Split(',');
            Array objs = Array.CreateInstance(Type, strs.Length);
            for (int i = 0; i < strs.Length; ++i)
                objs.SetValue(TypeFormatter.ConvertFromString(Type, strs[i], connection), i);
            return objs;
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is Array)
            {
                Array arr = (Array)value;
                string str = "";
                for (int i = 0; i < arr.Length; ++i)
                {
                    if (str != "")
                        str += ",";
                    str += TypeFormatter.ConvertToString(Type, arr.GetValue(i), connection);
                }
                return str;
            }
            throw new NotSupportedException();
        }
    }

    public class GenericTypeFormatter : ITypeFormatter
    {
        private Type Type { get; set; }
        private MethodInfo ParseMethod { get; set; }
        private bool ParseNoConn { get; set; }
        private MethodInfo ToStringMethod { get; set; }
        private bool ToStringNoConn { get; set; }

        public GenericTypeFormatter(Type type)
        {
            Type = type;
            ParseMethod = Type.GetMethod("Parse", new Type[] { typeof(string), typeof(Connection) });
            if (ParseMethod == null)
            {
                ParseMethod = Type.GetMethod("Parse", new Type[] { typeof(string) });
                if (ParseMethod == null)
                    throw new NotSupportedException();
                ParseNoConn = true;
            }
            if (!ParseMethod.IsStatic)
                throw new NotSupportedException();

            ToStringMethod = Type.GetMethod("ToString", new Type[] { typeof(Connection) });
            if (ToStringMethod == null)
            {
                ToStringMethod = Type.GetMethod("ToString", new Type[] { });
                if (ToStringMethod == null)
                    throw new NotSupportedException();
                ToStringNoConn = true;
            }
            if (ToStringMethod.IsStatic)
                throw new NotSupportedException();
        }

        public object ConvertFromString(string value, Connection connection)
        {
            if (ParseNoConn)
            {
                return ParseMethod.Invoke(null, new object[] { value });
            }
            else
            {
                object obj;
                if (!connection.GetObjectFromCache(Type, value, out obj))
                {
                    obj = ParseMethod.Invoke(null, new object[] { value, connection });
                    connection.AddObjectToCache(Type, value, obj);
                }
                return obj;
            }
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (ToStringNoConn)
                return (string)ToStringMethod.Invoke(value, new object[] { });
            else
                return (string)ToStringMethod.Invoke(value, new object[] { connection });
        }
    }

    [TypeFormatter(typeof(bool))]
    public class BooleanTypeFormatter : ITypeFormatter
    {
        public object ConvertFromString(string value, Connection connection)
        {
            if (value == "yes" || value == "true")
                return true;
            if (value == "no" || value == "false")
                return false;
            throw new ArgumentException();
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is bool)
            {
                return (bool)value ? "yes" : "no";
            }
            throw new ArgumentException();
        }
    }

    [TypeFormatter(typeof(string))]
    public class StringTypeFormatter : ITypeFormatter
    {
        public object ConvertFromString(string value, Connection connection)
        {
            return value;
        }

        public string ConvertToString(object value, Connection connection)
        {
            return value as string;
        }
    }

    [TypeFormatter(typeof(TimeSpan))]
    public class TimeSpanFormatter : ITypeFormatter
    {
        public virtual object ConvertFromString(string value, Connection connection)
        {
            int days = 0, hours = 0, minutes = 0, seconds = 0, milliseconds = 0;

            int weeksOffset = value.IndexOf('w');
            if (weeksOffset >= 0)
            {
                days += 7 * int.Parse(value.Substring(0, weeksOffset));
                value = value.Substring(weeksOffset + 1);
            }

            int daysOffset = value.IndexOf('d');
            if (daysOffset >= 0)
            {
                days += int.Parse(value.Substring(0, daysOffset));
                value = value.Substring(daysOffset + 1);
            }

            int hoursOffset = value.IndexOf('h');
            if (hoursOffset >= 0)
            {
                hours += int.Parse(value.Substring(0, hoursOffset));
                value = value.Substring(hoursOffset + 1);
            }

            int minutesOffset = value.IndexOf('h');
            if (minutesOffset >= 0)
            {
                minutes += int.Parse(value.Substring(0, minutesOffset));
                value = value.Substring(minutesOffset + 1);
            }

            int secondsOffset = value.IndexOf('h');
            if (secondsOffset >= 0)
            {
                seconds += int.Parse(value.Substring(0, secondsOffset));
                value = value.Substring(secondsOffset + 1);
            }

            if (hours == 0 && minutes == 0 && seconds == 0)
            {
                string[] dotsep = value.Split('.');
                string[] hms = dotsep[0].Split(':');

                if (hms.Length == 3)
                {
                    hours += int.Parse(hms[0]);
                    minutes += int.Parse(hms[1]);
                    seconds += int.Parse(hms[2]);
                }

                if (dotsep.Length > 1)
                    milliseconds += int.Parse(dotsep[1]);
            }

            return new TimeSpan(days, hours, minutes, seconds, milliseconds);
        }

        public virtual string ConvertToString(object value, Connection connection)
        {
            if (value is TimeSpan)
            {
                TimeSpan ts = (TimeSpan)value;
                string ret = "";

                if (ts.Days > 7)
                {
                    ret += (ts.Days / 7) + "w";
                }

                if ((ts.Days % 7) > 0)
                {
                    ret += (ts.Days % 7) + "d";
                }

                ret += String.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);

                if (ts.Milliseconds > 0)
                {
                    ret += "." + ts.Milliseconds;
                }
            }
            throw new NotSupportedException();
        }
    }

    public class TimeSpanNoneFormatter : TimeSpanFormatter
    {
        public override object ConvertFromString(string value, Connection connection)
        {
            if(value == "none")
                return TimeSpan.Zero;
            return base.ConvertFromString(value, connection);
        }

        public override string ConvertToString(object value, Connection connection)
        {
            if (value is TimeSpan)
            {
                TimeSpan ts = (TimeSpan)value;
                if (ts == TimeSpan.Zero)
                    return "none";
            }
            return base.ConvertToString(value, connection);
        }
    }


    [TypeFormatter(typeof(DateTime))]
    public class DateTimeFormatter : ITypeFormatter
    {
        private static DateTimeFormatInfo FormatInfo;

        static DateTimeFormatter()
        {
            FormatInfo = new DateTimeFormatInfo();
            FormatInfo.DateSeparator = "/";
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return DateTime.ParseExact(value, new string[] { "MMM/dd/yyyy HH:mm:ss", "MMM/dd HH:mm:ss", "HH:mm:ss" }, FormatInfo, DateTimeStyles.AllowWhiteSpaces);
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                if (dt.Year == 0)
                {
                    return dt.ToString("HH:mm:ss", FormatInfo);
                }
                else
                {
                    return dt.ToString("MMM/dd/yyyy HH:mm:ss", FormatInfo);
                }
            }
            throw new NotSupportedException();
        }
    }

    public enum VersionBuild : int
    {
        Alpha,
        Beta,
        Candidate,
        Regular
    }

    [TypeFormatter(typeof(Version))]
    public class VersionTypeFormatter : ITypeFormatter
    {
        public object ConvertFromString(string value, Connection connection)
        {
            if (value.Contains("rc"))
                value = value.Replace("rc", "." + ((int)VersionBuild.Candidate).ToString() + ".");
            else if (value.Contains("beta"))
                value = value.Replace("beta", "." + ((int)VersionBuild.Beta).ToString() + ".");
            else if (value.Contains("alpha"))
                value = value.Replace("alpha", "." + ((int)VersionBuild.Alpha).ToString() + ".");

            Version version = Version.Parse(value);

            if (version.Build != 0 && version.Revision == 0)
            {
                version = new Version(version.Major, version.Minor, (int)VersionBuild.Regular, version.Build);
            }

            return version;
        }

        public string ConvertToString(object value, Connection connection)
        {
            if(value is Version)
            {
                Version version = (Version)value;
                switch ((VersionBuild)version.Build)
                {
                    case VersionBuild.Regular:
                        if (version.Revision != 0)
                            return String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Revision);
                        else
                            return String.Format("{0}.{1}", version.Major, version.Minor);

                    case VersionBuild.Alpha:
                        return String.Format("{0}.{1}alpha{2}", version.Major, version.Minor, version.Revision);

                    case VersionBuild.Beta:
                        return String.Format("{0}.{1}beta{2}", version.Major, version.Minor, version.Revision);

                    case VersionBuild.Candidate:
                        return String.Format("{0}.{1}rc{2}", version.Major, version.Minor, version.Revision);
                }
            }
            throw new NotSupportedException();
        }
    }
}
