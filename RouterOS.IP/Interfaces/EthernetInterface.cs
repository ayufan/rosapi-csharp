﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using RouterOS.API;
using System.Net;

namespace RouterOS.Interfaces
{
    public class EthernetInterface : AddSetItemNode<EthernetInterface>, IInterface
    {
        [ValueName("name", IsKey=true)]
        public string Name { get; set; }

        [ValueName("arp")]
        public ARPMode? ARP { get; set; }

        [ValueName("speed")]
        public Bits? Speed { get; set; }
        
        [ValueName("bandwidth")]
        public DblValue<Bits,Bits>? Bandwidth { get; set; }

        [ValueName("auto-negotiation")]
        public bool? AutoNegotiation { get; set; }

        [ValueName("full-duplex")]
        public bool? FullDuplex { get; set; }

        [ValueName("mdix-enable", IsDynamic=true)]
        public bool? MdixEnable { get; set; }

        [ValueName("mac-address")]
        public MACAddress? MacAddress { get; set; }
        
        [ValueName("master-port")]
        public Ref<EthernetInterface>? MasterPort { get; set; }
        
        [ValueName("mtu", Default="1500")]
        public int? Mtu { get; set; }

        [ValueName("l2mtu", IsDynamic = true)]
        public int L2Mtu { get; set; }

        [ValueName("keepalive")]
        public int? KeepAlive { get; set; }

        [ValueName("local-address")]
        public IPAddress LocalAddress { get; set; }

        [ValueName("remote-address")]
        public IPAddress RemoteAddress { get; set; }

        [ValueName("comment", Default = "")]
        public string Comment { get; set; }

        public string Type
        {
            get { return "ether"; }
        }

        public override string[] APIScope
        {
            get { return new string[] { "interface", "ethernet" }; }
        }

        public EthernetInterface()
        {
        }

        public EthernetInterface(string name)
        {
            Name = name;
        }

        public EthernetInterface(IInterface @interface)
        {
            if (@interface.Type != Type)
                throw new InvalidCastException();
            GetMe(@interface.Connection, new EqualQuery("name", @interface.Name));
        }

        public static EthernetInterface Parse(string name, Connection conn)
        {
            if (name == null || name.Length == 0)
                return null;

            if (name == "none")
            {
                return new EthernetInterface(name);
            }
            else
            {
                EthernetInterface ei = new EthernetInterface();
                ei.GetMe(conn, new EqualQuery("name", name));
                return ei;
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public static EthernetInterface None { get; set; }

        static EthernetInterface()
        {
            None = new EthernetInterface("none");
        }
    }
}
