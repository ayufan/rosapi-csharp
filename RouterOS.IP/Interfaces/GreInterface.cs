﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using RouterOS.API;
using System.Net;

namespace RouterOS.Interfaces
{
    public class GreInterface : AddSetItemNode<GreInterface>, IInterface
    {
        [ValueName("name", IsKey=true)]
        public string Name { get; set; }

        [ValueName("arp")]
        public ARPMode? ARP { get; set; }
        
        [ValueName("mtu", Default="1500")]
        public int Mtu { get; set; }

        [ValueName("l2mtu", IsDynamic = true)]
        public int L2Mtu { get; set; }

        [ValueName("keepalive")]
        public int? KeepAlive { get; set; }

        [ValueName("local-address")]
        public IPAddress LocalAddress { get; set; }

        [ValueName("remote-address")]
        public IPAddress RemoteAddress { get; set; }

        [ValueName("comment", Default = "")]
        public string Comment { get; set; }

        public string Type
        {
            get { return "gre-tunnel"; }
        }

        public override string[] APIScope
        {
            get { return new string[] { "interface", "gre" }; }
        }

        public GreInterface()
        {
        }

        public GreInterface(IInterface @interface)
        {
            if (@interface.Type != Type)
                throw new InvalidCastException();
            GetMe(@interface.Connection, new EqualQuery("name", @interface.Name));
        }

        public static GreInterface Parse(string name, Connection conn)
        {
            if (name == null || name.Length == 0)
                return null;

            GreInterface gi = new GreInterface();
            gi.GetMe(conn, new EqualQuery("name", name));
            return gi;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
