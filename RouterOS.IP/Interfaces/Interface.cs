﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using RouterOS.API;

namespace RouterOS
{
    public enum ARPMode
    {
        [ValueName("disabled")]
        Disabled,

        [ValueName("enabled")]
        Enabled,

        [ValueName("proxy-arp")]
        ProxyArp,

        [ValueName("reply-only")]
        ReplyOnly
    }
}

namespace RouterOS.Interfaces
{
    public interface IInterface
    {
        ItemId? Id { get; }
        Connection Connection { get; }
        string Name { get; }
        string Type { get; }
    }

    public class Interface : SetItemNode<Interface>, IInterface
    {
        [ValueName("name", IsKey=true)]
        public string Name { get; set; }

        [ValueName("type", IsDynamic = true)]
        public string Type { get; set; }

        [ValueName("bytes", IsDynamic = true)]
        public DblValue<int,int>? Bytes { get; set; }

        [ValueName("packets", IsDynamic = true)]
        public DblValue<int,int>? Packets { get; set; }

        [ValueName("drops", IsDynamic = true)]
        public DblValue<int,int>? Drops { get; set; }

        [ValueName("errors", IsDynamic = true)]
        public DblValue<int,int>? Errors { get; set; }

        [ValueName("mtu")]
        public int Mtu { get; set; }

        [ValueName("l2mtu", IsDynamic = true)]
        public int L2Mtu { get; set; }

        [ValueName("dynamic", IsDynamic = true)]
        public bool? Dynamic { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        [ValueName("running", IsDynamic = true)]
        public bool? Running { get; set; }

        [ValueName("comment", Default = "")]
        public string Comment { get; set; }

        [ValueName("slave")]
        public bool? Slave { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "interface" }; }
        }

        public Interface()
        {
        }

        public Interface(IInterface @interface)
        {
            GetMe(@interface.Connection, new EqualQuery("name", @interface.Name));
        }

        public Interface(string name)
        {
            Name = name;
        }

        public static Interface Parse(string name, Connection conn)
        {
            if (name == null || name.Length == 0)
                return null;

            Interface i = new Interface();
            i.GetMe(conn, new EqualQuery("name", name));
            return i;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
