﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.Interfaces
{
    public enum BridgeProtocolMode
    {
        [ValueName("None")]
        None,

        [ValueName("STP")]
        STP,

        [ValueName("RSTP")]
        RSTP
    }

    public class BridgeInterface : AddSetItemNode<BridgeInterface>
    {
        [ValueName("name", IsKey = true)]
        public string Name { get; set; }

        [ValueName("arp")]
        public ARPMode? ARP { get; set; }

        [ValueName("admin-mac")]
        public MACAddress? AdminMacAddress { get; set; }

        [ValueName("mac-address", IsDynamic=true)]
        public MACAddress? MacAddress { get; set; }

        [ValueName("auto-mac")]
        public bool? AutoMacAddress { get; set; }

        [ValueName("mtu", Default = "1500")]
        public int? Mtu { get; set; }

        [ValueName("l2mtu", Default = "65536")]
        public int? L2Mtu { get; set; }

        [ValueName("l2mtu", IsDynamic = true)]
        public bool? Running { get; set; }

        [ValueName("comment", Default = "")]
        public string Comment { get; set; }

        [ValueName("ageing-time")]
        public TimeSpan? AgeingTime { get; set; }

        [ValueName("max-message-age")]
        public TimeSpan? MaxMessageAge { get; set; }

        [ValueName("forward-delay")]
        public TimeSpan? ForwardDelay { get; set; }

        [ValueName("priority")]
        public int? Priority { get; set; }

        [ValueName("transmit-hold-count")]
        public int? TransmitHoldCount { get; set; }

        [ValueName("protocol-mode")]
        public BridgeProtocolMode? ProtocolMode { get; set; }

        public BridgeInterface()
        {
        }

        public BridgeInterface(string name)
        {
            Name = name;
        }

        public BridgeInterface(IInterface @interface)
        {
            if (@interface.Type != Type)
                throw new InvalidCastException();
            GetMe(@interface.Connection, @interface.Name);
        }

        public static BridgeInterface Parse(string name, RouterOS.API.Connection conn)
        {
            if (name == null || name.Length == 0)
                return null;

            if (name == "none")
            {
                return new BridgeInterface(name);
            }
            else
            {
                BridgeInterface ei = new BridgeInterface();
                ei.GetMe(conn, name);
                return ei;
            }
        }

        public string Type
        {
            get { return "bridge"; }
        }

        public override string[] APIScope
        {
            get { return new string[] { "interface", "bridge" }; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
