﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using RouterOS.System;

namespace RouterOS
{
    public class Log : SetOrderItemNode<Log>
    {
        [ValueName("buffer")]
        public Ref<LoggingAction>? Buffer { get; set; }
        
        [ValueName("message")]
        public string Message { get; set; }

        [ValueName("time")]
        public DateTime? Time { get; set; }

        [ValueName("topics")]
        public ItemList<string> Topics { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "log" }; }
        }
    }
}
