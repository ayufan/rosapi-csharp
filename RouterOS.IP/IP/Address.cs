﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using RouterOS.DataTypes;
using RouterOS.Interfaces;

namespace RouterOS
{
    namespace IP
    {
        public sealed class Address : AddSetItemNode<Address>
        {
            [ValueName("address")]
            public IPPrefix IP { get; set; }

            [ValueName("network")]
            public IPAddress Network { get; set; }

            [ValueName("broadcast")]
            public IPAddress Broadcast { get; set; }

            [ValueName("interface")]
            public Ref<Interface>? Interface { get; set; }

            [ValueName("actual-interface", IsDynamic=true)]
            public Ref<Interface>? ActualInterface { get; set; }

            [ValueName("invalid", IsDynamic=true)]
            public bool? Invalid { get; set; }

            [ValueName("dynamic", IsDynamic=true)]
            public bool? Dynamic { get; set; }

            [ValueName("disabled", Default="true")]
            public bool? Disabled { get; set; }

            public override string[] APIScope
            {
                get { return new string[] { "ip", "address" }; }
            }
        }
    }
}
