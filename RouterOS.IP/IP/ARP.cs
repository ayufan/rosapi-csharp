﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using RouterOS.DataTypes;
using RouterOS.Interfaces;

namespace RouterOS.IP
{
    [Serializable]
    public sealed class ARP : AddSetItemNode<ARP>
    {
        [ValueName("address", IsKey=true)]
        public IPAddress Address { get; set; }

        [ValueName("comment", Default="")]
        public string Comment { get; set; }

        [ValueName("disabled", Default="true")]
        public bool? Disabled { get; set; }

        [ValueName("interface", IsKey=true)]
        public Ref<Interface>? Interface { get; set; }

        [ValueName("mac-address")]
        public MACAddress MacAddress { get; set; }

        [ValueName("dynamic", IsDynamic = true)]
        public bool? Dynamic { get; set; }

        [ValueName("dynamic", IsDynamic = true)]
        public bool? Invalid { get; set; }

        [ValueName("DHCP", IsDynamic = true)]
        public bool? IsDHCP { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "arp" }; }
        }
    }
}
