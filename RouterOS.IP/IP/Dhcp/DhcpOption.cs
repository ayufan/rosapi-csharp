﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.IP.Dhcp
{
    public class DhcpOption : DataTypes.AddSetItemNode<DhcpOption>
    {
        [ValueName("code")]
        public string Code { get; set; }

        [ValueName("name")]
        public string Name { get; set; }

        [ValueName("value")]
        public string Value { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server", "option" }; }
        }

        public static DhcpOption Parse(string name, API.Connection conn)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            DhcpOption opt = new DhcpOption();
            opt.GetMe(conn, name);
            return opt;
        }
    }
}
