﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using RouterOS.DataTypes;

namespace RouterOS.IP.Dhcp
{
    public class Network : AddSetItemNode<Network>
    {
        [ValueName("address")]
        public string Address { get; set; }

        [ValueName("comment")]
        public string Comment { get; set; }

        [ValueName("dns-server")]
        public ItemList<IPAddress> DnsServer { get; set; }

        [ValueName("gateway")]
        public ItemList<IPAddress> Gateway { get; set; }

        [ValueName("next-server")]
        public IPAddress NextServer { get; set; }

        [ValueName("wins-server")]
        public ItemList<IPAddress> WinsServer { get; set; }

        [ValueName("boot-file-name")]
        public string BootFileName { get; set; }

        [ValueName("dhcp-option")]
        public ItemList<Ref<DhcpOption>> DhcpOption { get; set; }

        [ValueName("domain")]
        public string Domain { get; set; }

        [ValueName("netmask")]
        public int? Netmask { get; set; }

        [ValueName("ntp-server")]
        public ItemList<IPAddress> NtpServer { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server", "alert" }; }
        }
    }
}
