﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using RouterOS.DataTypes;

namespace RouterOS.IP.Dhcp
{
    public enum LeaseStatus
    {
        [ValueName("bound")]
        Bound
    }

    public class Lease : AddSetItemNode<Lease>
    {
        [ValueName("active-address", IsDynamic = true)]
        public IPAddress ActiveAddress { get; set; }

        [ValueName("agent-remote-id", IsDynamic = true)]
        public string AgentRemoteId { get; set; }

        [ValueName("dynamic", IsDynamic = true)]
        public bool? Dynamic { get; set; }

        [ValueName("rate-limit")]
        public DblValue<Bits,Bits>? RateLimit { get; set; }

        [ValueName("active-client-id", IsDynamic = true)]
        public string ActiveClientId { get; set; }

        [ValueName("always-broadcast")]
        public bool? AlwaysBroadcast { get; set; }

        [ValueName("expires-after", IsDynamic = true)]
        public TimeSpan? ExpiresAfter { get; set; }

        [ValueName("server")]
        public Ref<Server>? Server { get; set; }

        [ValueName("active-mac-address", IsDynamic = true)]
        public MACAddress? ActiveMacAddress { get; set; }

        [ValueName("block-access")]
        public bool? BlockAccess { get; set; }

        [ValueName("host-name", IsDynamic = true)]
        public string HostName { get; set; }

        [ValueName("src-mac-address", IsDynamic = true)]
        public MACAddress? SrcMacAddress { get; set; }

        [ValueName("active-server", IsDynamic = true)]
        public Ref<Server>? ActiveServer { get; set; }

        [ValueName("blocked", IsDynamic = true)]
        public bool? Blocked { get; set; }

        [ValueName("last-seen", IsDynamic = true)]
        public TimeSpan? LastSeen { get; set; }

        [ValueName("status", IsDynamic = true)]
        public LeaseStatus? Status { get; set; }

        [ValueName("address")]
        public IPAddress Address { get; set; }

        [ValueName("client-id")]
        public string ClientId { get; set; }

        [ValueName("lease-time")]
        public TimeSpan? LeaseTime { get; set; }

        [ValueName("use-src-mac")]
        public bool? UseSrcMac { get; set; }

        [ValueName("address-list")]
        public Ref<Firewall.AddressList>? AddressList { get; set; }

        [ValueName("comment")]
        public string Comment { get; set; }

        [ValueName("mac-address")]
        public MACAddress? MacAddress { get; set; }

        [ValueName("agent-circuit-id", IsDynamic = true)]
        public string AgentCircuitId { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        [ValueName("radius", IsDynamic = true)]
        public bool? Radius { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server", "lease" }; }
        }

    }
}
