﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.IP.Dhcp
{
    public class Alert : AddSetItemNode<Alert>
    {
        [ValueName("alert-timeout")]
        public TimeSpan? AlertTimeout { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        [ValueName("invalid", IsDynamic = true)]
        public bool? Invalid { get; set; }

        [ValueName("unknown-server", IsDynamic = true)]
        public bool? UnknownServer { get; set; }

        [ValueName("comment")]
        public string Comment { get; set; }

        [ValueName("interface", IsKey=true)]
        public Ref<Interfaces.Interface>? Interface { get; set; }

        [ValueName("on-alert")]
        public string OnAlert { get; set; }

        [ValueName("valid-server")]
        public bool? ValidServer { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server", "alert" }; }
        }

        public override string ToString()
        {
            return Interface.ToString();
        }
    }
}
