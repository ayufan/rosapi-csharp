﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using System.Net;

namespace RouterOS.IP.Dhcp
{
    public enum BootpSupport
    {
        None,
        Dynamic,
        Static
    }

    public enum Authoritative
    {
        After10secDelay,
        After2secDelay,
        No,
        Yes
    }
    
    public class Server : AddSetItemNode<Server>
    {
        [ValueName("add-arp")]
        public bool? AddArp { get; set; }

        [ValueName("bootp-lease-time")]
        public TimeSpan? BootpLeaseTime { get; set; }

        [ValueName("interface", IsKey=true)]
        public Ref<Interfaces.Interface>? Interface { get; set; }

        [ValueName("relay")]
        public IPAddress Relay { get; set; }

        [ValueName("address-pool")]
        public Ref<Pool>? AddressPool { get; set; }

        [ValueName("bootp-support")]
        public BootpSupport? BootpSupport { get; set; }

        [ValueName("invalid", IsDynamic = true)]
        public bool? Invalid { get; set; }

        [ValueName("src-address")]
        public IPAddress SrcAddress { get; set; }

        [ValueName("always-broadcast")]
        public bool? AlwaysBroadcast { get; set; }

        [ValueName("delay-threshold")]
        public TimeSpan? DelayThreshold { get; set; }

        [ValueName("lease-time", Formatter=typeof(TimeSpanNoneFormatter))]
        public TimeSpan? LeaseTime { get; set; }

        [ValueName("use-radius")]
        public bool? UseRadius { get; set; }

        [ValueName("authoritative")]
        public Authoritative? Authoritative { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        [ValueName("name")]
        public string Name { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server" }; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
