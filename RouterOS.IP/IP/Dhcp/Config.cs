﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.IP.Dhcp
{
    public class Config : ValueNode<Config>
    {
        /** store-leases-disk
  StoreLeasesDisk ::= SymbolicNames | TimeInterval
  TimeInterval -- time interval
  SymbolicNames ::= immediately | never
        */

        public override string[] APIScope
        {
            get { return new string[] { "ip", "dhcp-server", "config" }; }
        }
    }
}
