﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;
using System.Net;

namespace RouterOS.IP
{
    public class Pool : AddSetItemNode<Pool>
    {
        [ValueName("name")]
        public string Name { get; set; }

        [ValueName("name")]
        public ItemList<IPRange> Ranges { get; set; }

        [ValueName("next-pool")]
        public ItemList<Ref<Pool>> NextPool { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "pool" }; }
        }
    }
}
