﻿using System;
using System.Collections.Generic;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.IP
{
    public class Service : SetItemNode<Service>
    {
        [ValueName("name", ReadOnly = true, IsKey = true)]
        public string Name { get; set; }

        [ValueName("port")]
        public ushort? Port { get; set; }

        [ValueName("address")]
        public IPMask Address { get; set; }

        [ValueName("certificate")]
        public string Certificate { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "service" }; }
        }
    }
}
