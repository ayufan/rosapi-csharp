﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using RouterOS.DataTypes;
using RouterOS.Interfaces;

namespace RouterOS.IP
{
    public class Route : AddSetItemNode<Route>
    {
        [ValueName("dst-address", Default="0.0.0.0/0")]
        public IPPrefix DstAddress { get; set; }

        [ValueName("gateway")]
        public ItemList<Ref<Interface>> Gateway { get; set; }

        [ValueName("pref-src")]
        public IPAddress PrefSource { get; set; }

        [ValueName("distance")]
        public int? Distance { get; set; }

        [ValueName("active", IsDynamic=true)]
        public bool? Active { get; set; }

        [ValueName("static", IsDynamic=true)]
        public bool? Static { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "ip", "route" }; }
        }
    }
}
