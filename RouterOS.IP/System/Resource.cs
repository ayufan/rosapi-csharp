﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.DataTypes;

namespace RouterOS.System
{
    public class Resource : ValueNode<Resource>
    {
        [ValueName("uptime", IsDynamic=true)]
        public TimeSpan? Uptime { get; set; }

        [ValueName("version", IsDynamic = true)]
        public Version Version { get; set; }

        [ValueName("free-memory", IsDynamic = true)]
        public int? FreeMemory { get; set; }

        [ValueName("total-memory", IsDynamic = true)]
        public int? TotalMemory { get; set; }

        [ValueName("cpu", IsDynamic = true)]
        public string Cpu { get; set; }

        [ValueName("cpu-count", IsDynamic = true)]
        public int? CpuCount { get; set; }

        [ValueName("cpu-frequency", IsDynamic = true)]
        public int? CpuFrequency { get; set; }

        [ValueName("cpu-load", IsDynamic = true)]
        public int? CpuLoad { get; set; }

        [ValueName("free-hdd-space", IsDynamic = true)]
        public int? FreeHddSpace { get; set; }

        [ValueName("total-hdd-space", IsDynamic = true)]
        public int? TotalHddSpace { get; set; }

        [ValueName("write-sect-since-reboot", IsDynamic = true)]
        public int? WriteSinceReboot { get; set; }

        [ValueName("write-sect-total", IsDynamic = true)]
        public int? WriteTotal { get; set; }

        [ValueName("bad-blocks", IsDynamic = true)]
        public int? BadBlocks { get; set; }

        [ValueName("architecture-name", IsDynamic = true)]
        public string ArchitectureName { get; set; }

        [ValueName("board-name", IsDynamic = true)]
        public string BoardName { get; set; }

        [ValueName("platform", IsDynamic = true)]
        public string Platform { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "system", "resource" }; }
        }
    }
}
