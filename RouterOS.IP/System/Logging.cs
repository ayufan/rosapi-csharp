﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;
using RouterOS.DataTypes;
using System.Net;

namespace RouterOS.System
{
    public enum LoggingTarget
    {
        [ValueName("disk")]
        Disk,

        [ValueName("echo")]
        Echo,

        [ValueName("email")]
        Email,

        [ValueName("memory")]
        Memory,

        [ValueName("remote")]
        Remote
    }

    public class LoggingAction : AddSetItemNode<LoggingAction>
    {
        [ValueName("name", IsKey=true)]
        public string Name { get; set; }

        [ValueName("target")]
        public LoggingTarget? Target { get; set; }

        [ValueName("comment", Default="")]
        public string Comment { get; set; }

        [ValueName("disk-file-name")]
        public string DiskFileName { get; set; }

        [ValueName("disk-lines-per-file")]
        public int DiskLinesPerFile { get; set; }

        [ValueName("disk-file-count")]
        public int DiskFileCount { get; set; }

        [ValueName("disk-stop-on-full")]
        public bool? DiskStopOnFull { get; set; }

        [ValueName("memory-lines")]
        public int MemoryLines { get; set; }

        [ValueName("memory-stop-on-full")]
        public bool? MemoryStopOnFull { get; set; }

        [ValueName("remote")]
        public IPAddress Remote { get; set; }

        [ValueName("remote-port")]
        public ushort? RemotePort { get; set; }

        [ValueName("src-address")]
        public IPAddress SrcAddress { get; set; }

        [ValueName("email-start-tls")]
        public bool? EmailStartTls { get; set; }

        [ValueName("email-to")]
        public string EmailTo { get; set; }

        [ValueName("remember")]
        public bool? Remember { get; set; }
        
        [ValueName("syslog-facility")]
        public string SyslogFacility { get; set; }

        [ValueName("syslog-severity")]
        public string SyslogSeverity { get; set; }

        [ValueName("bsd-syslog")]
        public bool? BsdSyslog { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "system", "logging", "action" }; }
        }

        public static LoggingAction Parse(string name, Connection connection)
        {
            if (name == null || name.Length == 0)
                return null;

            LoggingAction la = new LoggingAction();
            la.GetMe(connection, new EqualQuery("name", name));
            return la;
        }
    }      

    public class Logging : SetItemNode<Logging>
    {
        [ValueName("action")]
        public Ref<LoggingAction>? Action { get; set; }

        [ValueName("topics")]
        public ItemList<string> Topics { get; set; }

        [ValueName("prefix")]
        public string Prefix { get; set; }

        [ValueName("invalid", IsDynamic=true)]
        public bool? Invalid { get; set; }

        [ValueName("disabled")]
        public bool? Disabled { get; set; }

        public override string[] APIScope
        {
            get { return new string[] { "system", "logging" }; }
        }
    }
}
