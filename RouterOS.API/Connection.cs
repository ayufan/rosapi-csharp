﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace RouterOS.API
{
    /// <summary>
    /// Asynchronous response callback.
    /// </summary>
    /// <param name="conn">Connection which generated asynchronous response.</param>
    /// <param name="response">Received response.</param>
    public delegate void AsyncResponseCallback(Connection conn, Record response);

    /// <summary>
    /// Encapsulates MikroTik RouterOS interface using API.
    /// </summary>
    public class Connection : IDisposable
    {
        private TcpClient m_Connection;
        private EncodedStream m_Stream;
        private string m_Host;
        private ushort m_Port = 8728;
        private string m_Login = "admin";
        private string m_Password = "";
        private Dictionary<string, Queue<Record>> m_Dispatcher = new Dictionary<string, Queue<Record>>();
        private Dictionary<string, AsyncResponseCallback> m_Callbacks = new Dictionary<string, AsyncResponseCallback>();
        private int m_CallbackIndex = 1;

        /// <summary>
        /// Router hostname.
        /// </summary>
        public string Host
        {
            get { return m_Host; }
            set { m_Host = value; }
        }

        /// <summary>
        /// API service port.
        /// </summary>
        public ushort Port
        {
            get { return m_Port; }
            set { m_Port = value; }
        }

        /// <summary>
        /// Router username.
        /// </summary>
        public string Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }

        /// <summary>
        /// Router password.
        /// </summary>
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public bool Connected
        {
            get { return m_Connection != null && m_Stream != null && m_Connection.Connected; }
        }

        private Record ReadResponse(string tag)
        {
            Queue<Record> record;
            while (!m_Dispatcher.TryGetValue(tag, out record) || record.Count == 0)
                System.Threading.Thread.Sleep(0);
            return record.Dequeue();
        }

        private string WriteCommand(Command command, AsyncResponseCallback callback)
        {
            // build command
            List<string> commandLines = new List<string>();
            command.Build(commandLines);
            if (commandLines.Count == 0)
                return null;

            string tag = null;

            if (callback != null || !command.IsAsynchronous)
            {
                tag = (++m_CallbackIndex).ToString();
                commandLines.Add(".tag=" + tag);
                m_Dispatcher[tag] = new Queue<Record>();

                if (callback != null)
                    m_Callbacks.Add(tag, callback);
            }

            // send block
            m_Stream.WriteBlock(commandLines.ToArray());
            return tag;
        }

        /// <summary>
        /// Executes asynchronous command.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <param name="callback">Asynchronous callback to be executed.</param>
        /// <returns>Asynchronous operation identifier.</returns>
        public object ExecuteCommand(Command command, AsyncResponseCallback callback)
        {
            return WriteCommand(command, callback);
        }

        /// <summary>
        /// Executes command synchronously.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <returns>Execution status and command records.</returns>
        public Response ExecuteCommand(Command command)
        {
            List<Record> records = new List<Record>();
            Record message = null;
            string tag = null;

            try
            {
                // send command, synchronous
                tag = WriteCommand(command, null);
                if (tag == null)
                    return null;

                // read until !re
                message = ReadResponse(tag);
                while (message.Type == RecordType.Record)
                {
                    records.Add(message);
                    message = ReadResponse(tag);
                }
            }
            finally
            {
                if (tag != null)
                    m_Dispatcher.Remove(tag);

                // dispatch all messages
                Dispatch();
            }

            // create response
            return new Response(message, records.ToArray());
        }

        public void Cancel(object tagId)
        {
            ExecuteCommand(new CancelCommand(tagId));
        }

        public void Wait(object tagId)
        {
            while (m_Dispatcher.ContainsKey((string)tagId))
                System.Threading.Thread.Sleep(0);
        }

        /// <summary>
        /// Execute command synchronously returning only one record.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <returns>First record of executed command or null if no record found.</returns>
        public Record ExecuteOneRecord(Command command)
        {
            Response response = ExecuteCommand(command);
            if (response.Records.Length != 0)
                return response.Records[0];
            return null;
        }

        /// <summary>
        /// Explicitly connect to RouterOS.
        /// </summary>
        public void Connect()
        {
            if (Connected)
                throw new SocketException();

            try
            {
                // connect to routeros
                m_Connection = new TcpClient(m_Host, m_Port);
                m_Stream = new EncodedStream(m_Connection.GetStream());
                m_Stream.BlockDecoded += new EncodedStream.BlockDecodedDelegate(BlockDecoded);

                // get challenge login
                Response login = ExecuteCommand(new ChallengeLogin());

                // try to log in
                ExecuteCommand(new Login(m_Login, login.Ret, m_Password));
            }
            catch (Exception)
            {
                Disconnect();
                throw;
            }
        }

        private void BlockDecoded(string[] block)
        {
            Record r = new Record(this, block);
            if (r.Tag == null)
                return;

            // check if this is tagged response
            Queue<Record> record;
            if (!m_Dispatcher.TryGetValue(r.Tag, out record))
                return;
            record.Enqueue(r);

            // dispatch messages
            Dispatch();
        }

        /// <summary>
        /// Disconnect from RouterOS.
        /// </summary>
        public void Disconnect()
        {
            if (m_Stream != null)
                m_Stream.Dispose();
            m_Stream = null;
            if (m_Connection != null)
                m_Connection.Close();
            m_Connection = null;
        }

        /// <summary>
        /// Create connection to RouterOS using specified parameters.
        /// </summary>
        /// <param name="host">RouterOS hostname.</param>
        /// <param name="port">API service port.</param>
        /// <param name="login">RouterOS username.</param>
        /// <param name="password">RouterOS password.</param>
        public Connection(string host, ushort port, string login, string password)
        {
            m_Host = host;
            m_Port = port;
            m_Login = login;
            m_Password = password;
            Connect();
        }

        /// <summary>
        /// Create connection to RouterOS using specified parameters.
        /// </summary>
        /// <param name="host">RouterOS hostname.</param>
        /// <param name="login">RouterOS username.</param>
        /// <param name="password">RouterOS password.</param>
        public Connection(string host, string login, string password)
            : this(host, 8728, login, password)
        {
        }

        /// <summary>
        /// Create connection object, needs to be filled.
        /// </summary>
        public Connection()
        {
        }

        private bool Dispatch(Record response)
        {
            // check response tag
            if (response.Tag == null)
                return true;
            if (!m_Callbacks.ContainsKey(response.Tag))
                return true;

            bool result = true;

            // execute response callback
            try
            {
                m_Callbacks[response.Tag].Invoke(this, response);
            }
            finally
            {
                // remove data
                if (response.Type == RecordType.Done)
                {
                    m_Callbacks.Remove(response.Tag);
                    m_Dispatcher.Remove(response.Tag);
                    result = false;
                }
            }

            return result;
        }

        private bool Dispatch(string tag)
        {
            Queue<Record> record = m_Dispatcher[tag];

            while (record.Count != 0)
                if (!Dispatch(record.Dequeue()))
                    return false;
            return true;
        }

        /// <summary>
        /// Explicitly dispatch responses to callbacks.
        /// </summary>
        public void Dispatch()
        {
            for(bool done = false; !done; )
            {
                done = true;

                foreach (string tag in m_Dispatcher.Keys)
                {
                    // only dispatch commands with callbakcs
                    if (!m_Callbacks.ContainsKey(tag))
                        continue;
                
                    // dispatch messages
                    if(!Dispatch(tag))
                    {
                        // restart enumeration, collection changed
                        done = false;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Destroy connection.
        /// </summary>
        public void Dispose()
        {
            Disconnect();
        }

        private Dictionary<KeyValuePair<Type, object>, object> m_ObjectCache = new Dictionary<KeyValuePair<Type,object>,object>();

        public void ClearCache()
        {
            m_ObjectCache = new Dictionary<KeyValuePair<Type, object>, object>();
        }

        public bool GetObjectFromCache(Type type, object name, out object obj)
        {
            KeyValuePair<Type, Object> key = new KeyValuePair<Type, object>(type, name);
            return m_ObjectCache.TryGetValue(key, out obj);
        }

        public void AddObjectToCache(Type type, object name, object obj)
        {
            KeyValuePair<Type, Object> key = new KeyValuePair<Type, object>(type, name);
            m_ObjectCache[key] = obj;
        }
    }
}
