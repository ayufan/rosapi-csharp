﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouterOS.API
{
    /// <summary>
    /// Record status.
    /// </summary>
    public enum RecordType
    {
        /// <summary>
        /// Response finished successfully.
        /// </summary>
        Done,

        /// <summary>
        /// Response contains record data.
        /// </summary>
        Record,

        /// <summary>
        /// Response finished with error.
        /// </summary>
        Error,

        /// <summary>
        /// Response finished with CRITICAL error.
        /// </summary>
        Fatal
    };

    /// <summary>
    /// Contains one record data.
    /// </summary>
    public class Record
    {
        private Connection m_Connection;
        private Dictionary<string, string> m_KeyValues = new Dictionary<string, string>();
        private RecordType m_Type;
        private string m_Tag;

        /// <summary>
        /// Connection from which came record data.
        /// </summary>
        public Connection Connection
        {
            get { return m_Connection; }
            internal set { m_Connection = value; }
        }

        /// <summary>
        /// Returns named key value, ie. "interface", "address".
        /// </summary>
        /// <param name="key">RouterOS record key name.</param>
        /// <returns>Value for key.</returns>
        public string this[string key]
        {
            get { return m_KeyValues[key]; }
        }

        /// <summary>
        /// Returns list of keys contained in record.
        /// </summary>
        public IEnumerable<string> Keys
        {
            get { return m_KeyValues.Keys; }
        }

        /// <summary>
        /// Returns collection of keys and values contained in record.
        /// </summary>
        public Dictionary<string, string> KeyValues
        {
            get { return m_KeyValues; }
        }

        /// <summary>
        /// Checks if record contains specified key.
        /// </summary>
        /// <param name="key">RouterOS record key name.</param>
        /// <returns>true if found.</returns>
        public bool Contains(string key)
        {
            return m_KeyValues.ContainsKey(key);
        }

        /// <summary>
        /// Returns record identifier (.id).
        /// </summary>
        public string ItemId
        {
            get
            {
                if (Contains(".id"))
                    return this[".id"];
                return null;
            }
        }

        /// <summary>
        /// Checks if record contains data about item.
        /// </summary>
        public bool IsItem
        {
            get { return Contains(".id"); }
        }

        /// <summary>
        /// Checks if record contains data about values, not item.
        /// </summary>
        public bool IsValue
        {
            get { return !Contains(".id"); }
        }

        /// <summary>
        /// Returns record type.
        /// </summary>
        public RecordType Type
        {
            get { return m_Type; }
        }

        /// <summary>
        /// Returns tag identyfing command from which originated record.
        /// </summary>
        public string Tag
        {
            get { return m_Tag; }
        }

        /// <summary>
        /// Constructs null object.
        /// </summary>
        public Record()
        {
        }

        /// <summary>
        /// Constructs object and fills with response.
        /// </summary>
        /// <param name="connection">RouterOS connection object.</param>
        /// <param name="lines">List of response lines.</param>
        public Record(Connection connection, string[] lines)
        {
            // check response type
            switch (lines[0])
            {
                case "!done":
                    m_Type = RecordType.Done;
                    break;

                case "!re":
                    m_Type = RecordType.Record;
                    break;

                case "!fatal":
                    m_Type = RecordType.Fatal;
                    break;

                case "!trap":
                    m_Type = RecordType.Error;
                    break;

                default:
                    throw new ArgumentException("invalid response type: " + lines[0]);
            }

            // parse response
            for (int i = 1; i < lines.Length; ++i)
            {
                // ignore empty lines
                string line = lines[i];
                if(string.IsNullOrEmpty(line))
                    continue;

                // split response by '='
                string[] vars = line.Split(new char[] { '=' }, 3);

                switch (vars[0])
                {
                    case "":
                        // standard values, ie. .id, name
                        KeyValues[vars[1]] = vars[2];
                        break;

                    case ".tag":
                        // special values, ie. tags
                        m_Tag = vars[1];
                        break;

                    default:
                        throw new ArgumentException("undefined type for: " + vars[0]);
                }
            }

            m_Connection = connection;
        }
    }

    public class Response
    {
        private Connection m_Connection;
        private string m_Ret;
        private Record[] m_Records;

        public Connection Connection
        {
            get { return m_Connection; }
            internal set { m_Connection = value; }
        }

        public string Ret
        {
            get { return m_Ret; }
        }

        public Record[] Records
        {
            get { return m_Records; }
        }

        public Response(Record message, Record[] records)
        {
            if (message.Type != RecordType.Done)
            {
                throw new InvalidOperationException(message["message"]);            
            }

            m_Connection = message.Connection;
            if (message.Contains("ret"))
                m_Ret = message["ret"];
            m_Records = records;
        }

        public Response(Record message)
            : this(message, null)
        {
        }
    };
}
