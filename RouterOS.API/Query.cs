﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouterOS.API
{
    public abstract class Query
    {
        private Query m_Parent;

        public Query Parent
        {
            get { return m_Parent; }
            set
            {
                if (m_Parent != null && value != null)
                    throw new InvalidOperationException("query is already used");
                m_Parent = value;
            }
        }

        public virtual void Build(ICollection<string> queryResult)
        {
        }
    }

    /// <summary>
    /// Queries container.
    /// </summary>
    public class QueryList : List<Query>
    {
        public void Build(ICollection<string> queryResult)
        {
            foreach (Query q in this)
                q.Build(queryResult);
        }
    }

    /// <summary>
    /// And query. Performs logical AND of two subqueries.
    /// </summary>
    public class AndQuery : Query
    {
        Query m_Left, m_Right;

        public AndQuery(Query left, Query right)
        {
            m_Left = left;
            m_Left.Parent = this;
            m_Right = right;
            m_Right.Parent = this;
        }

        public override void Build(ICollection<string> queryResult)
        {
            m_Left.Build(queryResult);
            m_Right.Build(queryResult);
            queryResult.Add("?#&");
            base.Build(queryResult);
        }
    }


    /// <summary>
    /// And query. Performs logical OR of two subqueries.
    /// </summary>
    public class OrQuery : Query
    {
        Query m_Left, m_Right;

        public OrQuery(Query left, Query right)
        {
            m_Left = left;
            m_Left.Parent = this;
            m_Right = right;
            m_Right.Parent = this;
        }

        public override void Build(ICollection<string> queryResult)
        {
            m_Left.Build(queryResult);
            m_Right.Build(queryResult);
            queryResult.Add("?#|");
            base.Build(queryResult);
        }
    }

    /// <summary>
    /// Contains query. Checks if property do have assigned value.
    /// </summary>
    public sealed class ContainsQuery : Query
    {
        string m_Key;

        public ContainsQuery(string propName)
        {
            m_Key = propName;
        }

        public override void Build(ICollection<string> queryResult)
        {
            queryResult.Add("?" + m_Key);
            base.Build(queryResult);
        }
    }

    /// <summary>
    /// Not contains query. Checks if property doesn't have assigned value.
    /// </summary>
    public sealed class NotContainsQuery : Query
    {
        string m_Key;

        public NotContainsQuery(string propName)
        {
            m_Key = propName;
        }

        public override void Build(ICollection<string> queryResult)
        {
            queryResult.Add("?-" + m_Key);
            base.Build(queryResult);
        }
    }

    public abstract class ComparisionQuery : Query
    {
        string m_Key;
        string m_Value;

        public abstract string Operator { get; }

        public ComparisionQuery(string propName, string text)
        {
            m_Key = propName;
            m_Value = text;
            if (m_Value == null)
                throw new ArgumentNullException(m_Key);
        }

        public override void Build(ICollection<string> queryResult)
        {
            queryResult.Add(String.Format("?{0}{1}={2}", Operator, m_Key, m_Value));
            base.Build(queryResult);
        }
    }

    /// <summary>
    /// Equal query. Checks if property values equals specified text.
    /// </summary>
    public sealed class EqualQuery : ComparisionQuery
    {
        public EqualQuery(string propName, string text) : base(propName, text)
        {
        }

        public override string Operator
        {
            get { return ""; }
        }
    }

    /// <summary>
    /// Less query. Checks if property value is less than specified text.
    /// </summary>
    public class LessQuery : ComparisionQuery
    {
        public LessQuery(string propName, string text)
            : base(propName, text)
        {
        }

        public override string Operator
        {
            get { return "<"; }
        }
    }

    /// <summary>
    /// Greater query. Checks if property value is greater than specified text.
    /// </summary>
    public class GreaterQuery : ComparisionQuery
    {
        public GreaterQuery(string propName, string text) : base(propName, text)
        {
        }

        public override string Operator
        {
            get { return ">"; }
        }
    }

    /// <summary>
    /// Not query. Performs logical negation on specified query.
    /// </summary>
    public class NotQuery : Query
    {
        Query m_Expression;

        public NotQuery(Query expr)
        {
            m_Expression = expr;
            m_Expression.Parent = this;
        }

        public override void Build(ICollection<string> queryResult)
        {
            m_Expression.Build(queryResult);
            queryResult.Add("?#!");
            base.Build(queryResult);
        }
    }
}
