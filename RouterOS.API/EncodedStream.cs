﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace RouterOS.API
{
    internal class EncodedStream : IDisposable
    {
        public delegate void BlockDecodedDelegate(string[] block);

        private Stream m_Stream;
        private List<string> m_Lines = new List<string>();

        public event BlockDecodedDelegate BlockDecoded;

        #region Async Read
        private byte[] m_ReaderBuffer = new byte[4096];
        private int m_ReaderOffset = 0;
        private int m_ReaderSize = 0;
        private IAsyncResult m_ReaderCallback;
        #endregion

        #region Async Writer
        private Queue<byte[]> m_WriterQueue = new Queue<byte[]>(1000);
        private IAsyncResult m_WriterCallback;
        #endregion

        public EncodedStream(Stream stream)
        {
            m_Stream = stream;
            m_ReaderCallback = m_Stream.BeginRead(m_ReaderBuffer, m_ReaderSize, m_ReaderBuffer.Length - m_ReaderSize, new AsyncCallback(this.DataReaded), m_ReaderBuffer);
        }

        private void DataReaded(IAsyncResult result)
        {
            // clear callback
            if (result != m_ReaderCallback)
                return;

            m_ReaderSize += m_Stream.EndRead(result);
            m_ReaderCallback = null;

            while (m_ReaderCallback == null)
            {
                // parse data
                try { while (ParseBuffer()) ; }
                catch (Exception) { }

                // grow array by 4096
                if (m_ReaderSize + 4096 > m_ReaderBuffer.Length)
                    Array.Resize(ref m_ReaderBuffer, m_ReaderBuffer.Length + 4096);

                // begin read
                m_ReaderCallback = m_Stream.BeginRead(m_ReaderBuffer, m_ReaderSize, m_ReaderBuffer.Length - m_ReaderSize, new AsyncCallback(this.DataReaded), m_ReaderBuffer);
                if (m_ReaderCallback != null && m_ReaderCallback.IsCompleted)
                {
                    m_ReaderSize += m_Stream.EndRead(m_ReaderCallback);
                    m_ReaderCallback = null;
                }
            }
        }

        private void DataWritten(IAsyncResult result)
        {
            // finish write
            m_Stream.EndWrite(result);

            if (m_WriterCallback != result)
                return;

            //Trace.WriteLine(">= " + String.Join(", ", (byte[])result.AsyncState));

            // no data to write
            if (m_WriterQueue.Count == 0)
            {
                return;
            }

            // write next buffer
            byte[] buffer = m_WriterQueue.Dequeue();
            m_WriterCallback = m_Stream.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(this.DataWritten), buffer);
        }

        private void BeginWrite(byte[] buffer)
        {
            // pending writing operation, enqueue
            if (m_WriterCallback != null && !m_WriterCallback.IsCompleted && m_WriterQueue.Count != 0)
            {
                m_WriterQueue.Enqueue(buffer);
                return;
            }

            // queue not empty, enqueue
            if (m_WriterQueue.Count != 0)
            {
                m_WriterQueue.Enqueue(buffer);
                buffer = m_WriterQueue.Dequeue();
            }

            m_WriterCallback = m_Stream.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(this.DataWritten), buffer);
        }

        public void Wait()
        {
            if (m_ReaderCallback != null && !m_ReaderCallback.IsCompleted)
                m_ReaderCallback.AsyncWaitHandle.WaitOne();
        }

        private bool ParseBuffer()
        {
            if (m_ReaderOffset + 1 > m_ReaderSize)
                return false;

            int offset = m_ReaderOffset;

            // read string length
            uint len = m_ReaderBuffer[offset++];
            if ((len & 0x80) == 0x00)
            {
            }
            else if ((len & 0xC0) == 0x80)
            {
                if (offset + 1 > m_ReaderSize)
                    return false;
                len &= ~(uint)0xC0;
                len = (len << 8) + m_ReaderBuffer[offset++];
            }
            else if ((len & 0xE0) == 0xC0)
            {
                if (offset + 2 > m_ReaderSize)
                    return false;
                len &= ~(uint)0xE0;
                len = (len << 8) + m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
            }
            else if ((len & 0xF0) == 0xE0)
            {
                if (offset + 3 > m_ReaderSize)
                    return false;
                len &= ~(uint)0xF0;
                len = (len << 8) + m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
            }
            else
            {
                if (offset + 4 > m_ReaderSize)
                    return false;
                len = m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
                len = (len << 8) + m_ReaderBuffer[offset++];
            }

            if (len == 0)
            {
                if (m_Lines.Count != 0)
                {
                    if (BlockDecoded != null)
                        BlockDecoded(m_Lines.ToArray());
                    m_Lines.Clear();
                }
                System.Diagnostics.Trace.WriteLine("<<--");
            }
            else
            {
                // check if there is data in buffer
                if (offset + len > m_ReaderSize)
                    return false;

                // get line
                string line = Encoding.ASCII.GetString(m_ReaderBuffer, offset, (int)len);
                offset += (int)len;
                m_Lines.Add(line);

                System.Diagnostics.Trace.WriteLine("<< " + line);
            }

            // delete data from buffer
            Array.Copy(m_ReaderBuffer, offset, m_ReaderBuffer, 0, m_ReaderSize - offset);
            m_ReaderSize -= offset;
            return true;
        }

        public void WriteLine(string text)
        {
            // empty line
            if (text == null)
            {
                BeginWrite(new byte[1] { 0 });
                System.Diagnostics.Trace.WriteLine(">>--");
                return;
            }

            // convert text length to bytes
            byte[] len = BitConverter.GetBytes((uint)text.Length);
            if (text.Length < 0x80)
                BeginWrite(new byte[] { (byte)text.Length });
            else if (text.Length < 0x4000)
                BeginWrite(new byte[] { (byte)(len[2] | 0x80), len[3] });
            else if (text.Length < 0x200000)
                BeginWrite(new byte[] { (byte)(len[1] | 0xC0), len[2], len[3] });
            else if (text.Length < 0x10000000)
                BeginWrite(new byte[] { (byte)(len[0] | 0xE0), len[1], len[2], len[3] });
            else
                BeginWrite(new byte[] { 0xF0, len[0], len[1], len[2], len[3] });

            // send text
            byte[] data = Encoding.ASCII.GetBytes(text);
            BeginWrite(data);
            System.Diagnostics.Trace.WriteLine(">> " + text);
        }

        public void WriteBlock(string[] lines)
        {
            foreach (string line in lines)
                WriteLine(line);
            WriteLine(null);
        }

        public void Dispose()
        {
            if (m_Stream != null)
                m_Stream.Dispose();
        }
    }
}
