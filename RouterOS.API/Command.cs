﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace RouterOS.API
{
    public abstract class Command
    {
        public abstract bool Build(ICollection<string> commandLines);

        public virtual bool IsAsynchronous
        {
            get { return false; }
        }

        internal static string Join(params object[] objects)
        {
            string[] strings = new string[objects.Length];
            for (int i = 0; i < objects.Length; ++i)
                strings[i] = objects[i].ToString();
            return String.Join(",", strings);
        }
    }

    public class ChallengeLogin : Command
    {
        public override bool Build(ICollection<string> commandLines)
        {
            commandLines.Add("/login");
            return true;
        }
    }

    public sealed class Login : ChallengeLogin
    {
        private string m_Name;
        private string m_Response;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string Response
        {
            get { return m_Response; }
            set { m_Response = value; }
        }

        public void BuildResponse(string challenge, string password)
        {
            // save password
            byte[] data = new byte[1 + password.Length + challenge.Length / 2];
            Encoding.ASCII.GetBytes(password, 0, password.Length, data, 1);

            // get challenge
            for (int i = 0; i < challenge.Length; i += 2)
                data[1 + password.Length + i / 2] = Convert.ToByte(challenge.Substring(i, 2), 16);

            // get response
            byte[] response = new MD5CryptoServiceProvider().ComputeHash(data);
            m_Response = BitConverter.ToString(response).Replace("-", "").ToLower();
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if(!base.Build(commandLines))
                return false;
            if(m_Name != null)
                commandLines.Add("=name=" + m_Name);
            if(m_Response != null)
                commandLines.Add("=response=00" + m_Response);
            return true;
        }

        public Login()
        {
        }

        public Login(string name, string challenge, string password)
        {
            m_Name = name;
            BuildResponse(challenge, password);
        }
    }

    public abstract class ScopeCommand : Command
    {
        private string[] m_Scope;

        public string[] Scope
        {
            get { return m_Scope; }
            set { m_Scope = value; }
        }

        /// <summary>
        /// Command action, ie. getall, listen, set.
        /// </summary>
        public abstract string Action
        {
            get;
        }

        public override bool Build(ICollection<string> commandLines)
        {
            commandLines.Add("/" + String.Join("/", m_Scope) + "/" + Action);
            return true;
        }
    }

    public abstract class ValueCommand : ScopeCommand
    {
        private Dictionary<string, string> m_KeyValues = new Dictionary<string, string>();

        public string this[string key]
        {
            get { return m_KeyValues[key]; }
            set { m_KeyValues[key] = value; }
        }

        public IDictionary<string, string> KeyValues
        {
            get { return m_KeyValues; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;
            foreach (KeyValuePair<string, string> keyValue in m_KeyValues)
                commandLines.Add("=" + keyValue.Key + "=" + keyValue.Value);
            return true;
        }
    }

    public abstract class QueryCommand : ScopeCommand
    {
        private QueryList m_Query = new QueryList();

        public QueryList Query
        {
            get { return m_Query; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;
            if (m_Query != null)
                m_Query.Build(commandLines);
            return true;
        }
    }

    public class GetAllCommand : QueryCommand
    {
        private List<string> m_PropList = new List<string>();

        public List<string> PropList
        {
            get { return m_PropList; }
        }

        public override string Action
        {
            get { return "getall"; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;
            if (m_PropList.Count != 0)
                commandLines.Add(".proplist=" + String.Join(",", m_PropList.ToArray()));
            return true;
        }

        public GetAllCommand()
        {
        }

        public GetAllCommand(string[] scope, params Query[] query)
        {
            Scope = scope;
            Query.AddRange(query);
        }
    }

    public static class CommandHelpers
    {
        public static Response GetAll(this Connection connection, string[] scope, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, query));
        }

        public static object GetAll(this Connection connection, string[] scope, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, query), callback);
        }
    }

    public sealed class SetCommand : ValueCommand
    {
        public override string Action
        {
            get { return "set"; }
        }

        public SetCommand()
        {
        }

        public SetCommand(string[] scope)
        {
            Scope = scope;
        }
    }

    public sealed class AddCommand : ValueCommand
    {
        public override string Action
        {
            get { return "add"; }
        }

        public AddCommand()
        {
        }

        public AddCommand(string[] scope)
        {
            Scope = scope;
        }
    }
    
    public abstract class IdsCommand : ScopeCommand
    {
        private object[] m_Items;

        public object[] Items
        {
            get { return m_Items; }
            set { m_Items = value; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;

            if (m_Items != null)
            {
                commandLines.Add("=numbers=" + Join(m_Items));
                return true;
            }
            return false;
        }
        
        public IdsCommand()
        {
        }
    }

    public sealed class MoveCommand : IdsCommand
    {
        private object m_BeforeItem;

        public object BeforeItem
        {
            get { return m_BeforeItem; }
            set { m_BeforeItem = value; }
        }

        public override string Action
        {
            get { return "move"; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;

            if (m_BeforeItem != null)
            {
                commandLines.Add("=destination=" + m_BeforeItem.ToString());
                return true;
            }
            return false;
        }
        
        public MoveCommand()
        {
        }

        public MoveCommand(string[] scope, object beforeItem, params object[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class RemoveCommand : IdsCommand
    {
        public override string Action
        {
            get { return "remove"; }
        }

        public RemoveCommand()
        {
        }

        public RemoveCommand(string[] scope, params object[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class EnableCommand : IdsCommand
    {
        public override string Action
        {
            get { return "enable"; }
        }

        public EnableCommand()
        {
        }

        public EnableCommand(string[] scope, params object[] items)
        {
            Scope = scope;
            Items = items;
        }
    }
    
    public sealed class DisableCommand : IdsCommand
    {
        public override string Action
        {
            get { return "disable"; }
        }

        public DisableCommand()
        {
        }

        public DisableCommand(string[] scope, params object[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class UnsetCommand : ScopeCommand
    {
        private List<object> m_Items;
        private List<string> m_ValueNames;

        public UnsetCommand(string[] scope)
        {
            Scope = scope;
        }

        public List<object> Items
        {
            get { return m_Items; }
        }

        public List<string> ValueNames
        {
            get { return m_ValueNames; }
        }

        public override string Action
        {
            get { return "unset"; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            if (!base.Build(commandLines))
                return false;

            if (m_Items != null && m_ValueNames != null)
            {
                commandLines.Add("=numbers=" + Join(m_Items));
                commandLines.Add("=value-name=" + String.Join(",", m_ValueNames.ToArray()));
                return true;
            }

            return false;
        }
    }

    public sealed class ListenCommand : QueryCommand
    {
        public override string Action
        {
            get { return "listen"; }
        }

        public ListenCommand()
        {
        }

        public ListenCommand(string[] scope)
        {
            Scope = scope;
        }

        public override bool IsAsynchronous
        {
            get { return true; }
        }
    }

    public sealed class CancelCommand : Command
    {
        private object m_Tag;

        public object Tag
        {
            get { return m_Tag; }
            set { m_Tag = value; }
        }

        public override bool Build(ICollection<string> commandLines)
        {
            commandLines.Add("/cancel");
            commandLines.Add("=.tag=" + m_Tag.ToString());
            return true;
        }

        public CancelCommand(object tag)
        {
            m_Tag = tag;
        }

        public override bool IsAsynchronous
        {
            get { return true; }
        }
    }
}
